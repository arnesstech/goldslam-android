package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.MediAdapter;
import com.slam.gss.goldslam.adapters.OurClientsAdapter;
import com.slam.gss.goldslam.models.OurClients.Clients;
import com.slam.gss.goldslam.models.media.MediaResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class OurClientsFragment extends Fragment {

    @BindView(R.id.rv_txt_our_clients)
    RecyclerView rvClients;

    private ProgressDialog progressDialog;
    List<Clients> mClientList;
    LinearLayoutManager mLayoutManager;
    OurClientsAdapter mOurClientsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_our_clients, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        rvClients.setLayoutManager(mLayoutManager);
        rvClients.setItemAnimator(new DefaultItemAnimator());
        mClientList = new ArrayList<>();
        mOurClientsAdapter = new OurClientsAdapter(getActivity());
        loadClients();

    }

    private void loadClients() {
        Clients client = new Clients();
        mClientList.add(client);

        client = new Clients();
        mClientList.add(client);

        client = new Clients();
        mClientList.add(client);

        client = new Clients();
        mClientList.add(client);

        mOurClientsAdapter.addItem(mClientList);
        rvClients.setAdapter(mOurClientsAdapter);
    }

}
