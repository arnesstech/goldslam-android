package com.slam.gss.goldslam.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.slam.gss.goldslam.ProfileFragment;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.SpinnerAdapter;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.MasterData.Country;
import com.slam.gss.goldslam.models.createTournamentRequest.CreateTournamentRequest;
import com.slam.gss.goldslam.models.createTournamentRequest.CreateTournamentResponse;
import com.slam.gss.goldslam.network.RestApi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mahes on 7/5/2017.
 */

public class CreateTournamentFragment extends Fragment {

    @BindView(R.id.et_organisername)
    EditText etOrganiserName;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_mobile)
    EditText etMobile;

    @BindView(R.id.sp_sporttype)
    Spinner spSportsType;

    @BindView(R.id.et_tournamentname)
    EditText etTournments;

    @BindView(R.id.et_location)
    EditText etLocation;

    @BindView(R.id.et_address)
    EditText etAddress;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.et_from_date)
    EditText etFromDate;

    @BindView(R.id.et_to_date)
    EditText etToDate;

    @BindView(R.id.btn_Create)
    Button btnCreate;

    String fDate, tDate, sportsType = "select sportsType";
    Calendar myCalendar;

    private TextView temp_page_title_menu, temp_single_menu;
    private ArrayList<String> sportTypes = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_create_tournment_request, container, false);
        ButterKnife.bind(this, view);

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText("Create Tournament");
        temp_page_title_menu.setGravity(Gravity.CENTER);
        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.INVISIBLE);
        }

        /*getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
*/
        return view;

    }

    DatePickerDialog.OnDateSetListener fromDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            view.setMaxDate(System.currentTimeMillis());
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String myFormat = "dd-MMM-yyyy";//"MM/dd/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            etFromDate.setText(sdf.format(myCalendar.getTime()));
        }
    };

    DatePickerDialog.OnDateSetListener toDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            view.setMaxDate(System.currentTimeMillis());
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String myFormat = "dd-MMM-yyyy";//"MM/dd/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            etToDate.setText(sdf.format(myCalendar.getTime()));
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getSportsTypes();

        spSportsType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sportsType = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        myCalendar = Calendar.getInstance();

        etFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getActivity(), fromDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });


        etToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getActivity(), toDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });


        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validations()) {
                  /*validations();*/
                    createTournamentRequest();
                }
            }
        });
    }

    private void getSportsTypes() {
        Call<Country> getSportTypes = RestApi.get()
                .getRestService()
                .getSportTypes();

        getSportTypes.enqueue(new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {

                        if (response.body().getData().getMasterData().getOptions().length > 0) {
                            sportTypes.add("select sportsType");
                            for (int i = 0; i < response.body().getData().getMasterData().getOptions().length; i++) {
                                sportTypes.add(response.body().getData().getMasterData().getOptions()[i].getValue());
                            }

                        } else {
                            sportTypes = new ArrayList<String>();
                        }

                        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, sportTypes);
                        spSportsType.setAdapter(adapter);
                        System.out.println("sports types list::::" + sportTypes.size());

                    }

                } else {
                    //  Log.e("getCountries", "not successfull");
                    sportTypes = new ArrayList<String>();
                    SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, sportTypes);
                    spSportsType.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<Country> call, Throwable t) {

            }
        });

    }

    private boolean validations() {

        if (!validateOrganiserName()) {
            return false;
        }

        if (!validateEmail()) {
            return false;
        }

        if (!validateMobile()) {
            return false;
        }

        if ("select sportsType".equalsIgnoreCase(sportsType)) {
            Helper.showToast(getActivity(), "Please select sportType");
            return false;
        }

        if (!validateTournments()) {
            return false;
        }

        if (!validateLocation()) {
            return false;
        }

        if (!validateAddress()) {
            return false;
        }

        if (!validateNotes()) {
            return false;
        }

        if (!validateFromDate()) {
            return false;
        }

        if (!validateTodate()) {
            return false;
        }
        return true;
    }

    private boolean validateLocation() {
        if (TextUtils.isEmpty(etLocation.getText().toString().trim())) {
            etLocation.setError("Location is required!");
            requestFocus(etLocation);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateTournments() {
        if (TextUtils.isEmpty(etTournments.getText().toString().trim())) {
            etTournments.setError("Tournments is required!");
            requestFocus(etTournments);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateTodate() {
        if (TextUtils.isEmpty(etToDate.getText().toString().trim())) {
            etToDate.setError("ToDate is required!");
            requestFocus(etToDate);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateFromDate() {
        if (TextUtils.isEmpty(etFromDate.getText().toString().trim())) {
            etFromDate.setError("FromDate is required!");
            requestFocus(etFromDate);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateNotes() {
        if (TextUtils.isEmpty(etNotes.getText().toString().trim())) {
            etNotes.setError("Notes is required!");
            requestFocus(etNotes);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateAddress() {
        if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
            etAddress.setError("Address is required!");
            requestFocus(etAddress);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateMobile() {
        if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
            etMobile.setError("Mobile Number is required!");
            requestFocus(etMobile);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateEmail() {
        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            etEmail.setError("Email is required!");
            requestFocus(etEmail);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateOrganiserName() {
        if (TextUtils.isEmpty(etOrganiserName.getText().toString().trim())) {
            etOrganiserName.setError("OrganiserName is required!");
            requestFocus(etOrganiserName);
            return false;

        } else {
            return true;
        }
    }

    private void createTournamentRequest() {
        tDate = etToDate.getText().toString().trim();
        fDate = etFromDate.getText().toString().trim();
        CreateTournamentRequest createTournamentRequest = new CreateTournamentRequest();
        createTournamentRequest.setDocId("");
        createTournamentRequest.setOrganizername(etOrganiserName.getText().toString().trim());
        createTournamentRequest.setEmail(etEmail.getText().toString().trim());
        createTournamentRequest.setMobile(etMobile.getText().toString().trim());
        createTournamentRequest.setSporttype(sportsType);
        createTournamentRequest.setTournamentname(etTournments.getText().toString().trim());
        createTournamentRequest.setLocation(etLocation.getText().toString().trim());
        createTournamentRequest.setAddress(etAddress.getText().toString().trim());
        createTournamentRequest.setNotes(etNotes.getText().toString().trim());
        createTournamentRequest.setFromdate(fDate);
        createTournamentRequest.setTodate(tDate);
        createTournamentRequest.setStatus("PROPOSED");


        Call<CreateTournamentResponse> createTournament = RestApi.get().getRestService().createTounmentRequest(createTournamentRequest);
        createTournament.enqueue(new Callback<CreateTournamentResponse>() {
            @Override
            public void onResponse(Call<CreateTournamentResponse> call, Response<CreateTournamentResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null) {
                        Helper.showToast(getActivity(), response.body().getData().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateTournamentResponse> call, Throwable t) {

            }
        });
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
