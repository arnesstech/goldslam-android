package com.slam.gss.goldslam.models.Tournaments;

/**
 * Created by Thriveni on 6/22/2017.
 */

public class Venues {
    private String phone;

    private String workingHours;

    private String email;

    private String location;

    private Courts[] courts;

    private String name;

    private String docId;

    private String editable;

    private String mainActivity;

    private String[] sportTypes;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Courts[] getCourts() {
        return courts;
    }

    public void setCourts(Courts[] courts) {
        this.courts = courts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getEditable() {
        return editable;
    }

    public void setEditable(String editable) {
        this.editable = editable;
    }

    public String getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(String mainActivity) {
        this.mainActivity = mainActivity;
    }

    public String[] getSportTypes() {
        return sportTypes;
    }

    public void setSportTypes(String[] sportTypes) {
        this.sportTypes = sportTypes;
    }

    @Override
    public String toString() {
        return "ClassPojo [phone = " + phone + ", workingHours = " + workingHours + ", email = " + email + ", location = " + location + ", courts = " + courts + ", name = " + name + ", docId = " + docId + ", editable = " + editable + ", mainActivity = " + mainActivity + ", sportTypes = " + sportTypes + "]";
    }
}
