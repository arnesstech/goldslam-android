package com.slam.gss.goldslam.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.UserSubscriptionCreateAdapter;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.interfaces.MySubscriptionsListener;
import com.slam.gss.goldslam.models.MySubscriptions.MySubscriptionsResponse;
import com.slam.gss.goldslam.models.MySubscriptions.UserSubscriptions;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.Subscriptions;
import com.slam.gss.goldslam.network.RestApi;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 5/19/2017.
 */

public class MySubscriptionsFragment extends Fragment implements View.OnClickListener,MySubscriptionsListener {


    //@BindView(R.id.lv_user_subscriptions)
    ListView lvUserSubscriptions;

    //@BindView(R.id.btn_subscribe)
    Button btnSubscrbe;

    private UserSubscriptionCreateAdapter adapter;

    private TextView temp_page_title_menu, temp_single_menu;
    private ImageView imFilter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_subscriptions_fragment, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText(" My Subscriptions");
        temp_page_title_menu.setGravity(Gravity.CENTER);

        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.INVISIBLE);
        }

        imFilter = (ImageView) getActivity().findViewById(R.id.im_filter);
        if (imFilter != null) {
            imFilter.setVisibility(View.GONE);
        }

        /*android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);*/

        ButterKnife.bind(view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadSubscriptions();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lvUserSubscriptions = (ListView) view.findViewById(R.id.lv_user_subscriptions);
        btnSubscrbe = (Button) view.findViewById(R.id.btn_subscribe);
        btnSubscrbe.setOnClickListener(this);
    }

    private void loadSubscriptions() {

        SharedPreferences sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
        String userId = sp.getString("registrationDocId", "");
        Call<MySubscriptionsResponse> mySubscription = RestApi.get().getRestService().getUserSubscriptions(userId);
        mySubscription.enqueue(new Callback<MySubscriptionsResponse>() {
            @Override
            public void onResponse(Call<MySubscriptionsResponse> call, Response<MySubscriptionsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null && response.body().getData().getUserSubscriptions() != null) {

                        prepareData(response.body().getData().getUserSubscriptions());
                    } else {

                    }


                } else {

                }
            }

            @Override
            public void onFailure(Call<MySubscriptionsResponse> call, Throwable t) {
                Helper.showToast(getActivity(), "NetWork Problem");

            }
        });
    }

    private void prepareData(UserSubscriptions[] userSubscriptions) {

        List<UserSubscriptions> userSubscriptionss = new ArrayList<UserSubscriptions>(Arrays.asList(userSubscriptions));
        adapter = new UserSubscriptionCreateAdapter(getActivity());
        Log.e("userSubscriptions",userSubscriptions.length+"");
        adapter.addItems(userSubscriptionss);
        lvUserSubscriptions.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_subscribe:
                subscribeUserSubscription();
                break;
            default:
                break;
        }
    }

    private void subscribeUserSubscription() {
        SubscriptionFragment dialogFragment = SubscriptionFragment.newInstance(getActivity(),"mysubscriptions");
        dialogFragment.setListener(MySubscriptionsFragment.this);
        FragmentActivity activity = (FragmentActivity) (getActivity());
        FragmentManager fm = activity.getSupportFragmentManager();
        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogFragment.show(fm, "SubscriptionFragment");
    }

    @Override
    public void notifySubscriptions(String str) {
        if ("s".equalsIgnoreCase(str)) {
            loadSubscriptions();
        } else {

        }
    }
}
