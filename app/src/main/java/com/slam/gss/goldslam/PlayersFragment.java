package com.slam.gss.goldslam;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.scheduleplayers.Entries;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.PlayersResponse;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ramesh on 21/8/16.
 */
public class PlayersFragment extends BaseFragment {
    String docId, publishEntries;
    Call<PlayersResponse> call;

    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.empty)
    TextView empty;


    private boolean isRefreshing;
    private PlayersAdapter adapter;
    private int pos = 0, sno;
    private List<Entries> matchesss;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        docId = getActivity().getIntent().getStringExtra("docId");
        publishEntries = getActivity().getIntent().getStringExtra("publishEntries");
        adapter = new PlayersAdapter(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_live_player_schedule, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        listView.setAdapter(adapter);
        listView.setDividerHeight((int) (1 * getResources().getDisplayMetrics().density));
//        listView.setDivider(new ColorDrawable(Color.BLACK));
        view.findViewById(R.id.players_header).setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (publishEntries.equalsIgnoreCase("true")) {
            loadPlayer();
        } else {
            showEmptyView();
        }
    }

    public void loadPlayer() {
        if (isRefreshing) {
            return;
        }
        showProgressDialog();
        isRefreshing = true;
        Call<PlayersResponse> call = RestApi.get()
                .getRestService()
                .getPlayers(docId, "ENTRY");
        //
        call.enqueue(new Callback<PlayersResponse>() {
            @Override
            public void onResponse(Call<PlayersResponse> call, Response<PlayersResponse> response) {
                isRefreshing = false;
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {

                        String obj = new Gson().toJson(response.body().getData().getEntries());

                        // Log.i("Players Data is", response.body().getData().getEntries().toString());
                        filterWithDates(response.body().getData().getEntries());
                    }
                } else {
                    //Handle errors
                }
                dismissProgressDialog();
                //showEmptyView();
            }

            @Override
            public void onFailure(Call<PlayersResponse> call, Throwable t) {
                isRefreshing = false;
                if (call.isCanceled()) {
                    //Do nothing
                } else {
                    //Handle errors
                }
                showEmptyView();
                dismissProgressDialog();
            }
        });
    }

    public void filterWithDates(List<Entries> matches) {

        matchesss = new ArrayList<>();
        boolean flag = false;

        for (int i = 0; i < matches.size(); i++) {
            if (Boolean.parseBoolean(matches.get(i).getPublish())) {
                flag = true;
                matchesss.add(matches.get(i));
            } else {
            }
        }

        if (flag) {
            if (adapter != null) {
                adapter.addItems(matchesss);
            }
        } else {
            empty.setVisibility(View.VISIBLE);
            empty.setText("No Direct Entries");
        }
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        // showEmptyView();

    }

    private void showEmptyView() {
        if (!isAdded()) {
            return;
        }
        if (adapter.getCount() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
        }
    }


    class PlayersViewHolder extends AbstractViewHolder {
        public TextView getSno() {
            return sno;
        }

        public TextView getName() {
            return name;
        }

        public TextView getRank() {
            return rank;
        }

        public LinearLayout getPlayerLayout() {
            return llPlayersList;
        }

        public TextView getNoDirectEntries() {
            return noDirectEntries;
        }

        @BindView(R.id.no_direct_entries)
        TextView noDirectEntries;
        @BindView(R.id.txt_sno)
        TextView sno;
        @BindView(R.id.txt_name)
        TextView name;
        @BindView(R.id.txt_rank)
        TextView rank;

        @BindView(R.id.ll_players)
        LinearLayout llPlayersList;

        public PlayersViewHolder(View view) {
            super(view);
        }


    }

    private class PlayersAdapter extends AbstractBaseAdapter<Entries, PlayersViewHolder> {

        public PlayersAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.frag_players_row;
        }

        @Override
        public PlayersViewHolder getViewHolder(View convertView) {
            return new PlayersViewHolder(convertView);
        }

        @Override
        public void bindView(int position, PlayersViewHolder holder, Entries item) {
            try {
                int i = 0;

                holder.getName().setText(item.getTeamName());
                holder.getRootView().setBackgroundColor(Color.parseColor("#E9EEEC"));
                String rank = "";
                if (item.getMainEntryMode().equalsIgnoreCase("WILDCARD")) {
                    holder.getRootView().setBackgroundColor(Color.parseColor("#4ddbff"));
                    rank = item.getRank() + "(WC)";
                } else if (item.getMainEntryMode().equalsIgnoreCase("LUCKY_LOSER")) {
                    holder.getRootView().setBackgroundColor(Color.parseColor("#1aff8c"));
                    rank = item.getRank() + "(LL)";
                } else if (item.getMainEntryMode().equalsIgnoreCase("QUALIFYING")) {
                    holder.getRootView().setBackgroundColor(Color.parseColor("#9f9fdf"));
                    rank = item.getRank() + "(Q)";
                } else if (item.getMainEntryMode().equalsIgnoreCase("THRU_POINTS")) {
                    rank = item.getRank();
                    holder.getRootView().setBackgroundColor(Color.parseColor("#4CAF50"));
                } else {
                    rank = item.getRank();
                }

                holder.getRank().setText(rank);

                sno = position;
                Log.e("sno", sno + "");

                if (Boolean.parseBoolean(item.getPublish())) {
                    holder.getPlayerLayout().setVisibility(View.VISIBLE);
                    holder.getNoDirectEntries().setVisibility(View.GONE);

                  /*  Log.e("sno if",sno+"");

                    Log.e("Player name",item.getTeamName());

                    if (pos  != matchesss.size()) {
                        pos = sno;
                        Log.e("pos",pos+"");
                        holder.getSno().setText(String.valueOf(pos));
                    } else {
                        pos = 0;
                        pos = sno;
                        holder.getSno().setText(String.valueOf(pos));
                    }*/

                    holder.getSno().setText(String.valueOf(position + 1));

                } else {
                    Log.e("Player name", item.getTeamName());
                    Log.e("sno else ", sno + "");
                    i++;
                    //sno = --position ;
                    holder.getPlayerLayout().setVisibility(View.GONE);
                }


                if (i == adapter.getCount()) {
                    holder.getNoDirectEntries().setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void showProgressDialog() {
        super.showProgressDialog();
        if (progress != null) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void dismissProgressDialog() {
        super.dismissProgressDialog();
        if (progress != null) {
            progress.setVisibility(View.GONE);
        }
    }
}
