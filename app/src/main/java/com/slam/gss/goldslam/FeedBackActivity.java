package com.slam.gss.goldslam;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.feedBack.FeedBackRequest;
import com.slam.gss.goldslam.models.feedBack.FeedBackResponse;
import com.slam.gss.goldslam.network.RestApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 6/27/2017.
 */

public class FeedBackActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_subject)
    EditText etSubject;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_mobile)
    EditText etMobile;

    @BindView(R.id.et_comment)
    EditText etComment;

    @BindView(R.id.btn_feed_back_submit)
    Button btnSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_back);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_feed_back_submit:
                submitFeedBack();
                break;

            default:
                break;
        }
    }

    private void submitFeedBack() {

        if (!validateName()) {
            return;
        }
        if (!validateSubject()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validateMobile()) {
            return;
        }
        if (!validateMessage()) {
            return;
        } else {

            FeedBackRequest request = new FeedBackRequest();
            request.setEmail(etEmail.getText().toString().trim());
            request.setMessage(etComment.getText().toString().trim());
            request.setName(etName.getText().toString().trim());
            request.setPhone(etMobile.getText().toString().trim());
            request.setSubject(etSubject.getText().toString().trim());
            Call<FeedBackResponse> feedBack = RestApi.get().getRestService().feedBack(request);
            feedBack.enqueue(new Callback<FeedBackResponse>() {
                @Override
                public void onResponse(Call<FeedBackResponse> call, Response<FeedBackResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null && response.body().getData() != null) {

                            Helper.showToast(FeedBackActivity.this, "Submited feed back thank you");
                            startActivity(new Intent(FeedBackActivity.this, AppDashBoard.class));
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<FeedBackResponse> call, Throwable t) {

                }
            });

        }
    }

    private boolean validateName() {
        if (TextUtils.isEmpty(etName.getText().toString().trim())) {
            etName.setError("Name is required!");
            requestFocus(etName);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateSubject() {
        if (TextUtils.isEmpty(etSubject.getText().toString().trim())) {
            etSubject.setError("Subject is required!");
            requestFocus(etSubject);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateEmail() {
        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            etEmail.setError("Email is required!");
            requestFocus(etEmail);
            return false;

        } else {
            return true;
        }
    }

    private boolean validateMobile() {
        if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
            etMobile.setError("Mobile is required!");
            requestFocus(etMobile);
            return false;

        } else if (etMobile.getText().length() < 10) {
            etMobile.setError("Mobile Number less than 10 digits");
            requestFocus(etMobile);
            return false;
        } else {
            return true;
        }
    }

    private boolean validateMessage() {
        if (TextUtils.isEmpty(etComment.getText().toString().trim())) {
            etComment.setError("Message is required!");
            requestFocus(etComment);
            return false;

        } else {
            return true;
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

}
