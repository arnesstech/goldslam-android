package com.slam.gss.goldslam.models.Login;


/**
 * Created by Thriveni on 1/11/2017.
 */
public class Data {
    private String message;

    private String resetusernameflag;

    private UserAssociations[] userAssociations;

    private String aistaNo;

    private String apiToken;

    private String gssid;

    private String[] roles;

    private String name;

    private String userName;

    private String resetpasswordflag;

    private String registrationDocId;

    private String aitaNo;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getResetusernameflag ()
    {
        return resetusernameflag;
    }

    public void setResetusernameflag (String resetusernameflag)
    {
        this.resetusernameflag = resetusernameflag;
    }

    public UserAssociations[] getUserAssociations ()
    {
        return userAssociations;
    }

    public void setUserAssociations (UserAssociations[] userAssociations)
    {
        this.userAssociations = userAssociations;
    }

    public String getAistaNo ()
    {
        return aistaNo;
    }

    public void setAistaNo (String aistaNo)
    {
        this.aistaNo = aistaNo;
    }

    public String getApiToken ()
    {
        return apiToken;
    }

    public void setApiToken (String apiToken)
    {
        this.apiToken = apiToken;
    }

    public String getGssid ()
    {
        return gssid;
    }

    public void setGssid (String gssid)
    {
        this.gssid = gssid;
    }

    public String[] getRoles ()
    {
        return roles;
    }

    public void setRoles (String[] roles)
    {
        this.roles = roles;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUserName ()
    {
        return userName;
    }

    public void setUserName (String userName)
    {
        this.userName = userName;
    }

    public String getResetpasswordflag ()
    {
        return resetpasswordflag;
    }

    public void setResetpasswordflag (String resetpasswordflag)
    {
        this.resetpasswordflag = resetpasswordflag;
    }

    public String getRegistrationDocId ()
    {
        return registrationDocId;
    }

    public void setRegistrationDocId (String registrationDocId)
    {
        this.registrationDocId = registrationDocId;
    }

    public String getAitaNo ()
    {
        return aitaNo;
    }

    public void setAitaNo (String aitaNo)
    {
        this.aitaNo = aitaNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", resetusernameflag = "+resetusernameflag+", userAssociations = "+userAssociations+", aistaNo = "+aistaNo+", apiToken = "+apiToken+", gssid = "+gssid+", roles = "+roles+", name = "+name+", userName = "+userName+", resetpasswordflag = "+resetpasswordflag+", registrationDocId = "+registrationDocId+", aitaNo = "+aitaNo+"]";
    }
}
