package com.slam.gss.goldslam.models.eventadmin;

/**
 * Created by Thriveni on 9/12/2016.
 */
public class CourtsData {
    private String message;

    private Courts[] courts;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Courts[] getCourts ()
    {
        return courts;
    }

    public void setCourts (Courts[] courts)
    {
        this.courts = courts;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", courts = "+courts+"]";
    }
}
