package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class UpCommingEventsHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.im_event_logo)
    ImageView imEventLogo;

    @BindView(R.id.txt_event_description)
    TextView txtEventDescription;

    @BindView(R.id.txt_event_date)
    TextView txtEventDate;


    @BindView(R.id.txt_event_location)
    TextView txtEventLocation;



    public TextView getTxtEventDescription() {
        return txtEventDescription;
    }

    public TextView getTxtEventDate() {
        return txtEventDate;
    }

    public TextView getTxtEventLocation() {
        return txtEventLocation;
    }

    public ImageView getImEventLogo() {
        return imEventLogo;
    }

    public UpCommingEventsHolder(View itemView) {
        super(itemView);
    }
}
