package com.slam.gss.goldslam.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/27/2017.
 */

public class ContactUsFragment extends android.support.v4.app.Fragment {

    private TextView temp_page_title_menu, temp_single_menu;
    @BindView(R.id.txt_email)
    TextView txtEmail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, view);

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText("Contact Us");
        temp_page_title_menu.setGravity(Gravity.CENTER);
        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.INVISIBLE);
        }
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMail();
            }

            private void openMail() {
                String email = txtEmail.getText().toString();
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + email));
                    //intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject");
                    //intent.putExtra(Intent.EXTRA_TEXT, "your_text");
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //TODO smth
                }
            }
        });
    }
}
