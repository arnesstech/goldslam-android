package com.slam.gss.goldslam.adapters;

import android.widget.RatingBar;

/**
 * Created by mahes on 5/30/2017.
 */

public class LocationList {
    private String title, location;
   // private RatingBar rating;
    private int imageId;

    public LocationList(int imageId, String title, String location) {
        // TODO Auto-generated constructor stub
        this.imageId = imageId;
        this.title = title;
       /// this.rating = rating;
        this.location = location;

    }
    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String gettitle() {
        return title;
    }

    public void settitle(String title) {
        this.title = title;
    }

   /* public RatingBar getrating() {
        return rating;
    }

    public void setratting(RatingBar rating) {
        this.rating = rating;

    }*/

    public String getlocation() {
        return location;
    }

    public void setlocation(String location) {
        this.location = location;
    }

}
