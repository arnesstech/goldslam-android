package com.slam.gss.goldslam.models.MySubscriptions;

/**
 * Created by Thriveni on 5/19/2017.
 */

public class UserSubscriptions {
    private String amount;

    private String startDate;

    private String expiryDate;

    private String subscriptionType;

    private String subscriptionTypeId;

    private String subscriptionId;

    private String subscriptionName;

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getStartDate ()
    {
        return startDate;
    }

    public void setStartDate (String startDate)
    {
        this.startDate = startDate;
    }

    public String getExpiryDate ()
    {
        return expiryDate;
    }

    public void setExpiryDate (String expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public String getSubscriptionType ()
    {
        return subscriptionType;
    }

    public void setSubscriptionType (String subscriptionType)
    {
        this.subscriptionType = subscriptionType;
    }

    public String getSubscriptionTypeId ()
    {
        return subscriptionTypeId;
    }

    public void setSubscriptionTypeId (String subscriptionTypeId)
    {
        this.subscriptionTypeId = subscriptionTypeId;
    }

    public String getSubscriptionId ()
    {
        return subscriptionId;
    }

    public void setSubscriptionId (String subscriptionId)
    {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionName ()
    {
        return subscriptionName;
    }

    public void setSubscriptionName (String subscriptionName)
    {
        this.subscriptionName = subscriptionName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", startDate = "+startDate+", expiryDate = "+expiryDate+", subscriptionType = "+subscriptionType+", subscriptionTypeId = "+subscriptionTypeId+", subscriptionId = "+subscriptionId+", subscriptionName = "+subscriptionName+"]";
    }
}
