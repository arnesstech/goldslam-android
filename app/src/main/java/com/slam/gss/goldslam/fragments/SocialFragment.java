package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AmenitiesAdapter;
import com.slam.gss.goldslam.models.amenities.Amenities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.slam.gss.goldslam.fragments.AddCoachingCenterActivity.viewPager;

/**
 * Created by mahes on 6/19/2017.
 */

public class SocialFragment extends Fragment {
    @BindView(R.id.rv_amenities)
    RecyclerView rvAmenities;

    @BindView(R.id.btn_social_back)
    Button btnSocialBack;

    @BindView(R.id.btn_social_next)
    Button btnSocialNext;

    private ProgressDialog progressDialog;
    List<Amenities> amenitiesList;
    GridLayoutManager layoutManager;
    AmenitiesAdapter amenitiesAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.item_social, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        layoutManager = new GridLayoutManager(getActivity(), 3);
        amenitiesAdapter = new AmenitiesAdapter(getActivity());
        rvAmenities.setLayoutManager(layoutManager);
        amenitiesList = new ArrayList<>();
        Button btnSocialNext = (Button) getActivity().findViewById(R.id.btn_social_next);
        Button btnSocialBack = (Button) getActivity().findViewById(R.id.btn_social_back);

        btnSocialBack.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
            }
        });

        btnSocialNext.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
            }
        });

        loadAmenities();

    }

    private void loadAmenities() {
        Amenities amnetiesObj = new Amenities();
        amnetiesObj.setAmenitiesCheckbox("a");
        amnetiesObj.setAmenitiesTxtName("Club");
        amenitiesList.add(amnetiesObj);

        amnetiesObj = new Amenities();
        amnetiesObj.setAmenitiesCheckbox("x");
        amnetiesObj.setAmenitiesTxtName("Home");
        amenitiesList.add(amnetiesObj);

        amnetiesObj = new Amenities();
        amnetiesObj.setAmenitiesCheckbox("x");
        amnetiesObj.setAmenitiesTxtName("Gym");
        amenitiesList.add(amnetiesObj);

        amnetiesObj = new Amenities();
        amnetiesObj.setAmenitiesCheckbox("x");
        amnetiesObj.setAmenitiesTxtName("Room");
        amenitiesList.add(amnetiesObj);

        amnetiesObj = new Amenities();
        amnetiesObj.setAmenitiesCheckbox("x");
        amnetiesObj.setAmenitiesTxtName("Office");
        amenitiesList.add(amnetiesObj);

        amnetiesObj = new Amenities();
        amnetiesObj.setAmenitiesCheckbox("x");
        amnetiesObj.setAmenitiesTxtName("More");
        amenitiesList.add(amnetiesObj);


        amenitiesAdapter.addItem(amenitiesList);
        rvAmenities.setAdapter(amenitiesAdapter);
    }
}
