package com.slam.gss.goldslam.models.upCommingEvents;

import java.util.List;

/**
 * Created by Thriveni on 6/27/2017.
 */

public class Tournaments {
    private String series;

    private String phone;

    private String sno;

    private String location;

    private List<String> events;

    private List<AddlInfo> addlInfo;

    private String hospitality;

    private List<Courts> courts;

    private String onlinePayment;

    private String paymentStartDate;

    private List<Admins> admins;

    private String paymentEndDate;

    private String entryFeeDetails;

    private String tournamentName;

    private String prizeMoney;

    private List<String> attachments;

    private String entryStartDate;

    private String finalListDate;

    private String associationDocId;

    private String entryEndDate;

    private String tournamentId;

    private String otherDetails;

    private List<String> accommodations;

    private String status;

    private String associationId;

    private List<Moderators> moderators;

    private List<Venues> venues;

    private String toDate;

    private String entryWithdrawDate;

    private String associationName;

    private String fromDate;

    private String email;

    private String sportType;

    private String docId;

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }

    public List<AddlInfo> getAddlInfo() {
        return addlInfo;
    }

    public void setAddlInfo(List<AddlInfo> addlInfo) {
        this.addlInfo = addlInfo;
    }

    public String getHospitality() {
        return hospitality;
    }

    public void setHospitality(String hospitality) {
        this.hospitality = hospitality;
    }

    public List<Courts> getCourts() {
        return courts;
    }

    public void setCourts(List<Courts> courts) {
        this.courts = courts;
    }

    public String getOnlinePayment() {
        return onlinePayment;
    }

    public void setOnlinePayment(String onlinePayment) {
        this.onlinePayment = onlinePayment;
    }

    public String getPaymentStartDate() {
        return paymentStartDate;
    }

    public void setPaymentStartDate(String paymentStartDate) {
        this.paymentStartDate = paymentStartDate;
    }

    public List<Admins> getAdmins() {
        return admins;
    }

    public void setAdmins(List<Admins> admins) {
        this.admins = admins;
    }

    public String getPaymentEndDate() {
        return paymentEndDate;
    }

    public void setPaymentEndDate(String paymentEndDate) {
        this.paymentEndDate = paymentEndDate;
    }

    public String getEntryFeeDetails() {
        return entryFeeDetails;
    }

    public void setEntryFeeDetails(String entryFeeDetails) {
        this.entryFeeDetails = entryFeeDetails;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getPrizeMoney() {
        return prizeMoney;
    }

    public void setPrizeMoney(String prizeMoney) {
        this.prizeMoney = prizeMoney;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public String getEntryStartDate() {
        return entryStartDate;
    }

    public void setEntryStartDate(String entryStartDate) {
        this.entryStartDate = entryStartDate;
    }

    public String getFinalListDate() {
        return finalListDate;
    }

    public void setFinalListDate(String finalListDate) {
        this.finalListDate = finalListDate;
    }

    public String getAssociationDocId() {
        return associationDocId;
    }

    public void setAssociationDocId(String associationDocId) {
        this.associationDocId = associationDocId;
    }

    public String getEntryEndDate() {
        return entryEndDate;
    }

    public void setEntryEndDate(String entryEndDate) {
        this.entryEndDate = entryEndDate;
    }

    public String getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(String tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public List<String> getAccommodations() {
        return accommodations;
    }

    public void setAccommodations(List<String> accommodations) {
        this.accommodations = accommodations;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssociationId() {
        return associationId;
    }

    public void setAssociationId(String associationId) {
        this.associationId = associationId;
    }

    public List<Moderators> getModerators() {
        return moderators;
    }

    public void setModerators(List<Moderators> moderators) {
        this.moderators = moderators;
    }

    public List<Venues> getVenues() {
        return venues;
    }

    public void setVenues(List<Venues> venues) {
        this.venues = venues;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getEntryWithdrawDate() {
        return entryWithdrawDate;
    }

    public void setEntryWithdrawDate(String entryWithdrawDate) {
        this.entryWithdrawDate = entryWithdrawDate;
    }

    public String getAssociationName() {
        return associationName;
    }

    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSportType() {
        return sportType;
    }

    public void setSportType(String sportType) {
        this.sportType = sportType;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    @Override
    public String toString() {
        return "ClassPojo [series = " + series + ", phone = " + phone + ", sno = " + sno + ", location = " + location + ", events = " + events + ", addlInfo = " + addlInfo + ", hospitality = " + hospitality + ", courts = " + courts + ", onlinePayment = " + onlinePayment + ", paymentStartDate = " + paymentStartDate + ", admins = " + admins + ", paymentEndDate = " + paymentEndDate + ", entryFeeDetails = " + entryFeeDetails + ", tournamentName = " + tournamentName + ", prizeMoney = " + prizeMoney + ", attachments = " + attachments + ", entryStartDate = " + entryStartDate + ", finalListDate = " + finalListDate + ", associationDocId = " + associationDocId + ", entryEndDate = " + entryEndDate + ", tournamentId = " + tournamentId + ", otherDetails = " + otherDetails + ", accommodations = " + accommodations + ", status = " + status + ", associationId = " + associationId + ", moderators = " + moderators + ", venues = " + venues + ", toDate = " + toDate + ", entryWithdrawDate = " + entryWithdrawDate + ", associationName = " + associationName + ", fromDate = " + fromDate + ", email = " + email + ", sportType = " + sportType + ", docId = " + docId + "]";
    }
}
