package com.slam.gss.goldslam.helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Thriveni on 6/1/2017.
 */

public class JSONParser {
    //Your class here, or you can define it in the constructor

    //Filename
    InputStream jsonFileName;

    //constructor
    public JSONParser(InputStream jsonFileName){
        this.jsonFileName = jsonFileName;
    }


    //Returns a json object from an input stream
    public JSONObject getJsonObject(){

        //Create input stream
       // InputStream inputStreamObject = getRequestclass().getResourceAsStream(jsonFileName);

        try {
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(jsonFileName, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());

            //returns the json object
            return jsonObject;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //if something went wrong, return null
        return null;
    }

}

