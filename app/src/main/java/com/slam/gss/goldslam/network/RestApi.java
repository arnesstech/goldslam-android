package com.slam.gss.goldslam.network;

import android.content.Context;
import android.content.SharedPreferences;

import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.models.Associations.AssociationList;
import com.slam.gss.goldslam.models.Events.EventsReponse;
import com.slam.gss.goldslam.models.ForgotUserName.ForgotUserNameRequest;
import com.slam.gss.goldslam.models.ForgotUserName.ForgotUserNameResponse;
import com.slam.gss.goldslam.models.GetProfile.GetProfileResponse;
import com.slam.gss.goldslam.models.GetProfile.Profile;
import com.slam.gss.goldslam.models.MasterData.Country;
import com.slam.gss.goldslam.models.MasterData.SubscriptionType.SubscriptionTypes;
import com.slam.gss.goldslam.models.MySubscriptions.MySubscriptionsResponse;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.Subscriptions;
import com.slam.gss.goldslam.models.Subscriptions.UserSubscriptionCreate.UserSubscriptionCreateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserSubscriptionCreate.UserSubscriptionCreateResponse;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.UserTransactionCreateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.UserTransactionCreateResponse;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate.UserTransactionUpdateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate.UserTransactionUpdateResponse;
import com.slam.gss.goldslam.models.Tournaments.TournamentResponse;
import com.slam.gss.goldslam.models.UpdateProfile.UpdateProfileResponse;
import com.slam.gss.goldslam.models.UpdateUserName.UpdateUserNameRequest;
import com.slam.gss.goldslam.models.UpdateUserName.UpdateUserNameResponse;
import com.slam.gss.goldslam.models.createTournamentRequest.CreateTournamentRequest;
import com.slam.gss.goldslam.models.createTournamentRequest.CreateTournamentResponse;
import com.slam.gss.goldslam.models.drawresults.DrawResultsResponse;
import com.slam.gss.goldslam.models.eventadmin.CourtsMasterDataResponse;
import com.slam.gss.goldslam.models.eventadmin.EventAdminTournaments.EventAdminResponse;
import com.slam.gss.goldslam.models.eventadmin.MatchConditionsResponse;
import com.slam.gss.goldslam.models.eventadmin.MatchSchedule.MatchScheduleResponse;
import com.slam.gss.goldslam.models.feedBack.FeedBackRequest;
import com.slam.gss.goldslam.models.feedBack.FeedBackResponse;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.PlayersResponse;
import com.slam.gss.goldslam.models.scheduleplayers.SchedulePlayerResponse;
import com.slam.gss.goldslam.models.upCommingEvents.UpCommingEventsResponse;
import com.slam.gss.goldslam.models.uploadFile.UploadImageResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Ref: 1. https://futurestud.io/blog/android-basic-authentication-with-retrofit
 * Created by Ramesh on 17/8/16.
 */
public class RestApi {
    public static String BASE_URL = Connections.BASE_URL;
    private static SharedPreferences sp;

        private static RestApi instance = null;


    public static synchronized RestApi get() {
        if (instance == null) {
            instance = new RestApi();
        }
        return instance;
    }

    public static void init(Context context) {
        sp = context.getSharedPreferences("GSS", Context.MODE_PRIVATE);
    }

    public RestService getRestService() {
        if (sp == null) {
            throw new IllegalStateException("init method should be called first.");
        }
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        return buildAdapter(BASE_URL, RestService.class, builder);
    }

    private String getApiToken() {
        //TODO: Need to confirm on Basic or Bearer ?
        final String credentials = "Basic " + sp.getString("apiToken", "");
        //Log.e("apiToken",credentials);
        //final String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        //return base64EncodedCredentials;
        return credentials;
    }

    private <T> T buildAdapter(String baseUrl, Class<T> clazz, OkHttpClient.Builder builder) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //
        OkHttpClient defaultHttpClient = builder
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        // Request customization: add request headers
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("Authorization", getApiToken())
                                .header("App-Id", "PROSPORT")
                                .header("Client-Type", "APP");
                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(defaultHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(clazz);
    }

    public interface RestService {



        //http://gss.goldslamsports.com/prosports/api/matches?eventdocid=57580e5f0fff533d18098a0e&status=ALL&draw=CURRENT
        @GET("api/matches")
        Call<SchedulePlayerResponse> getScheduleMatches(@Query("eventdocid") String docId,
                                                        @Query("status") String status,
                                                        @Query("draw") String draw
        );

        //http://gss.goldslamsports.com/prosports/api/matches?eventdocid=57580e5f0fff533d18098a0e&draw=MAIN/QUALIFYING
        @GET("api/matches")
        Call<SchedulePlayerResponse> getEventScheduleMatches(@Query("eventdocid") String docId,
                                                             @Query("draw") String draw
        );


        // http://gss.goldslamsports.com/prosports/api/tournament/{tournmentDocID}/courts
        @GET("api/tournament/{tournmentDocID}/courts")
        Call<CourtsMasterDataResponse> getCourtsMasterData(@Path("tournmentDocID") String docId
        );


        @GET("api/masterdata/MATCH_CONDITION")
        Call<MatchConditionsResponse> getMatchCodition();


        // &status=ENTRY
        @GET("api/entries")
        Call<PlayersResponse> getPlayers(@Query("eventdocid") String docId,
                                         @Query("status") String status
        );

        @GET("api/bracket/event/{docId}")
        Call<DrawResultsResponse> getDrawResults(@Path("docId") String docId,
                                                 @Query("draw") String draw);

        //http://gss.goldslamsports.com/prosports/api/masterdata/COUNTRIES
        @GET("api/masterdata/COUNTRIES")
        Call<Country> getCountries();


        //http://gss.goldslamsports.com/prosports/api/masterdata/SPORT_TYPE
        @GET("api/masterdata/SPORT_TYPE")
        Call<Country> getSportTypes();

        //http://gss.goldslamsports.com/prosports/api/associations
        @GET("api/associations")
        @Headers({"Context:player"})
        Call<AssociationList> getAssociations();


        //  http://gss.goldslamsports.com/prosports/api/match/schedule
        @POST("api/match/schedule")
        Call<MatchScheduleResponse> scheduleMatch(@Body Matches matches);

        //http://gss.goldslamsports.com/prosports/api/updateusername
        @POST("api/updateusername")
        Call<UpdateUserNameResponse> updateUserName(@Body UpdateUserNameRequest usernameRequest);

        //http://gss.goldslamsports.com/prosports/api/registration/forgotusername
        @POST("api/registration/forgotusername")
        Call<ForgotUserNameResponse> forgotUserName(@Body ForgotUserNameRequest forgotPasswordReq);

        //http://gss.goldslamsports.com/prosports/api/registration/5875be7e0fff530f41f7f37c
        @GET("api/registration/{docId}")
        Call<GetProfileResponse> getProfileData(@Path("docId") String docId);

        //http://gss.goldslamsports.com/prosports/api/events?admindocid=57513d63fafa88684760f1e2
        @GET("api/events")
        Call<EventAdminResponse> getEventAdminTournaments(@Query("admindocid") String adminDocId);


        //http://gss.goldslamsports.com/prosports/api/subscriptions
        @GET("api/subscriptions")
        Call<Subscriptions> getSubscriptions();

        //http://gss.goldslamsports.com/prosports/api/usertransaction
        @POST("api/usertransaction")
        Call<UserTransactionCreateResponse> userTransactionCreate(@Body UserTransactionCreateRequest request);

        //http://gss.goldslamsports.com/prosports/api/usertransaction
        @POST("api/usertransaction")
        Call<UserTransactionUpdateResponse> userTransactionUpdate(@Body UserTransactionUpdateRequest request);

        //http://gss.goldslamsports.com/prosports/api/usersubscription
        @POST("api/usersubscription")
        Call<UserSubscriptionCreateResponse> userSubscriptionCreate(@Body UserSubscriptionCreateRequest request);


        //http://gss.goldslamsports.com/prosports/api/masterdata/SUBSCRIPTION_TYPE
        @GET("api/masterdata/SUBSCRIPTION_TYPE")
        Call<SubscriptionTypes> getSubscriptionTypes();

        //http://gss.goldslamsports.com/prosports/fileManager/uploadFile
        @Multipart
        @POST("fileManager/uploadFile")
        Call<UploadImageResponse> uploadAttachment(@Part MultipartBody.Part filePart);

        //http://gss.goldslamsports.com/prosports/api/myaccount/{registrationDocId fr login Response}
        @POST("api/myaccount/{docId}")
        Call<UpdateProfileResponse> updateProfile(@Path("docId") String docId,@Body Profile request);

        //http://gss.goldslamsports.com/prosports/api/usersubscriptions?userid=5875d8c40fff530f41ba7e6b
        @GET("api/usersubscriptions")
        @Headers({"Context:player"})
        Call<MySubscriptionsResponse> getUserSubscriptions(@Query("userid") String userId);


        //http://gss.goldslamsports.com/prosports/api/tournaments
        @GET("api/tournaments")
        @Headers({"Context:player"})
        Call<TournamentResponse> getTournaments();

        //http://gss.goldslamsports.com/prosports/api/events?tournamentdocid=57909dc20fff53405e09f1b0
        @GET("api/events")
        @Headers({"Context:player"})
        Call<EventsReponse> getEvents(@Query("tournamentdocid") String tournamentId);


        //http://gss.goldslamsports.com/prosports/api/upcomingevents
        @GET("api/upcomingevents")
        Call<UpCommingEventsResponse> getUpCommingEvents();

        //http://139.162.49.134:8080/prosports/api/contactus
        @POST("api/contactus")
        Call<FeedBackResponse> feedBack( @Body FeedBackRequest request);

        //http://gss.goldslamsports.com/prosports/api/createtournamentrequest
        @POST("api/createtournamentrequest")
        Call<CreateTournamentResponse> createTounmentRequest(@Body CreateTournamentRequest request);

    }


}

