package com.slam.gss.goldslam.models.Subscriptions.UserSubscriptionCreate;

/**
 * Created by Thriveni on 3/20/2017.
 */

public class UserSubscriptionCreateRequest {
    private String paymentAmount;

    private String duration;

    private String subscriptionFee;

    private String gssId;

    private String transactionId;

    private String userdocId;

    private String gatewayTransactionId;

    private String name;

    private String subscriptionType;

    private String remarks;

    private String subscriptionTypeId;

    private String subscriptionId;

    private String subscriptionName;

    private String discount;

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSubscriptionFee() {
        return subscriptionFee;
    }

    public void setSubscriptionFee(String subscriptionFee) {
        this.subscriptionFee = subscriptionFee;
    }

    public String getGssId() {
        return gssId;
    }

    public void setGssId(String gssId) {
        this.gssId = gssId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUserdocId() {
        return userdocId;
    }

    public void setUserdocId(String userdocId) {
        this.userdocId = userdocId;
    }

    public String getGatewayTransactionId() {
        return gatewayTransactionId;
    }

    public void setGatewayTransactionId(String gatewayTransactionId) {
        this.gatewayTransactionId = gatewayTransactionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSubscriptionTypeId() {
        return subscriptionTypeId;
    }

    public void setSubscriptionTypeId(String subscriptionTypeId) {
        this.subscriptionTypeId = subscriptionTypeId;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionName() {
        return subscriptionName;
    }

    public void setSubscriptionName(String subscriptionName) {
        this.subscriptionName = subscriptionName;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "ClassPojo [paymentAmount = " + paymentAmount + ", duration = " + duration + ", subscriptionFee = " + subscriptionFee + ", gssId = " + gssId + ", transactionId = " + transactionId + ", userdocId = " + userdocId + ", gatewayTransactionId = " + gatewayTransactionId + ", name = " + name + ", subscriptionType = " + subscriptionType + ", remarks = " + remarks + ", subscriptionTypeId = " + subscriptionTypeId + ", subscriptionId = " + subscriptionId + ", subscriptionName = " + subscriptionName + ", discount = " + discount + "]";
    }
}
