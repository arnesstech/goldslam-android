package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import java.util.ArrayList;

/**
 * Created by Thriveni on 8/21/2016.
 */
public class SpinnerAdapter extends ArrayAdapter<String> {

    ArrayList<String> _spinnerList;
    Context _ctx;

    public SpinnerAdapter(Context ctx, int txtViewResourceId, ArrayList<String> spinnerList) {
        super(ctx, txtViewResourceId, spinnerList);
        this._spinnerList = spinnerList;
        this._ctx = ctx;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = getCustomView(position, convertView, parent);
        TextView txtGender = (TextView) v.findViewById(R.id.txtGender);
       // txtGender.setTextColor(Color.WHITE);
        return v;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) this._ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mySpinner = inflater.inflate(R.layout.spinner_text, parent, false);

        TextView txtGender = (TextView) mySpinner.findViewById(R.id.txtGender);
        txtGender.setText(_spinnerList.get(position));
        //txtGender.setTextColor(Color.BLACK);
        return mySpinner;
    }
}