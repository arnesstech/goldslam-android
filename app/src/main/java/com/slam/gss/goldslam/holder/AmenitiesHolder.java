package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;

/**
 * Created by mahes on 6/20/2017.
 */

public class AmenitiesHolder  extends AbstrctRecyclerViewholder{
    @BindView(R.id.cb_amenities)
    CheckBox cbAmenities;

    @BindView(R.id.txt_amenities)
    TextView txtAmenities;

    public CheckBox getCbAmenities() {
        return cbAmenities;
    }


    public TextView getTxtTeamName() {
        return txtAmenities;
    }

    public AmenitiesHolder(View itemView) {
        super(itemView);
    }
}
