package com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster;

import com.slam.gss.goldslam.models.Paymentdetails;

/**
 * Created by Thriveni on 3/17/2017.
 */

public class SubscriptionMasters {
    private String startDate;

    private String duration;

    private String subscriptionFee;

    private String isactive;

    private String subscriptionType;

    private String remarks;

    private String endDate;

    private Paymentdetails paymentdetails;

    private String docId;

    private String subscriptionTypeId;

    private String subscriptionName;

    public String getStartDate ()
    {
        return startDate;
    }

    public void setStartDate (String startDate)
    {
        this.startDate = startDate;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getSubscriptionFee ()
    {
        return subscriptionFee;
    }

    public void setSubscriptionFee (String subscriptionFee)
    {
        this.subscriptionFee = subscriptionFee;
    }

    public String getIsactive ()
    {
        return isactive;
    }

    public void setIsactive (String isactive)
    {
        this.isactive = isactive;
    }

    public String getSubscriptionType ()
    {
        return subscriptionType;
    }

    public void setSubscriptionType (String subscriptionType)
    {
        this.subscriptionType = subscriptionType;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    public String getEndDate ()
    {
        return endDate;
    }

    public void setEndDate (String endDate)
    {
        this.endDate = endDate;
    }

    public Paymentdetails getPaymentdetails ()
    {
        return paymentdetails;
    }

    public void setPaymentdetails (Paymentdetails paymentdetails)
    {
        this.paymentdetails = paymentdetails;
    }

    public String getDocId ()
    {
        return docId;
    }

    public void setDocId (String docId)
    {
        this.docId = docId;
    }

    public String getSubscriptionTypeId ()
    {
        return subscriptionTypeId;
    }

    public void setSubscriptionTypeId (String subscriptionTypeId)
    {
        this.subscriptionTypeId = subscriptionTypeId;
    }

    public String getSubscriptionName ()
    {
        return subscriptionName;
    }

    public void setSubscriptionName (String subscriptionName)
    {
        this.subscriptionName = subscriptionName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [startDate = "+startDate+", duration = "+duration+", subscriptionFee = "+subscriptionFee+", isactive = "+isactive+", subscriptionType = "+subscriptionType+", remarks = "+remarks+", endDate = "+endDate+", paymentdetails = "+paymentdetails+", docId = "+docId+", subscriptionTypeId = "+subscriptionTypeId+", subscriptionName = "+subscriptionName+"]";
    }
}
