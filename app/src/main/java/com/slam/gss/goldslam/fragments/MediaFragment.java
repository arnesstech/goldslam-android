package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.MediAdapter;
import com.slam.gss.goldslam.adapters.PlayingClubLocatorsAdapter;
import com.slam.gss.goldslam.models.media.MediaResponse;
import com.slam.gss.goldslam.models.playingClubLocators.PlayingClubLocators;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class MediaFragment extends Fragment {

    @BindView(R.id.rv_txt_media)
    RecyclerView rvMedia;

    private ProgressDialog progressDialog;
    ArrayList<MediaResponse> mMediaList;
    GridLayoutManager mLayoutManager;
    MediAdapter mMediaAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLayoutManager = new GridLayoutManager(getActivity(),2);
        rvMedia.setLayoutManager(mLayoutManager);
        rvMedia.setItemAnimator(new DefaultItemAnimator());
        rvMedia.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mMediaList = new ArrayList<>();
        mMediaAdapter = new MediAdapter(getActivity());
        loadMedia();

    }

    private void loadMedia() {

        MediaResponse media = new MediaResponse();
        media.setMediaText("Some Text");
        mMediaList.add(media);

        media = new MediaResponse();
        media.setMediaText("Some Text");
        mMediaList.add(media);

        media = new MediaResponse();
        media.setMediaText("Some Text");
        mMediaList.add(media);


        media = new MediaResponse();
        media.setMediaText("Some Text");
        mMediaList.add(media);
        mMediaAdapter.addItem(mMediaList);

        rvMedia.setAdapter(mMediaAdapter);

    }
}
