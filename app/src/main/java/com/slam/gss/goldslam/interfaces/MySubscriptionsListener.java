package com.slam.gss.goldslam.interfaces;

/**
 * Created by Thriveni on 5/22/2017.
 */

public interface MySubscriptionsListener {
    void notifySubscriptions(String str);
}
