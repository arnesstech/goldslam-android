package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.TournamentsListener;
import com.slam.gss.goldslam.ViewTournamentFragment;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/21/2017.
 */

public class TournamentInfoFragment extends DialogFragment implements View.OnClickListener {
    @BindView(R.id.close_tournament_info)
    TextView closeDialog;

    @BindView(R.id.association_name)
    TextView txtAssociationName;

    @BindView(R.id.tournament_name)
    TextView txtTournamentName;

    @BindView(R.id.venue_details)
    TextView txtVenueDetails;

    @BindView(R.id.tv_tournament_dates)
    TextView txtTournamentDates;

    @BindView(R.id.tv_endtry_deadline_date)
    TextView txtEntryDeadLineDate;

    @BindView(R.id.tv_widhdrawal_deadline_date)
    TextView txtWidthrawalDeadLine;

    @BindView(R.id.tv_entry_sent_by)
    TextView txtEntrySentBy;

    @BindView(R.id.tv_prize_money)
    TextView txtPrizzeMoney;

    @BindView(R.id.tv_hospitality)
    TextView txtHospitality;

    @BindView(R.id.tv_additional_info)
    TextView txtAddInfo;


    static String mTournamentDocId = "";

    private TournamentsListener listener;

    public static TournamentInfoFragment newInstance(String docID) {
        mTournamentDocId = docID;
        TournamentInfoFragment fragment = new TournamentInfoFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.dialog_tournament_details, container, false);
        ButterKnife.bind(this, view);
        closeDialog.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadTournamentInfo();
    }

    private void loadTournamentInfo() {
        new TournamentDetailsAsyncTask().execute(Connections.BASE_URL + "api/tournament/" + mTournamentDocId);

    }

    class TournamentDetailsAsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
          //  dialog.setMessage("Connecting, please wait");
            dialog.setTitle("Loading");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {

            try {

                SharedPreferences sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
                String apiToken = sp.getString("apiToken", "");
                Log.e("apiToken", apiToken);
                String base64EncodedApiToken = Base64.encodeToString(apiToken.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);

                HttpClient httpClient = new DefaultHttpClient();
                String url = urls[0];
                Log.i("url", url);
                HttpGet httpGet = new HttpGet(url);
                // httpPost.setEntity(stringEntity);
                httpGet.setHeader("Authorization", base64EncodedApiToken);

                httpGet.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpGet.setHeader("Authorization", "Basic " + base64EncodedApiToken);
                httpGet.setHeader("Client-Type", "APP");
                httpGet.setHeader("App-Id", "PROSPORTS");

                HttpResponse httpResponse = httpClient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                System.out.println("TournamentDetails::::" + responseBody);

                return responseBody;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.i("Event entry response", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                //Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    //Toast.makeText(LoginActivity.this,"111111",Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());

                    dialog.dismiss();
                    JSONObject mInnerJObj = obj.getJSONObject("tournament");
                    String tournamentName = mInnerJObj.getString("tournamentName");
                    String entryWithdrawDate = mInnerJObj.getString("entryWithdrawDate");
                    String hospitality = mInnerJObj.getString("hospitality");
                    String prizeMoney = mInnerJObj.getString("prizeMoney");
                    String entryEndDate = mInnerJObj.getString("entryEndDate");
                    String fromDate = mInnerJObj.getString("fromDate");
                    String associationName = mInnerJObj.getString("associationName");
                    String otherDetails = mInnerJObj.getString("otherDetails");
                    String entrySent = mInnerJObj.getString("email");
                    String toDate = mInnerJObj.getString("toDate");
                    String dates = fromDate + " to " + toDate;
                    String venueDetails = "";

                    txtAssociationName.setText(associationName);
                    txtTournamentName.setText(tournamentName);
                    txtHospitality.setText(hospitality);
                    txtPrizzeMoney.setText(prizeMoney);
                    txtEntrySentBy.setText(entrySent);
                    txtWidthrawalDeadLine.setText(entryWithdrawDate);
                    txtTournamentDates.setText(dates);
                    txtEntryDeadLineDate.setText(entryEndDate);
                    if (!(otherDetails.equalsIgnoreCase("") || otherDetails.equalsIgnoreCase(null))) {
                        txtAddInfo.setText(otherDetails);
                    } else {
                        txtAddInfo.setText("");
                    }


                    String chiefReferee = "";
                    JSONArray mArray = mInnerJObj.getJSONArray("venues");
                    //System.out.println("Response:::" + obj.getString("message") + "===" + mArray);
                    JSONArray mArray1 = mInnerJObj.getJSONArray("moderators");


                    if (mArray != null) {
                        for (int n = 0; n < mArray.length(); n++) {
                            JSONObject mDetailObj = mArray.getJSONObject(n);
                            //System.out.println("Venue details"+mDetailObj.toString());
                            String location = mDetailObj.getString("location");
                            String email = mDetailObj.getString("email");
                            String phone = mDetailObj.getString("phone");

                            String venueObj =Html.fromHtml("<font color=\"#FFFFFF\">"+ location + "</font>") + "," + email + "," + phone;

                            // venueDetails = venueDetails + "\n" + (mDetailObj.getString("name") + "," + mDetailObj.getString("location") + "," + mDetailObj.getString("email") + "," + mDetailObj.getString("phone"));

                            venueDetails = venueDetails + "\n" + venueObj;
                        }
                    } else {
                        venueDetails = "";
                    }

                    txtVenueDetails.setText(venueDetails);

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                    dialog.dismiss();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Unable to fetch data from server");
                dialog.dismiss();
            }
        }
    }

    public void setListener(TournamentsListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_tournament_info:
                dismiss();

        }
    }
}
