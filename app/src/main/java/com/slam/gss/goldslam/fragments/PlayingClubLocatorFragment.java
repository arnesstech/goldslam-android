package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.PlayingClubLocatorsAdapter;
import com.slam.gss.goldslam.adapters.UpcommingEventsAdapter;
import com.slam.gss.goldslam.models.playingClubLocators.PlayingClubLocators;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class PlayingClubLocatorFragment extends Fragment {
    @BindView(R.id.rv_player_club_locators)
    RecyclerView rvPlayerClubLocators;

    private ProgressDialog progressDialog;
    ArrayList<PlayingClubLocators> mPlayingClubLocatorsList;
    RecyclerView.LayoutManager mLayoutManager;
    PlayingClubLocatorsAdapter mPlayingClubLocatorsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playing_club_locators, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvPlayerClubLocators.setLayoutManager(mLayoutManager);
        rvPlayerClubLocators.setItemAnimator(new DefaultItemAnimator());
        rvPlayerClubLocators.setHasFixedSize(true);
        mPlayingClubLocatorsList = new ArrayList<>();
        mPlayingClubLocatorsAdapter = new PlayingClubLocatorsAdapter(getActivity());
        rvPlayerClubLocators.setAdapter(mPlayingClubLocatorsAdapter);
        loadPlayingClubLocators();

    }

    private void loadPlayingClubLocators() {
        PlayingClubLocators locators = new PlayingClubLocators();
        locators.setLocation("hyderabad");
        locators.setName("SMC FOundation");
        locators.setRating("4.2");
        mPlayingClubLocatorsList.add(locators);


        locators = new PlayingClubLocators();
        locators.setLocation("hyderabad");
        locators.setName("SMC FOundation");
        locators.setRating("4.2");
        mPlayingClubLocatorsList.add(locators);

        locators = new PlayingClubLocators();
        locators.setLocation("hyderabad");
        locators.setName("SMC FOundation");
        locators.setRating("2.2");
        mPlayingClubLocatorsList.add(locators);

        locators = new PlayingClubLocators();
        locators.setLocation("vizag");
        locators.setName("SMC FOundation");
        locators.setRating("3.2");
        mPlayingClubLocatorsList.add(locators);
        mPlayingClubLocatorsAdapter.addItem(mPlayingClubLocatorsList);
        rvPlayerClubLocators.setAdapter(mPlayingClubLocatorsAdapter);


        //mPlayingClubLocatorsAdapter.notifyDataSetChanged();

    }


}
