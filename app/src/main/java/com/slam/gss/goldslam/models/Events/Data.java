package com.slam.gss.goldslam.models.Events;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thriveni on 6/22/2017.
 */

public class Data {
    private String message;

    private List<Events> events;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Events> getEvents() {
        if (events == null) {
            return new ArrayList<>();
        }
        return events;
    }

    public void setEvents(List<Events> events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", events = " + events + "]";
    }
}
