package com.slam.gss.goldslam.models.Associations;

/**
 * Created by Thriveni on 1/10/2017.
 */
public class Data {
    private String message;

    private Associations[] associations;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Associations[] getAssociations() {
        return associations;
    }

    public void setAssociations(Associations[] associations) {
        this.associations = associations;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", associations = " + associations + "]";
    }
}
