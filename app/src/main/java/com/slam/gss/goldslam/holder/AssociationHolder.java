package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;

/**
 * Created by Thriveni on 1/10/2017.
 */
public class AssociationHolder extends AbstractViewHolder {

    public TextView txtCountryName;

    public TextView getCountryName() {
        return txtCountryName;
    }

    public AssociationHolder(View v) {
        super(v);
        txtCountryName = (TextView) v.findViewById(R.id.txtGender);
    }
}