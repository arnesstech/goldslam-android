package com.slam.gss.goldslam.models.scheduleplayers;

import java.util.List;

public class Entries {
    private String teamName;

    private String status;

    private String mainEntryMode;

    private String dateTimeOfEntry;

    private String tournamentDocId;

    private String eventDocId;

    private String meansOfEntry;

    private String draw;

    private String associationName;

    private String signin;

    private String rank;

    private List<Players> players;

    private String action;

    private String tournamentName;

    private String eventName;

    private String docId;

    private String associationDocId;

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    private String publish;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMainEntryMode() {
        return mainEntryMode;
    }

    public void setMainEntryMode(String mainEntryMode) {
        this.mainEntryMode = mainEntryMode;
    }

    public String getDateTimeOfEntry() {
        return dateTimeOfEntry;
    }

    public void setDateTimeOfEntry(String dateTimeOfEntry) {
        this.dateTimeOfEntry = dateTimeOfEntry;
    }

    public String getTournamentDocId() {
        return tournamentDocId;
    }

    public void setTournamentDocId(String tournamentDocId) {
        this.tournamentDocId = tournamentDocId;
    }

    public String getEventDocId() {
        return eventDocId;
    }

    public void setEventDocId(String eventDocId) {
        this.eventDocId = eventDocId;
    }

    public String getMeansOfEntry() {
        return meansOfEntry;
    }

    public void setMeansOfEntry(String meansOfEntry) {
        this.meansOfEntry = meansOfEntry;
    }

    public String getDraw() {
        return draw;
    }

    public void setDraw(String draw) {
        this.draw = draw;
    }

    public String getAssociationName() {
        return associationName;
    }

    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    public String getSignin() {
        return signin;
    }

    public void setSignin(String signin) {
        this.signin = signin;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public List<Players> getPlayers() {
        return players;
    }

    public void setPlayers(List<Players> players) {
        this.players = players;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getAssociationDocId() {
        return associationDocId;
    }

    public void setAssociationDocId(String associationDocId) {
        this.associationDocId = associationDocId;
    }

    @Override
    public String toString() {
        return "ClassPojo [teamName = " + teamName + ", status = " + status + ", mainEntryMode = " + mainEntryMode + ", dateTimeOfEntry = " + dateTimeOfEntry + ", tournamentDocId = " + tournamentDocId + ", eventDocId = " + eventDocId + ", meansOfEntry = " + meansOfEntry + ", draw = " + draw + ", associationName = " + associationName + ", signin = " + signin + ", rank = " + rank + ", players = " + players + ", action = " + action + ", tournamentName = " + tournamentName + ", eventName = " + eventName + ", docId = " + docId + ", associationDocId = " + associationDocId + "]";
    }
}