package com.slam.gss.goldslam.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.network.HttpHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

/**
 * Created by Thriveni on 1/12/2017.
 */
public class UpdatePasswordFragment extends Fragment implements View.OnClickListener {

    private SharedPreferences sp;
    private SharedPreferences.Editor edit;
    TextView temp_page_title_menu, temp_single_menu;
    private ImageView imFilter;
    ProgressDialog progressDialog = null;
    private EditText edOldPwd, edNewPwd, edConfirmPwd;

    private String oldPassword = "", newPasword = "";
    private TextView txtUpdatePwd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.update_password, container, false);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

     /*   Toolbar tbar = (Toolbar) view.findViewById(R.id.toolbar);
        tbar.setVisibility(View.GONE);*/
        /*hideKeyboard(getActivity(),view);*/
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText("Change Password");

        imFilter = (ImageView) getActivity().findViewById(R.id.im_filter);
        if (imFilter != null) {
            imFilter.setVisibility(View.GONE);
        }

        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.GONE);

        }
        sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
        edit = sp.edit();


        edOldPwd = (EditText) view.findViewById(R.id.old_pwd);
        edNewPwd = (EditText) view.findViewById(R.id.new_pwd);
        edConfirmPwd = (EditText) view.findViewById(R.id.confirm_password);
        txtUpdatePwd = (TextView) view.findViewById(R.id.update_pwd);

        edOldPwd.clearFocus();

        txtUpdatePwd.setOnClickListener(this);

        return view;
    }

   /* public static void hideKeyboard(Activity activity, View viewToHide) {
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewToHide.getWindowToken(), 0);
    }*/

    private void updatePwd() {
        if (!validateOldPwd()) {
            return;
        }
        if (!validateNewPwd()) {
            return;
        }
        if (!validateConfirmPwd()) {
            return;
        } else {
            oldPassword = edOldPwd.getText().toString();
            newPasword = edNewPwd.getText().toString();
            updatePassword();
        }
    }

    private boolean validateOldPwd() {
        if (TextUtils.isEmpty(edOldPwd.getText().toString())) {
            //edOldPwd.setFocusable(true);
            edOldPwd.requestFocus();
            edOldPwd.setError("Please Enter Old Password");
            return false;
        } else {
            return true;
        }
    }

    private boolean validateNewPwd() {
        if ((TextUtils.isEmpty(edNewPwd.getText().toString())) || (edNewPwd.getText().toString().length() < 6) || (!(edNewPwd.getText().toString().matches(".*\\d+.*")))) {
            edNewPwd.requestFocus();
            edNewPwd.setError("Password must be 6 characters and have one number");
            return false;
        } else {
            return true;
        }
    }

    private boolean validateConfirmPwd() {
        if (TextUtils.isEmpty(edConfirmPwd.getText().toString())) {
            edConfirmPwd.requestFocus();
            edConfirmPwd.setError("Please Enter confirm Password");
            return false;
        } else if (!(TextUtils.equals(edConfirmPwd.getText().toString(), edNewPwd.getText().toString()))) {
            edConfirmPwd.requestFocus();
            edConfirmPwd.setError("both passwords should be equal");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_pwd:
                updatePwd();
                break;
            default:
                break;
        }
    }


    private void updatePassword() {
        new UpdatePasswordAsynTask(edNewPwd.getText().toString()).execute(Connections.UPDATE_PASSWORD_URL);
    }

    class UpdatePasswordAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;
        String newPassword;

        public UpdatePasswordAsynTask(String password) {
            this.newPassword = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                URL url = new URL(urls[0]);
                HttpHelper httpHelper = new HttpHelper();

                JSONObject updatePasswordObj = new JSONObject();
                updatePasswordObj.put("oldPassword", oldPassword);
                updatePasswordObj.put("registrationDocId", sp.getString("registrationDocId", ""));
                updatePasswordObj.put("userId", sp.getString("registrationDocId", ""));
                updatePasswordObj.put("newPassword", newPassword);


                Log.e("params", updatePasswordObj.toString() + urls[0]);

                String result = httpHelper.updatePassword(urls[0], updatePasswordObj);
                return result;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            Log.i("Update Password Result", result);
            JSONObject obj = null, jsonResponse = null;
            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    obj = new JSONObject(jsonResponse.get("data").toString());
                    // Helper.showToast(getActivity(), "password changed successfully");
                    //Intent intent = new Intent(getActivity(), AppDashBoard.class);
                    //startActivity(intent);

                    try {
                        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(getActivity());


                        alert.setMessage(obj.getString("message")); //Message here


                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                String password = edNewPwd.getText().toString();

                                String userName = sp.getString("userNameVal", "");

                                String credentails = userName + ":" + password;
                                sp.edit().putString("userCredentials", credentails).commit();

                                dialog.dismiss();
                                edOldPwd.setText("");
                                edConfirmPwd.setText("");
                                edNewPwd.setText("");

                            } // End of onClick(DialogInterface dialog, int whichButton)
                        }); //End of alert.setPositiveButton

                        android.support.v7.app.AlertDialog alertDialog = alert.create();
                        alertDialog.show();

                    } catch (Exception e) {

                    }

                } else {
                    obj = new JSONObject(jsonResponse.get("error").toString());
                    System.out.println("error object" + obj.toString());
                    // Helper.showToast(getActivity(), obj.getString("message"));

                    try {
                        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(getActivity());


                        alert.setMessage(obj.getString("message")); //Message here


                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                dialog.dismiss();
                                // edOldPwd.setText("");
                                //edConfirmPwd.setText("");
                                //edNewPwd.setText("");

                            } // End of onClick(DialogInterface dialog, int whichButton)
                        }); //End of alert.setPositiveButton

                        android.support.v7.app.AlertDialog alertDialog = alert.create();
                        alertDialog.show();

                    } catch (Exception e) {

                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
                //  Helper.showToast(getActivity(), "Network Error");
            }
        }
    }


}
