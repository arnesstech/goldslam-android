package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.PlayingClubLocatorHolder;
import com.slam.gss.goldslam.holder.ShoppingItemHolder;
import com.slam.gss.goldslam.models.Shopping.ShoppingItems;
import com.slam.gss.goldslam.models.playingClubLocators.PlayingClubLocators;

import java.util.ArrayList;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class ShopingItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ShoppingItems> mShoppingItems;
    private Context mContext;

    public ShopingItemAdapter(Context mContext, ArrayList<ShoppingItems> mShoppingItems) {
        this.mContext = mContext;
        this.mShoppingItems = mShoppingItems;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vEmp = inflater.inflate(R.layout.item_shop_now, parent, false);
        viewHolder = new ShoppingItemHolder(vEmp);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ShoppingItemHolder shoppingItemHolder = (ShoppingItemHolder) holder;
        configureViewHolderFilter(shoppingItemHolder, position);
    }

    private void configureViewHolderFilter(ShoppingItemHolder holder, int position) {
        holder.getTxtItemCost().setText(mShoppingItems.get(position).getCost());
        holder.getTxtItemName().setText(mShoppingItems.get(position).getName());
        holder.getRbItemRating().setRating(Float.parseFloat(mShoppingItems.get(position).getRating()));

        //Picasso.with(mContext).load(R.mipmap.ic_launcher);//Add Image from network response based on URL

        holder.getBtnAddToCard().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        holder.getBtnItemDetails().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mShoppingItems.size();
    }
}
