package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.slam.gss.goldslam.adapters.EventsListAdapter;
import com.slam.gss.goldslam.adapters.TournamentListAdapter;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.models.Tournament;
import com.slam.gss.goldslam.models.eventadmin.EventAdminTournaments.EventAdminResponse;
import com.slam.gss.goldslam.models.eventadmin.EventAdminTournaments.Events;
import com.slam.gss.goldslam.models.eventadmin.EventsListModel;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.network.RestApi;
import com.slam.gss.goldslam.utils.ApiResponseModel;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 9/8/2016.
 */
public class EventAdminFragment extends Fragment {

    EventsListAdapter listAdapter;
    private ListView eventListView;
    private ArrayList<EventsListModel> eventsList;
    private EventsListModel event;
    private int ctrl = 0;
    private ArrayList<Tournament> tournaments;
    private Tournament tournament;
    int totalHeadersCount = 0;
    private SharedPreferences sp;
    private SharedPreferences.Editor edit;
    private TextView temp_page_title_menu, temp_single_menu;
    private ImageView imFilter;
    ProgressDialog progressDialog = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_admin_fragment, container, false);
        eventListView = (ListView) view.findViewById(R.id.event_list);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText("Events");

        imFilter = (ImageView) getActivity().findViewById(R.id.im_filter);
        if (imFilter != null) {
            imFilter.setVisibility(View.GONE);
        }

        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.INVISIBLE);
            temp_single_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Helper.showToast(getActivity(), "events");
                }
            });
        }
        eventsList = new ArrayList<EventsListModel>();
        sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
        edit = sp.edit();
        //String serverURL = "http://gss.goldslamsports.com/prosports/api/events?admindocid=" + sp.getString("registrationDocId", "");

        String serverURL = Connections.BASE_URL + "api/events?admindocid=" + sp.getString("registrationDocId", "");


        //Log.i("eventUrl", serverURL);
        new ListAsynchronousTask(getActivity()).execute(serverURL);

        return view;
    }


    /*
        For get Events
     */
    public class ListAsynchronousTask extends AsyncTask<String, String, String> {

        private Context mContext;

        public ListAsynchronousTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading, please wait");
            progressDialog.setTitle("Connecting server");
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {
            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
            try {

                URL url = new URL(params[0]);
                HttpGet httpGet = new HttpGet(String.valueOf(url));
                httpGet.addHeader("Authorization", "Basic " + sp.getString("apiToken", ""));
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                Log.e("eventdata", httpResponse.toString());
                return responseBody;

            } catch (IOException e) {
                e.printStackTrace();

                return e.getMessage().toString();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                try {
                    JSONObject joMain = new JSONObject(s);
                    if (joMain != null) {
                        JSONObject subJson = joMain.getJSONObject("data");//new JSONObject("data");

                        String str = new Gson().toJson(joMain);
                        Log.e("event admin data", str);

                        Log.i("Event ADmin data", subJson.toString());
                        if (subJson != null) {
                            JSONArray mJsonArray = subJson.getJSONArray("events");
                            //acctokenGenerated = true;
                            if (mJsonArray != null) {
                                ctrl = mJsonArray.length();
                                JSONObject minnerObject;

                                for (int i = 0; i < mJsonArray.length(); i++) {
                                    totalHeadersCount++;
                                    minnerObject = mJsonArray.getJSONObject(i);


                                    // System.out.println("Inner Object::::" + minnerObject);
                                    if (minnerObject != null) {
                                        event = new EventsListModel();
                                        event.setDocId(minnerObject.getString("docId"));
                                        event.setTournamentName(minnerObject.getString("tournamentName"));
                                        event.setEventName(minnerObject.getString("eventName"));
                                        event.setStatus(minnerObject.getString("status"));
                                        event.setAssociationName(minnerObject.getString("associationName"));
                                        event.setEventStartDate(minnerObject.getString("eventStartDate"));
                                        event.setEventEndDate(minnerObject.getString("eventEndDate"));
                                        event.setTournamentDocId(minnerObject.getString("tournamentDocId"));
                                        eventsList.add(event);
                                        Helper.eventList.add(event);

                                    }

                                }

                                filterWithDates(eventsList);
                            }


                        }
                    }


                    progressDialog.dismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Helper.showToast(getActivity(), "Network error");
                    progressDialog.dismiss();
                }
            }
        }
    }

    private void filterWithDates(ArrayList<EventsListModel> eventsList) {


        /*for (EventsListModel eventModel : eventsList) {
            Log.e("event Date before sort",eventModel.getEventStartDate());
        }*/


        Collections.sort(eventsList, new Comparator<EventsListModel>() {
            @Override
            public int compare(EventsListModel lhs, EventsListModel rhs) {
                try {

                    return Utils.convertLiveDateToMillis1(rhs.getEventStartDate()).compareTo(Utils.convertLiveDateToMillis1(lhs.getEventStartDate()));

                } catch (Exception e) {
                    Log.e("Exception ",e.getMessage());
                    return 0;
                }

            }
        });


        listAdapter = new EventsListAdapter(getActivity(), eventsList);
        eventListView.setAdapter(listAdapter);

    }
}
