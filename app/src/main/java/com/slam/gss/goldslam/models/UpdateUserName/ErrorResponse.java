package com.slam.gss.goldslam.models.UpdateUserName;

/**
 * Created by Thriveni on 1/11/2017.
 */
public class ErrorResponse {
    private Error error;

    private String status;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [error = " + error + ", status = " + status + "]";
    }
}
