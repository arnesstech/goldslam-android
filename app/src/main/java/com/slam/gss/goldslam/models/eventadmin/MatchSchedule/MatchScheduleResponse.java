package com.slam.gss.goldslam.models.eventadmin.MatchSchedule;

/**
 * Created by Thriveni on 9/13/2016.
 */
public class MatchScheduleResponse {

    private String status;

    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", data = " + data + "]";
    }
}
