package com.slam.gss.goldslam.models.eventadmin;

/**
 * Created by Thriveni on 9/12/2016.
 */
public class MasterData {
    private String key;

    private MatchConditionOptions[] options;

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public MatchConditionOptions[] getOptions ()
    {
        return options;
    }

    public void setOptions (MatchConditionOptions[] options)
    {
        this.options = options;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [key = "+key+", options = "+options+"]";
    }
}
