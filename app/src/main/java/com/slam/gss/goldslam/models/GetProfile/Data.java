package com.slam.gss.goldslam.models.GetProfile;

/**
 * Created by Thriveni on 1/11/2017.
 */
public class Data {
    private String message;

    private Profile profile;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Profile getProfile ()
    {
        return profile;
    }

    public void setProfile (Profile profile)
    {
        this.profile = profile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", profile = "+profile+"]";
    }
}
