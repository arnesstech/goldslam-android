package com.slam.gss.goldslam.models.Tournaments;

/**
 * Created by Thriveni on 6/22/2017.
 */

public class Admins {
    private String email;

    private String name;

    private String docId;

    private String mobile;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "ClassPojo [email = " + email + ", name = " + name + ", docId = " + docId + ", mobile = " + mobile + "]";
    }
}
