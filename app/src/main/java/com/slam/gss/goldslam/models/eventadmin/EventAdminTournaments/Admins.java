package com.slam.gss.goldslam.models.eventadmin.EventAdminTournaments;

/**
 * Created by Thriveni on 3/1/2017.
 */
public class Admins {
    private String gssid;

    private String email;

    private String name;

    private String userName;

    private String docId;

    private String editable;

    private String isadmin;

    private String mobile;

    public String getGssid ()
    {
        return gssid;
    }

    public void setGssid (String gssid)
    {
        this.gssid = gssid;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUserName ()
    {
        return userName;
    }

    public void setUserName (String userName)
    {
        this.userName = userName;
    }

    public String getDocId ()
    {
        return docId;
    }

    public void setDocId (String docId)
    {
        this.docId = docId;
    }

    public String getEditable ()
    {
        return editable;
    }

    public void setEditable (String editable)
    {
        this.editable = editable;
    }

    public String getIsadmin ()
    {
        return isadmin;
    }

    public void setIsadmin (String isadmin)
    {
        this.isadmin = isadmin;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [gssid = "+gssid+", email = "+email+", name = "+name+", userName = "+userName+", docId = "+docId+", editable = "+editable+", isadmin = "+isadmin+", mobile = "+mobile+"]";
    }
}
