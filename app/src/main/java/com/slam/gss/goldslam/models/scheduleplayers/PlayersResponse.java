package com.slam.gss.goldslam.models.scheduleplayers;

/**
 * Created by Ramesh on 20/8/16.
 */
public class PlayersResponse {
    private String status;

    private PlayersData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PlayersData getData() {
        return data;
    }

    public void setData(PlayersData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", data = " + data + "]";
    }
}
