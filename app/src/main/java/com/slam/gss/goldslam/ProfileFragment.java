package com.slam.gss.goldslam;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.slam.gss.goldslam.CustomViews.MultiSelectionSpinner;
import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.adapters.AssociationsAdapter1;
import com.slam.gss.goldslam.adapters.SpinnerAdapter;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.holder.AssociationsHolder;
import com.slam.gss.goldslam.holder.UserAssociationsHolder;
import com.slam.gss.goldslam.models.Associations.AssociationList;
import com.slam.gss.goldslam.models.Associations.Associations;
import com.slam.gss.goldslam.models.Associations.UserAssociations;
import com.slam.gss.goldslam.models.GetProfile.GetProfileResponse;
import com.slam.gss.goldslam.models.GetProfile.Profile;
import com.slam.gss.goldslam.models.Login.Data;
import com.slam.gss.goldslam.models.Login.LoginResponse;
import com.slam.gss.goldslam.models.MasterData.Country;
import com.slam.gss.goldslam.models.UpdateProfile.UserAssociationList;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.Teams;
import com.slam.gss.goldslam.network.RestApi;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 8/21/2016.
 */
public class ProfileFragment extends Fragment implements MultiSelectionSpinner.OnMultipleItemsSelectedListener {

    private View view = null;
    private SharedPreferences preference = null;
    private SharedPreferences.Editor edit = null;
    private EditText edFirstName, edLastName, edEmail, edMobile, edDob, edFatherName, edMotherName, edLocation, edOccupation, edQualification, edPreferredSports, edSchoolName, edClassLevel, edAistaNo, edAitaNo, edStateCode;
    private String firstName = "", lastName = "", email = "", mobile = "", registerType = "Self", dob = "", gender = "", fatherName = "", motherName = "", occupation = "", address = "", qualification = "", schoolName = "", classLevel = "", aistaRegistrationNo = "", aitaRegistrationNo = "", stateCode = "";

    private LinearLayout edit_profile_laypout;
    private LinearLayout association_container, association_container1;
    private Calendar myCalendar;
    private DatePickerDialog fromDatePickerDialog;
    private Spinner genderSpinner, stateSpinner, spCountry;
    private ArrayList<String> genderList;
    private ArrayList<String> prefferedSports = new ArrayList<String>();
    private ArrayList<String> statesList = new ArrayList<>();
    private ArrayList<String> stateCodesList = new ArrayList<>();
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayList<String> sportTypes = new ArrayList<>();
    private ArrayList<String> selectedSportsTypes = new ArrayList<>();
    private ArrayList<Associations> AssociationsList = new ArrayList<>();
    private ArrayList<String> UserAssociationList = new ArrayList<>();
    List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> listt = new ArrayList<>();
    private LinearLayout userAssociationLayout;
    private LinearLayout stateLayout, llAssociationHeaders;
    private TextView txtNoAssociations;
    @Deprecated
    private ListView lvUserSelectedAssociations, lvAssociData;
    private Button btnAddAssociation;
    UserAssociationsAdapter adapter;
    UserAssociationsAdapter1 adapter1;
    private TextView updateProfileTextView, txtNoAssociaitionsMsg;
    TextView temp_single_menu, temp_page_title_menu;
    private ImageView imFilter;
    private String country = "", selectedSoprtTypesValues = "";
    private MultiSelectionSpinner preferredSports;
    private Associations associations;
    private SpinnerAdapter associationAdapter;
    private LinearLayout association_msg;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.edit_profile, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);

        imFilter = (ImageView) getActivity().findViewById(R.id.im_filter);
        if (imFilter != null) {
            imFilter.setVisibility(View.GONE);
        }

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.VISIBLE);
            temp_single_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateProfile();
                }
            });
        }
        temp_page_title_menu.setText("Profile");


        preference = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
        edit = preference.edit();
        initializeViews();

        getStates();
        getCountries();
        getSportsTypes();
        getAssociations();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    getProfileData();
                } catch (Exception e) {

                }

            }
        }, 1000);


        return view;
    }

    private void initializeViews() {

        myCalendar = Calendar.getInstance();


        edFirstName = (EditText) view.findViewById(R.id.p_firstname);
        edLastName = (EditText) view.findViewById(R.id.p_lastname);
        edEmail = (EditText) view.findViewById(R.id.p_email);
        edEmail.addTextChangedListener(new MyTextWatcher(edEmail));
        edMobile = (EditText) view.findViewById(R.id.p_mobile);
        edDob = (EditText) view.findViewById(R.id.p_dob);
        edFatherName = (EditText) view.findViewById(R.id.p_fatherName);
        edMotherName = (EditText) view.findViewById(R.id.p_motherName);
        edLocation = (EditText) view.findViewById(R.id.p_location);
        edQualification = (EditText) view.findViewById(R.id.p_qualification);

        /*
        State List
         */
        stateLayout = (LinearLayout) view.findViewById(R.id.state_layout);
        stateSpinner = (Spinner) view.findViewById(R.id.p_stateCode);
        stateSpinner.setPrompt("Select State");
        stateSpinner.setAdapter(new SpinnerAdapter(getActivity(), R.layout.spinner_text, statesList));
        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                stateCode = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*
        Gender List
         */
        genderSpinner = (Spinner) view.findViewById(R.id.p_gender);
        genderList = new ArrayList<>();
        genderList.add("Male");
        genderList.add("Female");
        genderSpinner.setPrompt("Select Gender");
        genderSpinner.setAdapter(new SpinnerAdapter(getActivity(), R.layout.spinner_text, genderList));
        genderSpinner.setEnabled(false);
        genderSpinner.setFocusable(false);
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                gender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*
        Country list
         */
        spCountry = (Spinner) view.findViewById(R.id.sp_country);
        spCountry.setAdapter(new SpinnerAdapter(getActivity(), R.layout.spinner_text, countryList));
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                country = parent.getItemAtPosition(position).toString();
                if (country.equalsIgnoreCase("India")) {
                    stateLayout.setVisibility(View.VISIBLE);
                } else {
                    stateLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        edDob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Helper.showToast(getActivity(), "date picker");
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        edit_profile_laypout = (LinearLayout) view.findViewById(R.id.edit_profile_layout);
        association_container = (LinearLayout) view.findViewById(R.id.association_container);
        association_container1 = (LinearLayout) view.findViewById(R.id.association_container1);

        userAssociationLayout = (LinearLayout) view.findViewById(R.id.user_association_layout);
        txtNoAssociations = (TextView) view.findViewById(R.id.txt_no_associations);
        lvUserSelectedAssociations = (ListView) view.findViewById(R.id.lv_user_association_data);
        llAssociationHeaders = (LinearLayout) view.findViewById(R.id.ll_association_headers);
        llAssociationHeaders.setVisibility(View.GONE);

        updateProfileTextView = (TextView) view.findViewById(R.id.p_update);
        updateProfileTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    updateProfileData();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });


        //preferred sports multi selcetion spinner
        preferredSports = (MultiSelectionSpinner) view.findViewById(R.id.msp_preferred_sports);


        //Add UserAssociation
        adapter = new UserAssociationsAdapter(getActivity());
        adapter1 = new UserAssociationsAdapter1(getActivity());
        btnAddAssociation = (Button) view.findViewById(R.id.btn_add_association);
        btnAddAssociation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.slam.gss.goldslam.models.UpdateProfile.UserAssociations obj = new com.slam.gss.goldslam.models.UpdateProfile.UserAssociations();
                obj.setAssociationid("");
                obj.setAssociationName("");
                obj.setRegistrationId("");
                listt.add(obj);
//                lvUserSelectedAssociations.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
                //
                addNewAssociation();
            }
        });


        association_msg = (LinearLayout) view.findViewById(R.id.association_msg_layout);
        association_msg.setVisibility(View.VISIBLE);

        txtNoAssociaitionsMsg = (TextView) view.findViewById(R.id.txt_no_associations_data);
        lvAssociData = (ListView) view.findViewById(R.id.lv_user_asso_data);
    }


    private boolean validateEmail() {
        if (edEmail.getText().toString().trim().isEmpty()) {
            //edEmail.setError(getString(R.string.err_msg_email));
            // edEmail.requestFocus();
            return true;
        } else {
            Pattern pattern;
            Matcher matcher;
            final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            pattern = Pattern.compile(EMAIL_PATTERN);
            matcher = pattern.matcher(edEmail.getText().toString());
            if (matcher.matches()) {
                Log.i("matcher value", String.valueOf(matcher.matches()));
                return true;

            } else {
                Log.i("matcher value", String.valueOf(matcher.matches()));
                edEmail.setError("In Valid Email");
                edEmail.requestFocus();
                return false;
            }


        }

        // return true;
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.p_email:
                    validateEmail();
                    break;

                default:
                    break;
            }
        }
    }

    private void getAssociations() {
        Call<AssociationList> getAssociations = RestApi.get()
                .getRestService()
                .getAssociations();

        getAssociations.enqueue(new Callback<AssociationList>() {
            @Override
            public void onResponse(Call<AssociationList> call, Response<AssociationList> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null && response.body().getData().getAssociations() != null) {
                        String obj = new Gson().toJson(response.body());
                        associations = new Gson().fromJson(obj, Associations.class);

                        if (response.body().getData().getAssociations().length > 0) {
//                            AssociationsList.add("Select Associations");
                            for (int i = 0; i < response.body().getData().getAssociations().length; i++) {
                                AssociationsList.add(response.body().getData().getAssociations()[i]);
                            }
                        }

                    }
//                    associationAdapter = new SpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, AssociationsList);

//                    System.out.println("associations" + AssociationsList.size());
                } else {
                    //
                    //  Log.e("getAssociaions", "not successfull");

                }
            }

            @Override
            public void onFailure(Call<AssociationList> call, Throwable t) {

            }
        });
    }

    private void getSportsTypes() {
        Call<Country> getSportTypes = RestApi.get()
                .getRestService()
                .getSportTypes();

        getSportTypes.enqueue(new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {

                        if (response.body().getData().getMasterData().getOptions().length > 0) {
                            for (int i = 0; i < response.body().getData().getMasterData().getOptions().length; i++) {
                                sportTypes.add(response.body().getData().getMasterData().getOptions()[i].getValue());
                            }
                            preferredSports.setItems(sportTypes);
                            preferredSports.setListener(ProfileFragment.this);

                        } else {
                            sportTypes = new ArrayList<String>();
                        }

                        System.out.println("sports types list::::" + sportTypes.size());

                    }

                } else {
                    //  Log.e("getCountries", "not successfull");
                }
            }

            @Override
            public void onFailure(Call<Country> call, Throwable t) {

            }
        });
    }

    private void getCountries() {
        Call<Country> getCountries = RestApi.get()
                .getRestService()
                .getCountries();

        getCountries.enqueue(new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {

                        if (response.body().getData().getMasterData().getOptions().length > 0) {
                            countryList.add("select country");
                            for (int i = 0; i < response.body().getData().getMasterData().getOptions().length; i++) {
                                countryList.add(response.body().getData().getMasterData().getOptions()[i].getName());
                            }
                        } else {
                        }
                        SpinnerAdapter adapter = new SpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
                        spCountry.setAdapter(adapter);

                        System.out.println("countr list lenth::" + countryList.size());

                    }

                } else {
                    //  Log.e("getCountries", "not successfull");
                }
            }

            @Override
            public void onFailure(Call<Country> call, Throwable t) {

            }
        });
    }

    private void getProfileData() throws IOException, JSONException {
        new GetProfilesyncTask().execute(Connections.GET_PROFILE);
    }

    public static class UserAssociations {
        public String getUserAssociatonName() {
            return userAssociatonName;
        }

        public void setUserAssociatonName(String userAssociatonName) {
            this.userAssociatonName = userAssociatonName;
        }

        public String getUserRegistrationNo() {
            return userRegistrationNo;
        }

        public void setUserRegistrationNo(String userRegistrationNo) {
            this.userRegistrationNo = userRegistrationNo;
        }

        public String userAssociatonName, userRegistrationNo;


    }

    class GetProfilesyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            Log.i("profile url", urls[0]);
            Log.i("reg id ", preference.getString("registrationDocId", ""));
            try {

                String docId = preference.getString("registrationDocId", "");//"57ab68e50fff53405e09f1f1";
                String apiToken = preference.getString("apiToken", "");
                // Log.i("ap values--->", docId + apiToken);
                String url = urls[0] + "/" + docId;

                //Log.i("get profile url", url);
                HttpGet httpGet = new HttpGet(url);

                // Log.e("credentials", apiToken);
                String base64EncodedapiToken = Base64.encodeToString(apiToken.getBytes(), Base64.NO_WRAP);
                httpGet.addHeader("Authorization", "Basic " + base64EncodedapiToken);

                //Log.i("header", "Basic " + base64EncodedapiToken);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonResponse = new JSONObject(responseBody);
                Log.i("getProfile Reponse", jsonResponse.toString());


                return responseBody.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                // Log.i("getProfle", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                //  Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    //Toast.makeText(LoginActivity.this,"111111",Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());


                    //Append profile data to corresponding fields
                    JSONObject profile = new JSONObject(obj.getString("profile"));
                    edFirstName.setText(profile.getString("firstName").toString());
                    edLastName.setText(profile.getString("lastName").toString());
                    edEmail.setText(profile.getString("email").toString());
                    edMobile.setText(profile.getString("mobile").toString());
                    edDob.setText(profile.getString("dob").toString());

                    if (!(TextUtils.isEmpty(profile.getString("fatherName")))) {
                        edFatherName.setText(profile.getString("fatherName"));
                    } else {
                        edFatherName.setText(profile.getString("fatherName"));
                    }
                    if (!(TextUtils.isEmpty(profile.getString("motherName")))) {
                        edMotherName.setText(profile.getString("motherName"));
                    } else {
                        edMotherName.setText(profile.getString("motherName"));
                    }
                    if (!(TextUtils.isEmpty(profile.getString("address")))) {
                        edLocation.setText(profile.getString("address"));
                    } else {
                        edLocation.setText(profile.getString("address"));
                    }
                    if (!(TextUtils.isEmpty(profile.getString("qualification")))) {
                        edQualification.setText(profile.getString("qualification"));
                    } else {
                        edQualification.setText(profile.getString("qualification"));
                    }


                    int position = 0;
                    if ("Male".equalsIgnoreCase(profile.getString("gender").toString())) {
                        position = 0;
                    } else {
                        position = 1;
                    }

                    genderSpinner.setSelection(position);

                    /*
                    states section
                     */

                    // Log.e("user stateCode", profile.getString("stateCode"));
                    // Log.e("user Country", profile.getString("country"));


                    /*if (profile.getString("country").equalsIgnoreCase("India")) {
                        final String userStateCode = profile.getString("stateCode");

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getStates();

                                if (stateCodesList.size() > 0) {
                                    Log.e("tri", "true");
                                    int statePosition = stateCodesList.indexOf(userStateCode);
                                    stateSpinner.setSelection(statePosition);
                                } else {
                                    Log.e("tri", "false");
                                }
                            }
                        }, 1000);
                    }*/

                    String userStateCode = profile.getString("stateCode");

                    int statePosition = stateCodesList.indexOf(userStateCode);
                    stateSpinner.setSelection(statePosition);


                    if (profile.getString("country").equalsIgnoreCase("")) {
                        spCountry.setSelection(0);
                    } else {
                        //Log.e("country", profile.getString("country"));
                        System.out.println("Country index" + countryList.indexOf(profile.getString("country")));
                        spCountry.setSelection(countryList.indexOf(profile.getString("country")));
                    }


                    if (profile.has("prefferedSports")) {
                        // Log.i("ProfileFragment", "prefreed sports");
                        JSONArray jsonArray = new JSONArray(profile.getString("prefferedSports"));

                        // Log.e("sports size ", "" + sportTypes.size());
                        int ii[] = new int[sportTypes.size()];
                        Log.e("sports types size", sportTypes.size() + "");

                        if (jsonArray.length() > 0 && sportTypes.size() > 0) {
                            selectedSportsTypes = new ArrayList<String>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Log.e("pre sports", jsonArray.get(i).toString());
                                selectedSportsTypes.add(jsonArray.get(i).toString());
                                ii[i] = i;

                            }
                            preferredSports.setSelection(ii);
                        } else {
                            ii = new int[]{};
                            preferredSports.setSelection(ii);
                        }
                    } else {
                        int[] ii = new int[]{};
                        preferredSports.setSelection(ii);
                    }


                    //getUser Associations
                    JSONArray associationsArray = new JSONArray(profile.getString("userAssociations"));

                    listt.clear();
                    if (associationsArray.length() > 0) {
                        txtNoAssociations.setVisibility(View.GONE);
                        // lvAssociData.setVisibility(View.VISIBLE);
                        association_container1.setVisibility(View.VISIBLE);
                        // txtNoAssociaitionsMsg.setVisibility(View.GONE);
                        txtNoAssociaitionsMsg.setText("User Associations");
                        txtNoAssociaitionsMsg.setTextSize(20);
                        llAssociationHeaders.setVisibility(View.VISIBLE);

                        System.out.println("user associations" + profile.getString("userAssociations"));

                        for (int i = 0; i < associationsArray.length(); i++) {
                            JSONObject assocJsonObj = associationsArray.getJSONObject(i);
                            com.slam.gss.goldslam.models.UpdateProfile.UserAssociations assocObj = new com.slam.gss.goldslam.models.UpdateProfile.UserAssociations();
                            assocObj.setAssociationid(assocJsonObj.getString("associationid"));
                            assocObj.setAssociationName(assocJsonObj.getString("associationName"));
                            assocObj.setRegistrationId(assocJsonObj.getString("registrationId") == "null" ? " " : assocJsonObj.getString("registrationId"));
                            listt.add(assocObj);
                        }
                        adapter.addItems(listt);
                        adapter1.addItems(listt);
                        //listt.clear();
                        lvUserSelectedAssociations.setAdapter(adapter);
                        lvAssociData.setAdapter(adapter1);
                        populateAllItems(listt);
                        populateUserAssoData(listt);

                    } else {
                        txtNoAssociations.setVisibility(View.VISIBLE);
                        lvUserSelectedAssociations.setVisibility(View.GONE);
                        lvAssociData.setVisibility(View.GONE);
                        association_container1.setVisibility(View.GONE);
                        txtNoAssociaitionsMsg.setVisibility(View.VISIBLE);
                        populateAllItems(new ArrayList<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations>());
                    }


                    if (obj.has("profile")) {
                        //try {
                        GetProfileResponse responseObj = new GetProfileResponse();
                        //responseObj.setData(responseObj.getData());
                        com.slam.gss.goldslam.models.GetProfile.Data profileData = new com.slam.gss.goldslam.models.GetProfile.Data();

                        profileData.setMessage(obj.getString("message"));

                        Profile profileObj = new Profile();
                        profileObj.setDocId(profile.getString("docId"));
                        profileObj.setName(profile.getString("name").toString());
                        profileObj.setFirstName(profile.getString("firstName").toString());
                        profileObj.setLastName(profile.getString("lastName").toString());
                        profileObj.setUserName(profile.getString("userName"));
                        profileObj.setEmail(profile.getString("email").toString());
                        profileObj.setMobile(profile.getString("mobile").toString());
                        profileObj.setStatus(profile.getString("status"));
                        profileObj.setRegisterType(profile.getString("registerType"));
                        profileObj.setDob(profile.getString("dob").toString());
                        profileObj.setGender(profile.getString("gender").toString());
                        profileObj.setTeamname(profile.getString("teamname"));
                        profileObj.setFatherName(profile.getString("fatherName").toString());
                        profileObj.setMotherName(profile.getString("motherName").toString());
                        profileObj.setOccupation(profile.getString("occupation"));
                        profileObj.setAddress(profile.getString("address").toString());
                        profileObj.setQualification(profile.getString("qualification"));
                        JSONArray jsonArray = new JSONArray(profile.getString("prefferedSports"));
                        String[] sports;
                        if (jsonArray.length() > 0) {
                            sports = new String[jsonArray.length()];
                            for (int i = 0; i < jsonArray.length(); i++) {
                                sports[i] = jsonArray.get(i).toString();
                            }
                        } else {
                            sports = new String[]{};
                        }
                        profileObj.setPrefferedSports(sports);
                        if(profile.has("profileImage")){
                            if(!TextUtils.isEmpty(profile.getString("profileImage"))){
                                preference.edit().putString("profilePicId",profile.getString("profileImage")).commit();
                                profileObj.setProfileImage(profile.getString("profileImage"));
                            }else{
                                profileObj.setProfileImage("");
                            }
                        }

                        profileObj.setSchoolName(profile.getString("schoolName"));
                        profileObj.setClassLevel(profile.getString("classLevel"));
                        profileObj.setStateCode(profile.getString("stateCode"));


                        JSONArray jsonArray1 = new JSONArray(profile.getString("userAssociations"));
                        com.slam.gss.goldslam.models.UpdateProfile.UserAssociations[] userAssociations;

                        ArrayList<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> userAss;
                        if (jsonArray1.length() > 0) {
                            userAssociations = new com.slam.gss.goldslam.models.UpdateProfile.UserAssociations[jsonArray1.length()];
                            userAss = new ArrayList<>();
                            for (int i = 0; i < jsonArray1.length(); i++) {
                                com.slam.gss.goldslam.models.UpdateProfile.UserAssociations associations = new com.slam.gss.goldslam.models.UpdateProfile.UserAssociations();
                                JSONObject objj = new JSONObject(jsonArray1.get(i).toString());
                                associations.setAssociationid(objj.getString("associationid"));
                                associations.setAssociationName(objj.getString("associationName"));
                                if (!objj.getString("registrationId").equalsIgnoreCase("null")) {
                                    associations.setRegistrationId(objj.getString("registrationId"));
                                } else {
                                    associations.setRegistrationId("");
                                }
                                userAss.add(associations);
                                Log.e("userAssociations", jsonArray1.get(i).toString());

                                userAssociations[i] = associations;
                            }

                        } else {
                            userAssociations = new com.slam.gss.goldslam.models.UpdateProfile.UserAssociations[]{};
                        }
                        profileObj.setUserAssociations(userAssociations);
                        profileObj.setGssid(profile.getString("gssid"));
                        profileObj.setCountry(profile.getString("country"));
                        profileObj.setPincode(profile.getString("pincode"));
                        // Log.e("profile data", profileObj.toString());

                        profileData.setProfile(profileObj);
                        responseObj.setStatus("200");
                        responseObj.setData(profileData);

                        String profileRes = new Gson().toJson(responseObj);

                        // Log.e("profile data", profileRes);
                        preference.edit().putString("userProfileObj", profileRes).commit();

                        /*} catch (Exception e) {

                        }*/


                    }


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));

                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Network Problem");
            }


        }
    }

    private void updateProfileData() throws ParseException {

        if (!validateEmail()) {
            return;
        }

        if (TextUtils.isEmpty(edFirstName.getText().toString())) {
            edFirstName.setError("Please enter first name");
            edFirstName.requestFocus();
        } else if ((TextUtils.isEmpty(edMobile.getText().toString()))) {
            edMobile.setError("Email or Mobile is mandatory to recover password incase if you forget");
            edMobile.requestFocus();
        } else if ((edMobile.getText().toString()).length() < 10) {
            edMobile.setError("Please enter valid mobile no");
            edMobile.requestFocus();
        } else if (TextUtils.isEmpty(edLocation.getText().toString())) {
            edLocation.setError("Please enter location");
            edLocation.requestFocus();
        } else if (country.equalsIgnoreCase("select country")) {
            Toast.makeText(getActivity(), "please enter country", Toast.LENGTH_SHORT).show();

        } else if (!validateEmail()) {
            return;
        } else {
            firstName = edFirstName.getText().toString();
            lastName = edLastName.getText().toString();
            email = edEmail.getText().toString();
            mobile = edMobile.getText().toString();
            dob = edDob.getText().toString();
            address = edLocation.getText().toString();
            fatherName = edFatherName.getText().toString();
            motherName = edMotherName.getText().toString();
            qualification = edQualification.getText().toString();
//            aistaRegistrationNo = edAistaNo.getText().toString();
  //          aitaRegistrationNo = edAitaNo.getText().toString();
            if (stateCode.equalsIgnoreCase("Select State")) {
                stateCode = "";
            }
            if (country.equalsIgnoreCase("India")) {

            } else {
                stateCode = "";
            }
            List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> lsit = getAllData();
            if (lsit.size() > 0) {
                for (int i = 0; i < lsit.size(); i++) {
                    /*if (TextUtils.isEmpty(lsit.get(i).getRegistrationId())) {
                        Helper.showToast(getActivity(), "Enter registration id.");
                        return;
                    }*/
                }
            }
            new UpdateProfileSyncTask().execute(Connections.UPDATE_PROFILE);

        }
    }


    class UpdateProfileSyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            JSONArray jsonArray = new JSONArray();
            if (selectedSportsTypes.size() > 0) {
                for (int i = 0; i < selectedSportsTypes.size(); i++) {
                    jsonArray.put(selectedSportsTypes.get(i));
                    //  Log.e("selected pref sp type", selectedSportsTypes.get(i));
                }
            } else {

            }
            List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> lsit = getAllData();
            JSONArray userProfileArray = new JSONArray();

            Log.i("list length", lsit.size() + "");

            if (lsit.size() > 0) {
                com.slam.gss.goldslam.models.Login.UserAssociations[] arr = new com.slam.gss.goldslam.models.Login.UserAssociations[lsit.size()];
                for (int i = 0; i < lsit.size(); i++) {
                    JSONObject obj = new JSONObject();
                    try {
                        com.slam.gss.goldslam.models.Login.UserAssociations ass = new com.slam.gss.goldslam.models.Login.UserAssociations();
                        obj.put("associationName", lsit.get(i).getAssociationName());
                        obj.put("registrationId", lsit.get(i).getRegistrationId());
                        obj.put("associationid", lsit.get(i).getAssociationid());

                        ass.setAssociationName(lsit.get(i).getAssociationName());
                        ass.setRegistrationId(lsit.get(i).getRegistrationId());
                        ass.setAssociationid(lsit.get(i).getAssociationid());

                        arr[i] = ass;
                        Log.i("data", arr[i].getAssociationName());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    userProfileArray.put(obj);
                }

                LoginResponse loginData = new Gson().fromJson(preference.getString("loginResponse", ""), LoginResponse.class);

                Data data = loginData.getData();
                //Data data = new Data();
                data.setUserAssociations(new com.slam.gss.goldslam.models.Login.UserAssociations[0]);
                data.setUserAssociations(arr);
                loginData.setData(data);

                Log.i("userAss Data length", loginData.getData().getUserAssociations().length + "");

                if (loginData.getData().getUserAssociations().length > 0) {
                    for (int i = 0; i < loginData.getData().getUserAssociations().length; i++) {
                        Log.i("added asso", loginData.getData().getUserAssociations()[i].getAssociationName());
                    }

                }
                String str = new Gson().toJson(loginData);
                preference.edit().putString("loginResponse", str).commit();

            } else {
                LoginResponse loginData = new Gson().fromJson(preference.getString("loginResponse", ""), LoginResponse.class);

                Data data = loginData.getData();

                data.setUserAssociations(new com.slam.gss.goldslam.models.Login.UserAssociations[0]);

                loginData.setData(data);

                String str = new Gson().toJson(loginData);

                preference.edit().putString("loginResponse", str).commit();

            }

            try {

                JSONObject updateProfile = new JSONObject();
                updateProfile.put("docId", preference.getString("registrationDocId", ""));
                updateProfile.put("firstName", firstName);
                updateProfile.put("lastName", lastName);
                updateProfile.put("mobile", mobile);
                updateProfile.put("email", email);
                updateProfile.put("dob", dob);
                updateProfile.put("gender", gender);
                updateProfile.put("registerType", "self");
                updateProfile.put("fatherName", fatherName);
                updateProfile.put("motherName", motherName);
                updateProfile.put("occupation", occupation);
                updateProfile.put("address", address);
                updateProfile.put("qualification", qualification);
                updateProfile.put("schoolName", schoolName);
                updateProfile.put("classLevel", classLevel);
                updateProfile.put("aistaRegistrationNo", aistaRegistrationNo);
                updateProfile.put("aitaRegistrationNo", aitaRegistrationNo);
                updateProfile.put("country", country);
                updateProfile.put("prefferedSports", jsonArray);
                // Log.e("preferred sports", selectedSoprtTypesValues);
                updateProfile.put("gssid", preference.getString("gssid", ""));
                updateProfile.put("userAssociations", userProfileArray);

                if (!TextUtils.isEmpty(preference.getString("profilePicId", ""))) {
                    updateProfile.put("profileImage", preference.getString("profilePicId", ""));
                } else {
                    updateProfile.put("profileImage", "");
                }

                if (!TextUtils.isEmpty(stateCode)) {
                    int stateIndex = statesList.indexOf(stateCode);
                    updateProfile.put("stateCode", String.valueOf(stateCodesList.get(stateIndex)));
                    System.out.println("selected state is" + String.valueOf(stateCodesList.get(stateIndex)));
                } else {
                    updateProfile.put("stateCode", "");
                }


                //  Log.e("profile url is:", urls[0]);
                System.out.println("updateProfileObject->" + updateProfile.toString());

                String apiToken = preference.getString("apiToken", "");
                String base64EncodedApiToken = Base64.encodeToString(apiToken.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
                HttpClient httpClient = new DefaultHttpClient();
                StringEntity stringEntity = new StringEntity(updateProfile.toString());
                String url = urls[0] + "/" + preference.getString("registrationDocId", "");
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Authorization", "Basic" + apiToken);
                httpPost.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpPost.setHeader("Authorization", "Basic " + base64EncodedApiToken);
                httpPost.setEntity(new StringEntity(updateProfile.toString()));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody;

            } catch (Exception e) {
                return new String("Exception: " + "Unable to fetch data from server");
            }

        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            JSONObject jsonResponse = null;
            String status = null;

            try {
                jsonResponse = new JSONObject(result);
                Log.i("updateProfileReponse", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {

                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                    edit_profile_laypout.setVisibility(View.GONE);

                    edFirstName.setEnabled(false);
                    edLastName.setEnabled(false);
                    edMobile.setEnabled(false);
                    edEmail.setEnabled(false);
                    edDob.setEnabled(false);
                    genderSpinner.setEnabled(false);
                    genderSpinner.setFocusable(false);
                    temp_single_menu.setVisibility(View.VISIBLE);
                    association_msg.setVisibility(View.VISIBLE);
                    llAssociationHeaders.setVisibility(View.GONE);
                    txtNoAssociaitionsMsg.setText("No registration number stored in your profile");
                    txtNoAssociaitionsMsg.setTextSize(16);

                    edit.putString("userName", edFirstName.getText().toString());
                    edit.commit();

                    try {
                        getProfileData();
                    } catch (Exception e) {

                    }

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Cannot fetch data from server");
            }

        }
    }


    @Override
    public void selectedIndices(List<Integer> indices) {

    }

    @Override
    public void selectedStrings(List<String> strings) {
        selectedSportsTypes = new ArrayList<String>();
        selectedSportsTypes.addAll(strings);

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void updateLabel() {
        String myFormat = "dd-MMM-yyyy";//"MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edDob.setText(sdf.format(myCalendar.getTime()));
    }


    public void getStates() {
        new GetStatesAsynTask().execute(Connections.BASE_URL + "api/masterdata/STATES");
    }

    class GetStatesAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            // dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {

            try {
                String apiToken = preference.getString("apiToken", "");
                HttpGet httpGet = new HttpGet(urls[0]);

                // Log.e("credentials", apiToken);
                String base64EncodedapiToken = Base64.encodeToString(apiToken.getBytes(), Base64.NO_WRAP);
                httpGet.addHeader("Authorization", "Basic " + base64EncodedapiToken);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {

                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    //Helper.showToast(getActivity(), obj.getString("message"));

                    JSONObject masterDataObj = new JSONObject(obj.get("masterData").toString());
                    JSONArray states = masterDataObj.getJSONArray("options");

                    statesList.clear();
                    stateCodesList.clear();
                    for (int i = 0; i < states.length(); i++) {
                        JSONObject userObj = states.getJSONObject(i);
                        String name = userObj.getString("name");
                        String stateCode = userObj.getString("value");
                        statesList.add(name);
                        stateCodesList.add(stateCode);
                    }

                    stateSpinner.setAdapter(new SpinnerAdapter(getActivity(), R.layout.spinner_text, statesList));
                    new SpinnerAdapter(getActivity(), R.layout.spinner_text, statesList).notifyDataSetChanged();
                    // Log.e("sportsList length", stateCodesList.size() + "");


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));

                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Error in connection");
            }


        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.edit_profile) {
            updateProfile();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateProfile() {
        edFirstName.setEnabled(true);
        edEmail.setEnabled(true);
        edLastName.setEnabled(true);
        edMobile.setEnabled(true);
        edit_profile_laypout.setVisibility(View.VISIBLE);
        userAssociationLayout.setVisibility(View.VISIBLE);
        association_msg.setVisibility(View.GONE);
        edDob.setEnabled(true);
        edEmail.setEnabled(true);
        genderSpinner.setEnabled(true);
        temp_single_menu.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.GONE);
            temp_single_menu.setOnClickListener(null);
        }

    }


    private class UserAssociationsAdapter extends AbstractBaseAdapter<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations, AssociationsHolder> {

        public UserAssociationsAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.item_association;
        }

        @Override
        public AssociationsHolder getViewHolder(View convertView) {
            return new AssociationsHolder(convertView);
        }

        @Override
        public void bindView(int position, AssociationsHolder holder, com.slam.gss.goldslam.models.UpdateProfile.UserAssociations item) {
            try {
                holder.getAssociations().setAdapter(associationAdapter);
                holder.getRegistrationNo().setText(item.getRegistrationId());

                String asscName = item.getAssociationName();

                System.out.print("AssociationList lemgth" + AssociationsList.size());

                if (AssociationsList.size() > 0) {
                    for (int i = 0; i < AssociationsList.size(); i++) {
                        System.out.print("AssociationName" + asscName);
                        System.out.print("AssociationName" + AssociationsList.get(i));

//                        if (asscName.equalsIgnoreCase(AssociationsList.get(i))) {
//                            holder.getAssociations().setSelection(i);
//                        } else {
//                            holder.getAssociations().setSelection(0);
//                        }
                    }
                }

                holder.getDeleteAction().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Helper.showToast(getActivity(), "cliced on trash button");
                    }
                });

                association_container1.addView(view);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class UserAssociationsAdapter1 extends AbstractBaseAdapter<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations, UserAssociationsHolder> {

        public UserAssociationsAdapter1(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.user_assocation_item;
        }

        @Override
        public UserAssociationsHolder getViewHolder(View convertView) {
            return new UserAssociationsHolder(convertView);
        }

        @Override
        public void bindView(int position, UserAssociationsHolder holder, com.slam.gss.goldslam.models.UpdateProfile.UserAssociations item) {
            try {
                holder.getUserAssoc().setText(item.getAssociationName());
                holder.getUserRegiNo().setText(item.getRegistrationId());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void populateUserAssoData(List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> list) {
        association_container1.removeAllViews();
        List<Associations> masterData = getAllAssociations();

        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                final com.slam.gss.goldslam.models.UpdateProfile.UserAssociations associations = list.get(i);
                final View view = LayoutInflater.from(getActivity()).inflate(R.layout.user_assocation_item, null);
                UserAssociationsHolder holder = new UserAssociationsHolder(view);
                holder.getUserRegiNo().setText(list.get(i).getRegistrationId());
                holder.getUserAssoc().setText(list.get(i).getAssociationName());
                association_container1.addView(view);
            }
        } else {
            association_container1.removeAllViews();
        }
    }

    private void populateAllItems(List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> list) {

//        association_container
        // Populate
        // Check the count to disable + association button
        // Populate spinner with all items and add select

        association_container.removeAllViews();
        List<Associations> masterData = getAllAssociations();

        for (int i = 0; (masterData != null && !masterData.isEmpty()) && i < list.size(); i++) {
            final com.slam.gss.goldslam.models.UpdateProfile.UserAssociations associations = list.get(i);
            final View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_association, null);
            //
            AssociationsHolder holder = new AssociationsHolder(view);
            final ArrayAdapter<Associations> associationAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, masterData);
            holder.getAssociations().setAdapter(associationAdapter);
            int pos = containsAssociation(associations.getAssociationid());
            holder.getAssociations().setSelection(pos);
            holder.getAssociations().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (associations.getAssociationid() != null) {
                        //Remove old selected values
                        selected.remove(associations.getAssociationid());
                        //  Log.e("populateAllItems", "Old value >>> " + associations.getAssociationid());
                    }
                    selected.put(associations.getAssociationid(), associationAdapter.getItem(position));
                    Associations associations1 = (Associations) parent.getSelectedItem();
                    associations.setAssociationid(associations1.getDocId());
                    associations.setAssociationName(associations1.getAssociationName());
                    // Log.e("populateAllItems", "New value >>> " + associations.getAssociationid());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            holder.getDeleteAction().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    association_container.removeView(view);
                    selected.remove(associations.getAssociationid());
                    showOrHideAdd(getAllAssociations());
                    resetSpinner();
                }
            });
            holder.getRegistrationNo().setText(associations.getRegistrationId());
            holder.getRegistrationNo().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    associations.setRegistrationId(s.toString().trim());
                }
            });
            view.setTag(associations);
            association_container.addView(view);
        }
        showOrHideAdd(masterData);
        disableAllSpinners();
    }

    private void showOrHideAdd(List<Associations> masterData) {
        btnAddAssociation.setVisibility(association_container.getChildCount() < masterData.size()
                ? View.VISIBLE : View.GONE);
    }

    private List<Associations> getAllAssociations() {
        return AssociationsList;
    }

    private int containsAssociation(String id) {
        for (int i = 0; i < AssociationsList.size(); i++) {
            if (id.equalsIgnoreCase(AssociationsList.get(i).getDocId())) {
                return i;
            }
        }
        return 0;
    }

    private void addNewAssociation() {
        disableAllSpinners();
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_association, null);
        //
        final com.slam.gss.goldslam.models.UpdateProfile.UserAssociations userAssociations = new com.slam.gss.goldslam.models.UpdateProfile.UserAssociations();
        final AssociationsHolder holder = new AssociationsHolder(view);
        final ArrayAdapter<Associations> associationAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, getTrimmedAssociations());
        holder.getAssociations().setAdapter(associationAdapter);

        holder.getAssociations().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //

                if (userAssociations.getAssociationid() != null) {
                    //Remove old selected values
                    selected.remove(userAssociations.getAssociationid());
                    //   Log.e("addNewAssociation", "Old value >>> " + userAssociations.getAssociationid());
                }
                Associations temp1 = ((Associations) parent.getAdapter().getItem(position));
                selected.put(temp1.getDocId(), temp1);
                // Log.e("addNewAssociation", "New value >>> " + temp1.getDocId());
                userAssociations.setAssociationid(temp1.getDocId());
                userAssociations.setAssociationName(temp1.getAssociationName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        holder.getAssociations().setEnabled(false);
        holder.getDeleteAction().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                association_container.removeView(view);
                selected.remove(userAssociations.getAssociationid());
                showOrHideAdd(getAllAssociations());
                resetSpinner();
            }
        });
        holder.getRegistrationNo().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                userAssociations.setRegistrationId(s.toString().trim());
            }
        });
        view.setTag(userAssociations);
        association_container.addView(view);
        showOrHideAdd(getAllAssociations());

    }

    private Map<String, Associations> selected = new HashMap<>();

    private List<Associations> getTrimmedAssociations() {
        List<Associations> associationses = new ArrayList<>();
        List<Associations> masterData = getAllAssociations();
        if (masterData != null) {
            for (Associations a : masterData) {
                if (selected.containsKey(a.getDocId())) {
                    continue;
                }
                associationses.add(a);
            }
        }
        return associationses;
    }

    private List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> getAllData() {
        List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> list = new ArrayList<>();
        for (int i = 0; i < association_container.getChildCount(); i++) {
            View view = association_container.getChildAt(i);
            com.slam.gss.goldslam.models.UpdateProfile.UserAssociations associations = (com.slam.gss.goldslam.models.UpdateProfile.UserAssociations) view.getTag();
            if (associations != null) {
                list.add(associations);
            }
        }
        return list;
    }

    private void disableAllSpinners() {
        List<com.slam.gss.goldslam.models.UpdateProfile.UserAssociations> list = new ArrayList<>();
        for (int i = 0; i < association_container.getChildCount(); i++) {
            View view = association_container.getChildAt(i);
            view.findViewById(R.id.sp_associations).setEnabled(false);
        }
    }

    private void resetSpinner() {
        for (int i = 0; i < association_container.getChildCount(); i++) {
            View view = association_container.getChildAt(i);
            boolean isEnable = view.findViewById(R.id.sp_associations).isEnabled();
            if (isEnable) {
                Spinner spinner = ((Spinner) view.findViewById(R.id.sp_associations));
                Associations associations = (Associations) spinner.getSelectedItem();
                List<Associations> list = getTrimmedAssociations();
                int sel = -1;
                if (associations != null) {
                    list.add(associations);
                    sel = list.size() - 1;
                }
                final ArrayAdapter<Associations> associationAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, list);
                ((Spinner) view.findViewById(R.id.sp_associations)).setAdapter(associationAdapter);
                if (sel >= 0) {
                    spinner.setSelection(sel);
                }
            }
        }
    }
}