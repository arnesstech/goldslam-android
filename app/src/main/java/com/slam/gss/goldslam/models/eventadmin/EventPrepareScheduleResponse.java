package com.slam.gss.goldslam.models.eventadmin;

import com.slam.gss.goldslam.models.scheduleplayers.Data;

/**
 * Created by Thriveni on 9/9/2016.
 */
public class EventPrepareScheduleResponse {
    private String status;

    private Data data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
