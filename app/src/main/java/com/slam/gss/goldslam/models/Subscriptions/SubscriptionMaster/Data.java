package com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster;

import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;

/**
 * Created by Thriveni on 3/17/2017.
 */

public class Data {
    private String message;

    private SubscriptionMasters[] subscriptionMasters;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SubscriptionMasters[] getSubscriptionMasters() {
        return subscriptionMasters;
    }

    public void setSubscriptionMasters(SubscriptionMasters[] subscriptionMasters) {
        this.subscriptionMasters = subscriptionMasters;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", subscriptionMasters = " + subscriptionMasters + "]";
    }
}
