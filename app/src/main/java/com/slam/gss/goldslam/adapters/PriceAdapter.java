package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.models.MySubscriptions.UserSubscriptions;
import com.slam.gss.goldslam.models.Pricing.PricingResponse;
import com.slam.gss.goldslam.models.eventadmin.EventsListModel;

import java.util.ArrayList;

/**
 * Created by Thriveni on 3/16/2017.
 */

public class PriceAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private Context _context;
    ArrayList<UserSubscriptions> _pricingList;
    UserSubscriptions tempValues=null;

    public PriceAdapter(Context context, ArrayList<UserSubscriptions> pricingList) {
        this._context = context;
        this._pricingList = pricingList;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = (LayoutInflater) _context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }


    @Override
    public int getCount() {
        if (_pricingList.size() <= 0)
            return 1;
        return _pricingList.size();
    }

    @Override
    public Object getItem(int position) {
       return _pricingList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public static class PriceViewHolder {

        public TextView txtSubscriptionName;
        public TextView txtSubscriptionTime;
        public TextView txtSubscriptionpackName;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        PriceViewHolder holder;
        if(convertView == null){
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.pricing_item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new PriceViewHolder();
            holder.txtSubscriptionName = (TextView) view.findViewById(R.id.txt_subscription_name);
            holder.txtSubscriptionTime=(TextView)view.findViewById(R.id.txt_subscription_valid_time);
            holder.txtSubscriptionpackName=(TextView) view.findViewById(R.id.txt_subscription_pack_name);

            /************  Set holder with LayoutInflater ************/
            view.setTag( holder );
        }else{
            holder=(PriceAdapter.PriceViewHolder) view.getTag();
        }

        if(_pricingList.size()<=0)
        {
            //holder.tournamentName.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            // _events=null;
            tempValues = ( UserSubscriptions ) _pricingList.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.txtSubscriptionName.setText( tempValues.getSubscriptionName() );
            /*holder.txtSubscriptionTime.setText( tempValues.getSub() );*/
            holder.txtSubscriptionpackName.setText(tempValues.getAmount() );

            /******** Set Item Click Listner for LayoutInflater for each row *******/

        }
        return view;
    }





}
