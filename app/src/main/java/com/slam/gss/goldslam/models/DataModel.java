package com.slam.gss.goldslam.models;

/**
 * Created by Thriveni on 8/8/2016.
 */
public class DataModel {

    public int icon;
    public String name;

    // Constructor.
    public DataModel(int icon, String name) {

        this.icon = icon;
        this.name = name;
    }

    // Constructor.
    public DataModel( String name) {

       // this.icon = icon;
        this.name = name;
    }
}
