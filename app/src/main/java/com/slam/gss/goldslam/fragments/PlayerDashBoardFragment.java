package com.slam.gss.goldslam.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.slam.gss.goldslam.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/6/2017.
 */

public class PlayerDashBoardFragment extends Fragment {

    @BindView(R.id.scoreboard_container)
    FrameLayout scoreboardContainer;

    @BindView(R.id.logo_container)
    FrameLayout logoContainer;

    @BindView(R.id.banner_container)
    FrameLayout bannerContainer;

    @BindView(R.id.upcomming_events_container)
    FrameLayout upcommingEventsContainer;

    @BindView(R.id.playing_club_locator_container)
    FrameLayout playing_club_locator_container;

    @BindView(R.id.shop_now_container)
    FrameLayout shop_now_container;

    @BindView(R.id.media_container)
    FrameLayout media_container;

    @BindView(R.id.our_clients_container)
    FrameLayout our_clients_container;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player_dash_board, container, false);

        ButterKnife.bind(view);
        return view;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(view);

        if (getChildFragmentManager().findFragmentById(R.id.scoreboard_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.scoreboard_container, new ScoreBoardFragment()).commit();
        }
        if (getChildFragmentManager().findFragmentById(R.id.logo_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.logo_container, new LogoFragment()).commit();
        }

        if (getChildFragmentManager().findFragmentById(R.id.banner_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.banner_container, new BannerFragment()).commit();
        }
        if (getChildFragmentManager().findFragmentById(R.id.upcomming_events_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.upcomming_events_container, new UpCommingEventsFragment()).commit();
        }

        if (getChildFragmentManager().findFragmentById(R.id.playing_club_locator_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.playing_club_locator_container, new PlayingClubLocatorFragment()).commit();
        }

       /* if (getChildFragmentManager().findFragmentById(R.id.shop_now_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.shop_now_container, new ShopNowFragment()).commit();
        }*/

        if (getChildFragmentManager().findFragmentById(R.id.media_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.media_container, new MediaFragment()).commit();
        }

        if (getChildFragmentManager().findFragmentById(R.id.our_clients_container) == null) {
            getChildFragmentManager().beginTransaction().add(R.id.our_clients_container, new OurClientsFragment()).commit();
        }


    }
}
