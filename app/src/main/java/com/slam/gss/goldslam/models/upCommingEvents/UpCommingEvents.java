package com.slam.gss.goldslam.models.upCommingEvents;

/**
 * Created by Thriveni on 6/7/2017.
 */

public class UpCommingEvents {
    String eventDescription, eventDate, eventLocation;

    public String getEventDate() {

        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventDescription() {

        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }
}
