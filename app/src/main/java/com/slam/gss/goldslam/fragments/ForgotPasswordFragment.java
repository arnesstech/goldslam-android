package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.slam.gss.goldslam.LoginActivity;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/6/2017.
 */

public class ForgotPasswordFragment extends DialogFragment {

    @BindView(R.id.forgot_email)
    EditText ed_Email;
    String email="";
    @BindView(R.id.submit)
    TextView txtSubmit;

    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return LayoutInflater.from(getActivity()).inflate(R.layout.forgot_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ed_Email.setImeOptions(EditorInfo.IME_ACTION_DONE);

        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(ed_Email.getText().toString())) {
                    ed_Email.setError(getString(R.string.err_forgotPassword_userName));
                    ed_Email.requestFocus();
                } else {
                    email = ed_Email.getText().toString();
                    new ForgotPasswordAsynTask().execute(Connections.REGISTRATION_URL);
                }
            }
        });


    }


    class ForgotPasswordAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("  Loading");
            pDialog.show();
            pDialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {
                String _url = urls[0] + "/" + email + "/reset/pwd";

                HttpPost httpPost = new HttpPost(_url);
                httpPost.addHeader("Authorization", "Basic");
                httpPost.addHeader("Client-Type", "APP");
                httpPost.addHeader("App-Id", "PROSPORTS");
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            Log.i("Login Result", result);
            JSONObject jsonResponse = null;
            String status = null;

            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {

                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                    dismiss();

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Network error");
            }

        }

    }

}
