package com.slam.gss.goldslam.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.slam.gss.goldslam.AppDashBoard;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Ramesh on 21/8/16.
 */
public class Utils {

    /**
     * Connection error messages
     */
    public static final String ALERT_NETWORK_NOT_FOUND = "Please check your connection and try again.";

    public static long convertLiveDateToMillis(String time) {
        //String format = "dd-MMM-yyyy HH:mm";
        String format = "dd-MMM-yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        try {
            date = dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTimeInMillis();


    }

    public static Date convertLiveDateToMillis1(String time) {
        String format = "dd-MMM-yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        try {
            date = dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return date;

    }

    public static Map<String, String> toMap(JSONObject object) throws JSONException {
        Map<String, String> map = new HashMap<String, String>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            String value = object.getString(key);

            map.put(key, value);
        }
        return map;
    }


    /* Shared preference methods */
    public static void putString(Activity activity, String key, String value, int type) {
        SharedPreferences sp = activity.getSharedPreferences("GSS", Context.MODE_PRIVATE);

        switch (type) {
            case 0:
                sp.edit().putInt(key, Integer.parseInt(value)).commit();
                break;

            case 1:
                sp.edit().putString(key, value).commit();
                break;

            case 2:
                sp.edit().putBoolean(key, Boolean.parseBoolean(value)).commit();
                break;

            default:
                break;

        }
    }

    public static String getString(Activity activity, String key, int type) {
        SharedPreferences sp = activity.getSharedPreferences("GSS", Context.MODE_PRIVATE);
        String value = "";
        switch (type) {
            case 0:
                value = String.valueOf(sp.getInt(key, 0));
                break;

            case 1:
                value = String.valueOf(sp.getString(key, ""));
                break;

            case 2:
                value = String.valueOf(sp.getBoolean(key, false));
                break;

            default:
                break;


        }
        return value;
    }

    public static void navigateToActivity(Activity sourceActivity, Class destinationActivity){
        Intent intent = new Intent(sourceActivity,destinationActivity);
        sourceActivity.startActivity(intent);
    }
}
