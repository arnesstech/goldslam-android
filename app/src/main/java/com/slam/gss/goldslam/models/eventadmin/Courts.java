package com.slam.gss.goldslam.models.eventadmin;

/**
 * Created by Thriveni on 9/12/2016.
 */
public class Courts {
    private String courtName;

    private String lightsAvalibility;

    private String courtType;

    private String courtId;

    private String remarks;

    private String venueName;

    private String venueDocId;

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public String getLightsAvalibility() {
        return lightsAvalibility;
    }

    public void setLightsAvalibility(String lightsAvalibility) {
        this.lightsAvalibility = lightsAvalibility;
    }

    public String getCourtType() {
        return courtType;
    }

    public void setCourtType(String courtType) {
        this.courtType = courtType;
    }

    public String getCourtId() {
        return courtId;
    }

    public void setCourtId(String courtId) {
        this.courtId = courtId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public String getVenueDocId() {
        return venueDocId;
    }

    public void setVenueDocId(String venueDocId) {
        this.venueDocId = venueDocId;
    }

    @Override
    public String toString() {
        return "ClassPojo [courtName = " + courtName + ", lightsAvalibility = " + lightsAvalibility + ", courtType = " + courtType + ", courtId = " + courtId + ", remarks = " + remarks + ", venueName = " + venueName + ", venueDocId = " + venueDocId + "]";
    }
}
