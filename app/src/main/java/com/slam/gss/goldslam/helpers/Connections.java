package com.slam.gss.goldslam.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Window;

import com.slam.gss.goldslam.BuildConfig;

/**
 * Created by Thriveni on 8/5/2016.
 */
public class Connections {


    public static int TIMEOUT = 40000;
    //    public static String BASE_URL = "http://gss.goldslamsports.com/prosports/api/";
    public static String DEV_URL="http://gss.goldslamsports.com/prosports/api/";
    public static String PRODUCTION_URL="http://139.162.49.134:8080/prosports/api/";
    public static String BASE_URL = BuildConfig.BASE_URL; //"http://139.162.49.134:8080/prosports/api/";
    public static String LOGIN_URL = BASE_URL + "api/auth/login";
    public static String REGISTRATION_URL = BASE_URL + "api/registration";
    public static String OTP_URL = BASE_URL + "api/registration/verification";
    public static String UPDATE_PASSWORD_URL = BASE_URL + "api/registration/pwd";
    public static String FORGOTPASSWORD_URL = BASE_URL + "";
    public static String GET_PROFILE = REGISTRATION_URL;
    public static String UPDATE_PROFILE = BASE_URL+"api/myaccount";//http://gss.goldslamsports.com/prosports/api/myaccount";
    public static String EVENT_WITHDRAW = BASE_URL + "api/entry/withdraw";
    public static String EVENT_ENTRY = BASE_URL + "api/entry";
    public static String FORGOT_USERNAME = BASE_URL + "api/registration/forgotusername";


    public static boolean isNetworkAvailable(Context con) {
        try {
            ConnectivityManager cm = (ConnectivityManager) con
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            //ExceptionHandler.logException(e.getMessage());
        }
        return false;
    }

    public static void showNetworkError(Context con) {
        createAlertSingle((Activity) con, "Please check your internet connection!", "Ok", false);
    }


    public static void createAlertSingle(final Activity mActivity, String message, String confirmBtn, final boolean finish) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        /*dialog.setContentView(R.layout.alert_dialog_single);
        TextView msg = (TextView) dialog.findViewById(R.id.msg);
        msg.setText(message);
        TextView btnConfirm = (TextView) dialog.findViewById(R.id.confirm);
        btnConfirm.setText(confirmBtn);
        btnConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(finish)
                    mActivity.finish();
            }
        });*/
        dialog.show();
    }


}
