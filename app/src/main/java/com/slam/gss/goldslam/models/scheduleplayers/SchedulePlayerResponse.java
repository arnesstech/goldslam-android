package com.slam.gss.goldslam.models.scheduleplayers;

/**
 * Created by Ramesh on 20/8/16.
 */
public class SchedulePlayerResponse {
    private String status;

    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", data = " + data + "]";
    }
}
