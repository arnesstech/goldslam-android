package com.slam.gss.goldslam.models;

/**
 * Created by Thriveni on 8/8/2016.
 */
public class Tournament {
    public String collegeName;
    public String tournamentName;
    public String address;
    public String fromDate;
    public String toDate;
    public String docID;
    public String email;
    public String tournamentAssociationId;

    public String getTournamentAssociationName() {
        return tournamentAssociationName;
    }

    public void setTournamentAssociationName(String tournamentAssociationName) {
        this.tournamentAssociationName = tournamentAssociationName;
    }

    public String getTournamentAssociationId() {
        return tournamentAssociationId;
    }

    public void setTournamentAssociationId(String tournamentAssociationId) {
        this.tournamentAssociationId = tournamentAssociationId;
    }

    public String tournamentAssociationName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getFromDate() {

        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTournamentName() {

        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }


    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }
}
