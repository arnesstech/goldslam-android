package com.slam.gss.goldslam;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.slam.gss.goldslam.adapters.MatchVenueAdapter;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.eventadmin.Courts;
import com.slam.gss.goldslam.models.eventadmin.CourtsMasterDataResponse;
import com.slam.gss.goldslam.models.eventadmin.MatchConditionOptions;
import com.slam.gss.goldslam.models.eventadmin.MatchConditionsResponse;
import com.slam.gss.goldslam.models.eventadmin.MatchSchedule.MatchScheduleResponse;
import com.slam.gss.goldslam.models.scheduleplayers.Court;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.Teams;
import com.slam.gss.goldslam.network.RestApi;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 9/16/2016.
 */
public class MatchScheduleFragment extends DialogFragment {

    @BindView(R.id.button_schedule)
    Button button_schedule;

    @BindView(R.id.match_venue)
    Spinner match_venue_spinner;

    @BindView(R.id.match_condition)
    Spinner match_condition_spinner;

    @BindView(R.id.match_schedule_date)
    EditText et_match_schedule_date;

    @BindView(R.id.match_schedule_time)
    EditText et_match_schedule_time;

    @BindView(R.id.match_remarks)
    EditText et_match_remarks;

    @BindView(R.id.txt_match_header)
    TextView txt_plyers;

    private Courts[] courts;
    private MatchConditionOptions[] matchCondtions;
    public String venue = "", matchCondition = "", temp_time = "";
    Calendar myCalendar;
    int selectedCourtPosition, selectedMatchConditionPosition;

    Matches mMatches;

    Context mContext = getActivity();

    private Spinner venueSpinner, matchConditionSpinner;

    public static MatchScheduleFragment newInstance(Matches matches) {
        MatchScheduleFragment fragment = new MatchScheduleFragment();
        fragment.setMatches(matches);
        return fragment;
    }

    public void setMatches(Matches matches) {
        this.mMatches = matches;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        getCourts();
        return LayoutInflater.from(getActivity()).inflate(R.layout.match_schedule, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String obj = new Gson().toJson(mMatches);
        Log.e("selected match is", obj);

        myCalendar = Calendar.getInstance();

        String _header = "";
        String _player1Name = Helper.selectedMatch.getTeams().size() > 0 ? Helper.selectedMatch.getTeams().get(0).getName() : "";
        String _player2Name = Helper.selectedMatch.getTeams().size() > 1 ? Helper.selectedMatch.getTeams().get(1).getName() : "";

        if(Helper.selectedMatch.getTeams().size()>1){
            _header = _player1Name+" " + "(vs)" + " " +_player2Name;
            txt_plyers.setText(_header);
        }else{
            _header = _player1Name +" " + _player2Name;
            txt_plyers.setText(_header);
        }


        String startTime = Helper.selectedMatch.getStartTime();



        if (Helper.selectedMatch.getStartTime() != null && Helper.selectedMatch.getStartTime() != "") {
            //  Log.i("if","if");
            et_match_schedule_date.setText(Helper.selectedMatch.getStartTime().split(" ")[0]);
            et_match_schedule_time.setText(Helper.selectedMatch.getStartTime().split(" ")[1]);

        } else {
            et_match_schedule_date.setText("");
            et_match_schedule_time.setText("");
        }
        match_venue_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                venue = parent.getItemAtPosition(position).toString();
                selectedCourtPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        match_condition_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                selectedMatchConditionPosition = position;
                matchCondition = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_match_schedule_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Helper.showToast(getActivity(),"date");
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });


        et_match_schedule_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Helper.showToast(getActivity(),"time");
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        et_match_schedule_time.setText(selectedHour + ":" + selectedMinute);
                        temp_time = selectedHour + ":" + selectedMinute + ":00" + ":000";
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        Log.e("remarks", Helper.selectedMatch.getMatchremarks());


    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void updateLabel() {

        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        et_match_schedule_date.setText(sdf.format(myCalendar.getTime()));
    }


    public void getCourts() {

        Call<CourtsMasterDataResponse> venueData = RestApi.get()
                .getRestService()
                .getCourtsMasterData(Helper.tornDocId);
        //
        venueData.enqueue(new Callback<CourtsMasterDataResponse>() {
            @Override
            public void onResponse(Call<CourtsMasterDataResponse> call, Response<CourtsMasterDataResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {
                        courts = response.body().getData().getCourts();


                        match_venue_spinner.setAdapter(new MatchVenueAdapter(getActivity(), courts));

                        if (Helper.selectedMatch.getCondition() == "" || Helper.selectedMatch.getCondition().equalsIgnoreCase(null)) {

                        } else {

                            int _venuePosition = 0;
                            Log.i("Match Condition", mMatches.getCondition());
                            Log.i("Match Start Time", mMatches.getStartTime());
                            Log.i("Match venue", mMatches.getCourt().getCourtId());
                            //2016-10-28T8:21:00:000Z


                            for (int i = 0; i < courts.length; i++) {
                                if (courts[i].getCourtId().equalsIgnoreCase(mMatches.getCourt().getCourtId())) {
                                    _venuePosition = i;
                                }
                            }
                            match_venue_spinner.setSelection(_venuePosition);
                        }
                        getMatchConditions();
                    }

                } else {
                    //Handle errors
                    getMatchConditions();
                }


            }

            @Override
            public void onFailure(Call<CourtsMasterDataResponse> call, Throwable t) {

                if (call.isCanceled()) {
                    //Do nothing
                } else {
                    //Handle errors
                }

            }
        });

    }

    public void getMatchConditions() {


        final Call<MatchConditionsResponse> matchConditionReponse = RestApi.get()
                .getRestService()
                .getMatchCodition();
        //
        matchConditionReponse.enqueue(new Callback<MatchConditionsResponse>() {
            @Override
            public void onResponse(Call<MatchConditionsResponse> call, Response<MatchConditionsResponse> response) {

                if (response.isSuccessful()) {
                    Log.i("matchCondition Response", response.body().toString());
                    if (response.body() != null && response.body().getData() != null) {
                        matchCondtions = response.body().getData().getMasterData().getOptions();
                        match_condition_spinner.setAdapter(new com.slam.gss.goldslam.adapters.MatchConditionAdapter(getActivity(), matchCondtions));

                        if (Helper.selectedMatch.getCondition() == "" || Helper.selectedMatch.getCondition().equalsIgnoreCase(null)) {

                        } else {

                            int _matchConditionPosition = 0;
                            // Log.i("Match Condition", mMatches.getCondition());
                            // Log.i("Match Start Time", mMatches.getStartTime());
                            // Log.i("Sele Match Condition is", mMatches.getCondition());
                            Log.i("Match remarks", mMatches.getRemarks());

                            // Log.i("selected Date",mMatches.getStartTime().split(" ")[0]);
                            // Log.i("selected Date",mMatches.getStartTime().split(" ")[1]);
                            // et_match_schedule_date.setText(mMatches.getStartTime().split(" ")[0]);
                            // et_match_schedule_time.setText(mMatches.getStartTime().split(" ")[0]);
                            et_match_remarks.setText(mMatches.getRemarks());


                            for (int i = 0; i < matchCondtions.length; i++) {
                                if (matchCondtions[i].getValue().equalsIgnoreCase(mMatches.getCondition())) {
                                    _matchConditionPosition = i;
                                }
                            }
                            match_condition_spinner.setSelection(_matchConditionPosition);
                        }

                    }
                } else {
                    //Handle errors
                }

            }

            @Override
            public void onFailure(Call<MatchConditionsResponse> call, Throwable t) {
                if (call.isCanceled()) {
                    //Do nothing
                } else {
                    //Handle errors
                }

            }
        });

    }

    @OnClick(R.id.button_schedule)
    public void onClick() {
//        //TODO: Click submit logic and dismiss dialog.
        try {
            match_schedule();
        } catch (Exception e) {
            e.printStackTrace();
        }

        dismiss();
    }

    private void match_schedule() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String s = et_match_schedule_date.getText().toString();
        String ss = sdf.format(new Date(s));

        String time = ss + "T" + temp_time + "Z";
        String time1 = s + " " + et_match_schedule_time.getText();

        Log.i("old startTime", time);
        Log.i("new startTime", time1);


        Helper.selectedMatch.setTime(time);
        Helper.selectedMatch.setStartTime(time1);
        Helper.selectedMatch.setDate(et_match_schedule_date.getText().toString());
        Helper.selectedMatch.setVenueDocId(courts[selectedCourtPosition].getVenueDocId());
        Helper.selectedMatch.setCondition(matchCondtions[selectedMatchConditionPosition].getValue());
        Helper.selectedMatch.setCourtId(courts[selectedCourtPosition].getCourtId());
        Helper.selectedMatch.setMatchremarks(et_match_remarks.getText().toString());
        Helper.selectedMatch.setRemarks(et_match_remarks.getText().toString());
        String court = courts[selectedCourtPosition].getVenueDocId() + "," + courts[selectedCourtPosition].getCourtId();

        Court c = new Court();
        c.setCourtId(courts[selectedCourtPosition].getCourtId());
        c.setVenueDocId(courts[selectedCourtPosition].getVenueDocId());
        Helper.selectedMatch.setCourt(c);
        String obj = new Gson().toJson(Helper.selectedMatch);
        Log.e("update match schedule", obj);

        Call<MatchScheduleResponse> schedule = RestApi.get()
                .getRestService()
                .scheduleMatch(Helper.selectedMatch);

        schedule.enqueue(new Callback<MatchScheduleResponse>() {
            @Override
            public void onResponse(Call<MatchScheduleResponse> call, Response<MatchScheduleResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null) {
                        String obj = new Gson().toJson(response.body().getData().getMessage());
                        Log.i("obj", obj);

                        if (Helper.selectedMatch.getStatus().equalsIgnoreCase("YET_TO_SCHEDULE")) {
                            Helper.selectedMatch.setStatus("SCHEDULED");
                        }
                        onCloseDialog();
                    }

                } else {
                    onCloseDialog();
                }

            }

            @Override
            public void onFailure(Call<MatchScheduleResponse> call, Throwable t) {
                onCloseDialog();
            }
        });

    }

    @OnClick(R.id.close_dialog)
    public void onCloseDialog() {
       // ((PrepareScheduleActivity) MatchScheduleFragment.this.getActivity()).OnEventUpdated();

        System.out.println("Activity context"+MatchScheduleFragment.this.getActivity());
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface frag) {
        super.onDismiss(frag);
        // DO Something
        ( (PrepareScheduleActivity)getActivity()).loadPlayer();
    }
}
