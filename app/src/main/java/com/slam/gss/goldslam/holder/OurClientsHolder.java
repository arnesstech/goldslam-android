package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.ImageView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class OurClientsHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.im_client_logo)
    ImageView imClientLogo;

    public ImageView getImClientLogo() {
        return imClientLogo;
    }

    public OurClientsHolder(View itemView) {
        super(itemView);
    }
}
