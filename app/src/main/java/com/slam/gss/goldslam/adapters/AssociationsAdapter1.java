package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.AssociationHolder;
import com.slam.gss.goldslam.holder.AssociationsHolder;
import com.slam.gss.goldslam.models.Associations.AssociationList;
import com.slam.gss.goldslam.models.Associations.Associations;
import com.slam.gss.goldslam.models.Associations.UserAssociations;

import java.util.ArrayList;

/**
 * Created by Thriveni on 1/10/2017.
 */
public class AssociationsAdapter1 extends ArrayAdapter<Associations> {

    ArrayList<Associations> _associationsList;
    Context _ctx;

    public AssociationsAdapter1(Context ctx, int txtViewResourceId, ArrayList<Associations> associationsList) {
        super(ctx, txtViewResourceId, associationsList);
        this._associationsList = associationsList;
        this._ctx = ctx;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = getCustomView(position, convertView, parent);
        TextView txtGender = (TextView) v.findViewById(R.id.txtGender);
        txtGender.setTextColor(Color.WHITE);
        return v;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) this._ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mySpinner = inflater.inflate(R.layout.spinner_text, parent, false);

        TextView txtGender = (TextView) mySpinner.findViewById(R.id.txtGender);
        txtGender.setText(_associationsList.get(position).getAssociationName());
        txtGender.setTextColor(Color.BLACK);
        return mySpinner;
    }
}
