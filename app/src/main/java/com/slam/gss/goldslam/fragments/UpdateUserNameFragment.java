package com.slam.gss.goldslam.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.slam.gss.goldslam.AppDashBoard;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.ForgotUserName.ForgotUserNameRequest;
import com.slam.gss.goldslam.models.ForgotUserName.ForgotUserNameResponse;
import com.slam.gss.goldslam.models.UpdateUserName.ErrorResponse;
import com.slam.gss.goldslam.models.UpdateUserName.UpdateUserNameRequest;
import com.slam.gss.goldslam.models.UpdateUserName.UpdateUserNameResponse;
import com.slam.gss.goldslam.network.RestApi;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Thriveni on 1/10/2017.
 */
public class UpdateUserNameFragment extends DialogFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    protected Context context;
    protected Fragment fragment;
    protected RadioButton rbNew, rbOld;
    protected TextView txtHeaderContent;
    protected String rbValue = "old", headerContent, gssId = "", userName = "";
    protected EditText edUserName;
    protected Button btnUpdate;
    protected UpdateUserNameRequest updateUserNameReq;
    protected SharedPreferences sp = null;
    protected SharedPreferences.Editor edit = null;
    protected String TAG = "UpdatedUserNameFragment";

    public static UpdateUserNameFragment newInstance() {
        UpdateUserNameFragment fragment = new UpdateUserNameFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return LayoutInflater.from(getActivity()).inflate(R.layout.update_username, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        sp = getActivity().getSharedPreferences("GSS", Context.MODE_APPEND);
        edit = sp.edit();

        txtHeaderContent = (TextView) view.findViewById(R.id.content);
        gssId = sp.getString("gssid", "");
        userName = sp.getString("Email", "");
        if (TextUtils.isEmpty(gssId)) {
            gssId = "No gssId";
        }
        headerContent = "Dear " + sp.getString("name", "") + " (Goldslam Code: " + gssId + "), you can personalize your username based on its availability to remember easily. Please choose your option to change to new username or keep the existing one (as shown below). Your password will not change by changing the user name and you can use the same password.";

        txtHeaderContent.setText(headerContent);

        rbNew = (RadioButton) view.findViewById(R.id.rb_new_user_name);
        rbOld = (RadioButton) view.findViewById(R.id.rb_old_username);

        edUserName = (EditText) view.findViewById(R.id.ed_update_username);
        edUserName.setText(userName);
        edUserName.setImeOptions(EditorInfo.IME_ACTION_DONE);
        btnUpdate = (Button) view.findViewById(R.id.btn_update_username);

        btnUpdate.setOnClickListener(this);

        rbNew.setOnCheckedChangeListener(this);
        rbOld.setOnCheckedChangeListener(this);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {

            case R.id.rb_new_user_name:
                if (isChecked) {
                    rbValue = "new";
                    edUserName.setText("");
                    edUserName.setError(null);
                }
                break;

            case R.id.rb_old_username:
                if (isChecked) {
                    rbValue = "old";
                    edUserName.setText(sp.getString("Email", ""));
                    edUserName.setError(null);
                }
                break;

            default:
                break;
        }
    }


    public void onCloseDialog() {
        if (sp.getBoolean("Authentication", false)) {
            edit.putBoolean("auth", true);
        } else {
            edit.putBoolean("auth", false);
        }
        edit.commit();
        Intent intent = new Intent(getActivity(), AppDashBoard.class);
        startActivity(intent);
        dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update_username:
                updateUserName();
                break;
            default:
                break;
        }
    }

    private void updateUserName() {
        if (TextUtils.isEmpty(edUserName.getText().toString())) {
            edUserName.setError("please enter userName");
        } else if (TextUtils.equals(rbValue, "new")) {
            if (edUserName.getText().toString().equalsIgnoreCase(userName)) {
                edUserName.setText("");
            } else {
                Log.i(TAG, "showUpdateUserNameDialog:: setOnClick>> " + edUserName.getText().toString());
                updateUserNameService();
            }
        } else if (TextUtils.equals(rbValue, "old")) {
            Log.i(TAG, "showUpdateUserNameDialog:: setOnClick>> " + edUserName.getText().toString());
            updateUserNameService();
        }

    }

    private void updateUserNameService() {
        UpdateUserNameRequest updateUserNameReq = new UpdateUserNameRequest();
        updateUserNameReq.setUserName(edUserName.getText().toString());
        updateUserNameReq.setUserid(sp.getString("registrationDocId", ""));

        final Call<UpdateUserNameResponse> updateUserName = RestApi.get()
                .getRestService()
                .updateUserName(updateUserNameReq);

        updateUserName.enqueue(new Callback<UpdateUserNameResponse>() {
                                   @Override
                                   public void onResponse(Call<UpdateUserNameResponse> call, Response<UpdateUserNameResponse> response) {
                                       if (response.isSuccessful()) {
                                           if (response.body() != null && response.body().getData() != null && response.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                                               Helper.showToast(getActivity(), "Your username changed successfully. Please use this for your future login");
                                               onCloseDialog();

                                           } else {
                                               if (userName.equalsIgnoreCase(edUserName.getText().toString())) {
                                                   Helper.showToast(getActivity(), "Already registered with this UserName");
                                               } else {
                                                   Helper.showToast(getActivity(), "Your username changed successfully. Please use this for your future login");
                                                   onCloseDialog();
                                               }

                                           }
                                       } else {
                                           Helper.showToast(getActivity(), "Error Please try again");
                                           //Log.i(TAG, response.errorBody().toString());
                                       }
                                   }

                                   @Override
                                   public void onFailure(Call<UpdateUserNameResponse> call, Throwable t) {
                                       Helper.showToast(getActivity(), "Network Problem");
                                   }
                               }

        );
    }


}

