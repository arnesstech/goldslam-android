package com.slam.gss.goldslam.models.createTournamentRequest;

/**
 * Created by mahes on 7/5/2017.
 */

public class CreateTournamentRequest {
    private String organizername;

    private String status;

    private String address;

    private String location;

    private String email;

    private String fromdate;

    private String tournamentname;

    private String todate;

    private String docId;

    private String notes;

    private String sporttype;

    private String mobile;

    public String getOrganizername ()
    {
        return organizername;
    }

    public void setOrganizername (String organizername)
    {
        this.organizername = organizername;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getFromdate ()
    {
        return fromdate;
    }

    public void setFromdate (String fromdate)
    {
        this.fromdate = fromdate;
    }

    public String getTournamentname ()
    {
        return tournamentname;
    }

    public void setTournamentname (String tournamentname)
    {
        this.tournamentname = tournamentname;
    }

    public String getTodate ()
    {
        return todate;
    }

    public void setTodate (String todate)
    {
        this.todate = todate;
    }

    public String getDocId ()
    {
        return docId;
    }

    public void setDocId (String docId)
    {
        this.docId = docId;
    }

    public String getNotes ()
    {
        return notes;
    }

    public void setNotes (String notes)
    {
        this.notes = notes;
    }

    public String getSporttype ()
    {
        return sporttype;
    }

    public void setSporttype (String sporttype)
    {
        this.sporttype = sporttype;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [organizername = "+organizername+", status = "+status+", address = "+address+", location = "+location+", email = "+email+", fromdate = "+fromdate+", tournamentname = "+tournamentname+", todate = "+todate+", docId = "+docId+", notes = "+notes+", sporttype = "+sporttype+", mobile = "+mobile+"]";
    }
}
