package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.models.eventadmin.MatchConditionOptions;

/**
 * Created by Thriveni on 9/15/2016.
 */
public class MatchConditionAdapter extends BaseAdapter implements View.OnClickListener {

    private Context _context;
    MatchConditionOptions[] _matchConditions;
    private LayoutInflater inflater = null;

    public MatchConditionAdapter(Context context, MatchConditionOptions[] matchConditions) {
        System.out.print("Addapter Constructor");
        this._context = context;
        this._matchConditions = matchConditions;


        /***********  Layout inflator to call external xml layout () ***********/
        inflater = (LayoutInflater) _context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        if (_matchConditions.length <= 0)
            return 0;
        return _matchConditions.length;
    }

    @Override
    public Object getItem(int position) {
        return _matchConditions[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(View v) {

    }

    public static class ViewHolder{
        TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder holder;
        TextView venueName = null;
        if (convertView == null) {
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.match_schedule_spinner_text, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.textView = (TextView) view.findViewById(R.id.txtSpinner);

            /************  Set holder with LayoutInflater ************/
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (_matchConditions.length <= 0) {

        } else {
            /************  Set Model values in Holder elements ***********/
            holder.textView.setText(_matchConditions[position].getName());
        }
        return view;
    }

}

