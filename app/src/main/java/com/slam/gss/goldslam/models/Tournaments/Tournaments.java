package com.slam.gss.goldslam.models.Tournaments;

/**
 * Created by Thriveni on 6/22/2017.
 */

public class Tournaments {
    private String series;

    private String phone;

    private String location;

    private Events[] events;

    private AddInfo[] addlInfo;

    private Courts[] courts;

    private String hospitality;

    private Admins[] admins;

    private String tournamentName;

    private String entryFeeDetails;

    private String prizeMoney;

    private String[] attachments;

    private String entryStartDate;

    private String associationDocId;

    private String finalListDate;

    private String entryEndDate;

    private String tournamentId;

    private String otherDetails;

    private Accommadations[] accommodations;

    private Moderators[] moderators;

    private String status;

    private String associationId;

    private Venues[] venues;

    private String toDate;

    private String entryWithdrawDate;

    private String associationName;

    private String fromDate;

    private String email;

    private String sportType;

    private String docId;

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public Courts[] getCourts() {
        return courts;
    }

    public void setCourts(Courts[] courts) {
        this.courts = courts;
    }

    public String getHospitality() {
        return hospitality;
    }

    public void setHospitality(String hospitality) {
        this.hospitality = hospitality;
    }

    public Admins[] getAdmins() {
        return admins;
    }

    public void setAdmins(Admins[] admins) {
        this.admins = admins;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getEntryFeeDetails() {
        return entryFeeDetails;
    }

    public void setEntryFeeDetails(String entryFeeDetails) {
        this.entryFeeDetails = entryFeeDetails;
    }

    public String getPrizeMoney() {
        return prizeMoney;
    }

    public void setPrizeMoney(String prizeMoney) {
        this.prizeMoney = prizeMoney;
    }

    public String[] getAttachments() {
        return attachments;
    }

    public void setAttachments(String[] attachments) {
        this.attachments = attachments;
    }

    public String getEntryStartDate() {
        return entryStartDate;
    }

    public void setEntryStartDate(String entryStartDate) {
        this.entryStartDate = entryStartDate;
    }

    public String getAssociationDocId() {
        return associationDocId;
    }

    public void setAssociationDocId(String associationDocId) {
        this.associationDocId = associationDocId;
    }

    public String getFinalListDate() {
        return finalListDate;
    }

    public void setFinalListDate(String finalListDate) {
        this.finalListDate = finalListDate;
    }

    public String getEntryEndDate() {
        return entryEndDate;
    }

    public void setEntryEndDate(String entryEndDate) {
        this.entryEndDate = entryEndDate;
    }

    public String getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(String tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssociationId() {
        return associationId;
    }

    public void setAssociationId(String associationId) {
        this.associationId = associationId;
    }


    public Venues[] getVenues() {
        return venues;
    }

    public void setVenues(Venues[] venues) {
        this.venues = venues;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getEntryWithdrawDate() {
        return entryWithdrawDate;
    }

    public void setEntryWithdrawDate(String entryWithdrawDate) {
        this.entryWithdrawDate = entryWithdrawDate;
    }

    public String getAssociationName() {
        return associationName;
    }

    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSportType() {
        return sportType;
    }

    public void setSportType(String sportType) {
        this.sportType = sportType;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    @Override
    public String toString() {
        return "ClassPojo [series = " + series + ", phone = " + phone + ", location = " + location + ", events = " + events + ", addlInfo = " + addlInfo + ", courts = " + courts + ", hospitality = " + hospitality + ", admins = " + admins + ", tournamentName = " + tournamentName + ", entryFeeDetails = " + entryFeeDetails + ", prizeMoney = " + prizeMoney + ", attachments = " + attachments + ", entryStartDate = " + entryStartDate + ", associationDocId = " + associationDocId + ", finalListDate = " + finalListDate + ", entryEndDate = " + entryEndDate + ", tournamentId = " + tournamentId + ", otherDetails = " + otherDetails + ", accommodations = " + accommodations + ", status = " + status + ", associationId = " + associationId + ", moderators = " + moderators + ", venues = " + venues + ", toDate = " + toDate + ", entryWithdrawDate = " + entryWithdrawDate + ", associationName = " + associationName + ", fromDate = " + fromDate + ", email = " + email + ", sportType = " + sportType + ", docId = " + docId + "]";
    }
}
