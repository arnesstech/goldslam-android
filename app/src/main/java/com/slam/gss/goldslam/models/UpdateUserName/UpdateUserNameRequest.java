package com.slam.gss.goldslam.models.UpdateUserName;

/**
 * Created by Thriveni on 1/6/2017.
 */
public class UpdateUserNameRequest {
    private String userid;

    private String userName;

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getUserName ()
    {
        return userName;
    }

    public void setUserName (String userName)
    {
        this.userName = userName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [userid = "+userid+", userName = "+userName+"]";
    }
}
