package com.slam.gss.goldslam.models.upCommingEvents;

/**
 * Created by Thriveni on 6/27/2017.
 */

public class AddlInfo {
    private String value;

    private String key;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "ClassPojo [value = " + value + ", key = " + key + "]";
    }
}
