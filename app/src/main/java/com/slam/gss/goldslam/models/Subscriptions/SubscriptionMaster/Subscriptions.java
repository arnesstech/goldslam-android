package com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster;

/**
 * Created by Thriveni on 3/17/2017.
 */

public class Subscriptions {

    private String status;

    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", data = " + data + "]";
    }
}
