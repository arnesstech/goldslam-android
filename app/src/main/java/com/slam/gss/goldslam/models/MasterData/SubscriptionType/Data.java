package com.slam.gss.goldslam.models.MasterData.SubscriptionType;

/**
 * Created by Thriveni on 3/21/2017.
 */

public class Data {
    private String message;

    private MasterData masterData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MasterData getMasterData() {
        return masterData;
    }

    public void setMasterData(MasterData masterData) {
        this.masterData = masterData;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", masterData = " + masterData + "]";
    }
}
