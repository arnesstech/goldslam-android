package com.slam.gss.goldslam.models.Events;

/**
 * Created by Thriveni on 6/23/2017.
 */

public class Paymentdetails {
    private String amount;

    private String gatewaycharge;

    private String gatewaypercentage;

    private String currency;

    private String totalamount;

    private String discount;

    private String servicecharge;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getGatewaycharge() {
        return gatewaycharge;
    }

    public void setGatewaycharge(String gatewaycharge) {
        this.gatewaycharge = gatewaycharge;
    }

    public String getGatewaypercentage() {
        return gatewaypercentage;
    }

    public void setGatewaypercentage(String gatewaypercentage) {
        this.gatewaypercentage = gatewaypercentage;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getServicecharge() {
        return servicecharge;
    }

    public void setServicecharge(String servicecharge) {
        this.servicecharge = servicecharge;
    }

    @Override
    public String toString() {
        return "ClassPojo [amount = " + amount + ", gatewaycharge = " + gatewaycharge + ", gatewaypercentage = " + gatewaypercentage + ", currency = " + currency + ", totalamount = " + totalamount + ", discount = " + discount + ", servicecharge = " + servicecharge + "]";
    }
}
