package com.slam.gss.goldslam.models.Tournaments;

import java.util.List;

/**
 * Created by Thriveni on 6/22/2017.
 */

public class Data {
    private String message;

    private List<Tournaments> tournaments;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Tournaments> getTournament() {
        return tournaments;
    }


    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", tournament = " + tournaments + "]";
    }
}
