package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.UpCommingEventsActivity;
import com.slam.gss.goldslam.adapters.UpcommingEventsAdapter;
import com.slam.gss.goldslam.models.upCommingEvents.Tournaments;
import com.slam.gss.goldslam.models.upCommingEvents.UpCommingEvents;
import com.slam.gss.goldslam.models.upCommingEvents.UpCommingEventsResponse;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 6/6/2017.
 */

public class UpCommingEventsFragment extends Fragment {

    @BindView(R.id.rv_upcomming_events)
    RecyclerView rvUpCommingEvents;

    private ProgressDialog progressDialog;
    ArrayList<Tournaments> mUpCommingEventsList;
    RecyclerView.LayoutManager mLayoutManager;
    UpcommingEventsAdapter mUpCommingEventsAdapter;

    @BindView(R.id.txt_exploreMore)
    TextView txtExlporeMore;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upcomming_events, container, false);
        ButterKnife.bind(this, view);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        txtExlporeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UpCommingEventsActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getActivity());
        rvUpCommingEvents.setLayoutManager(mLayoutManager);
        rvUpCommingEvents.setItemAnimator(new DefaultItemAnimator());
        rvUpCommingEvents.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mUpCommingEventsList = new ArrayList<>();
        mUpCommingEventsAdapter = new UpcommingEventsAdapter(getActivity());
        rvUpCommingEvents.setFocusable(false);
        loadUpCommingEvents();

    }

    private void loadUpCommingEvents() {
        Call<UpCommingEventsResponse> upcommingEvents = RestApi.get().getRestService().getUpCommingEvents();

        upcommingEvents.enqueue(new Callback<UpCommingEventsResponse>() {
            @Override
            public void onResponse(Call<UpCommingEventsResponse> call, Response<UpCommingEventsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        List<Tournaments> UpCommingTournamentsObj = response.body().getData().getTournaments();
                        mUpCommingEventsAdapter.add(UpCommingTournamentsObj);
                        rvUpCommingEvents.setAdapter(mUpCommingEventsAdapter);
                        mUpCommingEventsAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpCommingEventsResponse> call, Throwable t) {

            }
        });
    }

}
