package com.slam.gss.goldslam.models.eventadmin.EventAdminTournaments;

/**
 * Created by Thriveni on 3/1/2017.
 */
public class Data {
    private String message;

    private Events[] events;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Events[] getEvents ()
    {
        return events;
    }

    public void setEvents (Events[] events)
    {
        this.events = events;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", events = "+events+"]";
    }
}
