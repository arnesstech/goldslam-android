package com.slam.gss.goldslam.models.feedBack;

/**
 * Created by Thriveni on 6/30/2017.
 */

public class Data {
    private String message;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+"]";
    }
}
