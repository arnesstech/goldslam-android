package com.slam.gss.goldslam.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.slam.gss.goldslam.AppDashBoard;
import com.slam.gss.goldslam.LoginActivity;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.TournamentsListener;
import com.slam.gss.goldslam.adapters.SubscriptionAdapter;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.helpers.JSONParser;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.interfaces.MySubscriptionsListener;
import com.slam.gss.goldslam.interfaces.PricingListener;
import com.slam.gss.goldslam.models.MasterData.SubscriptionType.Options;
import com.slam.gss.goldslam.models.MasterData.SubscriptionType.SubscriptionTypes;
import com.slam.gss.goldslam.models.Paymentdetails;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.Subscriptions;
import com.slam.gss.goldslam.models.Subscriptions.UserSubscriptionCreate.UserSubscriptionCreateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserSubscriptionCreate.UserSubscriptionCreateResponse;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.Data;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.UserTransactionCreateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.UserTransactionCreateResponse;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate.UserTransactionUpdateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate.UserTransactionUpdateResponse;
import com.slam.gss.goldslam.network.RestApi;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 3/17/2017.
 */

public class SubscriptionFragment extends DialogFragment implements View.OnClickListener {

    @BindView(R.id.lv_subscriptions)
    ListView lvSubscriptions;

    @BindView(R.id.progress)
    ProgressBar progress;

    @BindView(R.id.empty)
    TextView empty;

    @BindView(R.id.btn_layout)
    LinearLayout llBtnLayout;

    @BindView(R.id.btn_cancel_subscription)
    Button btnCancel;

    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @BindView(R.id.txt_header_text)
    TextView txtHeaderText;

    public static Activity activity;

    private List<Subscriptions> subscriptionsList;
    public static List<SubscriptionMasters> subscriptionMasterData;
    private SubscriptionAdapter adapter;
    public static SubscriptionMasters tempSelectedSubscription;
    private SharedPreferences preferences;
    private List<Options> subscriptionTypes;
    private ProgressDialog progressDialog = null;
    private SharedPreferences sp;
    public static String strrrr;
    public MySubscriptionsListener listener;
    public PricingListener listener2;
    public TournamentsListener listener1;
    private int randomInt = 0;
    private PaytmPGService service = null;

    public static SubscriptionFragment newInstance(Activity loginActivity, String str) {
        activity = loginActivity;
        SubscriptionFragment fragment = new SubscriptionFragment();
        strrrr = str;
        Log.e("page is", strrrr);


        return fragment;

    }

    public void setListener(MySubscriptionsListener listener) {
        this.listener = listener;
    }

    public void setListener1(TournamentsListener listener) {
        this.listener1 = listener;
    }

    public void setListener2(PricingListener listener) {
        this.listener2 = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new SubscriptionAdapter(getActivity());
        sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);

        tempSelectedSubscription = null;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(true);
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_subscription, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        preferences = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if ("tournament".equalsIgnoreCase(strrrr)) {
            txtHeaderText.setText("Your subscription expired,please subscribe to our services to view tournament draws, schedules and scores");
        } else if ("mysubscriptions".equalsIgnoreCase(strrrr)) {
            txtHeaderText.setText("Please subscribe to our services to view tournament draws, schedules and scores");
        } else if ("pricing".equalsIgnoreCase(strrrr)) {
            txtHeaderText.setText("Please subscribe to our services to view tournament draws, schedules and scores");
        } else {
            txtHeaderText.setText("Please subscribe to our services to view tournament draws, schedules and scores");
        }

        loadSubscriptions();
        loadSubscriptionTypes();
    }

    private void loadSubscriptionTypes() {

        Call<SubscriptionTypes> getSubscription = RestApi.get().getRestService().getSubscriptionTypes();

        getSubscription.enqueue(new Callback<SubscriptionTypes>() {
            @Override
            public void onResponse(Call<SubscriptionTypes> call, Response<SubscriptionTypes> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null && response.body().getData().getMasterData() != null && response.body().getData().getMasterData().getOptions() != null) {

                        if (response.body().getData().getMasterData().getOptions().length > 0) {
                            subscriptionTypes = Arrays.asList(response.body().getData().getMasterData().getOptions());
                        }

                    } else {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<SubscriptionTypes> call, Throwable t) {

            }
        });
    }

    private void loadSubscriptions() {

        showProgressDialog();

        Call<Subscriptions> getSubscriptions = RestApi.get().getRestService().getSubscriptions();
        getSubscriptions.enqueue(new Callback<Subscriptions>() {
            @Override
            public void onResponse(Call<Subscriptions> call, Response<Subscriptions> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null && response.body().getData().getSubscriptionMasters() != null) {

                        if (response.body().getData().getSubscriptionMasters().length > 0) {
                            subscriptionMasterData = new ArrayList<SubscriptionMasters>(Arrays.asList(response.body().getData().getSubscriptionMasters()));
                            adapter.addItems(subscriptionMasterData);
                            lvSubscriptions.setAdapter(adapter);

                            dismissProgressDialog();
                            showEmptyView();

                        } else {
                            dismissProgressDialog();
                            showEmptyView();
                        }
                    } else {

                        dismissProgressDialog();
                        showEmptyView();
                    }


                } else {
                    dismissProgressDialog();
                    showEmptyView();
                }
            }

            @Override
            public void onFailure(Call<Subscriptions> call, Throwable t) {
                Helper.showToast(getActivity(), "NetWork Problem");
                dismissProgressDialog();
                showEmptyView();
            }
        });
    }


    private void showEmptyView() {
        if (!isAdded()) {
            return;
        }
        if (adapter.getCount() == 0) {
            empty.setVisibility(View.VISIBLE);
            lvSubscriptions.setVisibility(View.GONE);
            //llBtnLayout.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
        } else {
            empty.setVisibility(View.GONE);
            lvSubscriptions.setVisibility(View.VISIBLE);
            llBtnLayout.setVisibility(View.VISIBLE);
        }
    }


    protected void showProgressDialog() {
        if (progress != null) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    //@Override
    protected void dismissProgressDialog() {
        if (progress != null) {
            progress.setVisibility(View.GONE);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel_subscription:
                cancelSubscriptionPayment();
                break;
            case R.id.btn_submit:
                Log.e("page name", strrrr);

                if (tempSelectedSubscription != null) {
                    if ("mysubscriptions".equalsIgnoreCase(strrrr)) {
                        showPayDialog();
                    } else if ("tournament".equalsIgnoreCase(strrrr)) {
                        showPayDialog();
                    } else if ("pricing".equalsIgnoreCase(strrrr)) {
                        showPayDialog();
                    } else {

                    }
                } else {
                    Helper.showToast(getActivity(), "Please select atleast one subscription plan to make payment");
                }


                break;
            default:
                break;
        }

    }

    private void cancelSubscriptionPayment() {
        dismiss();
/*
        Intent intent = new Intent(getContext(), AppDashBoard.class);
        startActivity(intent);*/
    }

    private void showPayDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Subscription Payment");
        alertDialogBuilder.setMessage("Would you like to pay subscription fee  " + getActivity().getResources().getString(R.string.Rs) + " " + tempSelectedSubscription.getSubscriptionFee());
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        userTractionCreate();

                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void userTractionCreate() {
        Log.e("selected Item", tempSelectedSubscription.toString());
        String subscriptionTypeId = null;
        int index = 0;
        if (subscriptionTypes.size() > 0) {
            for (int i = 0; i < subscriptionTypes.size(); i++) {
                if (TextUtils.equals(subscriptionTypes.get(i).getName(), tempSelectedSubscription.getSubscriptionType())) {
                    index = i;
                }
            }
        }

        subscriptionTypeId = subscriptionTypes.get(index).getValue();

        UserTransactionCreateRequest req = new UserTransactionCreateRequest();
        req.setUserdocId(preferences.getString("registrationDocId", ""));
        req.setName(preferences.getString("name", ""));
        req.setGssId(preferences.getString("gssid", ""));
        req.setAmount(tempSelectedSubscription.getSubscriptionFee());
        req.setDiscount("0");
        req.setPaymentAmount(tempSelectedSubscription.getSubscriptionFee());
        req.setTransactionType("subscription");
        req.setStatus("Open");
        req.setPaymentType("Paymentgateway");
        req.setRemarks("test");
        req.setEventId("");
        req.setEventName("");
        req.setSubscriptionId(tempSelectedSubscription.getDocId());
        req.setSubscriptionName(tempSelectedSubscription.getSubscriptionName());
        req.setSubscriptionType(tempSelectedSubscription.getSubscriptionType());
        req.setSubscriptionTypeId(subscriptionTypeId);
        req.setDuration(tempSelectedSubscription.getDuration());

        Paymentdetails paymentdetails = new Paymentdetails();
        paymentdetails.setAmount(tempSelectedSubscription.getPaymentdetails().getAmount());
        paymentdetails.setCurrency(tempSelectedSubscription.getPaymentdetails().getCurrency());
        paymentdetails.setTotalamount(tempSelectedSubscription.getPaymentdetails().getTotalamount());
        paymentdetails.setDiscount(tempSelectedSubscription.getPaymentdetails().getDiscount());
        paymentdetails.setGatewaycharge(tempSelectedSubscription.getPaymentdetails().getGatewaycharge());
        paymentdetails.setGatewaypercentage(tempSelectedSubscription.getPaymentdetails().getGatewaypercentage());
        paymentdetails.setServicecharge(tempSelectedSubscription.getPaymentdetails().getServicecharge());
        req.setPaymentdetails(paymentdetails);

        Log.e("user trans req object", req.toString());

        Call<UserTransactionCreateResponse> userTransactionCreate = RestApi.get().getRestService().userTransactionCreate(req);
        userTransactionCreate.enqueue(new Callback<UserTransactionCreateResponse>() {
            @Override
            public void onResponse(Call<UserTransactionCreateResponse> call, Response<UserTransactionCreateResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {
                        paytmIntegration(response.body().getData());
                    } else {

                    }


                } else {

                }
            }

            @Override
            public void onFailure(Call<UserTransactionCreateResponse> call, Throwable t) {
                Helper.showToast(getActivity(), "Network problem");
            }
        });
    }

    public void paytmIntegration(final Data data) {
        new PostParamsAsynTask(data).execute();
    }


    class PostParamsAsynTask extends AsyncTask<Void, Void, Void> {
        Data data1;
        String email, mobile_no, amount;

        public PostParamsAsynTask(final Data data) {
            this.data1 = data;

            SharedPreferences sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
            String userEmail = sp.getString("email", "");
            String userMobile = sp.getString("mobile", "");
            if (userEmail == null || userEmail.equalsIgnoreCase("")) {
                userEmail = "info@goldslamsports.com";
                email = "info@goldslamsports.com";
            } else {

            }

            if (userMobile == null || userMobile.equalsIgnoreCase("")) {
                userMobile = "7330944544";
                mobile_no = "7330944544";
            } else {

            }
            String totalAmout = data.getPaymentdetails().getTotalamount();
            amount = totalAmout;

        }

        @Override
        protected Void doInBackground(Void... params) {

            String orderId = data1.getOrderid();//"ORD1899";
            String custID = data1.getUserdocId();
            //"48";

            Log.e("email", orderId);
            Log.e("mobile", custID);

            /*String postParameters = "MID=GOLDSL51183304129113&ORDER_ID=" + orderId + "&CUST_ID=" + custID + "&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=v&TXN_AMOUNT=" + amount + "&WEBSITE=GOLDSWAP&CALLBACK_URL=https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp&EMAIL=" + email + "&MOBILE_NO=" + mobile_no;
            String url = "http://goldslamsports.com/paytmios/PaytmgenerateChecksum.php";*/

            //String postParameters = "MID=GOLDSL51183304129113&ORDER_ID=ORD97885&CUST_ID=CUST225&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=v&TXN_AMOUNT=1&WEBSITE=GOLDSWAP&CALLBACK_URL=https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp&EMAIL=test@gmail.com&MOBILE_NO=9160125353";
            //String postParameters = "MID=GOLDSL51183304129113&ORDER_ID=" + orderId + "&CUST_ID=" + custID + "&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=WAP&TXN_AMOUNT=" + amount + "&WEBSITE=GOLDSWAP&CALLBACK_URL=https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp&EMAIL=" + email + "&MOBILE_NO=" + mobile_no;
            String postParameters = "MID=GOLDSL51183304129113&ORDER_ID=" + orderId + "&CUST_ID=" + custID + "&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=WAP&TXN_AMOUNT=" + amount + "&WEBSITE=GOLDSWAP&CALLBACK_URL=https://goldslamsports.com/paytmios/PaytmgenerateChecksum.php&EMAIL=" + email + "&MOBILE_NO=" + mobile_no;

            String url = "https://goldslamsports.com/paytmios/PaytmgenerateChecksum.php";

            Log.e("postParams", postParameters);

            HttpURLConnection urlConnection = null;
            try {
                // create connection
                URL urlToRequest = new URL(url);
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                // handle POST parameters
                if (postParameters != null) {

                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setFixedLengthStreamingMode(
                            postParameters.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");

                    //send the POST out
                    PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
                    out.print(postParameters);
                    out.close();
                }

                // handle issues
                int statusCode = urlConnection.getResponseCode();
                if (statusCode != HttpURLConnection.HTTP_OK) {
                    // throw some exception
                }

                InputStream in =
                        new BufferedInputStream(urlConnection.getInputStream());

                JSONObject jsonObject = new JSONParser(in).getJsonObject();

                try {
                    System.out.println("response is::::" + jsonObject.toString());
                    String checksumHash = jsonObject.getString("CHECKSUMHASH").toString();
                    paytmIntegrationEx(jsonObject, data1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (MalformedURLException e) {
                // handle invalid URL
            } catch (SocketTimeoutException e) {
                // hadle timeout
            } catch (IOException e) {
                // handle I/0
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void paytmIntegrationEx(JSONObject jsonObject, final Data data) throws JSONException {
        Map<String, String> retMap = new HashMap<String, String>();

        if (jsonObject != JSONObject.NULL) {
            retMap = Utils.toMap(jsonObject);
        }

        PaytmPGService Service = PaytmPGService.getProductionService();
        PaytmOrder Order = new PaytmOrder(retMap);
        Service.initialize(Order, null);
        Service.startPaymentTransaction(getActivity(), true, true,
                new PaytmPaymentTransactionCallback() {

                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {

                        userTransactionUpdate(data, "", inErrorMessage);
                    }

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction : " + inResponse);

                        if ("01".equalsIgnoreCase(inResponse.getString("RESPCODE"))) {
                            userSubscription(data, inResponse);
                        } else {
                            userTransactionUpdate(data, inResponse.getString("BANKTXNID"), inResponse.getString("RESPMSG"));
                        }
                    }

                    @Override
                    public void networkNotAvailable() {

                        userTransactionUpdate(data, "", "networkNotAvailable");
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {


                        userTransactionUpdate(data, "", inErrorMessage);
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {
                        userTransactionUpdate(data, "", inErrorMessage);
                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        // TODO Auto-generated method stub

                        userTransactionUpdate(data, "", "You pressed back button ,please try again");
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);

                        userTransactionUpdate(data, inResponse.getString("BANKTXNID"), inResponse.getString("RESPMSG"));
                    }

                });
    }

    private void paytmIntegration1(String checksumHash, final Data data) {


        //service = PaytmPGService.getStagingService(); //for testing environment
        service = PaytmPGService.getProductionService(); //for production environment
        String userEmail = sp.getString("email", "");
        String userMobile = sp.getString("mobile", "");

        if (userEmail == null || userEmail.equalsIgnoreCase("")) {
            userEmail = "info@goldslamsports.com";
        }

        if (userMobile == null || userMobile.equalsIgnoreCase("")) {
            userMobile = "7330944544";
        }
        //String checksumSignin_url = "http://gss.goldslamsports.com/generateChecksum.php?ORDER_ID=" + data.getOrderid() + "&TXN_AMOUNT=2";
        String totalAmout = data.getAmount();//data.getPaymentdetails().getTotalamount();
        // String checksumSignin_url = "http://gss.goldslamsports.com/generateChecksum.php?ORDER_ID=" + data.getOrderid() + "&CUST_ID=" + data.getUserdocId() + "&TXN_AMOUNT=" + data.getAmount() + "&EMAIL=" + userEmail + "&MOBILE_NO=" + userMobile;
        //String checksumSignin_url = "http://gss.goldslamsports.com/generateChecksum.php?ORDER_ID=" + data.getOrderid() + "&CUST_ID=" + data.getUserdocId() + "&TXN_AMOUNT=" + totalAmout + "&EMAIL=" + userEmail + "&MOBILE_NO=" + userMobile;
        String checksumSignin_url = "https://goldslamsports.com/paytmandroid/generateChecksum.php?ORDER_ID=" + data.getOrderid() + "&CUST_ID=" + data.getUserdocId() + "&TXN_AMOUNT=" + totalAmout + "&EMAIL=" + userEmail + "&MOBILE_NO=" + userMobile;
        // String checksumValidation_url = "http://gss.goldslamsports.com/verifyChecksum.php";
        String checksumValidation_url = "https://goldslamsports.com/paytmandroid/verifyChecksum.php";
        Log.e("signin_url", checksumSignin_url);
        PaytmMerchant merchant = new PaytmMerchant(checksumSignin_url, checksumValidation_url);


        Map<String, String> paramMap = new HashMap<>();
        // paramMap.put("MID", "GOLDSL30257208217900"); //dev
        paramMap.put("MID", "GOLDSL51183304129113"); //prod
        paramMap.put("ORDER_ID", data.getOrderid());
        paramMap.put("CUST_ID", data.getUserdocId());
        //paramMap.put("INDUSTRY_TYPE_ID", "Retail"); //dev
        paramMap.put("INDUSTRY_TYPE_ID", "Retail109"); //prod
        paramMap.put("CHANNEL_ID", "WAP");
        // paramMap.put("TXN_AMOUNT", data.getAmount());
        paramMap.put("TXN_AMOUNT", totalAmout);
        // paramMap.put("WEBSITE", "APP_STAGING"); //dev
        paramMap.put("WEBSITE", "GOLDSLWAP"); //prod
        //paramMap.put("CALLBACK_URL" , "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp");//prod
        paramMap.put("CALLBACK_URL", "https://goldslamsports.com/paytmandroid/verifyChecksum.php");//prod
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("EMAIL", userEmail);
        paramMap.put("MOBILE_NO", userMobile);



       /* PaytmOrder order = new PaytmOrder(paramMap);

        service.initialize(order, merchant, null);
        service.startPaymentTransaction(getActivity(), true, true,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void onTransactionSuccess(Bundle bundle) {
                        Log.i("Success", "onTransactionSuccess :" + bundle);

                        if ("TXN_SUCCESS".equalsIgnoreCase(bundle.getString("STATUS"))) {
                            userSubscription(data, bundle);
                        }
                    }

                    @Override
                    public void onTransactionFailure(String s, Bundle bundle) {
                        userTransactionUpdate(data, bundle.getString("BANKTXNID"),bundle.getString("RESPMSG"));
                    }


                    @Override
                    public void networkNotAvailable() {
                        Log.i("Failure", "networkNotAvailable");
                        userTransactionUpdate(data, "","networkNotAvailable");
                    }

                    @Override
                    public void clientAuthenticationFailed(String s) {
                        Log.i("Failure", "clientAuthenticationFailed " + s);
                        userTransactionUpdate(data, "",s);
                    }

                    @Override
                    public void someUIErrorOccurred(String s) {
                        Log.i("Failure", "someUIErrorOccurred " + s);
                        Toast.makeText(getActivity(), s + "Please try again!!!!", Toast.LENGTH_SHORT).show();
                        userTransactionUpdate(data, "",s);
                    }

                    @Override
                    public void onErrorLoadingWebPage(int i, String s, String s1) {
                        Log.i("Failure", "onErrorLoadingWebPage" + s + " " + s1);
                        userTransactionUpdate(data, "",s);
                    }

                    @Override
                    public void onBackPressedCancelTransaction() {
                        userTransactionUpdate(data, "","You pressed back button ,please try again");

                    }
                });
*/

    }


    private void userSubscription(Data data, final Bundle bundle) {
        String subscriptionTypeId = null;

        int index = 0;

        if (subscriptionTypes.size() > 0) {
            for (int i = 0; i < subscriptionTypes.size(); i++) {
                if (TextUtils.equals(subscriptionTypes.get(i).getName(), tempSelectedSubscription.getSubscriptionType())) {
                    index = i;
                }
            }
        }
        subscriptionTypeId = subscriptionTypes.get(index).getValue();

        UserSubscriptionCreateRequest req = new UserSubscriptionCreateRequest();
        req.setTransactionId(!TextUtils.isEmpty(data.getTransactionDocId()) ? data.getTransactionDocId() : "");
        req.setUserdocId(preferences.getString("registrationDocId", ""));
        req.setName(preferences.getString("name", ""));
        req.setGssId(preferences.getString("gssid", ""));
        req.setSubscriptionFee(tempSelectedSubscription.getSubscriptionFee());
        req.setDiscount(tempSelectedSubscription.getPaymentdetails().getDiscount());
        req.setPaymentAmount(tempSelectedSubscription.getSubscriptionFee());
        req.setSubscriptionId(tempSelectedSubscription.getDocId());
        req.setSubscriptionName(tempSelectedSubscription.getSubscriptionName());
        req.setSubscriptionType(tempSelectedSubscription.getSubscriptionType());
        req.setSubscriptionTypeId(subscriptionTypeId);
        req.setDuration(tempSelectedSubscription.getDuration());
        req.setRemarks("");
        req.setGatewayTransactionId(bundle.getString("BANKTXNID"));

        Call<UserSubscriptionCreateResponse> userSubscriptionCreate = RestApi.get().getRestService().userSubscriptionCreate(req);
        userSubscriptionCreate.enqueue(new Callback<UserSubscriptionCreateResponse>() {
            @Override
            public void onResponse(Call<UserSubscriptionCreateResponse> call, Response<UserSubscriptionCreateResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null && response.body().getData().getMessage() != null) {

                        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Subscription Payment Status "); //Set Alert dialog title here
                        //alert.setMessage("Payment completed successfully"); //Message here
                        String expiryDate = "";
                        if (response.body().getData().getExpiryDate() != null) {
                            expiryDate = response.body().getData().getExpiryDate();
                        } else {
                            expiryDate = "";
                        }


                        String message = "Thanks for your subscriptions. Your subscription will expire on" + "\n " + expiryDate;//bundle.getString("RESPMSG");
                        alert.setMessage(message); //Message here

                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if ("mysubscriptions".equalsIgnoreCase(strrrr)) {
                                    listener.notifySubscriptions("s");
                                } else if ("tournaments".equalsIgnoreCase(strrrr)) {
                                    sp.edit().putBoolean("subscriptionFlag", false).commit();
                                    listener1.notifyTournament("s");
                                } else if ("pricing".equalsIgnoreCase(strrrr)) {
                                    listener2.notifyPricing("s");
                                }
                                dismiss();
                                dialog.dismiss();

                            } // End of onClick(DialogInterface dialog, int whichButton)
                        }); //End of alert.setPositiveButton

                        AlertDialog alertDialog = alert.create();
                        alertDialog.show();

                        // progressDialog.dismiss();

                    }
                } else {
                    //  progressDialog.dismiss();
                    dismiss();
                }
            }

            @Override
            public void onFailure(Call<UserSubscriptionCreateResponse> call, Throwable t) {
                // progressDialog.dismiss();
            }
        });

    }

    private void userTransactionUpdate(Data data, final String gatewayTransactionID, final String responseMsg) {

        int index = 0;
        String subscriptionTypeId = null;

        if (subscriptionTypes.size() > 0) {
            for (int i = 0; i < subscriptionTypes.size(); i++) {
                if (TextUtils.equals(subscriptionTypes.get(i).getName(), tempSelectedSubscription.getSubscriptionType())) {
                    index = i;
                }
            }
        }
        Log.e("selected type", subscriptionTypes.get(index).getValue());
        subscriptionTypeId = subscriptionTypes.get(index).getValue();


        UserTransactionUpdateRequest req = new UserTransactionUpdateRequest();
        req.setTransactionDocId(!TextUtils.isEmpty(data.getTransactionDocId()) ? data.getTransactionDocId() : "");
        req.setUserdocId(sp.getString("registrationDocId", ""));
        req.setName(sp.getString("name", ""));
        req.setGssId(sp.getString("gssid", ""));
        req.setPaymentAmount(tempSelectedSubscription.getSubscriptionFee());
        req.setDiscount("0");
        req.setPaymentAmount(data.getAmount());
        req.setTransactionType("subscription");
        req.setStatus("Failed");
        req.setPaymentType("Paymentgateway");
        req.setGatewayTransactionId(gatewayTransactionID);
        req.setOrderid(!TextUtils.isEmpty(data.getOrderid()) ? data.getOrderid() : "");
        req.setRemarks("");
        req.setEventId(data.getEventId());
        req.setEventName(data.getEventName());
        req.setSubscriptionId(tempSelectedSubscription.getDocId());
        req.setSubscriptionName(tempSelectedSubscription.getSubscriptionName());
        req.setSubscriptionType(tempSelectedSubscription.getSubscriptionType());
        req.setSubscriptionTypeId(subscriptionTypeId);
        req.setDuration(tempSelectedSubscription.getDuration());


        Call<UserTransactionUpdateResponse> userTransactionUpdate = RestApi.get().getRestService().userTransactionUpdate(req);
        userTransactionUpdate.enqueue(new Callback<UserTransactionUpdateResponse>() {
            @Override
            public void onResponse(Call<UserTransactionUpdateResponse> call, Response<UserTransactionUpdateResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {

                        // preferences.edit().putBoolean("auth", true).commit();

                        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                        alert.setTitle("Subscription Payment Status"); //Set Alert dialog title here
                        String message = "";
                        if ("PENDING".equalsIgnoreCase(responseMsg)) {
                            message = "pending transaction,please try again";
                        } else {
                            message = responseMsg;
                        }

                        alert.setMessage(message); //Message here

                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                Log.e("str is", strrrr);

                                if ("mysubscriptions".equalsIgnoreCase(strrrr)) {
                                    listener.notifySubscriptions("s");
                                } else if ("tournaments".equalsIgnoreCase(strrrr)) {
                                    listener1.notifyTournament("s");
                                    sp.edit().putBoolean("subscriptionFlag", true).commit();
                                } else if ("pricing".equalsIgnoreCase(strrrr)) {
                                    listener2.notifyPricing("s");
                                }

                                dialog.dismiss();
                                dismiss();
                            } // End of onClick(DialogInterface dialog, int whichButton)
                        }); //End of alert.setPositiveButton

                        AlertDialog alertDialog = alert.create();
                        alertDialog.show();

                    } else {
                        dismiss();
                    }


                } else {
                    dismiss();
                }
            }

            @Override
            public void onFailure(Call<UserTransactionUpdateResponse> call, Throwable t) {
                Helper.showToast(activity, "Network problem");
                dismiss();
            }
        });


    }
}
