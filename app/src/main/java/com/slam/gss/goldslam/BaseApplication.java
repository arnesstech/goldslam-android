package com.slam.gss.goldslam;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.slam.gss.goldslam.network.RestApi;

/**
 * Created by Ramesh on 17/8/16.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //Initialise rest api
        RestApi.init(this);
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
