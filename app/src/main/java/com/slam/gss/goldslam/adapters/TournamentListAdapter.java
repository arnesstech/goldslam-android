package com.slam.gss.goldslam.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.slam.gss.goldslam.PlayerScreenActivity;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.TournamentsListener;
import com.slam.gss.goldslam.fragments.SubscriptionFragment;
import com.slam.gss.goldslam.fragments.TournamentInfoFragment;
import com.slam.gss.goldslam.fragments.ViewTournamentFragmentNew;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.helpers.JSONParser;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.models.Events.Events;
import com.slam.gss.goldslam.models.Login.LoginResponse;
import com.slam.gss.goldslam.models.Paymentdetails;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.Data;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.UserTransactionCreateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionCreate.UserTransactionCreateResponse;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate.UserTransactionUpdateRequest;
import com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate.UserTransactionUpdateResponse;
import com.slam.gss.goldslam.models.TournamentEventsModel;
import com.slam.gss.goldslam.models.Tournaments.Tournaments;
import com.slam.gss.goldslam.network.RestApi;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 8/8/2016.
 */
public class TournamentListAdapter extends BaseExpandableListAdapter {

    String rankString = "", pointsString = "", registratinNoString = "", patnerDocid = "", patnerRank = "", patnerPoints = "", patnerRegId = "";

    private Context _context;

    private SharedPreferences sp;
    private SharedPreferences.Editor edit;

    private List<Tournaments> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Events>> _listDataChild;
    //    private List<Tournaments> _tournaments;
    private String eventDocId = "";
    private Dialog entryDialog;
    private Dialog withdrawDialog;
    EditText rankEditText, pointsEditText, registrationNoEditText, patnerRankEditText, patnerPointsEditText, patnerRegistrationNoEditText, patnerNameEditText;
    TextView closeDialogTextView;
    Spinner patnerNameSpinner;
    LinearLayout patnerLayout;
    AutoCompleteTextView autoCompleteTextView;
    List<String> docIds = new ArrayList();
    List<String> docIds1 = new ArrayList();
    List<String> users = new ArrayList();
    List<String> userAistaNo = new ArrayList();
    List<String> userAitaNo = new ArrayList();
    Map<String, List<String>> cacheMapUsers = new HashMap<>();
    Map<String, List<String>> cacheMapdocIds = new HashMap<>();
    ArrayAdapter<String> adapter;
    private ProgressDialog progressDialog;
    private int randomInt = 0;
    private PaytmPGService service = null;
    private TournamentsListener listener;
    private Activity activity = null;
    public ViewTournamentFragmentNew fragment = null;


    public TournamentListAdapter(Context context, List<Tournaments> listDataHeader, HashMap<String, List<Events>> listChildData,
                                 List<Tournaments> tournaments, ViewTournamentFragmentNew fragment, Activity activity) {
        this.activity = activity;
        this._context = context;
        sp = _context.getSharedPreferences("GSS", Context.MODE_PRIVATE);
        edit = sp.edit();
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
//        this._tournaments = tournaments;
        this.fragment = fragment;
        listener = fragment;
    }

    public void setListener(TournamentsListener listener) {
        this.listener = listener;
    }


    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        try {

            return this._listDataChild.get(this._listDataHeader.get(groupPosition).getTournamentName()).size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public Tournaments getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public Events getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition).getTournamentName()).get(childPosititon);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {
        final Tournaments groupItem = getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.view_tournament_header, null);
        }
        TextView tournamentName = (TextView) convertView.findViewById(R.id.txt_tournament_name);
        TextView address = (TextView) convertView.findViewById(R.id.txt_tournament_address);
        TextView txtTournamentDate = (TextView) convertView.findViewById(R.id.tournament_date);
        String _collegeName = groupItem.getTournamentName();
        tournamentName.setText(_collegeName);
        address.setText(groupItem.getLocation());

        final LinearLayout llHeaderLayout = (LinearLayout) convertView.findViewById(R.id.ll_header_layout);

        if ((TextUtils.isEmpty(groupItem.getFromDate())) && (TextUtils.isEmpty(groupItem.getToDate()))) {
            txtTournamentDate.setText("");
        } else {
            String tournamentDate = groupItem.getFromDate() + " - " + groupItem.getToDate();
            txtTournamentDate.setText(tournamentDate);
        }

        ImageView infoImageView = (ImageView) convertView.findViewById(R.id.iv_tournament_info);

        infoImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                TournamentInfoFragment dialogFragment = TournamentInfoFragment.newInstance(groupItem.getDocId());
                dialogFragment.setListener(fragment);
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);

                dialogFragment.show(fragment.getChildFragmentManager(), "Tournament Info Fragment");

            }
        });

        View ll_header_layout = convertView.findViewById(R.id.ll_header_layout);
        ll_header_layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (_listDataChild.get(_listDataHeader.get(groupPosition).getTournamentName()).size() == 0) {
                    Helper.showToast(_context, "No events");
                }

                if (isExpanded) {
                    llHeaderLayout.setBackgroundColor(_context.getResources().getColor(R.color.expanded_tournament_header));
                    ((ExpandableListView) parent).collapseGroup(groupPosition);
                } else {
                    llHeaderLayout.setBackgroundColor(_context.getResources().getColor(R.color.collapse_tournament_header));

                    ((ExpandableListView) parent).expandGroup(groupPosition, true);
                }


            }
        });


//        }


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View convertView, ViewGroup viewGroup) {

        final Events selectedEvent = getChild(groupPosition, childPosition);
        final Tournaments selctedTournament = (Tournaments) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView eventsTextView = (TextView) convertView.findViewById(R.id.event_textview);
        TextView startTimeTextView = (TextView) convertView.findViewById(R.id.start_time_textview);
        TextView endTimeTextView = (TextView) convertView.findViewById(R.id.end_time_textview);
        TextView paidTextView = (TextView) convertView.findViewById(R.id.txt_paid);

        final Button registerButton = (Button) convertView.findViewById(R.id.register_button);
        final TextView txtWithdrawn = (TextView) convertView.findViewById(R.id.txt_withdrawn);
        final Button payActionButton = (Button) convertView.findViewById(R.id.pay_button);
        Button viewButton = (Button) convertView.findViewById(R.id.view_button);


        registerButton.setText(selectedEvent.getAction());
        String payAction = selectedEvent.getPayAction();

        if (!TextUtils.isEmpty(payAction)) {
            if (TextUtils.equals("PAY", payAction)) {
                payActionButton.setText("PAY");
                payActionButton.setVisibility(View.VISIBLE);
                paidTextView.setVisibility(View.GONE);

            } else if (TextUtils.equals("PAID", payAction)) {
                paidTextView.setText("PAID");
                paidTextView.setVisibility(View.VISIBLE);
                payActionButton.setVisibility(View.GONE);

            } else {
                paidTextView.setVisibility(View.GONE);
                payActionButton.setVisibility(View.GONE);
            }
        } else {
            payActionButton.setVisibility(View.GONE);
            paidTextView.setVisibility(View.GONE);
        }

        payActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals("PAY", payActionButton.getText().toString())) {
                    // showPayDialog(mTournamentEventsModel);
                    showPayDialog(selectedEvent);
                }
            }
        });

        //Fixed issue on scrolling
        registerButton.setVisibility(View.VISIBLE);
        if (selectedEvent.getAction().equalsIgnoreCase("ENTRY")) {
            registerButton.setText("ENTRY");
            txtWithdrawn.setVisibility(View.GONE);
        } else if (selectedEvent.getAction().equalsIgnoreCase("WITHDRAWN")) {
            registerButton.setText("WITHDRAWN");
            registerButton.setVisibility(View.GONE);
            txtWithdrawn.setVisibility(View.VISIBLE);
        } else if (selectedEvent.getAction().equalsIgnoreCase("WITHDRAW")) {
            registerButton.setText("WITHDRAW");
            registerButton.setVisibility(View.VISIBLE);
            txtWithdrawn.setVisibility(View.GONE);
        } else {
            registerButton.setVisibility(View.INVISIBLE);
            txtWithdrawn.setVisibility(View.GONE);
        }

        Log.e("payAction ", selectedEvent.getPayAction());

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Event Doc Id", selectedEvent.getDocId());
                eventDocId = selectedEvent.getDocId();
                if (registerButton.getText().toString().equalsIgnoreCase("ENTRY")) {
                    //showRegistrationDialog(tournament, mTournamentEventsModel);
                    showRegistrationDialog(selectedEvent);

                    /*RegisterEventFragment dialogFragment = RegisterEventFragment.newInstance(selectedEvent);
                    dialogFragment.setListener(fragment);
                    dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialogFragment.show(fragment.getChildFragmentManager(), "Association Filter Fragment");
                    */
                } else if (registerButton.getText().toString().equalsIgnoreCase("WITHDRAW")) {

                    if ("PAID".equalsIgnoreCase(selectedEvent.getPayAction())) {
                        String str = "Are you sure you want to withdraw from the tournament? You entry fee will be refunded within 3 to 4 working days based on terms & conditions.";
                        showWithdrawDialog(selectedEvent, str);
                    } else {
                        showWithdrawDialog(selectedEvent, "Do you want to withdraw from tournament");
                    }


                } else if (registerButton.getText().toString().equalsIgnoreCase("WITHDRAWN")) {

                } else if (registerButton.getText().toString() == null || registerButton.getText().toString().isEmpty()) {

                }
            }
        });

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sp.getBoolean("subscriptionFlag", true)) {
                    SubscriptionFragment dialogFragment = SubscriptionFragment.newInstance(activity, "tournament");
                    dialogFragment.setListener1(fragment);
                    FragmentActivity activity = (FragmentActivity) (_context);
                    FragmentManager fm = activity.getSupportFragmentManager();
                    dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialogFragment.show(fm, "SubscriptionFragment");
                } else {
                    Intent intent = new Intent(_context, PlayerScreenActivity.class);
                    intent.putExtra("docId", selectedEvent.getDocId());
                    intent.putExtra("eventName", selectedEvent.getEventName());
                    intent.putExtra("publishEntries", selectedEvent.getPublishEntries());
                    intent.putExtra("showToggle", Boolean.parseBoolean(selectedEvent.getShowToggle()));
                    intent.putExtra("eventStatus", selectedEvent.getStatus());
                    _context.startActivity(intent);
                }

            }
        });

        eventsTextView.setText(selectedEvent.getEventName());
        startTimeTextView.setText(selectedEvent.getSigninStartDateTime());
        endTimeTextView.setText(selectedEvent.getSigninEndDateTime());
        return convertView;
    }


    // private void showPayDialog(final TournamentEventsModel mTournamentEventsModel) {
    private void showPayDialog(final Events mTournamentEventsModel) {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(_context);
        alertDialogBuilder.setTitle("Entry Fee Payment");
        StringBuilder builder = new StringBuilder();

        //Tournament Name
        builder.append(" Tournament Name: ");
        builder.append("<b>" + mTournamentEventsModel.getTournamentName());
        builder.append("</b><br><br>");

        String s = mTournamentEventsModel.getPaymentdetails().getCurrency();//"\u20B9";

        builder.append("Entry fee : ");
        builder.append("<b>" + s + " " + mTournamentEventsModel.getPaymentdetails().getAmount());
        builder.append("</b><br><br>");
        //service charge
        builder.append("Service Charge : ");
        String serviceCharge = mTournamentEventsModel.getPaymentdetails().getServicecharge() != null ? mTournamentEventsModel.getPaymentdetails().getServicecharge() : "";
        builder.append("<b>" + mTournamentEventsModel.getPaymentdetails().getServicecharge());
        builder.append("</b><br><br>");

        //gateway charge
        builder.append("Gateway Charge : ");
        String gatewayCharge = mTournamentEventsModel.getPaymentdetails().getGatewaycharge() != null ? mTournamentEventsModel.getPaymentdetails().getGatewaycharge() : "";
        builder.append("<b>" + gatewayCharge);
        builder.append("</b><br><br>");

        //Total amount
        builder.append("Total Amount : ");
        String total = mTournamentEventsModel.getPaymentdetails().getTotalamount() != null ? mTournamentEventsModel.getPaymentdetails().getTotalamount() : "";
        builder.append("<b>" + total);
        builder.append("</b><br><br>");


        builder.append("Would You like to pay ?");
        builder.append("<br>");


        alertDialogBuilder.setMessage(Html.fromHtml(builder.toString()));


        // alertDialogBuilder.setMessage("Would you like to pay entry fee " + _context.getResources().getString(R.string.Rs) + " " + mTournamentEventsModel.getEntryFee() + " towards " + mTournamentEventsModel.getEventName());
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        entryFreePay(mTournamentEventsModel);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void entryFreePay(final Events mTournamentEventsModel) {
        progressDialog = ProgressDialog.show(_context, "", "Loading....", true);

        Paymentdetails paymentdetails = new Paymentdetails();

        UserTransactionCreateRequest req = new UserTransactionCreateRequest();
        req.setAmount(mTournamentEventsModel.getPaymentdetails().getTotalamount());
        req.setPaymentAmount(mTournamentEventsModel.getPaymentdetails().getTotalamount());
        req.setDiscount(mTournamentEventsModel.getPaymentdetails().getDiscount());
        req.setEventId(mTournamentEventsModel.getDocId());
        req.setEventName(mTournamentEventsModel.getEventName());
        req.setGssId(sp.getString("gssid", ""));
        req.setName(sp.getString("name", ""));
        req.setPaymentType("Paymentgateway");
        req.setUserdocId(sp.getString("registrationDocId", ""));
        req.setRemarks("");
        req.setTransactionType("EntryFee");
        req.setStatus("Open");
        paymentdetails.setAmount(mTournamentEventsModel.getPaymentdetails().getAmount());
        paymentdetails.setCurrency(mTournamentEventsModel.getPaymentdetails().getCurrency());
        paymentdetails.setTotalamount(mTournamentEventsModel.getPaymentdetails().getTotalamount());
        paymentdetails.setDiscount(mTournamentEventsModel.getPaymentdetails().getDiscount());
        paymentdetails.setGatewaycharge(mTournamentEventsModel.getPaymentdetails().getGatewaycharge());
        paymentdetails.setGatewaypercentage(mTournamentEventsModel.getPaymentdetails().getGatewaypercentage());
        paymentdetails.setServicecharge(mTournamentEventsModel.getPaymentdetails().getServicecharge());
        req.setPaymentdetails(paymentdetails);


        Call<UserTransactionCreateResponse> userTransactionCreate = RestApi.get().getRestService().userTransactionCreate(req);
        userTransactionCreate.enqueue(new Callback<UserTransactionCreateResponse>() {
            @Override
            public void onResponse(Call<UserTransactionCreateResponse> call, Response<UserTransactionCreateResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {

                        if (!TextUtils.isEmpty(response.body().getData().getTransactionDocId())) {

                            new PostParamsAsynTask(mTournamentEventsModel, response.body().getData()).execute();
                        } else {

                        }


                    } else {
                    }


                } else {
                }
            }

            @Override
            public void onFailure(Call<UserTransactionCreateResponse> call, Throwable t) {
                Helper.showToast(_context, "Network problem");
            }
        });
    }

    private void updateEntryFeePay(final Events mTournamentEventsModel, Data data, final String bankId, final String responseMsg, final int i, String s) {

        Log.e("getway ID:::: " + "", bankId + "flag value is:::::::" + i);
        UserTransactionUpdateRequest req = new UserTransactionUpdateRequest();
        req.setTransactionDocId(!TextUtils.isEmpty(data.getTransactionDocId()) ? data.getTransactionDocId() : "");
        req.setUserdocId(sp.getString("registrationDocId", ""));
        req.setName(sp.getString("name", ""));
        req.setGssId(sp.getString("gssid", ""));
        req.setAmount(mTournamentEventsModel.getEntryFee());
        req.setDiscount("0");
        req.setPaymentAmount(mTournamentEventsModel.getEntryFee());
        req.setTransactionType("EntryFee");
        if (i == 0) {
            req.setStatus("Completed");
            req.setGatewayTransactionId(bankId);
        } else {
            req.setStatus("Failed");
            req.setGatewayTransactionId(bankId);
        }

        req.setPaymentType("Paymentgateway");

        Paymentdetails details = new Paymentdetails();
        details.setTotalamount(mTournamentEventsModel.getPaymentdetails().getTotalamount());
        details.setServicecharge(mTournamentEventsModel.getPaymentdetails().getServicecharge());
        details.setDiscount(mTournamentEventsModel.getPaymentdetails().getDiscount());
        details.setAmount(mTournamentEventsModel.getPaymentdetails().getAmount());
        details.setDiscount(mTournamentEventsModel.getPaymentdetails().getDiscount());
        details.setGatewaycharge(mTournamentEventsModel.getPaymentdetails().getGatewaycharge());
        details.setGatewaypercentage(mTournamentEventsModel.getPaymentdetails().getGatewaypercentage());
        details.setCurrency(mTournamentEventsModel.getPaymentdetails().getCurrency());


        req.setOrderid(!TextUtils.isEmpty(data.getOrderid()) ? data.getOrderid() : "");
        req.setRemarks("");
        req.setEventId(mTournamentEventsModel.getDocId());
        req.setEventName(mTournamentEventsModel.getEventName());
        req.setSubscriptionId("");
        req.setSubscriptionName("");
        req.setSubscriptionType("");
        req.setSubscriptionTypeId("");

        //Log.e("user entryupdate pay", req.toString());

        Call<UserTransactionUpdateResponse> userTransactionUpdate = RestApi.get().getRestService().userTransactionUpdate(req);
        userTransactionUpdate.enqueue(new Callback<UserTransactionUpdateResponse>() {
            @Override
            public void onResponse(Call<UserTransactionUpdateResponse> call, Response<UserTransactionUpdateResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {
                        progressDialog.dismiss();
                        notifyDataSetChanged();

                        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(_context);
                        alert.setTitle("Entry Fee Payment Status"); //Set Alert dialog title here

                        String resMessage = "";
                        if (i == 0) {
                            resMessage = "Your Entry fee payment successful. Refer Terms & Conditions";
                        } else {
                            resMessage = responseMsg;
                        }
                        alert.setMessage(resMessage); //Message here


                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (i == 0) {
                                    dialog.dismiss();
                                    if (listener != null) {
                                        listener.notifyTournament("s");
                                    }
                                } else {
                                    dialog.dismiss();
                                    if (listener != null) {
                                        listener.notifyTournament("c");
                                    }
                                }

                            } // End of onClick(DialogInterface dialog, int whichButton)
                        }); //End of alert.setPositiveButton

                        android.support.v7.app.AlertDialog alertDialog = alert.create();
                        alertDialog.show();

                    } else {
                        //progressDialog.dismiss();
                    }


                } else {
                    // progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<UserTransactionUpdateResponse> call, Throwable t) {
                Helper.showToast(_context, "Network problem");
                // progressDialog.dismiss();
            }
        });
    }


    class EventsAsyncTask extends AsyncTask<String, Void, String> {
        List<TournamentEventsModel> eventsList;
        ProgressDialog dialog;
        String eventDocId;
        private Context mContextt;
        TournamentEventsModel mTournamentEventsModel;

        public EventsAsyncTask(Context context, String eventDocId, final TournamentEventsModel mTournamentEventsModel) {
            mContextt = context;
            this.eventDocId = eventDocId;
            eventsList = new ArrayList<>();
            this.mTournamentEventsModel = mTournamentEventsModel;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(_context);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Events Data Retrieveing.");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            //  Log.i("GetTournaments url", urls[0]);
            try {
                String url = Connections.BASE_URL + "api/events?tournamentdocid=";
                HttpGet httpGet = new HttpGet(url);
                String credentials = "Basic";
                credentials = credentials + sp.getString("apiToken", "");
                Log.e("apiToken", credentials);
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                httpGet.addHeader("Authorization", credentials);
                httpGet.addHeader("Client-Type", "APP");
                httpGet.addHeader("App-Id", "PROSPORTS");
                httpGet.addHeader("Context", "player");

                Log.e("event url", url);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            if (eventsList != null)
                eventsList.clear();

            String status = null;
            try {
                jsonResponse = new JSONObject(result);

                status = jsonResponse.get("status").toString();

                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Log.i("GetEvents Response", obj.toString());
                    JSONArray jsonEventsArray = obj.getJSONArray("events");
                    if (jsonEventsArray != null) {
                        //TournamentEventsModel mTournamentEventsModel = null;

                        JSONObject eventsJsonObject = null;
                        for (int i = 0; i < jsonEventsArray.length(); i++) {
                            // mTournamentEventsModel = new TournamentEventsModel();
                            eventsJsonObject = jsonEventsArray.getJSONObject(i);

                            mTournamentEventsModel.setEventTeamSize(Integer.parseInt(eventsJsonObject.getString("teamSize")));
                            mTournamentEventsModel.setEventDocId(eventsJsonObject.getString("docId"));
                            mTournamentEventsModel.setEventName(eventsJsonObject.getString("eventName"));
                            mTournamentEventsModel.setSigningStartTime(eventsJsonObject.getString("signinStartDateTime"));
                            mTournamentEventsModel.setSigningEndTime(eventsJsonObject.getString("signinEndDateTime"));
                            mTournamentEventsModel.setEventAction(eventsJsonObject.getString("action"));
                            mTournamentEventsModel.setEventAssociationName(eventsJsonObject.getString("associationName"));
                            mTournamentEventsModel.setEventAssociationId(eventsJsonObject.getString("associationDocId"));
                            mTournamentEventsModel.setEventPublishEntries(eventsJsonObject.getString("publishEntries"));
                            mTournamentEventsModel.setShowToggle(Boolean.parseBoolean(eventsJsonObject.getString("showToggle")));
                            mTournamentEventsModel.setEventStatus(eventsJsonObject.getString("status"));
                            mTournamentEventsModel.setPayAction(eventsJsonObject.getString("payAction"));
                            mTournamentEventsModel.setEntryFee(eventsJsonObject.getString("entryFee"));

                        }
                    }

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());


                }

                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(_context, "Network error");
            }

        }

    }

    class PostParamsAsynTask extends AsyncTask<Void, Void, Void> {
        Data data1;
        String email, mobile_no, amount;
        Events mTournamentObj;

        public PostParamsAsynTask(final Events mTournamentEventsModel, final Data data) {
            this.data1 = data;
            this.mTournamentObj = mTournamentEventsModel;

            SharedPreferences sp = _context.getSharedPreferences("GSS", Context.MODE_PRIVATE);
            String userEmail = sp.getString("email", "");
            String userMobile = sp.getString("mobile", "");
            if (userEmail == null || userEmail.equalsIgnoreCase("")) {
                userEmail = "info@goldslamsports.com";
                email = "info@goldslamsports.com";
            } else {

            }

            if (userMobile == null || userMobile.equalsIgnoreCase("")) {
                userMobile = "7330944544";
                mobile_no = "7330944544";
            } else {

            }
            String totalAmout = data.getPaymentdetails().getTotalamount();
            amount = totalAmout;

        }


        @Override
        protected Void doInBackground(Void... params) {

            String orderId = data1.getOrderid();
            String custID = data1.getUserdocId();

            Log.e("orderID", orderId);
            Log.e("custID", custID);
            Log.e("email", email);
            Log.e("mobile", mobile_no);


            //String postParameters = "MID=GOLDSL51183304129113&ORDER_ID=ORD97885&CUST_ID=CUST225&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=v&TXN_AMOUNT=1&WEBSITE=GOLDSWAP&CALLBACK_URL=https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp&EMAIL=test@gmail.com&MOBILE_NO=9160125353";
            String postParameters = "MID=GOLDSL51183304129113&ORDER_ID=" + orderId + "&CUST_ID=" + custID + "&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=WAP&TXN_AMOUNT=" + amount + "&WEBSITE=GOLDSWAP&CALLBACK_URL=https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp&EMAIL=" + email + "&MOBILE_NO=" + mobile_no;

            String url = "https://goldslamsports.com/paytmios/PaytmgenerateChecksum.php";

            Log.e("postParams", postParameters);

            HttpURLConnection urlConnection = null;
            try {
                // create connection
                URL urlToRequest = new URL(url);
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                // handle POST parameters
                if (postParameters != null) {

                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setFixedLengthStreamingMode(
                            postParameters.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");

                    //send the POST out
                    PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
                    out.print(postParameters);
                    out.close();
                }

                // handle issues
                int statusCode = urlConnection.getResponseCode();
                if (statusCode != HttpURLConnection.HTTP_OK) {
                    // throw some exception
                }

                InputStream in =
                        new BufferedInputStream(urlConnection.getInputStream());

                JSONObject jsonObject = new JSONParser(in).getJsonObject();

                try {
                    System.out.println("response is::::" + jsonObject.toString());
                    String checksumHash = jsonObject.getString("CHECKSUMHASH").toString();
                    paytmIntegrationEx(jsonObject, data1, mTournamentObj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (MalformedURLException e) {
                // handle invalid URL
            } catch (SocketTimeoutException e) {
                // hadle timeout
            } catch (IOException e) {
                // handle I/0
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    class PostParamsAsynTask1 extends AsyncTask<Void, Void, Void> {
        Data data1;
        Events mTournamentEventsModel;
        String email, mobile_no, amount;

        public PostParamsAsynTask1(final Events mTournamentEventsModel, final Data data) {
            this.data1 = data;
            this.mTournamentEventsModel = mTournamentEventsModel;


            SharedPreferences sp = _context.getSharedPreferences("GSS", Context.MODE_PRIVATE);
            String userEmail = sp.getString("email", "");
            String userMobile = sp.getString("mobile", "");
            if (userEmail == null || userEmail.equalsIgnoreCase("")) {
                userEmail = "info@goldslamsports.com";
                email = "info@goldslamsports.com";
            } else {

            }

            if (userMobile == null || userMobile.equalsIgnoreCase("")) {
                userMobile = "7330944544";
                mobile_no = "7330944544";
            } else {

            }
            String totalAmout = data.getPaymentdetails().getTotalamount();
            amount = totalAmout;
        }

        @Override
        protected Void doInBackground(Void... params) {

            String orderId = data1.getOrderid();//"ORD1899";
            String custID = data1.getUserdocId();
            //"48";

            Log.e("orderId is", data1.getOrderid());

            Log.e("email", email + ":::" + orderId);
            Log.e("mobile", mobile_no + ":::" + custID);

            //String postParameters = "MID=GOLDSL51183304129113&ORDER_ID="+data.getOrderid()+"&CUST_ID="+data.getUserdocId()+"&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=v&TXN_AMOUNT="+amount+"&WEBSITE=GOLDSWAP&CALLBACK_URL=https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp&EMAIL="+email+"&MOBILE_NO="+mobile_no;
            String postParameters = "MID=GOLDSL51183304129113&ORDER_ID=" + orderId + "&CUST_ID=" + custID + "&INDUSTRY_TYPE_ID=Retail109&CHANNEL_ID=WAP&TXN_AMOUNT=" + amount + "&WEBSITE=GOLDSWAP&CALLBACK_URL=https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp&EMAIL=" + email + "&MOBILE_NO=" + mobile_no;

            String url = "http://goldslamsports.com/paytmios/PaytmgenerateChecksum.php";

            Log.e("postParams", postParameters);

            HttpURLConnection urlConnection = null;
            try {
                // create connection
                URL urlToRequest = new URL(url);
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                // handle POST parameters
                if (postParameters != null) {

                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setFixedLengthStreamingMode(
                            postParameters.getBytes().length);
                    urlConnection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded");

                    //send the POST out
                    PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
                    out.print(postParameters);
                    out.close();
                }

                // handle issues
                int statusCode = urlConnection.getResponseCode();
                if (statusCode != HttpURLConnection.HTTP_OK) {
                    // throw some exception
                }

                InputStream in =
                        new BufferedInputStream(urlConnection.getInputStream());

                JSONObject jsonObject = new JSONParser(in).getJsonObject();

                try {
                    System.out.println("response is::::" + jsonObject.toString());
                    String checksumHash = jsonObject.getString("CHECKSUMHASH").toString();
                    paytmIntegrationEx(jsonObject, data1, mTournamentEventsModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (MalformedURLException e) {
                // handle invalid URL
            } catch (SocketTimeoutException e) {
                // hadle timeout
            } catch (IOException e) {
                // handle I/0
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void paytmIntegrationEx(JSONObject jsonObject, final Data data, final Events mTournamentEventsModel) throws JSONException {
        Map<String, String> retMap = new HashMap<String, String>();

        if (jsonObject != JSONObject.NULL) {
            retMap = Utils.toMap(jsonObject);
        }

        PaytmPGService Service = PaytmPGService.getProductionService();
        PaytmOrder Order = new PaytmOrder(retMap);
        Service.initialize(Order, null);

        Service.startPaymentTransaction(_context, true, true,
                new PaytmPaymentTransactionCallback() {

                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {

                        updateEntryFeePay(mTournamentEventsModel, data, "", inErrorMessage, 1, "someUIErrorOccurred()");
                    }

                    @Override
                    public void onTransactionResponse(Bundle bundle) {
                        Log.d("LOG", "Payment Transaction : " + bundle);
                        if ("01".equalsIgnoreCase(bundle.getString("RESPCODE"))) {
                            updateEntryFeePay(mTournamentEventsModel, data, bundle.getString("BANKTXNID"), bundle.getString("RESPMSG"), 0, "onTransactionSuccess()");

                        } else {

                            updateEntryFeePay(mTournamentEventsModel, data, bundle.getString("BANKTXNID"), bundle.getString("RESPMSG"), 1, "onTransactionSuccess()");

                        }
                    }

                    @Override
                    public void networkNotAvailable() {

                        updateEntryFeePay(mTournamentEventsModel, data, "", "networkNotAvailable, Please try again", 1, "networkNotAvailable()");
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {


                        updateEntryFeePay(mTournamentEventsModel, data, "", inErrorMessage, 1, "clientAuthenticationFailed()");
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {
                        updateEntryFeePay(mTournamentEventsModel, data, "", inErrorMessage, 1, "onErrorLoadingWebPage()");
                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        // TODO Auto-generated method stub

                        updateEntryFeePay(mTournamentEventsModel, data, "", "You pressed back button ,please try again", 1, "onBackPressedCancelTransaction()");

                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle bundle) {
                        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);

                        updateEntryFeePay(mTournamentEventsModel, data, "", bundle.getString("RESPMSG"), 1, "onTransactionFailure()");
                    }

                });

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    //public void showRegistrationDialog(final Tournament tournament, final TournamentEventsModel mTournamentEventsModel) {
    public void showRegistrationDialog(final Events events) {
        cacheMapUsers.clear();
        cacheMapdocIds.clear();
        Log.e("event teamSize", String.valueOf(events.getTeamSize()));

        entryDialog = new Dialog(_context, R.style.full_screen_dialog);
        //setting custom layout to dialog
        entryDialog.setContentView(R.layout.dialog_event_registration);
        entryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        entryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        rankEditText = (EditText) entryDialog.findViewById(R.id.et_rank);
        pointsEditText = (EditText) entryDialog.findViewById(R.id.et_points);
        registrationNoEditText = (EditText) entryDialog.findViewById(R.id.et_registration_no);
        patnerRankEditText = (EditText) entryDialog.findViewById(R.id.et_patner_rank);
        patnerPointsEditText = (EditText) entryDialog.findViewById(R.id.et_patner_points);
        patnerRegistrationNoEditText = (EditText) entryDialog.findViewById(R.id.et_patner_reg_no);
        autoCompleteTextView = (AutoCompleteTextView) entryDialog.findViewById(R.id.et_patner_name);

        closeDialogTextView = (TextView) entryDialog.findViewById(R.id.close_dialog);
        closeDialogTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rankEditText.getWindowToken(), 0);
                entryDialog.dismiss();
            }
        });

        LoginResponse loginResponse = new Gson().fromJson(sp.getString("loginResponse", ""), LoginResponse.class);

        if (loginResponse.getData() != null && loginResponse.getData().getUserAssociations().length > 0) {
            for (int i = 0; i < loginResponse.getData().getUserAssociations().length; i++) {
                Log.i("User Association Data", loginResponse.getData().getUserAssociations()[i].toString());
                Log.i("AsociationId", loginResponse.getData().getUserAssociations()[i].getAssociationid());
                //Log.i("RegistrationId", loginResponse.getData().getUserAssociations()[i].getRegistrationId());
                Log.i("Event AsociationId", events.getAssociationDocId());

                if (loginResponse.getData().getUserAssociations()[i].getAssociationid().equalsIgnoreCase(events.getAssociationDocId())) {
                    registrationNoEditText.setText(loginResponse.getData().getUserAssociations()[i].getRegistrationId());
                    registrationNoEditText.setText(loginResponse.getData().getUserAssociations()[i].getRegistrationId() == "null" ? " " : loginResponse.getData().getUserAssociations()[i].getRegistrationId());

                } else {
                }
            }

        }

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("user selected pos id is", position + "" + "id:::::::" + id);
                try {
                    // patnerDocid = docIds.get(position);
                    patnerDocid = docIds.get(position);
                    Log.e("selected patner id is", patnerDocid + "----->" + docIds1.get(position));

                } catch (Exception e) {
                    patnerDocid = "";
                    Log.e("exception", e.getMessage());
                }

            }
        });

        patnerLayout = (LinearLayout) entryDialog.findViewById(R.id.patner_layout);

        if (Integer.parseInt(events.getTeamSize()) == 2) {
            patnerLayout.setVisibility(View.VISIBLE);

            autoCompleteTextView.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable editable) {
                    // TODO Auto-generated method stub
                    String key = editable.toString().trim();

                    List<String> list = cacheMapUsers.get(key);
                    if (list != null) {
                        if (adapter != null) {
                            users = new ArrayList<>(list);
                            docIds = new ArrayList<>(cacheMapdocIds.get(key));
                            adapter.clear();
                            adapter.addAll(list);
                            adapter.notifyDataSetChanged();
                        }

                    } else if (!(TextUtils.isEmpty(key))) {
                        String k = key;
                        String temp = k.replaceAll(" ", "%20");
                        new GetUserssyncTask(key).execute(Connections.BASE_URL + "api/user/" + temp);
                    } else {
                        autoCompleteTextView.setError("Please select partner");
                    }


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // TODO Auto-generated method stub

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String newText = s.toString();

                }

            });


            //autoCompleteTextView.setThreshold(1);
            adapter = new ArrayAdapter<String>(_context, R.layout.drop_down_partner, users);
//            adapter = new ArrayAdapter<String>(_context, R.layout.list_item, users);
            autoCompleteTextView.setAdapter(adapter);

        } else {
            Log.i("", "invisible");
            patnerLayout.setVisibility(View.GONE);
            patnerLayout.removeAllViewsInLayout();

        }


        //adding button click event
        Button submitButton = (Button) entryDialog.findViewById(R.id.button_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                // clickOnEntryRegirstrationSubmitButton(tournament, mTournamentEventsModel);
                                                clickOnEntryRegirstrationSubmitButton(events);
                                            }
                                        }

        );
        entryDialog.show();
    }

    // private void clickOnEntryRegirstrationSubmitButton(Tournament tournament, TournamentEventsModel mTournamentEventsModel) {
    private void clickOnEntryRegirstrationSubmitButton(final Events events) {
        /*if (registrationNoEditText.getText().toString().isEmpty()) {
            registrationNoEditText.setError("Please enter registration no");
        } else*/
        {
            rankString = rankEditText.getText().toString();
            pointsString = pointsEditText.getText().toString();
            registratinNoString = registrationNoEditText.getText().toString();

            if (Integer.parseInt(events.getTeamSize()) == 2) {

                if (autoCompleteTextView.getText().toString().isEmpty()) {
                    autoCompleteTextView.setError("please select partner");
                } else {
                    patnerRank = patnerRankEditText.getText().toString();
                    patnerPoints = patnerPointsEditText.getText().toString();
                    patnerRegId = patnerRegistrationNoEditText.getText().toString();

                    InputMethodManager imm = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(patnerRegistrationNoEditText.getWindowToken(), 0);


                    if (sp.getString("registrationDocId", "").equalsIgnoreCase(patnerDocid)) {
                        autoCompleteTextView.setError("Please select other player");
                    } else {
                        //showConfirmationDialog(tournament, mTournamentEventsModel, true);
                        showConfirmationDialog(events, true);
//                        new EventRegistrationAsyncTask(mTournamentEventsModel).execute(Connections.BASE_URL + "entry", String.valueOf(mTournamentEventsModel.getEventTeamSize()));
                        Log.i("patnerDocid", patnerDocid);
                    }
                }
            } else {
                InputMethodManager imm = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(patnerRegistrationNoEditText.getWindowToken(), 0);
                Log.i("patnerRegId", patnerRegId);
                // showConfirmationDialog(tournament, mTournamentEventsModel, false);
                showConfirmationDialog(events, false);
//                new EventRegistrationAsyncTask(mTournamentEventsModel).execute(Connections.BASE_URL + "entry", String.valueOf(mTournamentEventsModel.getEventTeamSize()));
            }

        }
    }

    // private void showConfirmationDialog(Tournament tournament, final TournamentEventsModel eventsModel, final boolean isDoubles) {
    private void showConfirmationDialog(final Events events, final boolean isDoubles) {
        String associationName = events.getAssociationName();
        String tournamentName = events.getTournamentName();
        String eventName = events.getEventName();
//        rankString = rankEditText.getText().toString();
//        pointsString = pointsEditText.getText().toString();
//        registratinNoString = registrationNoEditText.getText().toString();

        StringBuilder builder = new StringBuilder();
        //Association Name
        builder.append("Association Name: <b>");
        builder.append(associationName);
        builder.append("</b><br>");
        //Tournament Name
        builder.append("Tournament Name: <b>");
        builder.append(tournamentName);
        builder.append("</b><br>");
        //Event Name
        builder.append("Event Name: <b>");
        builder.append(eventName);
        builder.append("</b><br>");
        //Player name
        builder.append("Player Name: <b>");
        builder.append("" + sp.getString("name", ""));
        builder.append("</b><br>");
        //Player registerno
        builder.append("Registration No: <b>");
        builder.append(registratinNoString);
        builder.append("</b><br>");
        //Player rank
        builder.append("Rank : <b>");
        builder.append(rankString);
        builder.append("</b><br>");
        //Player Points
        builder.append("Points : <b>");
        builder.append(pointsString);
        builder.append("</b><br>");

        if (isDoubles) {
            //Patner Name
            builder.append("Partner Name: <b>");
            builder.append("" + autoCompleteTextView.getText().toString());
            builder.append("</b><br>");
            //Patner Register No
            builder.append("Partner Registration No: <b>");
            builder.append(patnerRegId);
            builder.append("</b><br>");
            //Patner Rank
            builder.append("Partner Rank : <b>");
            builder.append(patnerRank);
            builder.append("</b><br>");
            //Patner Points
            builder.append("Patner Points : <b>");
            builder.append(patnerPoints);
            builder.append("</b><br>");

        }

        builder.append("<br>");
        builder.append("<br>");
        //Patner Points
        builder.append("Your name will be displayed in the acceptance list once tournament admin approves your entry <b>");
        builder.append("</b><br>");

        AlertDialog.Builder builder1 = new AlertDialog.Builder(_context);
        builder1.setMessage(Html.fromHtml(builder.toString()));
        builder1.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isDoubles) {
                    new EventRegistrationAsyncTask(events).execute(Connections.BASE_URL + "api/entry", String.valueOf(events.getTeamSize()));
                } else {
                    new EventRegistrationAsyncTask(events).execute(Connections.BASE_URL + "api/entry", String.valueOf(events.getTeamSize()));
                }

            }
        });
        builder1.setNegativeButton("Cancel", null);
        builder1.setCancelable(false);
        AlertDialog dialog = builder1.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    class GetUserssyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;
        String key;

        public GetUserssyncTask(String key) {
            this.key = key;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(_context);
            dialog.setMessage("Loading users, please wait");
            dialog.setTitle("Connecting server");
            // dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            Log.i("getusers url", urls[0]);
            try {
                String url = urls[0];
                HttpGet httpGet = new HttpGet(url);
                String apiToken = _context.getSharedPreferences("GSS", Context.MODE_PRIVATE).getString("apiToken", "");

                String base64EncodedapiToken = Base64.encodeToString(apiToken.getBytes(), Base64.NO_WRAP);
                httpGet.addHeader("Authorization", "Basic " + base64EncodedapiToken);

                Log.i("header", "Basic " + base64EncodedapiToken);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                Log.i("usersResponse", responseBody);
                return responseBody;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            //dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    // Helper.showToast(_context, obj.getString("message"));
                    Log.i("httpResponse", obj.toString());

                    JSONArray usersArray = obj.getJSONArray("users");

                    users.clear();
                    docIds.clear();
                    docIds1.clear();

                    users = new ArrayList();
                    docIds = new ArrayList<>();
                    docIds1 = new ArrayList<>();

                    for (int i = 0; i < usersArray.length(); i++) {
                        JSONObject userObj = usersArray.getJSONObject(i);
                        String name = userObj.getString("name");
                        String docId = userObj.getString("docId");

                        if (name != null) {
                            docIds.add(docId);
                            docIds1.add(docId);
                            users.add(name.toLowerCase());
                        }

                    }


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    // Helper.showToast(_context, obj.getString("message"));

                }
                if (adapter != null) {
                    //
                    cacheMapUsers.put(key, users);
                    cacheMapdocIds.put(key, docIds);
                    adapter.clear();
                    adapter.addAll(users);
                    adapter.notifyDataSetChanged();
                    //  autoCompleteTextView.showDropDown();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                // Helper.showToast(_context, e.getMessage());
            }


        }
    }

    class EventRegistrationAsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;
        //TournamentEventsModel mTournamentEventsModel;
        Events mTournamentEventsModel;

/*
        public EventRegistrationAsyncTask(TournamentEventsModel mTournamentEventsModel) {
            this.mTournamentEventsModel = mTournamentEventsModel;
        }
*/

        public EventRegistrationAsyncTask(Events mTournamentEventsModel) {
            this.mTournamentEventsModel = mTournamentEventsModel;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(_context);
            dialog.setMessage("Registering , please wait");


            dialog.setTitle("Register");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {

            try {

                Log.i("eventTeamSize", urls[1]);
                JSONArray jsonArray = new JSONArray();
                JSONObject obj = new JSONObject();
                obj.put("docId", sp.getString("registrationDocId", ""));
                obj.put("rank", rankString);
                obj.put("points", pointsString);
                obj.put("regNo", registratinNoString);
                jsonArray.put(obj);

                if (Integer.parseInt(urls[1]) == 2) {
                    Log.i("If", "if");

                    Log.i("patner Id isss", patnerDocid);
                    JSONObject obj1 = new JSONObject();
                    obj1.put("docId", patnerDocid);
                    obj1.put("rank", patnerRank);
                    obj1.put("points", patnerPoints);
                    obj1.put("regNo", patnerRegId);
                    jsonArray.put(obj1);
                }

                Log.i("playersArray", jsonArray.toString());
                JSONObject entryObject = new JSONObject();
                entryObject.put("eventDocId", eventDocId);
                entryObject.put("players", jsonArray);
                entryObject.put("registerType", "SELF");

                Log.e("entryObj", entryObject.toString());

                HttpClient httpClient = new DefaultHttpClient();
                StringEntity stringEntity = new StringEntity(entryObject.toString());
                String url = urls[0];
                Log.i("url", url);
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpPost.setHeader("Authorization", "Basic " + sp.getString("apiToken", ""));
                httpPost.setEntity(new StringEntity(entryObject.toString()));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                Log.i("httpResponse", responseBody);

                return responseBody;


            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.i("Event entry response", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                //Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    //Toast.makeText(LoginActivity.this,"111111",Toast.LENGTH_SHORT).show();
                    mTournamentEventsModel.setAction("WITHDRAW");
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Helper.showToast(_context, obj.getString("message"));
                    entryDialog.dismiss();
                    notifyDataSetChanged();

                    if (listener != null) {
                        listener.notifyTournament("s");
                    } else {
                        listener.notifyTournament("c");
                    }

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(_context, obj.getString("message"));
                    entryDialog.dismiss();
                    if (listener != null) {
                        listener.notifyTournament("c");
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(_context, "Unable to fetch data from server");
                entryDialog.dismiss();
                if (listener != null) {
                    listener.notifyTournament("c");
                }
            }
        }
    }


    public void showWithdrawDialog(final Events mTournamentEventsModel, String str) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(_context);
        alertDialogBuilder.setTitle("Withdraw From Tournament");
        alertDialogBuilder.setMessage(str);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        new EventWithdrawAsyncTask(mTournamentEventsModel).execute(Connections.EVENT_WITHDRAW);
                    }
                });

        alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void showWithdrawDialog1(final Events mTournamentEventsModel, String str) {

        withdrawDialog = new Dialog(_context, R.style.full_screen_dialog);
        //setting custom layout to dialog
        withdrawDialog.setContentView(R.layout.dialog_withdraw);
        withdrawDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        withdrawDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        //adding button click event
        Button cancelButton = (Button) withdrawDialog.findViewById(R.id.button_cancel);
        Button okButton = (Button) withdrawDialog.findViewById(R.id.button_ok);
        TextView txtTitle = (TextView) withdrawDialog.findViewById(R.id.txt_title);
        txtTitle.setText(str);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //call withdrawn service
                new EventWithdrawAsyncTask(mTournamentEventsModel).execute(Connections.EVENT_WITHDRAW);
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawDialog.dismiss();
            }
        });
        withdrawDialog.show();
    }

    class EventWithdrawAsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;
        Events mTournamentEventsModel;

        public EventWithdrawAsyncTask(Events mTournamentEventsModel) {
            this.mTournamentEventsModel = mTournamentEventsModel;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(_context);
            dialog.setMessage("Withdraw event, please wait");
            dialog.setTitle("WithDraw");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {

            try {

                JSONObject eventWithdrawObj = new JSONObject();
                eventWithdrawObj.put("eventDocId", eventDocId);
                eventWithdrawObj.put("playerDocId", sp.getString("registrationDocId", ""));

                Log.e("withdrawObj", eventWithdrawObj.toString());

                String apiToken = sp.getString("apiToken", "");
                Log.e("apiToken", apiToken);
                String base64EncodedApiToken = Base64.encodeToString(apiToken.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);

                HttpClient httpClient = new DefaultHttpClient();

                String url = urls[0];
                Log.i("url", url);
                HttpPost httpPost = new HttpPost(url);
                // httpPost.setEntity(stringEntity);
                httpPost.setHeader("Authorization", base64EncodedApiToken);

                httpPost.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpPost.setHeader("Authorization", "Basic " + base64EncodedApiToken);
                Log.i("header", "Basic " + base64EncodedApiToken);
                httpPost.setEntity(new StringEntity(eventWithdrawObj.toString()));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                Log.i("httpResponse", responseBody);

                return responseBody;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.i("Event entry response", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                //Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    //Toast.makeText(LoginActivity.this,"111111",Toast.LENGTH_SHORT).show();
                    mTournamentEventsModel.setAction("WITHDRAWN");
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Helper.showToast(_context, obj.getString("message"));
                    dialog.dismiss();
                    notifyDataSetChanged();

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(_context, obj.getString("message"));
                    dialog.dismiss();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(_context, "Unable to fetch data from server");
                dialog.dismiss();
            }
        }
    }


}



