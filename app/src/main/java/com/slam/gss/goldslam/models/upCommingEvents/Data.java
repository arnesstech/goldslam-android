package com.slam.gss.goldslam.models.upCommingEvents;

import java.util.List;

/**
 * Created by Thriveni on 6/27/2017.
 */

public class Data {
    private String message;

    private List<Tournaments> tournaments;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Tournaments> getTournaments ()
    {
        return tournaments;
    }

    public void setTournaments (List<Tournaments> tournaments)
    {
        this.tournaments = tournaments;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", tournaments = "+tournaments+"]";
    }
}
