package com.slam.gss.goldslam.models.News.GetNews;

/**
 * Created by Thriveni on 5/9/2017.
 */

public class Data {
    private News[] news;

    public News[] getNews ()
    {
        return news;
    }

    public void setNews (News[] news)
    {
        this.news = news;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [news = "+news+"]";
    }
}
