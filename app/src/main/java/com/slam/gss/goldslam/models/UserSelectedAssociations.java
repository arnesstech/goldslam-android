package com.slam.gss.goldslam.models;

/**
 * Created by Thriveni on 2/20/2017.
 */
public class UserSelectedAssociations {

    public String getUserSelectedAssociaitonsId() {
        return userSelectedAssociaitonsId;
    }

    public void setUserSelectedAssociaitonsId(String userSelectedAssociaitonsId) {
        this.userSelectedAssociaitonsId = userSelectedAssociaitonsId;
    }

    private String userSelectedAssociaitonsId;
    private String userSelectedAssociationName;

    public boolean isAssociationSelectionFlag() {
        return associationSelectionFlag;
    }

    public void setAssociationFlag(boolean associationSelectionFlag) {
        this.associationSelectionFlag = associationSelectionFlag;
    }


    public void setAssociationSelectionFlag(boolean associationSelectionFlag) {
        this.associationSelectionFlag = associationSelectionFlag;
    }

    private boolean associationSelectionFlag;

    public String getUserSelectedAssociationName() {
        return userSelectedAssociationName;
    }

    public void setUserSelectedAssociationName(String userSelectedAssociationName) {
        this.userSelectedAssociationName = userSelectedAssociationName;
    }
}
