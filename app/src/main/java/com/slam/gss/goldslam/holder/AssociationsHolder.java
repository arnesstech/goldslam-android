package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;

/**
 * Created by Thriveni on 1/10/2017.
 */
public class AssociationsHolder extends AbstractViewHolder {

    Spinner spAssociations ;
    EditText edRegistrationNo;
    ImageView imDelete;

    public EditText getRegistrationNo() {
        return edRegistrationNo;
    }

    public ImageView getDeleteAction() {
        return imDelete;
    }

    public Spinner getAssociations() {
        return spAssociations;
    }
    public AssociationsHolder(View view) {
        super(view);
        spAssociations = (Spinner) view.findViewById(R.id.sp_associations);
        edRegistrationNo = (EditText) view.findViewById(R.id.ed_registration_no);
        imDelete = (ImageView) view.findViewById(R.id.im_delete);
    }
}
