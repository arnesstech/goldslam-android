package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.holder.UserSubscriptionCreateHolder;
import com.slam.gss.goldslam.models.MySubscriptions.UserSubscriptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Thriveni on 5/19/2017.
 */

public class UserSubscriptionCreateAdapter extends AbstractBaseAdapter<UserSubscriptions, UserSubscriptionCreateHolder> {

    public UserSubscriptionCreateAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_create_user_subscriptions;
    }

    @Override
    public UserSubscriptionCreateHolder getViewHolder(View convertView) {
        return new UserSubscriptionCreateHolder(convertView);
    }

    @Override
    public void bindView(int position, UserSubscriptionCreateHolder holder, UserSubscriptions item) {

        //Log.e("user subscriptions::", item.getSubscriptionName());
        holder.getTxtSubscriptionType().setText(item.getSubscriptionType());

        String dateFormat = "dd-MMM-yyyy";
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(item.getStartDate()));
        String startDate= formatter.format(calendar.getTime());

        Calendar calendar1 = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(item.getExpiryDate()));
        String endDate= formatter.format(calendar.getTime());

        holder.getTxtSubscriptionEndDate().setText(endDate);
        holder.getTxtSubscriptionStartDate().setText(startDate);
        holder.getTxtSubscriptionFee().setText(item.getAmount());
        holder.getTxtSubscriptionName().setText(item.getSubscriptionName());


    }
}
