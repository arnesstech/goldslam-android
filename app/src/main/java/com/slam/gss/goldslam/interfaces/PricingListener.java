package com.slam.gss.goldslam.interfaces;

/**
 * Created by Thriveni on 5/25/2017.
 */

public interface PricingListener {
    void notifyPricing(String str);
}
