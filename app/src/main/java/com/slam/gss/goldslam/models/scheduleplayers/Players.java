package com.slam.gss.goldslam.models.scheduleplayers;

public class Players
{
    private String rank;

    private String seed;

    private String name;

    private String state;

    private String points;

    private String docId;

    private String regNo;

    public String getRank ()
    {
        return rank;
    }

    public void setRank (String rank)
    {
        this.rank = rank;
    }

    public String getSeed ()
    {
        return seed;
    }

    public void setSeed (String seed)
    {
        this.seed = seed;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getPoints ()
    {
        return points;
    }

    public void setPoints (String points)
    {
        this.points = points;
    }

    public String getDocId ()
    {
        return docId;
    }

    public void setDocId (String docId)
    {
        this.docId = docId;
    }

    public String getRegNo ()
    {
        return regNo;
    }

    public void setRegNo (String regNo)
    {
        this.regNo = regNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [rank = "+rank+", seed = "+seed+", name = "+name+", state = "+state+", points = "+points+", docId = "+docId+", regNo = "+regNo+"]";
    }
}