package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.TeamAdapter;
import com.slam.gss.goldslam.models.Team.Team;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.slam.gss.goldslam.fragments.AddCoachingCenterActivity.viewPager;


/**
 * Created by mahes on 6/19/2017.
 */

public class MoreFragment extends Fragment {

    @BindView(R.id.rv_teams)
    RecyclerView rvTeams;
    @BindView(R.id.btn_more_back)
    Button btnMoreBack;

    @BindView(R.id.btn_more_finish)
    Button btnMoreFinish;

    private ProgressDialog progressDialog;
    List<Team> mTeamList;
    LinearLayoutManager mLayoutManager;
    TeamAdapter mTeamAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.item_more, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getActivity());
        rvTeams.setLayoutManager(mLayoutManager);
        rvTeams.setItemAnimator(new DefaultItemAnimator());
        mTeamList = new ArrayList<>();
        mTeamAdapter = new TeamAdapter(getActivity());

        btnMoreBack.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
            }
        });

        btnMoreFinish.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "coaching center added", Toast.LENGTH_SHORT).show();
            }
        });

        loadTeams();

    }


    private void loadTeams() {
        Team teamObj = new Team();
        teamObj.setTeamDescription("sdfbadsjkfbaksdjfadsjf");
        teamObj.setTeamName("My Team");
        mTeamList.add(teamObj);

        teamObj = new Team();
        teamObj.setTeamDescription("sdfbadsjkfbaksdjfadsjf");
        teamObj.setTeamName("My Team");
        mTeamList.add(teamObj);

        teamObj = new Team();
        teamObj.setTeamDescription("sdfbadsjkfbaksdjfadsjf");
        teamObj.setTeamName("My Team");
        mTeamList.add(teamObj);

        teamObj = new Team();
        teamObj.setTeamDescription("sdfbadsjkfbaksdjfadsjf");
        teamObj.setTeamName("My Team");
        mTeamList.add(teamObj);

        mTeamAdapter.addItem(mTeamList);
        rvTeams.setAdapter(mTeamAdapter);

    }


}
