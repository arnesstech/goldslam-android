package com.slam.gss.goldslam.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thriveni on 2/20/2017.
 */
public class UserSelectedAssociationsList {

    public List<UserSelectedAssociations> getUserSelectedAssociations() {
        if (userSelectedAssociations == null) {
            return new ArrayList<>();
        }
        return userSelectedAssociations;
    }

    public void setUserSelectedAssociations(List<UserSelectedAssociations> userSelectedAssociations) {
        this.userSelectedAssociations = userSelectedAssociations;
    }

    private List<UserSelectedAssociations> userSelectedAssociations;
}
