package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.slam.gss.goldslam.fragments.ForgotPasswordFragment;
import com.slam.gss.goldslam.fragments.ForgotUserNameFragment;
import com.slam.gss.goldslam.fragments.PlayersCoachingFragment;
import com.slam.gss.goldslam.fragments.UpdateUserNameFragment;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.models.Login.Data;
import com.slam.gss.goldslam.models.Login.LoginResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni.Yalavarthi on 25-07-2016.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.login)
    Button btnLogin;

    @BindView(R.id.forgot_pwd)
    TextView txtForgotPassword;

    /*  @BindView(R.id.register)
      TextView txtRegister;*/
    @BindView(R.id.ll_sign_up_layout)
    LinearLayout llSignup;

    @BindView(R.id.forgot_username)
    TextView txtForgotUserName;

    @BindView(R.id.txt_terms_conditions)
    TextView txtTermsConditions;

    @BindView(R.id.txt_about)
    TextView txtAboutUs;

    // @BindView(R.id.username)
    EditText edUserName;

    @BindView(R.id.password)
    EditText edPassword;

    public String userName = "", password = "", email = "";
    public EditText edUpdateUserName;
    public Intent intent;
    private CheckBox rememberMe;
    private String TAG = "LoginActivity";
    int val = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);

        initializeViews();

        //DEBUG
        if (BuildConfig.DEBUG) {
            //Super Admin
            //edUserName.setText("superadmin@goldslamsports.in");
            //edPassword.setText("r@ju3690");

            //Test User
            //  edUserName.setText("ttt123");
            //edPassword.setText("t@1234567");
        } else {
            edUserName.setText("");
            edPassword.setText("");
        }

        boolean rememberMeFlag = Boolean.parseBoolean(Utils.getString(LoginActivity.this, "rememberMe", 2));

        if (rememberMeFlag == false) {
            edUserName.setText("");
            rememberMe.setChecked(false);
        } else {
            edUserName.setText(Utils.getString(LoginActivity.this, "Email", 1));
            rememberMe.setChecked(true);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    public void initializeViews() {
        edUserName = (EditText) findViewById(R.id.username);
        edUserName.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);
        rememberMe = (CheckBox) findViewById(R.id.rememberMe);
        rememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    if (TextUtils.isEmpty(edUserName.getText().toString())) {

                    } else {
                        Utils.putString(LoginActivity.this, "rememberMe", String.valueOf(true), 2);
                        Utils.putString(LoginActivity.this, "Email", edUserName.getText().toString(), 1);

                    }

                } else {
                    Utils.putString(LoginActivity.this, "rememberMe", String.valueOf(false), 2);
                    Utils.putString(LoginActivity.this, "Email", "", 1);
                }
            }
        });

        btnLogin.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);
        txtForgotUserName.setOnClickListener(this);
        txtTermsConditions.setOnClickListener(this);
        txtAboutUs.setOnClickListener(this);
        llSignup.setOnClickListener(this);

        //edUserName.addTextChangedListener(new MyTextWatcher(edUserName));
        //edPassword.addTextChangedListener(new MyTextWatcher(edPassword));
    }


    private boolean validateUserName() {
        if (edUserName.getText().toString().trim().isEmpty()) {
            Log.e("userName", "empty");
            edUserName.setError(getString(R.string.err_login_username));
            requestFocus(edUserName);
            return false;
        } else {
            return true;
        }

    }

    private boolean validatePassword() {
        if (edPassword.getText().toString().trim().isEmpty()) {
            edPassword.setError(getString(R.string.err_login_password));
            requestFocus(edPassword);
            return false;
        } else {
            return true;
        }


    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {

            case R.id.rb_old_username:
                if (isChecked) {
                    edUpdateUserName.setText(edUserName.getText().toString()
                    );
                }
                break;

            case R.id.rb_new_user_name:
                if (isChecked) {
                    edUpdateUserName.setText("");
                }

                break;

            default:
                break;
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.username:
                    validateUserName();
                    break;

                case R.id.password:
                    validatePassword();
                    break;
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                login();
                break;
            case R.id.ll_sign_up_layout:
                register();
                break;
            case R.id.forgot_pwd:
                forgotPassword();
                break;

            case R.id.forgot_username:
                forgotUserName();
                break;

            case R.id.txt_terms_conditions:
                navigateTermsConditions();
                break;

            case R.id.txt_about:
                navigateToAboutUs();
                break;

            default:
                break;
        }

    }

    private void navigateToAboutUs() {
        //Utils.navigateToActivity(LoginActivity.this, PlayersCoachingFragment.class);
        Utils.navigateToActivity(LoginActivity.this, AboutUsActivity.class);
    }

    private void navigateTermsConditions() {
        String url = "http://gss.goldslamsports.com/legel/#terms";//dev
        //String url = "http://www.goldslamsports.com/legel/#terms";//prod
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void forgotUserName() {
        DialogFragment dialogFragment = ForgotUserNameFragment.newInstance();
        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogFragment.show(getSupportFragmentManager(), "forgotusername_fragment");
    }

    public void forgotPassword() {
        DialogFragment dialogFragment = ForgotPasswordFragment.newInstance();
        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogFragment.show(getSupportFragmentManager(), "forgotpassword_fragment");
    }

    private void register() {
        Utils.navigateToActivity(LoginActivity.this, RegisterActivity.class);
    }


    private void login() {

        if (!validateUserName()) {
            return;
        }

        if (!validatePassword()) {
            return;
        } else {
            userName = edUserName.getText().toString();
            password = edPassword.getText().toString();

            new LOGINsyncTask().execute(Connections.LOGIN_URL);
        }
    }


    class LOGINsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            try {

                HttpPost httpPost = new HttpPost(urls[0]);
                String credentials = userName + ":" + password;
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                httpPost.addHeader("Authorization", "Basic " + base64EncodedCredentials);
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                return responseBody.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;
            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.e("login Response", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {

                    /*
                    Save the user credentials details to shared preference .
                     */

                    String userCredentials = userName + ":" + password;
                    Utils.putString(LoginActivity.this, "userCredentials", userCredentials, 1);
                    Utils.putString(LoginActivity.this, "userNameVal", userName, 1);


                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());

                    /*
                    Procedd rolles and show the menu objects based on this roles
                     */
                    JSONArray jsonArray = obj.getJSONArray("roles");
                    String[] roles = new String[jsonArray.length()];
                    ArrayList<String> rolesArray = new ArrayList<>();
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            rolesArray.add(jsonArray.getString(i));
                            roles[i] = jsonArray.getString(i);
                        }
                    }

                    if (rolesArray.contains("EVENT_ADMIN")) {
                        Utils.putString(LoginActivity.this, "role", "EVENT_ADMIN", 1);
                    } else if (rolesArray.contains("NORMAL_USER")) {
                        Utils.putString(LoginActivity.this, "role", "NORMAL_USER", 1);
                    }

                    LoginResponse responseObj = new LoginResponse();
                    responseObj.setData(responseObj.getData());

                    Data data = new Data();

                    data.setApiToken(obj.getString("apiToken"));
                    data.setName(obj.getString("name"));
                    data.setRegistrationDocId(obj.getString("registrationDocId"));
                    data.setMessage(obj.getString("message"));
                    data.setGssid(obj.getString("gssid"));
                    data.setResetpasswordflag(obj.getString("resetpasswordflag"));
                    data.setResetusernameflag(obj.getString("resetusernameflag"));
                    data.setResetusernameflag(obj.getString("userName"));
                    data.setRoles(roles);

                    JSONArray associationsArray = obj.getJSONArray("userAssociations");
                    com.slam.gss.goldslam.models.Login.UserAssociations[] userAssociationsarray = new com.slam.gss.goldslam.models.Login.UserAssociations[associationsArray.length()];

                    if (associationsArray.length() > 0) {
                        for (int i = 0; i < associationsArray.length(); i++) {
                            com.slam.gss.goldslam.models.Login.UserAssociations assocObj = new com.slam.gss.goldslam.models.Login.UserAssociations();
                            JSONObject objj = associationsArray.getJSONObject(i);
                            assocObj.setAssociationid(objj.getString("associationid"));
                            assocObj.setAssociationName(objj.getString("associationName"));
                            assocObj.setRegistrationId(objj.getString("registrationId"));

                            userAssociationsarray[i] = assocObj;
                        }
                    }
                    data.setUserAssociations(userAssociationsarray);

                    responseObj.setStatus("200");
                    responseObj.setData(data);


                    String str = new Gson().toJson(responseObj);

                    Utils.putString(LoginActivity.this, "loginResponse", str, 1);
                    Utils.putString(LoginActivity.this, "apiToken", obj.getString("apiToken"), 1);
                    Utils.putString(LoginActivity.this, "registrationDocId", obj.getString("registrationDocId"), 1);
                    Utils.putString(LoginActivity.this, "gssid", obj.getString("gssid"), 1);
                    Utils.putString(LoginActivity.this, "name", obj.getString("name"), 1);
                    Utils.putString(LoginActivity.this, "Email", userName, 1);
                    Utils.putString(LoginActivity.this, "userName", userName, 1);
                    Utils.putString(LoginActivity.this, "subscriptionFlag", obj.getString("subscriptionRequired"), 2);

                    if (obj.has("email")) {
                        Utils.putString(LoginActivity.this, "userEmail", obj.getString("email"), 1);
                    } else {
                        Utils.putString(LoginActivity.this, "userEmail", "", 1);
                    }

                    if (obj.has("mobile")) {
                        Utils.putString(LoginActivity.this, "userMobile", obj.getString("mobile"), 1);
                    } else {
                        Utils.putString(LoginActivity.this, "userMobile", "", 1);

                    }


                    String alertMsg = "";
                    if (obj.has("alertmessage")) {
                        alertMsg = obj.getString("alertmessage");
                    } else {
                        alertMsg = "";
                    }

                    if ("".equalsIgnoreCase(alertMsg)) {
                        Helper.showToast(LoginActivity.this, obj.getString("message"));
                        Utils.putString(LoginActivity.this, "auth", "true", 2);
                        Utils.navigateToActivity(LoginActivity.this, AppDashBoard.class);

                    } else if (!"".equalsIgnoreCase(obj.getString("alertmessage"))) {
                        showLoginAlertDialog(obj.getString("alertmessage"), 2);
                    } else if (Boolean.parseBoolean(obj.getString("resetusernameflag"))) {
                        updateUserName();
                    } else if (Boolean.parseBoolean(obj.getString("resetpasswordflag"))) {
                        Utils.navigateToActivity(LoginActivity.this, UpdatePasswordActivity.class);
                    } else {
                        Utils.putString(LoginActivity.this, "auth", "true", 2);
                        Utils.navigateToActivity(LoginActivity.this, AppDashBoard.class);
                    }


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(LoginActivity.this, obj.getString("message"));

                }

            } catch (JSONException e) {
                e.printStackTrace();

            }


        }
    }

    public void showLoginAlertDialog(String str, final int i) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
            alertDialogBuilder.setTitle("Attention");
            alertDialogBuilder.setMessage(str);
            alertDialogBuilder.setPositiveButton("close",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                            switch (i) {
                                case 0:
                                    updateUserName();
                                    break;
                                case 1:
                                    Utils.navigateToActivity(LoginActivity.this, UpdatePasswordActivity.class);
                                    intent = new Intent(LoginActivity.this, UpdatePasswordActivity.class);
                                    startActivity(intent);
                                    finish();
                                    break;
                                case 2:

                                    Utils.putString(LoginActivity.this, "auth", "true", 2);
                                    Utils.navigateToActivity(LoginActivity.this, AppDashBoard.class);

                                    break;
                                case 3:
                                    arg0.dismiss();
                                    break;

                                default:
                                    break;

                            }

                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } catch (Exception e) {

        }
    }

    private void updateUserName() {
        DialogFragment dialogFragment = UpdateUserNameFragment.newInstance();
        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogFragment.show(getSupportFragmentManager(), "update_username_fragment");
    }


    @Override
    public void onBackPressed() {
        val++;
        if (val == 1) {
            finish();
        } else {
            Helper.showToast(LoginActivity.this, "Press again to exit");
        }
    }
}