package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;

/**
 * Created by Ramesh on 28/06/17.
 */

public class BaseFragment extends Fragment {
    //

    private ProgressDialog progressDialog;

    public void showProgress() {
        showProgress("Please wait..");
    }

    public void showProgress(String title) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
        }
        progressDialog.setMessage(title);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
