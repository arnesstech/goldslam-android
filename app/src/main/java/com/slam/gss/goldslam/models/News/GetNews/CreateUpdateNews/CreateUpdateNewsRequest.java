package com.slam.gss.goldslam.models.News.GetNews.CreateUpdateNews;

/**
 * Created by Thriveni on 5/9/2017.
 */

public class CreateUpdateNewsRequest {

    private String status;

    private String newscontent;

    private String newsid;

    private String deleteflag;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getNewscontent ()
    {
        return newscontent;
    }

    public void setNewscontent (String newscontent)
    {
        this.newscontent = newscontent;
    }

    public String getNewsid ()
    {
        return newsid;
    }

    public void setNewsid (String newsid)
    {
        this.newsid = newsid;
    }

    public String getDeleteflag ()
    {
        return deleteflag;
    }

    public void setDeleteflag (String deleteflag)
    {
        this.deleteflag = deleteflag;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", newscontent = "+newscontent+", newsid = "+newsid+", deleteflag = "+deleteflag+"]";
    }
}
