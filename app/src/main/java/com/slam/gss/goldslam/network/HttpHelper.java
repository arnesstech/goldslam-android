package com.slam.gss.goldslam.network;

import android.content.Context;
import android.util.Log;

import com.slam.gss.goldslam.RegisterActivity;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.utils.ApiResponseModel;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Thriveni on 8/8/2016.
 */
public class HttpHelper {
    private HttpClient httpClient = new DefaultHttpClient();
    private HttpPost httpPost;
    private StringEntity stringEntity;
    private HttpResponse httpResponse;
    public static String registrationStatusMessage = "";


    public String userRegistration(String url, JSONObject registrationObj, RegisterActivity registerActivity) {

        //Log.i("url", url);
        //Log.i("paramssss", registrationObj.toString());
        String registrationResponse = "";
        httpPost = new HttpPost(url);
        try {
            stringEntity = new StringEntity(registrationObj.toString());
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpResponse = httpClient.execute(httpPost);
            String responseBody = EntityUtils.toString(httpResponse.getEntity());
            //Log.i("httpResponse",responseBody);

            JSONObject jsonResponse = new JSONObject(responseBody);
            return jsonResponse.toString();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (JSONException e) {
            e.printStackTrace();
            return e.getMessage();
        }

    }


    public String otpVerification(String url, JSONObject otpverificationObj) {

        Log.i("url", url);
        Log.i("paramssss", otpverificationObj.toString());
        String registrationResponse = "";
        httpPost = new HttpPost(url);
        try {
            stringEntity = new StringEntity(otpverificationObj.toString());
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpResponse = httpClient.execute(httpPost);
            String responseBody = EntityUtils.toString(httpResponse.getEntity());
            Log.i("httpResponse",responseBody);

            JSONObject jsonResponse = new JSONObject(responseBody);
            return jsonResponse.toString();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }catch (JSONException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public String updatePassword(String url, JSONObject updatePasswordObj) {

        Log.i("url", url);
        Log.i("paramssss", updatePasswordObj.toString());
        String registrationResponse = "";
        httpPost = new HttpPost(url);
        try {
            stringEntity = new StringEntity(updatePasswordObj.toString());
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpResponse = httpClient.execute(httpPost);
            String responseBody = EntityUtils.toString(httpResponse.getEntity());
            Log.i("httpResponse",responseBody);

            JSONObject jsonResponse = new JSONObject(responseBody);
            return jsonResponse.toString();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }catch (JSONException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }


    public ApiResponseModel executeHttpPostRequest(String urlString,List<NameValuePair> params, String methodType)
    {
        StringBuffer result = new StringBuffer("");
        ApiResponseModel resp=new ApiResponseModel();
        try{
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setReadTimeout(Connections.TIMEOUT);
            connection.setConnectTimeout(Connections.TIMEOUT);
            connection.setRequestProperty("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            connection.setRequestMethod(methodType);
            connection.setDoInput(true);
            connection.setDoOutput(true);


            /*List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("firstName", "Murali"));
            params.add(new BasicNameValuePair("lastName", "M"));
            params.add(new BasicNameValuePair("mobile", "9087444488"));
            params.add(new BasicNameValuePair("email", "muralimoida@gmail.com"));
            params.add(new BasicNameValuePair("gender", "Male"));
            params.add(new BasicNameValuePair("registerType", "self"));
            params.add(new BasicNameValuePair("userType", "user"));*/

            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));
            writer.flush();
            writer.close();
            os.close();

            connection.connect();

            resp.code= connection.getResponseCode();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e)
        {
            //ExceptionHandler.logException(e.getMessage());
        }
        resp.response=result.toString();
        return resp;
    }


    public ApiResponseModel executeHttpGetRequest(String urlString,Context context)
    {
        StringBuffer result = new StringBuffer("");
        ApiResponseModel resp=new ApiResponseModel();
        try{
            urlString = urlString.replaceAll(" ", "%20");
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setReadTimeout(Connections.TIMEOUT);
            connection.setConnectTimeout(Connections.TIMEOUT);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            resp.code= connection.getResponseCode();
            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e)
        {
            //ExceptionHandler.logException(e.getMessage());
        }
        resp.response=result.toString();
        return resp;
    }


    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }



}
