package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 3/17/2017.
 */

public class SubscriptionHolder extends AbstractViewHolder {

    @BindView(R.id.subscriptionName)
    TextView txtSubscriptionName;

    @BindView(R.id.subscriptionfee)
    TextView txtSubscriptionFee;

    @BindView(R.id.rb_subscription_pay)
    RadioButton rbSubscriptionPay;

    @BindView(R.id.subscriptionDuration)
    TextView txtSubscriptionDuration;

    @BindView(R.id.subscriptionType)
    TextView txtSubscriptionType;

    @BindView(R.id.subscriptionStartDate)
    TextView txtSubscriptionStartDate;

    @BindView(R.id.subscriptionEndDate)
    TextView txtSubscriptionEndDate;


    public TextView getSubscriptionName() {
        return txtSubscriptionName;
    }

    public TextView getSubscriptionFee() {
        return txtSubscriptionFee;
    }

    public TextView getTxtSubscriptionDuration() {
        return txtSubscriptionDuration;
    }

    public TextView getTxtSubscriptionType() {
        return txtSubscriptionType;
    }

    public TextView getTxtSubscriptionStartDate() {
        return txtSubscriptionStartDate;
    }

    public TextView getTxtSubscriptionEndDate() {
        return txtSubscriptionEndDate;
    }


    public RadioButton getSubscriptionPay() {
        return rbSubscriptionPay;
    }

    public SubscriptionHolder(View view) {
        super(view);
        ButterKnife.bind(view);

    }




        /*holder.getSubscriptionPay().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //showDialog(item);
                // SubscriptionFragment.tempSelectedSubscription = item;

                if (selectedPosition == position) {
                    holder.getSubscriptionPay().setChecked(true);
                }
            }
        });*/

}
