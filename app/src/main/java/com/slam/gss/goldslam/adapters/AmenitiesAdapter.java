package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.AmenitiesHolder;
import com.slam.gss.goldslam.holder.TeamHolder;
import com.slam.gss.goldslam.models.Team.Team;
import com.slam.gss.goldslam.models.amenities.Amenities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahes on 6/20/2017.
 */

public class AmenitiesAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private List<Amenities> AmenitiesList = new ArrayList<>();
    private Context mContext;

    public AmenitiesAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void addItem(List<Amenities> amenitiesList) {
        AmenitiesList = new ArrayList<>(amenitiesList);
        notifyDataSetChanged();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vEmp = inflater.inflate(R.layout.item_amenities, parent, false);
        viewHolder = new AmenitiesHolder(vEmp);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AmenitiesHolder mAmenitiesHolderHolder = (AmenitiesHolder) holder;
        configureViewHolderFilter(mAmenitiesHolderHolder, position);

    }

    private void configureViewHolderFilter(AmenitiesHolder mTeamHolderHolder, int position) {
        mTeamHolderHolder .getTxtTeamName().setText(AmenitiesList.get(position).getAmenitiesTxtName());
    }

    @Override
    public int getItemCount() {
        return AmenitiesList.size();
    }
}
