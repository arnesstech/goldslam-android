package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;
import com.slam.gss.goldslam.adapters.SpinnerAdapter;
import com.slam.gss.goldslam.adapters.TabsPagerAdapter;
import com.slam.gss.goldslam.models.drawresults.Data;
import com.slam.gss.goldslam.models.drawresults.DrawResultsResponse;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ramesh on 20/8/16.
 */
public class DrawResultsFragment extends BaseFragment {
    String docId;
    Call<DrawResultsResponse> call;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.empty)
    TextView empty;
    private boolean isRefreshing;
    private ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private TabLayout tabLayout;
    private Spinner spDraw;
    private String selectedDraw = "MAIN";
    private ArrayList<String> draws = new ArrayList<>();
    private ProgressDialog progressDialog;
    private boolean showToggle;
    private String eventStatus;
    @BindView(R.id.txt_event_status)
    TextView txtEventStatus;

   /* @BindView(R.id.btn_showToggle)
    Button btnShowToggle;*/

    @BindView(R.id.ll_draw_single)
    LinearLayout llDrawSingle;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        docId = getActivity().getIntent().getStringExtra("docId");
        eventStatus = getActivity().getIntent().getStringExtra("eventStatus");
        showToggle = getActivity().getIntent().getBooleanExtra("showToggle", false);

        selectedDraw = eventStatus;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_draws_player_schedule, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        tabLayout = (TabLayout) getView().findViewById(R.id.tabs);
        viewPager = (ViewPager) getView().findViewById(R.id.pager);

        SegmentedGroup segmented2 = (SegmentedGroup) view.findViewById(R.id.segmented2);
        segmented2.setTintColor(getResources().getColor(R.color.radio_button_selected_color));


        RadioButton rb = (RadioButton) view.findViewById(R.id.button22);
        RadioButton rb1 = (RadioButton) view.findViewById(R.id.button21);

        Log.e("event status is", eventStatus);
        Log.e("Toggle val is", showToggle+"");

        if ("MAIN".equalsIgnoreCase(eventStatus)) {
            rb1.setText(eventStatus);
            rb1.setChecked(true);
        } else if ("QUALIFYING".equalsIgnoreCase(eventStatus)) {
            rb1.setText(eventStatus);
            rb.setChecked(true);
        }

        if (TextUtils.isEmpty(eventStatus)) {
            segmented2.setVisibility(View.GONE);
            llDrawSingle.setVisibility(View.GONE);
             selectedDraw = "MAIN";
        } else {
            selectedDraw = eventStatus;
            if (showToggle) {
                segmented2.setVisibility(View.VISIBLE);
                llDrawSingle.setVisibility(View.GONE);
            } else {
                llDrawSingle.setVisibility(View.VISIBLE);
                txtEventStatus.setText(eventStatus);
                segmented2.setVisibility(View.GONE);
            }
        }


        segmented2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.button21:
                        selectedDraw = "MAIN";
                        loadPlayer();
                        break;
                    case R.id.button22:
                        selectedDraw = "QUALIFYING";
                        loadPlayer();
                        break;

                    default:
                        break;
                }
            }
        });


        loadPlayer();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadPlayer();
    }

    public void loadPlayer() {

        if (isRefreshing) {
            return;
        }
        showProgressDialog();
        isRefreshing = true;
        Call<DrawResultsResponse> call = RestApi.get()
                .getRestService()
                .getDrawResults(docId, selectedDraw);

        System.out.println("draw is" + selectedDraw);
        //
        call.enqueue(new Callback<DrawResultsResponse>() {
            @Override
            public void onResponse(Call<DrawResultsResponse> call, Response<DrawResultsResponse> response) {
                isRefreshing = false;

                // Log.e("Draws response",response.body().toString());

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null) {
                        Log.i("draw data", response.body().getData().toString());
                        filterWithDates(response.body().getData());
                    } else {
                        showEmptyView(new ArrayList<Team>());
                    }
                } else {
                    //Handle errors
                    showEmptyView(new ArrayList<Team>());
                }
                dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<DrawResultsResponse> call, Throwable t) {
                isRefreshing = false;
                if (call.isCanceled()) {
                    //Do nothing
                } else {
                    //Handle errors
                }
                showEmptyView(new ArrayList<Team>());
                dismissProgressDialog();
            }
        });
    }

    class Team {
        public String getRoundName() {
            return roundName;
        }

        public void setRoundName(String roundName) {
            this.roundName = roundName;
        }

        private String roundName;

        public Player getPlayer1() {
            return player1;
        }

        Player player1;

        public Player getPlayer2() {
            return player2;
        }

        Player player2;

        public int getRound() {
            return round;
        }

        int round;

        public boolean isHeader() {
            return header;
        }

        public void setHeader(boolean header) {
            this.header = header;
        }

        private boolean header;

        Team(int round) {
            this.round = round;
            player2 = new Player("NONE");
            player1 = new Player("NONE");

        }

        public Player getWinPlayer() {

            Log.e("player results", player1.result + "...>" + player2.result);

            if ("1".equalsIgnoreCase(player1.result)) {
                return player1;
            } else if ("1".equalsIgnoreCase(player2.result)) {
                return player2;
            } else {
                return new Player("NONE", "-1", "-");
            }
        }
    }

    class Player {
        String name;
        String result;

        public String getScore() {
            return score;
        }

        public String getResult() {
            return result;
        }

        String score;

        public Player(String name) {
            this(name, "-1", "-");
        }

        Player(String name, String result, String score) {
            this.name = name;
            this.result = result;
            this.score = score;
        }

        public String getName() {
            return name;
        }
    }


    public void filterWithDates(Data matches) {
         Log.e("matches",matches.toString());
        //
        List<Object> teams = matches.getTeams();
        List<String> fullTeamMembers = new ArrayList<>();
        for (Object team : teams) {
            if (team instanceof List) {
                for (String s : (List<String>) team) {
                    fullTeamMembers.add(s);
                }
            }
        }
        int fullSize = fullTeamMembers.size();        //

        List<Team> finalteamList = new ArrayList<>();
        List<Object> list = matches.getResults1();
        //*********************/
        //Calculating rounds
        int temp = fullSize;
        int totalRounds = 0;
        while (true) {
            temp = temp / 2;
            totalRounds++;
            if (temp <= 1) {
                break;
            }
        }
        /*************************/

        int round = 1;
        List<Team> tempTeam = new ArrayList<>();
        for (Object team : list) {//5 iteration

            int count = 0;
            List<Team> teamList = new ArrayList<>();
            for (Object s : (List<Object>) team) { //16,8,4,2,1
                if (!(s instanceof List)) {
                    continue;
                }
                List<String> s2 = (List<String>) s;
                //
                Team team1 = new Team(round);
                if (count == 0) {
                    team1.setHeader(true);
                    int diff = totalRounds - round;
                    if (diff == 0) {
                        team1.setRoundName("Final");
                    } else if (diff == 1) {
                        team1.setRoundName("Semi Final");
                    } else if (diff == 2) {
                        team1.setRoundName("Quarter Final");
                    } else {
                        team1.setRoundName("Round " + round);
                    }
                }
                if (round == 1) {
                    try {
                        Player p1 = new Player(fullTeamMembers.get(count),
                                s2.size() > 0 ? s2.get(0) : "-1",
                                s2.size() > 2 ? s2.get(2) : "-");
                        //Log.e("player2",tempTeam.get(count + 1).getWinPlayer().getName());
                        Player p2 = new Player(fullTeamMembers.get(count + 1),
                                s2.size() > 1 ? s2.get(1) : "-1",
                                s2.size() > 3 ? s2.get(3) : "-");
                        team1.player1 = p1;
                        team1.player2 = p2;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        if (tempTeam.size() == 1) {
                            Player p1 = new Player(tempTeam.get(count).getWinPlayer().getName(), s2.get(0), s2.get(2));
                            team1.player1 = p1;
                        }
                        Player p1 = new Player(tempTeam.get(count)
                                .getWinPlayer().getName(),
                                s2.size() > 0 ? s2.get(0) : "-1",
                                s2.size() > 2 ? s2.get(2) : "-");

                        Log.e("player2",tempTeam.get(count + 1).getWinPlayer().getName());
                        Player p2 = new Player(tempTeam.get(count + 1).getWinPlayer().getName(),
                                s2.size() > 1 ? s2.get(1) : "-1",
                                s2.size() > 3 ? s2.get(3) : "-");
                        team1.player1 = p1;
                        team1.player2 = p2;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                count = count + 2;
                teamList.add(team1);
                finalteamList.add(team1);
            }
            round++;
            tempTeam.clear();
            tempTeam.addAll(teamList);
        }
        setTabs(finalteamList);
//        if (adapter != null) {
//            adapter.addItems(finalteamList);
//        }
        showEmptyView(tempTeam);

    }

    private void setTabs(List<Team> list) {
        mAdapter = new TabsPagerAdapter(getChildFragmentManager(), 1);
        List<Team> temp = new ArrayList<>();
        String round = "";
        for (Team team : list) {
            if (team.isHeader()) {
                //Log.e("Ramesh", "Added new item>" + team.getRoundName());
                temp = new ArrayList<>();
                DrawsFragment fragment = new DrawsFragment();
                temp.add(team);
                fragment.setData(temp);
                mAdapter.addFragment(fragment, team.getRoundName());
                mAdapter.notifyDataSetChanged();
            } else {
                temp.add(team);
                //Log.e("Ramesh", "Added items to list>" + team.getRoundName());
            }
        }
        viewPager.setAdapter(mAdapter);
        viewPager.getAdapter().notifyDataSetChanged();
        tabLayout.setupWithViewPager(viewPager);
    }


    public static class DrawsFragment extends BaseFragment {
        List<Team> list = new ArrayList<>();
        @BindView(R.id.listView)
        ListView listView;
        @BindView(R.id.progress)
        ProgressBar progress;
        @BindView(R.id.empty)
        TextView empty;
        private DrawResutsAdapter adapter;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            adapter = new DrawResutsAdapter(getActivity());
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return LayoutInflater.from(getActivity()).inflate(R.layout.frag_live_player_schedule_sub, container, false);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            ButterKnife.bind(this, view);
        }

        public void setData(List<Team> list) {
            this.list = list;
        }

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            adapter.clearAll();
            adapter.addItemsAll(list);
            listView.setAdapter(adapter);
        }

        private void showEmptyView() {
            if (!isAdded()) {
                return;
            }
            if (adapter.getCount() == 0) {
                empty.setVisibility(View.VISIBLE);
            } else {
                empty.setVisibility(View.GONE);
            }
        }

        @Override
        protected void showProgressDialog() {
            super.showProgressDialog();
            if (progress != null) {
                progress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void dismissProgressDialog() {
            super.dismissProgressDialog();
            if (progress != null) {
                progress.setVisibility(View.GONE);
            }
        }
    }


    private void filterAsHeaders(Map<String, List<Matches>> map, List<Matches> list) {
        for (Matches m : list) {
            String roundName = m.getRoundName();
            if (map.get(roundName) != null) {
                map.get(roundName).add(m);
            } else {
                List<Matches> temp = new ArrayList<>();
                m.setHeader(true);
                temp.add(m);
                map.put(roundName, temp);
            }
        }
    }

    @OnClick(R.id.refresh_layout)
    public void onClick() {
        //loadPlayer();
    }

    public static class DrawResultViewHolder extends AbstractViewHolder {

        @BindView(R.id.txt_header)
        TextView header;

        @BindView(R.id.txt_player1)
        TextView player1;
        @BindView(R.id.txt_player2)
        TextView player2;
        @BindViews({
                R.id.txt_pscore1, R.id.txt_pscore2, R.id.txt_pscore3,
                R.id.txt_pscore4, R.id.txt_pscore5,
        })
        List<TextView> scoreView1;
        @BindViews({
                R.id.txt_p2score1, R.id.txt_p2score2, R.id.txt_p2score3,
                R.id.txt_p2score4, R.id.txt_p2score5,
        })
        List<TextView> scoreView2;

        public DrawResultViewHolder(View view) {
            super(view);
        }

        public TextView getPlayer2() {
            return player2;
        }

        public TextView getPlayer1() {
            return player1;
        }

        public List<TextView> getScoreView1() {
            return scoreView1;
        }

        public TextView getHeader() {
            return header;
        }

        public List<TextView> getScoreView2() {
            return scoreView2;
        }

    }

    private static class DrawResutsAdapter extends AbstractBaseAdapter<Team, DrawResultViewHolder> {

        public DrawResutsAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.frag_draw_results_row;
        }

        @Override
        public DrawResultViewHolder getViewHolder(View convertView) {
            return new DrawResultViewHolder(convertView);
        }

        @Override
        public void bindView(int position, DrawResultViewHolder holder, Team item) {
            try {
                if (item.isHeader()) {
                    //Never display header
                    holder.getHeader().setVisibility(View.GONE);
                } else {
                    holder.getHeader().setVisibility(View.GONE);
                }
                holder.getHeader().setText(item.getRoundName());

                Log.e("DrawResultsFragment","player1 name"+item.getPlayer1().getName());
                Log.e("DrawResultsFragment","player2 name"+item.getPlayer2().getName());


                if (item.getPlayer1().getName().equalsIgnoreCase("NONE")) {
                    holder.getPlayer1().setText("-");
                } else {
                    holder.getPlayer1().setText(item.getPlayer1().getName());
                }

                if (item.getPlayer2().getName().equalsIgnoreCase("NONE")) {
                    holder.getPlayer2().setText("-");
                } else {
                    holder.getPlayer2().setText(item.getPlayer2().getName());
                }

                //Reset Background color every time
                holder.getPlayer1().setBackgroundColor(Color.TRANSPARENT);
                holder.getPlayer2().setBackgroundColor(Color.TRANSPARENT);
                //Reset Text color every time
                holder.getPlayer1().setTextColor(Color.BLACK);
                holder.getPlayer2().setTextColor(Color.BLACK);
                //
                if (item.getPlayer1() == item.getWinPlayer()) {
                    holder.getPlayer1().setTextColor(Color.parseColor("#4CAF50"));
                    holder.getPlayer2().setTextColor(Color.BLACK);
                } else if (item.getPlayer2() == item.getWinPlayer()) {
                    holder.getPlayer1().setTextColor(Color.BLACK);
                    holder.getPlayer2().setTextColor(Color.parseColor("#4CAF50"));
                } else {


                }

                //First player scores
                String s1 = item.getPlayer1().getScore();
                String s2 = item.getPlayer2().getScore();

                setPlayerScores(TextUtils.split(s1, ","), holder.getScoreView1());
                //Second player scores
                setPlayerScores(TextUtils.split(s2, ","), holder.getScoreView2());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void setPlayerScores(String[] scores2, List<TextView> viewList) {
            int index = 0;
            for (TextView textView : viewList) {
                if (index < scores2.length) {
                    textView.setText(scores2[index]);
                } else {
                    textView.setText("-");
                }
                index++;
            }

        }


    }

    private void showEmptyView(List<Team> tempTeam) {
        if (!isAdded()) {
            return;
        }
        if (tempTeam.size() == 0) {
            empty.setVisibility(View.VISIBLE);
            tabLayout.setVisibility(View.GONE);
        } else {
            empty.setVisibility(View.GONE);
            tabLayout.setVisibility(View.VISIBLE);
        }

    }
}
