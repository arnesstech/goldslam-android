package com.slam.gss.goldslam.models.amenities;

/**
 * Created by mahes on 6/20/2017.
 */

public class Amenities {
    String amenitiesCheckbox, amenitiesTxtName;

    public String getAmenitiesCheckbox() {
        return amenitiesCheckbox;
    }

    public void setAmenitiesCheckbox(String amenitiesCheckbox) {
        this.amenitiesCheckbox = amenitiesCheckbox;
    }

    public String getAmenitiesTxtName() {
        return amenitiesTxtName;
    }

    public void setAmenitiesTxtName(String amenitiesTxtName) {
        this.amenitiesTxtName = amenitiesTxtName;
    }
}
