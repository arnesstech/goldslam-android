package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.slam.gss.goldslam.adapters.TournamentListAdapter;
import com.slam.gss.goldslam.fragments.AssociationsFragment;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.Paymentdetails;
import com.slam.gss.goldslam.models.Tournament;
import com.slam.gss.goldslam.models.TournamentEventsModel;
import com.slam.gss.goldslam.models.UserSelectedAssociationsList;
import com.slam.gss.goldslam.models.scheduleplayers.SchedulePlayerResponse;
import com.slam.gss.goldslam.network.RestApi;
import com.slam.gss.goldslam.utils.ApiResponseModel;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 8/8/2016.
 */
public class ViewTournamentFragment extends Fragment implements TournamentsListener, TextWatcher {


    TournamentListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<TournamentEventsModel> eventsList;
    HashMap<String, List<TournamentEventsModel>> listDataChild;
    private int ctrl = 0;
    private ArrayList<Tournament> tournaments;
    private Tournament tournament;
    int totalHeadersCount = 0, count = 0;
    private SharedPreferences sp;
    private SharedPreferences.Editor edit;
    private TextView temp_page_title_menu, temp_single_menu;
    private ImageView imFilter;
    private EditText etSearchTournaments;
    private String app_verions;
    private TextView txtNoTournaments;

    @Override
    public void notifyTournament(String str) {
        if ("s".equalsIgnoreCase(str)) {
            Helper.showToast(getActivity(), "loadNotifications");
            loadTournaments();
        } else {

        }

    }


    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_view_tournament, container, false);
        expListView = (ExpandableListView) view.findViewById(R.id.exlv_tournaments);
        txtNoTournaments = (TextView) view.findViewById(R.id.txt_no_tournaments);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText("Tournaments");
        temp_page_title_menu.setGravity(Gravity.CENTER);
        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.VISIBLE);
        }
        imFilter = (ImageView) view.findViewById(R.id.im_filterr);
        imFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AssociationsFragment dialogFragment = AssociationsFragment.newInstance();
                dialogFragment.setListener(ViewTournamentFragment.this);
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "Association Filter Fragment");

            }
        });

        etSearchTournaments = (EditText) view.findViewById(R.id.ed_search_tournaments);
        // etSearchTournaments.addTextChangedListener(this);

        tournaments = new ArrayList<Tournament>();
        eventsList = new ArrayList<TournamentEventsModel>();

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<TournamentEventsModel>>();

        sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
        edit = sp.edit();


        getAppVersion();


        //loadTournaments();

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getAppData();
    }

    private void loadTournaments() {
        String serverURL = Connections.BASE_URL + "api/tournaments";
        ListAsynchronousTask mListAsynchronousTask = new ListAsynchronousTask(getActivity());
        mListAsynchronousTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverURL);
    }

    private void getAppVersion() {
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
            Log.e("appversion", info.versionName + "");
            app_verions = info.versionName;
            // app_verions = "4.1";
        } catch (PackageManager.NameNotFoundException e) {
            //Log.e("nameNotFound", e.getMessage());
        }
    }

    private void getAppData() {
        //Every 24 hours , reprompt the user
        long cTime = sp.getLong("c_time", 0);
        long cDate = System.currentTimeMillis();
        long diff = cDate - cTime;
        long period = 86400000L;
        //
//        if (diff > period) {
        new GetAppDataAsynTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Connections.BASE_URL + "api/appdata/android");
//        } else {
        loadTournaments();
//        }
        //new GetAppDataAsynTask().execute("http://gss.goldslamsports.com/prosports/api/appdata/android");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    class GetAppDataAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
//            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {

            Log.i("app data url", urls[0]);
            try {

                String url = urls[0];

                //Log.i("get profile url", url);
                HttpGet httpGet = new HttpGet(url);

                httpGet.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpGet.setHeader("Client-Type", "APP");
                httpGet.setHeader("App-Id", "PROSPORTS");


                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonResponse = new JSONObject(responseBody);
                // Log.i("getProfile Reponse", jsonResponse.toString());


                return responseBody.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.i("appData", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());

                    // Log.e("App Data ", obj.toString() + "appVaersion" + app_verions);

                    //app_verions = "3.4"; //3.8, 3.9
                    if (Double.parseDouble(app_verions) < Double.parseDouble(obj.getString("appVersion"))) {

                        Calendar calendar = Calendar.getInstance();

                        if (calendar.getTimeInMillis() < Long.parseLong(obj.getString("updateDeadLine"))) {
                            //Every 24 hours , reprompt the user
                            long cTime = sp.getLong("c_time", 0);
                            long cDate = System.currentTimeMillis();
                            long diff = cDate - cTime;
                            long period = 86400000L;

                            if (diff > period) {
                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setTitle("Update");
                                alertDialogBuilder.setMessage("Latest version available in the Playstore.Please Update");
                                alertDialogBuilder.setPositiveButton("Update",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                navigateToPlayStore();
                                            }
                                        });

                                alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                        loadTournaments();
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.setCancelable(false);
                                alertDialog.setCanceledOnTouchOutside(false);
                                alertDialog.show();
                                sp.edit().putLong("c_time", cDate).apply();
                            } else {
//                                loadTournaments();
                            }

                        } else {
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setTitle("Update");
                            alertDialogBuilder.setMessage("Latest version available in the Playstore.Please Update");
                            alertDialogBuilder.setPositiveButton("Update",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            navigateToPlayStore();
                                        }
                                    });

                            alertDialogBuilder.setNegativeButton("", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    loadTournaments();
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.show();
                            sp.edit().putLong("c_time", 0).apply();
                        }

                    } else {
                        sp.edit().putLong("c_time", System.currentTimeMillis()).apply();
//                        loadTournaments();
                    }


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    //Helper.showToast(, obj.getString("message"));
//                    loadTournaments();

                }

            } catch (JSONException e) {
                e.printStackTrace();
                //Helper.showToast(AppDashBoard.this, e.getMessage());
//                loadTournaments();
            }


        }
    }

    private void navigateToPlayStore() {
        final String appPackageName = getActivity().getPackageName();
        try {
            Intent viewIntent =
                    new Intent("android.intent.action.VIEW",
                            Uri.parse("market://details?id=" + appPackageName));
            startActivity(viewIntent);
        } catch (Exception e) {
            Helper.showToast(getActivity(), "Unable to Connect Try Again...");
            e.printStackTrace();
        }

    }

    /**
     * For To get Tournaments headings
     */

    public class ListAsynchronousTask extends AsyncTask<String, String, String> {
        private String Error = null;
        ApiResponseModel resp = new ApiResponseModel();
        String result = "";
        String responseString;
        String serverResponse = null;

        private String message = "";

        private Context mContext;

        public ListAsynchronousTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
            try {
                InputStream is;
                URL url = new URL(params[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
                connection.setRequestProperty("Accept", "*/*");

                Log.i("apiToken", Helper.apiToken);
                //connection.setRequestProperty("Authorization",  Helper.apiToken);
                connection.setRequestProperty("Authorization", sp.getString("apiToken", ""));
                connection.setRequestProperty("Client-Type", "APP");
                connection.setRequestProperty("App-Id", "PROSPORTS");
                connection.setRequestProperty("Context", "player");

                connection.connect();
                try {
                    is = connection.getInputStream();
                } catch (FileNotFoundException exception) {
                    //log.error(exception.getMessage(), exception);
                    is = connection.getErrorStream();
                }

                BufferedReader theReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder response = new StringBuilder();
                String reply;
                while ((reply = theReader.readLine()) != null) {
                    response.append(reply);
                }

                serverResponse = response.toString();
                // System.out.println("Message::" + serverResponse + "==" + connection.getResponseCode());

            } catch (IOException e) {
                e.printStackTrace();
                result = null;
            }

            if (result != null)
                responseString = result.toString();


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (serverResponse != null) {
                try {
                    JSONObject joMain = new JSONObject(serverResponse/*resp.response*/);
                    tournaments.clear();
                    listDataHeader.clear();
                    listDataChild.clear();
                    //listDataChild.clear();
                    totalHeadersCount = 0;
                    count = 0;
                    if (joMain != null) {
                        JSONObject subJson = joMain.getJSONObject("data");//new JSONObject("data");
                        if (subJson != null) {
                            JSONArray mJsonArray = subJson.getJSONArray("tournaments");
                            //acctokenGenerated = true;
                            if (mJsonArray != null) {
                                ctrl = mJsonArray.length();
                                JSONObject minnerObject;

                                for (int i = 0; i < mJsonArray.length(); i++) {
                                    totalHeadersCount++;
                                    minnerObject = mJsonArray.getJSONObject(i);
                                    //System.out.println("Inner Object::::" + minnerObject);
                                    if (minnerObject != null) {
                                        tournament = new Tournament();
                                        tournament.setCollegeName("" + minnerObject.get("tournamentName"));
                                        tournament.setTournamentName("" + minnerObject.get("tournamentName"));
                                        tournament.setAddress("" + minnerObject.get("location"));
                                        tournament.setDocID(minnerObject.getString("docId"));
                                        tournament.setFromDate("" + minnerObject.get("fromDate"));
                                        tournament.setToDate("" + minnerObject.get("toDate"));
                                        tournament.setTournamentAssociationId("" + minnerObject.get("associationDocId"));
                                        tournament.setTournamentAssociationName("" + minnerObject.get("associationName"));

                                        // tournament.setEmail(""+minnerObject.get("email"));
//                                        tournament.setFromDate("2012-02-12");
//                                        tournament.setToDate("202-05-12");
                                        UserSelectedAssociationsList list = new Gson().fromJson(sp.getString("AssociationFilter", ""), UserSelectedAssociationsList.class);
                                        boolean isAtleastSelected = false;
                                        boolean isTournaments = false;

//Thriveni
                                        if (list != null && !list.getUserSelectedAssociations().isEmpty()) {
                                            Log.e("flag", "list not empty");

                                            for (int ctr = 0; ctr < list.getUserSelectedAssociations().size(); ctr++) {

                                                String associationName = minnerObject.getString("associationName");
                                                boolean selected = list.getUserSelectedAssociations().get(ctr).isAssociationSelectionFlag();
                                                if (selected) {
                                                    isAtleastSelected = true;
                                                }


                                                if (list.getUserSelectedAssociations().get(ctr).getUserSelectedAssociationName().equalsIgnoreCase(associationName)
                                                        && (list.getUserSelectedAssociations().get(ctr).isAssociationSelectionFlag())) {
                                                    Log.e("flag val", list.getUserSelectedAssociations().get(ctr).getUserSelectedAssociationName() + "....." + list.getUserSelectedAssociations().get(ctr).isAssociationSelectionFlag());


                                                    count++;
                                                    tournaments.add(tournament);
                                                    // Adding child data
                                                    listDataHeader.add("" + minnerObject.get("tournamentName"));


                                                    // txtNoTournaments.setVisibility(View.GONE);
                                                    //expListView.setVisibility(View.VISIBLE);


                                                    EventsAsyncTask mEventsAsyncTask = new EventsAsyncTask(mContext, minnerObject.get("tournamentName").toString(), count);
                                                    mEventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Connections.BASE_URL + "api/events?tournamentdocid=" + minnerObject.get("docId"));

                                                } else {
                                                    // txtNoTournaments.setVisibility(View.VISIBLE);
                                                    //expListView.setVisibility(View.GONE);
                                                }
                                            }
                                        }


                                        if (!isAtleastSelected) {
                                            Log.e("hello", "hello");
                                            tournaments.add(tournament);
                                            count++;

                                            // txtNoTournaments.setVisibility(View.GONE);
                                            //expListView.setVisibility(View.VISIBLE);

                                            // Adding child data
                                            listDataHeader.add("" + minnerObject.get("tournamentName"));
                                            EventsAsyncTask mEventsAsyncTask = new EventsAsyncTask(mContext, minnerObject.get("tournamentName").toString(), count);
                                            mEventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Connections.BASE_URL + "api/events?tournamentdocid=" + minnerObject.get("docId"));

                                        }


                                    }

                                }
                            }

                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } /*else {
                txtNoTournaments.setVisibility(View.VISIBLE);
            }*/

        }
    }


    class EventsAsyncTask extends AsyncTask<String, Void, String> {
        List<TournamentEventsModel> eventsList;
        ProgressDialog dialog;
        int value;
        private Context mContext;
        String title;

        public EventsAsyncTask(Context context, String title, int accValue) {
            mContext = context;
            value = accValue;
            eventsList = new ArrayList<>();
            this.title = title;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Tournaments Data Retrieveing.");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            //  Log.i("GetTournaments url", urls[0]);
            try {
                HttpGet httpGet = new HttpGet(urls[0]);
                String credentials = "Basic";
                credentials = credentials + sp.getString("apiToken", "");
                Log.e("apiToken", credentials);
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                httpGet.addHeader("Authorization", credentials);
                httpGet.addHeader("Client-Type", "APP");
                httpGet.addHeader("App-Id", "PROSPORTS");
                httpGet.addHeader("Context", "player");

                // Log.e("event url", urls[0]);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            if (eventsList != null)
                eventsList.clear();

            /*if(listDataChild!=null)
                listDataChild.clear();*/

            String status = null;
            try {
                jsonResponse = new JSONObject(result);

                status = jsonResponse.get("status").toString();

                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Log.i("GetEvents Response", obj.toString());
                    JSONArray jsonEventsArray = obj.getJSONArray("events");
                    if (jsonEventsArray != null) {
                        TournamentEventsModel mTournamentEventsModel;
                        Paymentdetails paytmDetails;

                        JSONObject eventsJsonObject = null;
                        for (int i = 0; i < jsonEventsArray.length(); i++) {
                            mTournamentEventsModel = new TournamentEventsModel();
                            eventsJsonObject = jsonEventsArray.getJSONObject(i);

                            mTournamentEventsModel.setEventTeamSize(Integer.parseInt(eventsJsonObject.getString("teamSize")));
                            mTournamentEventsModel.setEventDocId(eventsJsonObject.getString("docId"));
                            mTournamentEventsModel.setEventName(eventsJsonObject.getString("eventName"));
                            mTournamentEventsModel.setSigningStartTime(eventsJsonObject.getString("signinStartDateTime"));
                            mTournamentEventsModel.setSigningEndTime(eventsJsonObject.getString("signinEndDateTime"));
                            mTournamentEventsModel.setEventAction(eventsJsonObject.getString("action"));
                            mTournamentEventsModel.setEventAssociationName(eventsJsonObject.getString("associationName"));
                            mTournamentEventsModel.setEventAssociationId(eventsJsonObject.getString("associationDocId"));
                            mTournamentEventsModel.setEventPublishEntries(eventsJsonObject.getString("publishEntries"));
                            mTournamentEventsModel.setShowToggle(Boolean.parseBoolean(eventsJsonObject.getString("showToggle")));
                            mTournamentEventsModel.setEventStatus(eventsJsonObject.getString("status"));
                            mTournamentEventsModel.setPayAction(eventsJsonObject.getString("payAction"));
                            mTournamentEventsModel.setEntryFee(eventsJsonObject.getString("entryFee"));
                            mTournamentEventsModel.setTournamentName(eventsJsonObject.getString("tournamentName"));

                            paytmDetails = new Paymentdetails();

                            JSONObject paytmObj = eventsJsonObject.getJSONObject("paymentdetails");
                            paytmDetails.setAmount(paytmObj.getString("amount"));
                            paytmDetails.setCurrency(paytmObj.getString("currency"));
                            paytmDetails.setGatewaypercentage(paytmObj.getString("gatewaypercentage"));
                            paytmDetails.setGatewaycharge(paytmObj.getString("gatewaycharge"));
                            paytmDetails.setDiscount(paytmObj.getString("discount"));
                            paytmDetails.setTotalamount(paytmObj.getString("totalamount"));
                            paytmDetails.setServicecharge(paytmObj.getString("servicecharge"));

                            mTournamentEventsModel.setPaymentdetails(paytmDetails);


                            eventsList.add(mTournamentEventsModel);
//                            testEventsRetrofit(i, mTournamentEventsModel.getEventDocId());
                        }
                    }

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());


                }

                try {
                    listDataChild.put(listDataHeader.get(value - 1), eventsList);
                    //Log.e("Ramesh", "title>>" + listDataHeader.get(value-1) + "pos>" + value + " >>size " + eventsList.size());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Header, Child data
                /*listDataChild.put(listDataHeader.get(1), eventsList);// Header, Child data
                listDataChild.put(listDataHeader.get(2), eventsList);// Header, Child data
                listDataChild.put(listDataHeader.get(3), eventsList);// Header, Child data*/

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Network error");
            }

/*
            if (eventsList.size() > 0) {
                txtNoTournaments.setVisibility(View.GONE);
                expListView.setVisibility(View.VISIBLE);
            } else {
                txtNoTournaments.setVisibility(View.VISIBLE);
                expListView.setVisibility(View.GONE);
            }*/

            //preparing list data
            //prepareListData();

            if (totalHeadersCount == ctrl) {
                // listAdapter.setListener(ViewTournamentFragmentNew.this);
//                listAdapter = new TournamentListAdapter(mContext, listDataHeader, listDataChild, null/*tournaments*/, ViewTournamentFragmentNew.this, getActivity());

                //setting list adapter
                expListView.setAdapter(listAdapter);
                expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        //Log.d("onGroupClick:", "worked----->"+listDataChild.get(listDataHeader.get(groupPosition)).size());

                        if (listDataChild.get(listDataHeader.get(groupPosition)).size() == 0) {
                            Helper.showToast(getActivity(), "No events");
                        }
                        return false;
                    }
                });
            }


        }
    }

    public void testEventsRetrofit(final int i, final String docId) {
        //INIT

        RestApi.get()
                .getRestService()
                .getScheduleMatches(docId, "ALL", "CURRENT")
                .enqueue(new Callback<SchedulePlayerResponse>() {
                    @Override
                    public void onResponse(Call<SchedulePlayerResponse> call, Response<SchedulePlayerResponse> response) {
                        // Log.e("Ramesh", "index>>" + i + "docId>>" + docId + "<<<Response>>" + response.body().toString());
                    }

                    @Override
                    public void onFailure(Call<SchedulePlayerResponse> call, Throwable t) {

                    }
                });
    }


}
