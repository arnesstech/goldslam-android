package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.MediaHolder;
import com.slam.gss.goldslam.holder.PlayingClubLocatorHolder;
import com.slam.gss.goldslam.models.media.MediaResponse;
import com.slam.gss.goldslam.models.playingClubLocators.PlayingClubLocators;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class MediAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MediaResponse> mMediaList;
    private Context mContext;

    public MediAdapter(Context mContext) {
        this.mContext = mContext;

    }

    public void addItem(List<MediaResponse> mediaResponse) {
        mMediaList = new ArrayList<>(mediaResponse);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vEmp = inflater.inflate(R.layout.item_media, parent, false);
        viewHolder = new MediaHolder(vEmp);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MediaHolder mediaHolderHolder = (MediaHolder) holder;
        configureViewHolderFilter(mediaHolderHolder, position);
    }

    private void configureViewHolderFilter(MediaHolder holder, int position) {
        holder.getTxtMediaInfo().setText(mMediaList.get(position).getMediaText());
    }

    @Override
    public int getItemCount() {
        return mMediaList.size();
    }
}
