package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.PlayingClubLocatorsAdapter;
import com.slam.gss.goldslam.adapters.ShopingItemAdapter;
import com.slam.gss.goldslam.models.Shopping.ShoppingItems;
import com.slam.gss.goldslam.models.playingClubLocators.PlayingClubLocators;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class ShopNowFragment extends Fragment {

    @BindView(R.id.rv_shopping_items)
    RecyclerView rvShoppingItems;

    private ProgressDialog progressDialog;
    ArrayList<ShoppingItems> mShoppingItemsList;
    RecyclerView.LayoutManager mLayoutManager;
    ShopingItemAdapter mShoppingItemsAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_shop_now, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        rvShoppingItems.setLayoutManager(mLayoutManager);
        rvShoppingItems.setItemAnimator(new DefaultItemAnimator());
        rvShoppingItems.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mShoppingItemsList = new ArrayList<>();
        mShoppingItemsAdapter = new ShopingItemAdapter(getActivity(), mShoppingItemsList);
        loadShoppingItems();
    }

    private void loadShoppingItems() {

        ShoppingItems items = new ShoppingItems();
        items.setCost("10.2");
        items.setName("SMC FOundation");
        items.setRating("4.2");
        mShoppingItemsList.add(items);

        items = new ShoppingItems();
        items.setCost("10.2");
        items.setName("SMC FOundation");
        items.setRating("4.2");
        mShoppingItemsList.add(items);

        items = new ShoppingItems();
        items.setCost("10.2");
        items.setName("SMC FOundation");
        items.setRating("4.2");
        mShoppingItemsList.add(items);

        items = new ShoppingItems();
        items.setCost("10.2");
        items.setName("SMC FOundation");
        items.setRating("4.2");
        mShoppingItemsList.add(items);

        rvShoppingItems.setAdapter(mShoppingItemsAdapter);
        mShoppingItemsAdapter.notifyDataSetChanged();

    }
}
