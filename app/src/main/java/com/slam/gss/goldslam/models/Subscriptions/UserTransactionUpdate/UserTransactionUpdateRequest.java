package com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate;

import com.slam.gss.goldslam.models.Paymentdetails;

/**
 * Created by Thriveni on 3/20/2017.
 */

public class UserTransactionUpdateRequest {
    private String userdocId;

    private String status;

    private String paymentType;

    private String gatewayTransactionId;

    private String subscriptionType;

    private String remarks;

    private Paymentdetails paymentdetails;

    private String subscriptionId;

    private String discount;

    private String transactionDocId;

    private String amount;

    private String paymentAmount;

    private String transactionType;

    private String duration;

    private String gssId;

    private String eventId;

    private String name;

    private String eventName;

    private String orderid;

    private String subscriptionTypeId;

    private String subscriptionName;

    public String getUserdocId ()
    {
        return userdocId;
    }

    public void setUserdocId (String userdocId)
    {
        this.userdocId = userdocId;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getPaymentType ()
    {
        return paymentType;
    }

    public void setPaymentType (String paymentType)
    {
        this.paymentType = paymentType;
    }

    public String getGatewayTransactionId ()
    {
        return gatewayTransactionId;
    }

    public void setGatewayTransactionId (String gatewayTransactionId)
    {
        this.gatewayTransactionId = gatewayTransactionId;
    }

    public String getSubscriptionType ()
    {
        return subscriptionType;
    }

    public void setSubscriptionType (String subscriptionType)
    {
        this.subscriptionType = subscriptionType;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    public Paymentdetails getPaymentdetails ()
    {
        return paymentdetails;
    }

    public void setPaymentdetails (Paymentdetails paymentdetails)
    {
        this.paymentdetails = paymentdetails;
    }

    public String getSubscriptionId ()
    {
        return subscriptionId;
    }

    public void setSubscriptionId (String subscriptionId)
    {
        this.subscriptionId = subscriptionId;
    }

    public String getDiscount ()
    {
        return discount;
    }

    public void setDiscount (String discount)
    {
        this.discount = discount;
    }

    public String getTransactionDocId ()
    {
        return transactionDocId;
    }

    public void setTransactionDocId (String transactionDocId)
    {
        this.transactionDocId = transactionDocId;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getPaymentAmount ()
    {
        return paymentAmount;
    }

    public void setPaymentAmount (String paymentAmount)
    {
        this.paymentAmount = paymentAmount;
    }

    public String getTransactionType ()
    {
        return transactionType;
    }

    public void setTransactionType (String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getGssId ()
    {
        return gssId;
    }

    public void setGssId (String gssId)
    {
        this.gssId = gssId;
    }

    public String getEventId ()
    {
        return eventId;
    }

    public void setEventId (String eventId)
    {
        this.eventId = eventId;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getEventName ()
    {
        return eventName;
    }

    public void setEventName (String eventName)
    {
        this.eventName = eventName;
    }

    public String getOrderid ()
    {
        return orderid;
    }

    public void setOrderid (String orderid)
    {
        this.orderid = orderid;
    }

    public String getSubscriptionTypeId ()
    {
        return subscriptionTypeId;
    }

    public void setSubscriptionTypeId (String subscriptionTypeId)
    {
        this.subscriptionTypeId = subscriptionTypeId;
    }

    public String getSubscriptionName ()
    {
        return subscriptionName;
    }

    public void setSubscriptionName (String subscriptionName)
    {
        this.subscriptionName = subscriptionName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [userdocId = "+userdocId+", status = "+status+", paymentType = "+paymentType+", gatewayTransactionId = "+gatewayTransactionId+", subscriptionType = "+subscriptionType+", remarks = "+remarks+", paymentdetails = "+paymentdetails+", subscriptionId = "+subscriptionId+", discount = "+discount+", transactionDocId = "+transactionDocId+", amount = "+amount+", paymentAmount = "+paymentAmount+", transactionType = "+transactionType+", duration = "+duration+", gssId = "+gssId+", eventId = "+eventId+", name = "+name+", eventName = "+eventName+", orderid = "+orderid+", subscriptionTypeId = "+subscriptionTypeId+", subscriptionName = "+subscriptionName+"]";
    }}
