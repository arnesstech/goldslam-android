package com.slam.gss.goldslam.models.uploadFile;

/**
 * Created by Thriveni on 4/6/2017.
 */

public class Data {
    private String fileId;

    private String message;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClassPojo [fileId = " + fileId + ", message = " + message + "]";
    }
}
