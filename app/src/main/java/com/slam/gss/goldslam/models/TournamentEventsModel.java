package com.slam.gss.goldslam.models;

/**
 * Created by Murali on 13-08-2016.
 */
public class TournamentEventsModel {

    public String eventName;
    public String signingStartTime;
    public String signingEndTime;
    public String eventDocId;
    public String eventAction;
    public int eventTeamSize;
    public String eventAssociationName;
    public boolean showToggle;
    public String eventStatus;
    public String payAction;
    public String entryFee;
    private Paymentdetails paymentdetails;
    private String tournamentName;

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public Paymentdetails getPaymentdetails() {
        return paymentdetails;
    }

    public void setPaymentdetails(Paymentdetails paymentdetails) {
        this.paymentdetails = paymentdetails;
    }

    public String getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(String entryFee) {
        this.entryFee = entryFee;
    }

    public String getPayAction() {
        return payAction;
    }

    public void setPayAction(String payAction) {
        this.payAction = payAction;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public boolean isShowToggle() {
        return showToggle;
    }

    public void setShowToggle(boolean showToggle) {
        this.showToggle = showToggle;
    }

    public String getEventAssociationId() {
        return eventAssociationId;
    }

    public void setEventAssociationId(String eventAssociationId) {
        this.eventAssociationId = eventAssociationId;
    }

    public String eventAssociationId;

    public String getEventPublishEntries() {
        return eventPublishEntries;
    }

    public void setEventPublishEntries(String eventPublishEntries) {
        this.eventPublishEntries = eventPublishEntries;
    }

    public String eventPublishEntries;

    public String getEventAssociationName() {
        return eventAssociationName;
    }

    public void setEventAssociationName(String eventAssociationName) {
        this.eventAssociationName = eventAssociationName;
    }

    public int getEventTeamSize() {
        return eventTeamSize;
    }

    public void setEventTeamSize(int eventTeamSize) {
        this.eventTeamSize = eventTeamSize;
    }


    public String getEventAction() {
        return eventAction;
    }

    public void setEventAction(String eventAction) {
        this.eventAction = eventAction;
    }

    public String getSigningStartTime() {
        return signingStartTime;
    }

    public void setSigningStartTime(String signingStartTime) {
        this.signingStartTime = signingStartTime;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getSigningEndTime() {
        return signingEndTime;
    }

    public void setSigningEndTime(String signingEndTime) {
        this.signingEndTime = signingEndTime;
    }

    public String getEventDocId() {
        return eventDocId;
    }

    public void setEventDocId(String eventDocId) {
        this.eventDocId = eventDocId;
    }
}
