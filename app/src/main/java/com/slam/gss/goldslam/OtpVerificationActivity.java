package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.network.HttpHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

/**
 * Created by Thriveni on 8/10/2016.
 */
public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edMobileNo, edVerificationCode;
    private Button btnSubmit, btnCancel;
    public HttpHelper httpHelper = null;
    public String otpVerificationResponse = "", verificationCode = "", mobileNo = "";
    private TextInputLayout mobile_Layout, verificationCode_Layout;
    private Intent intent=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verification);

       // edMobileNo = (EditText) findViewById(R.id.mobileNo);
        edVerificationCode = (EditText) findViewById(R.id.verificationCode);
        edMobileNo.addTextChangedListener(new MyTextWatcher(edMobileNo));
        edVerificationCode.addTextChangedListener(new MyTextWatcher(edVerificationCode));

       // mobile_Layout = (TextInputLayout) findViewById(R.id.mobileNo_layout_name);
       // verificationCode_Layout = (TextInputLayout) findViewById(R.id.verification_code_layout_name);

       /* btnSubmit = (Button) findViewById(R.id.submit);
        btnCancel = (Button) findViewById(R.id.cancel);

        btnSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);*/
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
//                case R.id.edVerificationCode:
//                    validateMobileNo();
//                    break;

                case R.id.verificationCode:
                    validateVerificationCode();
                    break;

                default:
                    break;
            }
        }
    }

    private boolean validateMobileNo() {
        if (edVerificationCode.getText().toString().trim().isEmpty()) {
            edVerificationCode.setError(getString(R.string.err_msg_mobile));
            edVerificationCode.requestFocus();
            return false;
        } else {
            mobile_Layout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateVerificationCode() {
        if (edVerificationCode.getText().toString().trim().isEmpty()) {
            edVerificationCode.setError(getString(R.string.err_msg_email));
            verificationCode_Layout.requestFocus();
            return false;
        } else {
            verificationCode_Layout.setErrorEnabled(false);
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
          /*  case R.id.submit:
                otpVerification();
                break;
            case R.id.cancel:
                //Intent intent = new Intent(OtpVerificationActivity.this,)
                finish();
                break;*/

            default:
                break;

        }
    }

    private void otpVerification() {
        if (!validateMobileNo()) {
            return;
        }

        if (!validateVerificationCode()) {
            return;
        } else {
            mobileNo = edMobileNo.getText().toString();
            verificationCode = edVerificationCode.getText().toString();
            new OtpVerificationAsynTask().execute(Connections.OTP_URL);
        }
    }



    class OtpVerificationAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(OtpVerificationActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                URL url = new URL(urls[0]);
                httpHelper = new HttpHelper();

                JSONObject otpverificationObj = new JSONObject();
                otpverificationObj.put("mobileOrEmail", mobileNo);
                otpverificationObj.put("verificationCode", verificationCode);


                Log.e("params", otpverificationObj.toString() + urls[0]);

                otpVerificationResponse = httpHelper.otpVerification(urls[0], otpverificationObj);
                return otpVerificationResponse;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            JSONObject jsonResponse = null;
            String status = null;

            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {

                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Log.i("OtpVerificationData",obj.toString());
                    Helper.registrationDocId = obj.getString("registrationDocId");
                    Helper.apiToken= obj.get("apiToken").toString();

                    Helper.showToast(getApplicationContext(), obj.getString("message"));

                    /*intent = new Intent(OtpVerificationActivity.this,PasswordActivity.class);
                    startActivity(intent);
                    finish();*/
                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getApplicationContext(), obj.getString("message"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getApplicationContext(), e.getMessage());
            }

        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent  = new Intent(OtpVerificationActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
