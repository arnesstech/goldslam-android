package com.slam.gss.goldslam.models.UpdateProfile;

/**
 * Created by Thriveni on 4/6/2017.
 */

public class Profile {
    private String lastName;

    private String occupation;

    private String schoolName;

    private String motherName;

    private String profileImage;

    private String gssid;

    private String status;

    private String classLevel;

    private String stateCode;

    private String country;

    private String pincode;

    private String qualification;

    private String registerType;

    private UserAssociations[] userAssociations;

    private String address;

    private String email;

    private String teamname;

    private String name;

    private String dob;

    private String userName;

    private String gender;

    private String docId;

    private String firstName;

    private String[] prefferedSports;

    private String fatherName;

    private String mobile;

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getOccupation ()
    {
        return occupation;
    }

    public void setOccupation (String occupation)
    {
        this.occupation = occupation;
    }

    public String getSchoolName ()
    {
        return schoolName;
    }

    public void setSchoolName (String schoolName)
    {
        this.schoolName = schoolName;
    }

    public String getMotherName ()
    {
        return motherName;
    }

    public void setMotherName (String motherName)
    {
        this.motherName = motherName;
    }

    public String getProfileImage ()
    {
        return profileImage;
    }

    public void setProfileImage (String profileImage)
    {
        this.profileImage = profileImage;
    }

    public String getGssid ()
    {
        return gssid;
    }

    public void setGssid (String gssid)
    {
        this.gssid = gssid;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getClassLevel ()
    {
        return classLevel;
    }

    public void setClassLevel (String classLevel)
    {
        this.classLevel = classLevel;
    }

    public String getStateCode ()
    {
        return stateCode;
    }

    public void setStateCode (String stateCode)
    {
        this.stateCode = stateCode;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getPincode ()
    {
        return pincode;
    }

    public void setPincode (String pincode)
    {
        this.pincode = pincode;
    }

    public String getQualification ()
    {
        return qualification;
    }

    public void setQualification (String qualification)
    {
        this.qualification = qualification;
    }

    public String getRegisterType ()
    {
        return registerType;
    }

    public void setRegisterType (String registerType)
    {
        this.registerType = registerType;
    }

    public UserAssociations[] getUserAssociations ()
    {
        return userAssociations;
    }

    public void setUserAssociations (UserAssociations[] userAssociations)
    {
        this.userAssociations = userAssociations;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getTeamname ()
    {
        return teamname;
    }

    public void setTeamname (String teamname)
    {
        this.teamname = teamname;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getUserName ()
    {
        return userName;
    }

    public void setUserName (String userName)
    {
        this.userName = userName;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getDocId ()
    {
        return docId;
    }

    public void setDocId (String docId)
    {
        this.docId = docId;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String[] getPrefferedSports ()
    {
        return prefferedSports;
    }

    public void setPrefferedSports (String[] prefferedSports)
    {
        this.prefferedSports = prefferedSports;
    }

    public String getFatherName ()
    {
        return fatherName;
    }

    public void setFatherName (String fatherName)
    {
        this.fatherName = fatherName;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lastName = "+lastName+", occupation = "+occupation+", schoolName = "+schoolName+", motherName = "+motherName+", profileImage = "+profileImage+", gssid = "+gssid+", status = "+status+", classLevel = "+classLevel+", stateCode = "+stateCode+", country = "+country+", pincode = "+pincode+", qualification = "+qualification+", registerType = "+registerType+", userAssociations = "+userAssociations+", address = "+address+", email = "+email+", teamname = "+teamname+", name = "+name+", dob = "+dob+", userName = "+userName+", gender = "+gender+", docId = "+docId+", firstName = "+firstName+", prefferedSports = "+prefferedSports+", fatherName = "+fatherName+", mobile = "+mobile+"]";
    }
}
