package com.slam.gss.goldslam.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.LocationList;
import com.slam.gss.goldslam.adapters.LocationListAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

import static com.slam.gss.goldslam.R.id.btnMap;

/**
 * Created by Thriveni on 6/6/2017.
 */

public class PlayersCoachingFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener, AdapterView.OnItemClickListener {
    public static final String[] titles = new String[]{"S M C Sports Foundation", "ABC Acadamey", "S M C Sports Foundation",
            "S M C Sports Foundation", "S M C Sports Foundation"};

    public static final String[] locatinView = new String[]{
            "Hyderabad", "Visakhapatnam", "Vijayawada", "Vizianagaram", "Skml"
    };

    public static final Integer[] images = {R.mipmap.ic_app_logo, R.mipmap.ic_app_logo, R.mipmap.ic_app_logo
            , R.mipmap.ic_app_logo, R.mipmap.ic_app_logo};
    //   public static final RatingBar[] rating = {};
    private ImageView imMap, imGrid;
    private GoogleMap mMap;
    LinearLayout mapLayout, gridLayout;
    ListView showListView;
    List<LocationList> listShow;

    private TextView temp_page_title_menu, temp_single_menu, txtMap, txtGrid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players_coachings, container, false);

        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        listShow = new ArrayList<LocationList>();
        for (int i = 0; i < titles.length; i++) {
            LocationList item = new LocationList(images[i], titles[i], locatinView[i]);
            listShow.add(item);
        }

        showListView = (ListView) getActivity().findViewById(R.id.gridList);
        LocationListAdapter adapter = new LocationListAdapter(getActivity(),
                R.layout.item_players_coachings, listShow);
        showListView.setAdapter(adapter);
        showListView.setOnItemClickListener(this);

        gridLayout = (LinearLayout) getActivity().findViewById(R.id.ll_Grid);
        imMap = (ImageView) getActivity().findViewById(btnMap);
        imGrid = (ImageView) getActivity().findViewById(R.id.btnGrid);
        txtGrid = (TextView) getActivity().findViewById(R.id.txt_grid);
        txtMap = (TextView) getActivity().findViewById(R.id.txt_map);
        mapLayout = (LinearLayout) getActivity().findViewById(R.id.ll_map);
        showListView = (ListView) getActivity().findViewById(R.id.gridList);
        imGrid.setOnClickListener(this);
        imMap.setOnClickListener(this);

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);

        temp_page_title_menu.setText("Players Coaching");
        temp_page_title_menu.setGravity(Gravity.CENTER);

        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);

        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.VISIBLE);
            temp_single_menu.setText("ADD");
            temp_single_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), AddCoachingCenterActivity.class);
                    startActivity(intent);
                }
            });
        }
    }


    public void onMapSearch(View view) {
        EditText locationSearch = (EditText) getActivity().findViewById(R.id.SearchMapstxt);
        String location = locationSearch.getText().toString();
        List<Address> addressList = null;

        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(getActivity());
            try {
                addressList = geocoder.getFromLocationName(location, 1);

            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(27.746974, 85.301582);
        LatLng india = new LatLng(15.496777, 73.827827);
        mMap.addMarker(new MarkerOptions().position(india).title("Panjim, Goa"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(india));
        mMap.addMarker(new MarkerOptions().position(sydney).title("Kathmandu, Nepal"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case btnMap:
                if (!(mapLayout.getVisibility() == View.VISIBLE)) {
                    gridLayout.setVisibility(View.GONE);
                    mapLayout.setVisibility(View.VISIBLE);
                    imMap.setImageResource(R.drawable.ic_map_on);
                    txtMap.setTextColor(getResources().getColor(R.color.wallet_holo_blue_light));
                    imGrid.setImageResource(R.drawable.ic_grid_off);
                    txtGrid.setTextColor(getResources().getColor(R.color.light_gray));
                } else {
                    mapLayout.setVisibility(View.VISIBLE);
                    gridLayout.setVisibility(View.GONE);
                    imGrid.setImageResource(R.drawable.ic_grid_off);
                    txtGrid.setTextColor(getResources().getColor(R.color.light_gray));
                    imMap.setImageResource(R.drawable.ic_map_on);
                    txtMap.setTextColor(getResources().getColor(R.color.wallet_holo_blue_light));
                }

                break;
            case R.id.btnGrid:
                if ((mapLayout.getVisibility() == View.VISIBLE)) {
                    gridLayout.setVisibility(View.VISIBLE);
                    mapLayout.setVisibility(View.GONE);
                    imGrid.setImageResource(R.drawable.ic_grid_on);
                    txtGrid.setTextColor(getResources().getColor(R.color.wallet_holo_blue_light));
                    imMap.setImageResource(R.drawable.ic_map_off);
                    txtMap.setTextColor(getResources().getColor(R.color.light_gray));
                } else {
                    mapLayout.setVisibility(View.VISIBLE);
                    gridLayout.setVisibility(View.GONE);
                    imGrid.setImageResource(R.drawable.ic_grid_off);
                    txtGrid.setTextColor(getResources().getColor(R.color.light_gray));
                    imMap.setImageResource(R.drawable.ic_map_on);
                    txtMap.setTextColor(getResources().getColor(R.color.wallet_holo_blue_light));
                }

                break;
            default:
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast toast = Toast.makeText(getActivity(),
                "Item " + (position + 1) + ": " + listShow.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}


