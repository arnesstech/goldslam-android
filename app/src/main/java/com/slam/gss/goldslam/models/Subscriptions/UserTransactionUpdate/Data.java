package com.slam.gss.goldslam.models.Subscriptions.UserTransactionUpdate;

/**
 * Created by Thriveni on 3/20/2017.
 */

public class Data {
    private String userdocId;

    private String status;

    private String paymentType;

    private String gatewayTransactionId;

    private String remarks;

    private String discount;

    private String message;

    private String transactionDocId;

    private String amount;

    private String paymentAmount;

    private String transactionType;

    private String gssId;

    private String eventId;

    private String name;

    private String eventName;

    private String orderid;

    public String getUserdocId() {
        return userdocId;
    }

    public void setUserdocId(String userdocId) {
        this.userdocId = userdocId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getGatewayTransactionId() {
        return gatewayTransactionId;
    }

    public void setGatewayTransactionId(String gatewayTransactionId) {
        this.gatewayTransactionId = gatewayTransactionId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTransactionDocId() {
        return transactionDocId;
    }

    public void setTransactionDocId(String transactionDocId) {
        this.transactionDocId = transactionDocId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getGssId() {
        return gssId;
    }

    public void setGssId(String gssId) {
        this.gssId = gssId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    @Override
    public String toString() {
        return "ClassPojo [userdocId = " + userdocId + ", status = " + status + ", paymentType = " + paymentType + ", gatewayTransactionId = " + gatewayTransactionId + ", remarks = " + remarks + ", discount = " + discount + ", message = " + message + ", transactionDocId = " + transactionDocId + ", amount = " + amount + ", paymentAmount = " + paymentAmount + ", transactionType = " + transactionType + ", gssId = " + gssId + ", eventId = " + eventId + ", name = " + name + ", eventName = " + eventName + ", orderid = " + orderid + "]";
    }
}
