package com.slam.gss.goldslam.models.eventadmin.MatchSchedule;

/**
 * Created by Thriveni on 9/13/2016.
 */
public class Data
{
    private String message;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+"]";
    }
}
