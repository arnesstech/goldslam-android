package com.slam.gss.goldslam.models.MasterData;

/**
 * Created by Thriveni on 1/6/2017.
 */
public class MasterData {
    private String key;

    private Options[] options;

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public Options[] getOptions ()
    {
        return options;
    }

    public void setOptions (Options[] options)
    {
        this.options = options;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [key = "+key+", options = "+options+"]";
    }
}
