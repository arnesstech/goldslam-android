package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;
import com.slam.gss.goldslam.adapters.SpinnerAdapter;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.eventadmin.Courts;
import com.slam.gss.goldslam.models.eventadmin.CourtsMasterDataResponse;
import com.slam.gss.goldslam.models.eventadmin.MatchConditionOptions;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.SchedulePlayerResponse;
import com.slam.gss.goldslam.models.scheduleplayers.Teams;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 9/9/2016.
 */
public class PrepareScheduleActivity extends BaseActivity {

    private Intent intent;
    private int eventPosition = 0;
    private Spinner drawTypeSpinner, venueSpinner, matchConditionSpinner;

    private String docId, tournamentDocId;
    private Call<SchedulePlayerResponse> call;
    private Call<CourtsMasterDataResponse> venueData;

    private Courts[] courts;
    private MatchConditionOptions[] matchCondtions;
    public List<Matches> matchesList;
    public String venue, matchCondition, drawTypeVal = "MAIN", eventName;
    Calendar myCalendar;
    private ProgressDialog progressDialog = null;


    ListView listView;
    //@BindView(R.id.progress)
    ProgressBar progress;
    //@BindView(R.id.empty)
    TextView empty;

    EditText edDate, edTime;

    private boolean isRefreshing;
    private ScheduleAdapter adapter;
    int selectedCourtPosition, selectedMatchConditionPosition;
    private EditText match_remarks;

    public interface OnDBUpdatedListener {
        public void OnDBUpdated();
    }

    public void OnDBUpdated() {
        // Reload list here
        progressDialog = ProgressDialog.show(PrepareScheduleActivity.this, "", "Loading...", true);
        loadPlayer();
    }

    public interface OnEventUpdatedListener {
        public void OnEventUpdated();
    }

    public void OnEventUpdated() {
        // Reload list here
        progressDialog = ProgressDialog.show(PrepareScheduleActivity.this, "", "Loading...", true);
        loadPlayer();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prepare_schedule);


        myCalendar = Calendar.getInstance();
        adapter = new ScheduleAdapter(this);
        intent = getIntent();
        eventPosition = intent.getIntExtra("eventposition", 1);
        //docId = Helper.eventList.get(eventPosition).getDocId();
        docId = getIntent().getStringExtra("docId");
        Log.e("docId", docId + "tounId" + tournamentDocId);
        //tournamentDocId = Helper.eventList.get(eventPosition).getTournamentDocId();
        tournamentDocId = getIntent().getStringExtra("tournamentId");
        eventName = intent.getStringExtra("eventName");
        Helper.tornDocId = tournamentDocId;

        empty = (TextView) findViewById(R.id.empty);

        listView = (ListView) findViewById(R.id.listView);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle(Helper.eventList.get(eventPosition).getEventName());
        getSupportActionBar().setTitle(eventName);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        drawTypeSpinner = (Spinner) findViewById(R.id.drawType);
        ArrayList<String> DrawType = new ArrayList<String>();
        DrawType.add("MAIN");
        DrawType.add("QUALIFYING");
        drawTypeSpinner.setAdapter(new SpinnerAdapter(this, R.layout.spinner_text, DrawType));

        drawTypeSpinner.setPopupBackgroundResource(R.drawable.spinner_items_background);
        drawTypeSpinner.setVisibility(View.GONE);


        drawTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                drawTypeVal = parent.getItemAtPosition(position).toString();
                loadPlayer();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        progressDialog = ProgressDialog.show(PrepareScheduleActivity.this, "", "Loading...", true);
        loadPlayer();

        SegmentedGroup drawTypeSeg = (SegmentedGroup) findViewById(R.id.drawTypeseg);
        drawTypeSeg.setTintColor(getResources().getColor(R.color.radio_button_selected_color));


        RadioButton main = (RadioButton) findViewById(R.id.main);
        RadioButton qual = (RadioButton) findViewById(R.id.qual);
        main.setChecked(true);


        drawTypeSeg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.main:
                        drawTypeVal = "MAIN";
                        progressDialog = ProgressDialog.show(PrepareScheduleActivity.this, "", "Loading...", true);
                        loadPlayer();
                        break;
                    case R.id.qual:
                        drawTypeVal = "QUALIFYING";
                        progressDialog = ProgressDialog.show(PrepareScheduleActivity.this, "", "Loading...", true);
                        loadPlayer();
                        break;

                    default:
                        break;
                }
            }
        });


    }

    public void loadPlayer() {
        // Helper.showToast(PrepareScheduleActivity.this,"loadPlayer");
        if (isRefreshing) {
            return;
        }
        //String draw = drawTypeSpinner.getSelectedItem().toString();
        isRefreshing = true;
        Call<SchedulePlayerResponse> call = RestApi.get()
                .getRestService()
                .getEventScheduleMatches(docId, drawTypeVal);
        //
        call.enqueue(new Callback<SchedulePlayerResponse>() {
            @Override
            public void onResponse(Call<SchedulePlayerResponse> call, Response<SchedulePlayerResponse> response) {
                isRefreshing = false;
                if (response.isSuccessful()) {
                    Log.i("SchedulResponse", response.body().toString());
                    if (response.body() != null && response.body().getData() != null) {
                        progressDialog.dismiss();
                        matchesList = response.body().getData().getMatches();
                        filterWithDates(response.body().getData().getMatches());

                    }

                } else {
                    //Handle errors
                    progressDialog.dismiss();
                }
//                dismissProgressDialog();
                showEmptyView();


            }

            @Override
            public void onFailure(Call<SchedulePlayerResponse> call, Throwable t) {
                progressDialog.dismiss();

                isRefreshing = false;
                if (call.isCanceled()) {
                    //Do nothing
                } else {
                    //Handle errors
                }
                showEmptyView();
//                dismissProgressDialog();
            }
        });
    }

    public void filterWithDates(List<Matches> matches) {
//        Collections.sort(matches, new Comparator<Matches>() {
//            @Override
//            public int compare(Matches lhs, Matches rhs) {
//                return (int) (Utils.convertLiveDateToMillis(rhs.getStartTime()) -
//                        Utils.convertLiveDateToMillis(lhs.getStartTime()));
//            }
//        });
        Collections.sort(matches, new Comparator<Matches>() {
            @Override
            public int compare(Matches lhs, Matches rhs) {
                try {
                    return (Integer.parseInt(rhs.getRoundNo()) - Integer.parseInt(lhs.getRoundNo()));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });

        //
        List<Matches> live = new ArrayList<>();
        List<Matches> completed = new ArrayList<>();
        List<Matches> others = new ArrayList<>();
//        for (Matches matches1 : matches) {
//            if ("live".equalsIgnoreCase(matches1.getStatus())) {
//                live.add(matches1);
//            } else if ("completed".equalsIgnoreCase(matches1.getStatus())) {
//                completed.add(matches1);
//            } else {
//                others.add(matches1);
//            }
//        }
        for (Matches matches1 : matches) {
            others.add(matches1);
        }

        Map<String, List<Matches>> map = new LinkedHashMap<>();
        filterAsHeaders(map, live);
        filterAsHeaders(map, completed);
        filterAsHeaders(map, others);
        //
        List<Matches> finalList = new ArrayList<>();
        for (Map.Entry<String, List<Matches>> entry : map.entrySet()) {
            Log.i("entry values", entry.getValue().toString());
            finalList.addAll(entry.getValue());
        }
        if (adapter != null) {
            adapter.addItems(finalList);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        showEmptyView();

    }

    private void showEmptyView() {
        if (adapter == null) {
            return;
        }
        if (adapter.getCount() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
        }
    }

    private void filterAsHeaders(Map<String, List<Matches>> map, List<Matches> list) {
        for (Matches m : list) {
            String roundName = m.getRoundName();

            if (map.get(roundName) != null) {
                map.get(roundName).add(m);
            } else {
                List<Matches> temp = new ArrayList<>();
                m.setHeader(true);
                temp.add(m);
                map.put(roundName, temp);
            }
            List<Matches> test = map.get(roundName);
            if (test != null) {
                Collections.sort(test, new Comparator<Matches>() {
                    @Override
                    public int compare(Matches lhs, Matches rhs) {
                        try {
                            return (Integer.parseInt(lhs.getMatchNo()) - Integer.parseInt(rhs.getRoundNo()));
                        } catch (Exception e) {
                            e.printStackTrace();
                            return 0;
                        }

                    }
                });
            }
            for (Matches temp : test) {
                temp.setHeader(false);
            }
            if (!test.isEmpty()) {
                test.get(0).setHeader(true);
            }
        }
    }

    class PrepareScheduleViewHolder extends AbstractViewHolder {

        @BindView(R.id.txt_date)
        TextView date;

        @BindView(R.id.txt_player1_position)
        TextView player1Position;


        @BindView(R.id.txt_player2_position)
        TextView player2Position;
        @BindView(R.id.txt_header)
        TextView header;

        @BindView(R.id.txt_time)
        TextView time;

        @BindView(R.id.txt_venue)
        TextView venue;
        @BindView(R.id.txt_player1)
        TextView player1;
        @BindView(R.id.txt_player2)
        TextView player2;
        @BindViews({
                R.id.txt_pscore1, R.id.txt_pscore2, R.id.txt_pscore3,
                R.id.txt_pscore4, R.id.txt_pscore5,
        })
        List<TextView> scoreView1;
        @BindViews({
                R.id.txt_p2score1, R.id.txt_p2score2, R.id.txt_p2score3,
                R.id.txt_p2score4, R.id.txt_p2score5,
        })
        List<TextView> scoreView2;

        public View getScore1() {
            return score1;
        }

        public View getScore2() {
            return score2;
        }

        public TextView getPlayer1Position() {
            return player1Position;
        }

        public TextView getPlayer2Position() {
            return player2Position;
        }

        //
        @BindView(R.id.score_1)
        View score1;
        @BindView(R.id.score_2)
        View score2;

        @BindView(R.id.btn_schedule)
        View btnSchedule;

        @BindView(R.id.btn_change_schedule)
        View btnChangeSchedule;
        @BindView(R.id.btn_add_score)
        View btnScore;

        public PrepareScheduleViewHolder(View view) {
            super(view);
        }

        public View getBtnSchedule() {
            return btnSchedule;
        }

        public View getBtnChangeSchedule() {
            return btnChangeSchedule;
        }

        public View getBtnScore() {
            return btnScore;
        }

        public TextView getPlayer2() {
            return player2;
        }

        public TextView getPlayer1() {
            return player1;
        }

        public List<TextView> getScoreView1() {
            return scoreView1;
        }

        public TextView getHeader() {
            return header;
        }

        public List<TextView> getScoreView2() {
            return scoreView2;
        }

        public TextView getDate() {
            return date;
        }

        public TextView getTime() {
            return time;
        }

        public TextView getVenue() {
            return venue;
        }
    }

    private class ScheduleAdapter extends AbstractBaseAdapter<Matches, PrepareScheduleViewHolder> {

        public ScheduleAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.prepare_schedule_row;
        }

        @Override
        public PrepareScheduleViewHolder getViewHolder(View convertView) {
            return new PrepareScheduleViewHolder(convertView);
        }

        @Override
        public void bindView(int position, PrepareScheduleViewHolder holder, final Matches item) {

            try {
                if (item.isHeader()) {
                    holder.getHeader().setVisibility(View.VISIBLE);
                } else {
                    holder.getHeader().setVisibility(View.GONE);
                }
                holder.getHeader().setText(item.getRoundName());
                List<Teams> teams = item.getTeams();

                // holder.getVenue().setText(item.getCourt().getCourtName());
                final String[] split = item.getStartTime().split(" ");
                String sDate = split.length > 0 ? split[0] : "";
                String sTime = split.length > 1 ? split[1] : "";
                holder.getDate().setText(sDate);
                holder.getTime().setText(sTime);
                holder.getPlayer1().setText(teams.size() > 0 ? teams.get(0).getPosition() + " " + teams.get(0).getName() : "");
                holder.getPlayer2().setText(teams.size() > 1 ? teams.get(1).getPosition() + " " + teams.get(1).getName() : "");
                //First player scores
                setPlayerScores(teams.size() > 0 ? teams.get(0).getScores() : new ArrayList<String>(),
                        holder.getScoreView1());
                //Second player scores
                setPlayerScores(teams.size() > 1 ? teams.get(1).getScores() : new ArrayList<String>(),
                        holder.getScoreView2());
                //
                holder.getScore1().setVisibility(View.GONE);
                holder.getScore2().setVisibility(View.GONE);
                holder.getBtnScore().setVisibility(View.GONE);
                holder.getBtnChangeSchedule().setVisibility(View.GONE);
                holder.getBtnSchedule().setVisibility(View.GONE);
                if ("LIVE".equalsIgnoreCase(item.getStatus())
                        || "COMPLETED".equalsIgnoreCase(item.getStatus())) {
                    //
                    holder.getBtnScore().setVisibility(View.VISIBLE);
                    //Show score layouts
                    holder.getScore1().setVisibility(View.VISIBLE);
                    holder.getScore2().setVisibility(View.VISIBLE);

                } else if ("SCHEDULED".equalsIgnoreCase(item.getStatus())) {
                    holder.getBtnChangeSchedule().setVisibility(View.VISIBLE);
                    holder.getBtnScore().setVisibility(View.VISIBLE);

                } else if ("YET_TO_SCHEDULE".equalsIgnoreCase(item.getStatus())) {
                    holder.getBtnSchedule().setVisibility(View.VISIBLE);
                } else if ("BYE".equalsIgnoreCase(item.getStatus())) {
                    //Need to confirm what we need to on this status.
                }
                final int mPos = position;
                holder.getBtnScore().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogFragment dialogFragment = AddScoreFragment.newInstance(item);
                        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);

                        dialogFragment.show(getSupportFragmentManager(), "score_fragment");
                    }
                });
                holder.getBtnChangeSchedule().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Helper.selectedMatch = item;
                        DialogFragment matchScheduleFragment = MatchScheduleFragment.newInstance(item);
                        matchScheduleFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                        matchScheduleFragment.show(getSupportFragmentManager(), "schedule_fragment");
                    }
                });
                holder.getBtnSchedule().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Helper.selectedMatch = item;

                        DialogFragment matchScheduleFragment = MatchScheduleFragment.newInstance(item);
                        matchScheduleFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                        matchScheduleFragment.show(getSupportFragmentManager(), "schedule_fragment");

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void setPlayerScores(List<String> scores2, List<TextView> viewList) {
            int index = 0;
            for (TextView textView : viewList) {
                if (index < scores2.size()) {
                    textView.setText(scores2.get(index));
                } else {
                    textView.setText("-");
                }
                index++;
            }

        }

    }


}
