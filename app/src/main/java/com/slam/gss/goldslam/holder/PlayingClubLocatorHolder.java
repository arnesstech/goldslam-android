package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class PlayingClubLocatorHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.txt_playing_club_name)
    TextView txtPlayingClubName;

    @BindView(R.id.rb_rating)
    RatingBar rbRating;

    @BindView(R.id.txt_rating)
    TextView txtRating;

    @BindView(R.id.txt_location)
    TextView txtLocation;

    @BindView(R.id.list_image)
    ImageView imCluBImage;

    public ImageView getImCluBImage(){
        return imCluBImage;
    }

    public TextView getTxtPlayingClubName() {
        return txtPlayingClubName;
    }

    public RatingBar getRbRating() {
        return rbRating;
    }

    public TextView getTxtRating() {
        return txtRating;
    }

    public TextView getTxtLocation() {
        return txtLocation;
    }

    public PlayingClubLocatorHolder(View itemView) {
        super(itemView);
    }
}
