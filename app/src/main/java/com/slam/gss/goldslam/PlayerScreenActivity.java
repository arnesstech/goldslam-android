package com.slam.gss.goldslam;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.slam.gss.goldslam.adapters.TabsPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ramesh on 20/8/16.
 */
public class PlayerScreenActivity extends BaseActivity {

    private TabsPagerAdapter1 mAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private List<TabsData> dataList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra("eventName"));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //
        viewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new TabsPagerAdapter1(getSupportFragmentManager());
        mAdapter.addFragment(new LiveFragment(), "Live");
        mAdapter.addFragment(new ScheduleFragment(), "Schedule");
        mAdapter.addFragment(new DrawResultsFragment(), "Draws");
        mAdapter.addFragment(new PlayersFragment(), "Players");
        viewPager.setAdapter(mAdapter);
        //
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        dataList = createTabsList();
    }

    public class TabsPagerAdapter1 extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private int ctrl;


        public TabsPagerAdapter1(final FragmentManager manager) {
            super(manager);
            this.ctrl=ctrl;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
           /* if(ctrl == 1){
                return super.getItemPosition(object);
            }else{
                return POSITION_NONE;
            }*/

        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        /**
         * @param fragment
         * @param title
         */
        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }


    protected List<TabsData> createTabsList() {
        List<TabsData> list = new ArrayList<>();
        list.add(new TabsData("Live"));
        list.add(new TabsData("Schedule"));
        list.add(new TabsData("Draws"));
        list.add(new TabsData("Players"));
        return list;
    }

    public class TabsData {

        private String title;
        private Drawable drawable;

        public TabsData(String title) {
            this.title = title;
        }

        public TabsData(String title, Drawable drawableId) {
            this.title = title;
            this.drawable = drawableId;
        }

        public String getTitle() {
            return title;
        }

        public Drawable getDrawable() {
            return drawable;
        }
    }

}
