package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;

import butterknife.ButterKnife;

/**
 * Created by Thriveni on 1/6/2017.
 */
public class CountryHolder extends AbstractViewHolder {

    public TextView txtCountryName;

    public TextView getCountryName() {
        return txtCountryName;
    }

    public CountryHolder(View v) {
        super(v);
        txtCountryName = (TextView) v.findViewById(R.id.txtGender);
    }
}
