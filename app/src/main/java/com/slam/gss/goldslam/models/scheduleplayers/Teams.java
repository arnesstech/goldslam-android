package com.slam.gss.goldslam.models.scheduleplayers;

import java.util.List;

public class Teams {
    private String entryMode;

    private String position;

    private String rank;

    private List<String> scores;

    private String status;

    private Players[] players;

    private String name;

    private String nextMatchDocId;

    private String remarks;

    private String docId;

    private String previousMatchDocId;

    public String getEntryMode() {
        return entryMode;
    }

    public void setEntryMode(String entryMode) {
        this.entryMode = entryMode;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public List<String> getScores() {
        return scores;
    }

    public void setScores(List<String> scores) {
        this.scores = scores;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Players[] getPlayers() {
        return players;
    }

    public void setPlayers(Players[] players) {
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNextMatchDocId() {
        return nextMatchDocId;
    }

    public void setNextMatchDocId(String nextMatchDocId) {
        this.nextMatchDocId = nextMatchDocId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getPreviousMatchDocId() {
        return previousMatchDocId;
    }

    public void setPreviousMatchDocId(String previousMatchDocId) {
        this.previousMatchDocId = previousMatchDocId;
    }

    @Override
    public String toString() {
        return getName();
    }
}