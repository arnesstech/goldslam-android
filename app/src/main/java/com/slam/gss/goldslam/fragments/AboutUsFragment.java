package com.slam.gss.goldslam.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.slam.gss.goldslam.R;

/**
 * Created by Thriveni on 3/16/2017.
 */

public class AboutUsFragment extends Fragment {
    private TextView temp_page_title_menu, temp_single_menu;
    private ImageView imFilter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_about_us,container,false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText("     About Us");
        temp_page_title_menu.setGravity(Gravity.CENTER);

        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.INVISIBLE);
        }

        imFilter = (ImageView) getActivity().findViewById(R.id.im_filter);
        if (imFilter != null) {
            imFilter.setVisibility(View.GONE);
        }

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        return view;
    }
}
