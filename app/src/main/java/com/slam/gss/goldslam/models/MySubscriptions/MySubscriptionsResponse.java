package com.slam.gss.goldslam.models.MySubscriptions;

/**
 * Created by Thriveni on 5/19/2017.
 */

public class MySubscriptionsResponse {
    private String status;

    private Data data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
