package com.slam.gss.goldslam;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.slam.gss.goldslam.fragments.DialogBox;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.models.Login.Data;
import com.slam.gss.goldslam.models.Login.LoginResponse;
import com.slam.gss.goldslam.network.NetworkConnectivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

/**
 * Created by Thriveni on 26-07-2016.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_activity);

        Format formatter = new SimpleDateFormat("dd-MM-yyyy");
        String s = formatter.format(new Date());

        String lastAppOpendTime = Utils.getString(this, "lastAppOpendTime", 1);
        if (s.equalsIgnoreCase(lastAppOpendTime)) {
            Utils.putString(SplashActivity.this, "newDate", String.valueOf(false), 2);
        } else {
            Utils.putString(SplashActivity.this, "newDate", String.valueOf(false), 2);
            Utils.putString(SplashActivity.this, "lastAppOpendTime", s, 1);
        }
        networkConnectionCheck();
    }

    private void loginCheck() {
        boolean authenticationFlag = Boolean.parseBoolean(Utils.getString(SplashActivity.this,"auth",2));
         if (authenticationFlag) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, AppDashBoard.class));
                    finish();
                }
            }, 500);
        } else {
            new Handler().postDelayed(new Runnable() {


                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }, 500);
        }
    }

    private void networkConnectionCheck() {
        Log.e("network status", "" + NetworkConnectivity.isAvailable(getApplicationContext()));
        if (!NetworkConnectivity.isAvailable(getApplicationContext())) {
            showDialog(Utils.ALERT_NETWORK_NOT_FOUND);
        } else {

            String userCredentials = Utils.getString(SplashActivity.this,"userCredentials",1);
            Log.e("SplashActivity", "Credentails:::::" + userCredentials);

            if ("".equalsIgnoreCase(userCredentials)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        finish();
                    }
                }, 500);
            } else {
                new LOGINsyncTask(userCredentials).execute(Connections.LOGIN_URL);
            }
        }


    }

    class LOGINsyncTask extends AsyncTask<String, Void, String> {

        String userCredentails = "";

        public LOGINsyncTask(String userCredentails) {
            this.userCredentails = userCredentails;
        }

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(SplashActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            try {

                HttpPost httpPost = new HttpPost(urls[0]);
                String credentials = userCredentails;
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                httpPost.addHeader("Authorization", "Basic " + base64EncodedCredentials);
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                return responseBody.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;
            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.e("login Response", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Utils.putString(SplashActivity.this,"subscriptionFlag",obj.getString("subscriptionRequired"),2);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            /*startActivity(new Intent(SplashActivity.this, AppDashBoard.class));*/
                            Utils.navigateToActivity(SplashActivity.this,AppDashBoard.class);

                            finish();
                        }
                    }, 200);

                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                            finish();
                        }
                    }, 200);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


    public void showDialog(String message) {
        DialogBox newFragment = DialogBox.newInstance("",message);
        newFragment.show(getSupportFragmentManager(), DialogBox.TAG_DIALOG);
        newFragment.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
    }
}



