package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.TournamentsListener;
import com.slam.gss.goldslam.ViewTournamentFragment;
import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.holder.UserAssociationsHolder;
import com.slam.gss.goldslam.models.Associations.AssociationList;
import com.slam.gss.goldslam.models.Associations.Associations;
import com.slam.gss.goldslam.models.UpdateProfile.UserAssociations;
import com.slam.gss.goldslam.models.UserSelectedAssociations;
import com.slam.gss.goldslam.models.UserSelectedAssociationsList;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 2/20/2017.
 */
public class AssociationsFragment extends DialogFragment {
    private Associations associations;
    private ListView lvAssociations;
    private TextView txtNoAssociationsData;
    private List<com.slam.gss.goldslam.models.UserSelectedAssociations> AssociationsList = new ArrayList<>();
    private List<com.slam.gss.goldslam.models.UserSelectedAssociations> tempAssociationsList = new ArrayList<>();
    private UserSelectedAssociationsAdapter selectedAssociationsAdapter;
    private Button btnSubmit, btnCancel;
    private com.slam.gss.goldslam.models.UserSelectedAssociations userSelectedAssociations;
    private UserSelectedAssociationsList selectedAssociationsList = new UserSelectedAssociationsList();
    private UserSelectedAssociationsList tempList = new UserSelectedAssociationsList();
    private List<com.slam.gss.goldslam.models.UserSelectedAssociations> assList = new ArrayList<>();
    private SharedPreferences sp;
    private SharedPreferences.Editor edit;
    private TournamentsListener listener;
    private ProgressDialog progressDialog;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.empty)
    TextView empty;
    @BindView(R.id.btn_layout)
    LinearLayout llBtnLayout;

    Call<AssociationList> getAssociations;
    ArrayList<String> tempAssIds = new ArrayList<>();


    public static AssociationsFragment newInstance() {
        AssociationsFragment fragment = new AssociationsFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedAssociationsAdapter = new UserSelectedAssociationsAdapter(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return LayoutInflater.from(getActivity()).inflate(R.layout.select_asociations, container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getAssociations != null) {
            getAssociations.cancel();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        lvAssociations = (ListView) view.findViewById(R.id.lv_associations);
        lvAssociations.setAdapter(selectedAssociationsAdapter);

        txtNoAssociationsData = (TextView) view.findViewById(R.id.txt_no_associations_data);
        txtNoAssociationsData.setVisibility(View.INVISIBLE);

        sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);

        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempList.setUserSelectedAssociations(AssociationsList);
                String obj = new Gson().toJson(tempList);
                Log.e("tempList", obj.toString());
                sp.edit().putString("AssociationFilter", obj).commit();
                if (listener != null) {
                    listener.notifyTournament("c");
                }
                dismiss();
            }
        });

        btnSubmit = (Button) view.findViewById(R.id.btn_filter);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isSelected = false;

                for (int i = 0; i < AssociationsList.size(); i++) {
                    AssociationsList.get(i).setAssociationFlag(false);
                    if (tempAssIds.size() > 0) {
                        for (int j = 0; j < tempAssIds.size(); j++) {
                            // Log.e("Id", AssociationsList.get(i).getUserSelectedAssociaitonsId() + ".....>" + tempAssIds.get(j));

                            if (AssociationsList.get(i).getUserSelectedAssociaitonsId().equalsIgnoreCase(tempAssIds.get(j))) {

                                AssociationsList.get(i).setAssociationFlag(true);
                                isSelected=true;
                            } else {
                                // AssociationsList.get(i).setAssociationFlag(false);
                            }
                        }

                        selectedAssociationsList.setUserSelectedAssociations(AssociationsList);
                        String obj = new Gson().toJson(selectedAssociationsList);

                        sp.edit().putString("AssociationFilter", obj).commit();
                        if (listener != null) {
                            listener.notifyTournament("s");
                        }
                        dismiss();

                    } else {

                        AssociationsList.get(i).setAssociationFlag(false);
                    }
                }

                if(!isSelected){
                    Helper.showToast(getActivity(), "Please select an association");
                }


            }
        });

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadAssociations();

    }

    private void loadAssociations() {

        showProgressDialog();

        getAssociations = RestApi.get()
                .getRestService()
                .getAssociations();

        getAssociations.enqueue(new Callback<AssociationList>() {
            @Override
            public void onResponse(Call<AssociationList> call, Response<AssociationList> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null && response.body().getData().getAssociations() != null) {
                        String obj = new Gson().toJson(response.body());
                        Log.e("asss", obj.toString());
                        associations = new Gson().fromJson(obj, Associations.class);

                        Associations[] mainLIst = response.body().getData().getAssociations();
                        List<UserSelectedAssociations> temList = new ArrayList<UserSelectedAssociations>();

                        if (mainLIst.length > 0) {
                            UserSelectedAssociationsList previousFilterList = new Gson().fromJson(sp.getString("AssociationFilter", ""), UserSelectedAssociationsList.class);
                            if (previousFilterList == null) {
                                previousFilterList = new UserSelectedAssociationsList();
                            }
                            for (int i = 0; i < mainLIst.length; i++) {
                                userSelectedAssociations = new com.slam.gss.goldslam.models.UserSelectedAssociations();
                                userSelectedAssociations.setUserSelectedAssociationName(mainLIst[i].getAssociationName());
                                userSelectedAssociations.setUserSelectedAssociaitonsId(mainLIst[i].getAssociationId());
                                Associations tempObj = mainLIst[i];
                                try {
                                    boolean isChecked = false;
                                    for (int j = 0; j < previousFilterList.getUserSelectedAssociations().size(); j++) {
                                        UserSelectedAssociations previousObj = previousFilterList.getUserSelectedAssociations().get(j);
                                        // if (previousFilterList != null && previousFilterList.getUserSelectedAssociations() != null && previousFilterList.getUserSelectedAssociations().size() > 0) {
                                        if (previousObj.isAssociationSelectionFlag() && previousObj.getUserSelectedAssociaitonsId().equalsIgnoreCase(tempObj.getAssociationId())) {
                                            //tempAssIds.add(previousFilterList.getUserSelectedAssociations().get(i).getUserSelectedAssociaitonsId());
                                            //userSelectedAssociations.setAssociationSelectionFlag(true);
                                            isChecked = true;
                                            break;
                                        }
                                        /* else {
                                            userSelectedAssociations.setAssociationSelectionFlag(false);
                                        }*/
                                        // } else {
                                        //userSelectedAssociations.setAssociationSelectionFlag(false);
                                        //}

                                    }

                                    if (isChecked) {
                                        tempAssIds.add(previousFilterList.getUserSelectedAssociations().get(i).getUserSelectedAssociaitonsId());
                                        userSelectedAssociations.setAssociationSelectionFlag(true);
                                    } else {
                                        userSelectedAssociations.setAssociationSelectionFlag(false);
                                    }
//                                    if (previousFilterList != null && previousFilterList.getUserSelectedAssociations() != null && previousFilterList.getUserSelectedAssociations().size() > 0) {
//                                        if (previousFilterList.getUserSelectedAssociations().get(i).isAssociationSelectionFlag()) {
//                                            tempAssIds.add(previousFilterList.getUserSelectedAssociations().get(i).getUserSelectedAssociaitonsId());
//                                            userSelectedAssociations.setAssociationSelectionFlag(true);
//                                        } else {
//                                            userSelectedAssociations.setAssociationSelectionFlag(false);
//                                        }
//                                    } else {
//                                        userSelectedAssociations.setAssociationSelectionFlag(false);
//                                    }
                                    temList.add(userSelectedAssociations);
                                    tempList.setUserSelectedAssociations(temList);
                                    AssociationsList.add(userSelectedAssociations);
                                    tempAssociationsList.add(userSelectedAssociations);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            selectedAssociationsAdapter.addItems(temList);
                            lvAssociations.setAdapter(selectedAssociationsAdapter);

                            dismissProgressDialog();
                            showEmptyView();

                        } else {
                            dismissProgressDialog();
                            showEmptyView();
                        }

                    }

                } else {

                    dismissProgressDialog();
                    showEmptyView();
                }
            }

            @Override
            public void onFailure(Call<AssociationList> call, Throwable t) {

                showEmptyView();
                dismissProgressDialog();

            }
        });
    }

    private void showEmptyView() {
        if (!isAdded()) {
            return;
        }
        if (selectedAssociationsAdapter.getCount() == 0) {
            empty.setVisibility(View.VISIBLE);
            lvAssociations.setVisibility(View.GONE);
            llBtnLayout.setVisibility(View.GONE);
        } else {
            empty.setVisibility(View.GONE);
            lvAssociations.setVisibility(View.VISIBLE);
            llBtnLayout.setVisibility(View.VISIBLE);
        }
    }


    protected void showProgressDialog() {
        if (progress != null) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    //@Override
    protected void dismissProgressDialog() {
        if (progress != null) {
            progress.setVisibility(View.GONE);
        }
    }

    public void setListener(TournamentsListener listener) {
        this.listener = listener;
    }

    public class UserSelectionAssociationsHolder extends AbstractViewHolder {

        TextView txtUserAssocName;
        CheckBox ckAssociationSelection;

        public TextView getUserAssoc() {
            return txtUserAssocName;
        }

        public CheckBox getSelectedAssociation() {
            return ckAssociationSelection;
        }


        public UserSelectionAssociationsHolder(View view) {
            super(view);
            txtUserAssocName = (TextView) view.findViewById(R.id.txt_select_association);
            ckAssociationSelection = (CheckBox) view.findViewById(R.id.ck_select_associations);
        }
    }

    private class UserSelectedAssociationsAdapter extends AbstractBaseAdapter<UserSelectedAssociations, UserSelectionAssociationsHolder> {

        public UserSelectedAssociationsAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.select_association_item;
        }

        @Override
        public UserSelectionAssociationsHolder getViewHolder(View convertView) {
            return new UserSelectionAssociationsHolder(convertView);
        }


        @Override
        public void bindView(final int position, UserSelectionAssociationsHolder holder, final UserSelectedAssociations item) {
            try {
                holder.getUserAssoc().setText(item.getUserSelectedAssociationName());
                holder.getSelectedAssociation().setOnCheckedChangeListener(null);
                holder.getSelectedAssociation().setChecked(item.isAssociationSelectionFlag());
                holder.getSelectedAssociation().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            // AssociationsList.get(position).setAssociationSelectionFlag(true);

                            if (tempAssIds.contains(item.getUserSelectedAssociaitonsId())) {

                            } else {
                                tempAssIds.add(AssociationsList.get(position).getUserSelectedAssociaitonsId());
                            }


                            for (int i = 0; i < tempAssIds.size(); i++) {
                                // Log.e("tempp Add",tempAssIds.get(i));
                            }


                        } else {
//                            AssociationsList.get(position).setAssociationSelectionFlag(false);

                            int index = tempAssIds.indexOf(AssociationsList.get(position).getUserSelectedAssociaitonsId());
                            tempAssIds.remove(index);

                            for (int i = 0; i < tempAssIds.size(); i++) {
                                // Log.e("tempp Del",tempAssIds.get(i));
                            }
                        }


                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
