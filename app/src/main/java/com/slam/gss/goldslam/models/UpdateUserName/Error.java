package com.slam.gss.goldslam.models.UpdateUserName;

/**
 * Created by Thriveni on 1/11/2017.
 */
public class Error {
    private String message;

    private Errors[] errors;

    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Errors[] getErrors() {
        return errors;
    }

    public void setErrors(Errors[] errors) {
        this.errors = errors;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", errors = " + errors + ", code = " + code + "]";
    }
}
