package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class ShoppingItemHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.item_name)
    TextView txtItemName;

    @BindView(R.id.item_rating)
    RatingBar rbItemRating;

    @BindView(R.id.item_cost)
    TextView txtItemCost;

    @BindView(R.id.btn_add_to_cart)
    Button btnAddToCard;

    @BindView(R.id.item_logo)
    ImageView imItemLogo;

    @BindView(R.id.btn_item_details)
    Button btnItemDetails;

    public TextView getTxtItemName() {
        return txtItemName;
    }

    public TextView getTxtItemCost() {
        return txtItemCost;
    }

    public Button getBtnAddToCard() {
        return btnAddToCard;
    }

    public Button getBtnItemDetails() {
        return btnItemDetails;
    }

    public RatingBar getRbItemRating() {
        return rbItemRating;
    }

    public ImageView getImItemLogo() {
        return imItemLogo;
    }

    public ShoppingItemHolder(View itemView) {
        super(itemView);

    }
}
