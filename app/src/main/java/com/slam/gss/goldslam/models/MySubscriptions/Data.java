package com.slam.gss.goldslam.models.MySubscriptions;

/**
 * Created by Thriveni on 5/19/2017.
 */

public class Data {
    private String message;

    private UserSubscriptions[] userSubscriptions;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public UserSubscriptions[] getUserSubscriptions ()
    {
        return userSubscriptions;
    }

    public void setUserSubscriptions (UserSubscriptions[] userSubscriptions)
    {
        this.userSubscriptions = userSubscriptions;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", userSubscriptions = "+userSubscriptions+"]";
    }
}
