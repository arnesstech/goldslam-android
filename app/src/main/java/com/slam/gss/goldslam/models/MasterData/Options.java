package com.slam.gss.goldslam.models.MasterData;

/**
 * Created by Thriveni on 1/6/2017.
 */
public class Options {
    private String name;

    private String value;

    private String key;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", value = "+value+", key = "+key+"]";
    }
}
