package com.slam.gss.goldslam;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.network.HttpHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

/**
 * Created by Thriveni on 1/12/2017.
 */
public class UpdatePasswordActivity extends Activity implements View.OnClickListener {

    private EditText edOldPwd, edNewPwd, edConfirmPwd;
    private String oldPassword = "", newPasword = "";
    private TextView txtUpdatePwd;
    private SharedPreferences sp;
    private SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_password);


        edOldPwd = (EditText) findViewById(R.id.old_pwd);
        edNewPwd = (EditText) findViewById(R.id.new_pwd);
        edConfirmPwd = (EditText) findViewById(R.id.confirm_password);
        txtUpdatePwd = (TextView) findViewById(R.id.update_pwd);
        txtUpdatePwd.setOnClickListener(this);
        sp = getSharedPreferences("GSS", Context.MODE_PRIVATE);
        edit = sp.edit();

    }

    private void updatePwd() {
        if (!validateOldPwd()) {
            return;
        }
        if (!validateNewPwd()) {
            return;
        }
        if (!validateConfirmPwd()) {
            return;
        } else {
            oldPassword = edOldPwd.getText().toString();
            newPasword = edNewPwd.getText().toString();
            updatePassword();
        }
    }

    private boolean validateOldPwd() {
        if (TextUtils.isEmpty(edOldPwd.getText().toString())) {
            edOldPwd.setFocusable(true);
            edOldPwd.setError("Please Enter Old Password");
            return false;
        } else {
            return true;
        }
    }

    private boolean validateNewPwd() {
        if ((TextUtils.isEmpty(edNewPwd.getText().toString())) || (edNewPwd.getText().toString().length() < 6) || (!(edNewPwd.getText().toString().matches(".*\\d+.*")))) {
            edNewPwd.requestFocus();
            edNewPwd.setError("Password must be 6 characters and have one number");
            return false;
        } else {
            return true;
        }
    }

    private boolean validateConfirmPwd() {
        if (TextUtils.isEmpty(edConfirmPwd.getText().toString())) {
            edConfirmPwd.requestFocus();
            edConfirmPwd.setError("Please enter confirm password");
            return false;
        } else if (!edConfirmPwd.getText().toString().trim().equals(edNewPwd.getText().toString())) {
            edConfirmPwd.requestFocus();
            edConfirmPwd.setError("Both passwords should be same");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_pwd:
                updatePwd();
                break;
            default:
                break;
        }
    }


    private void updatePassword() {
        new UpdatePasswordAsynTask(edNewPwd.getText().toString()).execute(Connections.UPDATE_PASSWORD_URL);
    }

    class UpdatePasswordAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;
        String newPassword;

        public UpdatePasswordAsynTask(String password) {
            this.newPassword = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(UpdatePasswordActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                URL url = new URL(urls[0]);
                HttpHelper httpHelper = new HttpHelper();

                JSONObject updatePasswordObj = new JSONObject();
                updatePasswordObj.put("oldPassword", oldPassword);
                updatePasswordObj.put("registrationDocId", sp.getString("registrationDocId", ""));
                updatePasswordObj.put("userId", sp.getString("registrationDocId", ""));
                updatePasswordObj.put("newPassword", newPassword);


                Log.e("params", updatePasswordObj.toString() + urls[0]);

                String result = httpHelper.updatePassword(urls[0], updatePasswordObj);
                return result;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            Log.i("Update Password Result", result);
            JSONObject obj = null, jsonResponse = null;
            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    obj = new JSONObject(jsonResponse.get("data").toString());
                    Helper.showToast(getApplicationContext(), obj.get("message").toString());
                    Intent intent = new Intent(UpdatePasswordActivity.this, AppDashBoard.class);
                    startActivity(intent);
                    finish();

                } else {
                    obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getApplicationContext(), obj.getString("message"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getApplicationContext(), "Network Error");
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
