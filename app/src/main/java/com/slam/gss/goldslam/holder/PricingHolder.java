package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 3/16/2017.
 */

public class PricingHolder extends AbstractViewHolder {

    @BindView(R.id.txt_subscription_name)
    TextView subscriptionName;

    @BindView(R.id.txt_subscription_valid_time)
    TextView subscriptionValidTime;

    @BindView(R.id.txt_subscription_pack_name)
    TextView subscriptionPackName;

    public TextView getSubscriptionName() {
        return subscriptionName;
    }

    public TextView getSubscriptionValidTime() {
        return subscriptionValidTime;
    }

    public TextView getSubscriptionPackName() {
        return subscriptionPackName;
    }

    public PricingHolder(View view) {
        super(view);
        ButterKnife.bind(view);
    }
}
