package com.slam.gss.goldslam;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.holder.SubscriptionHolder;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;

import junit.framework.Test;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 5/2/2017.
 */

public class TestActivity extends AppCompatActivity {

    @BindView(R.id.lv_subscriptions)
    ListView lvSubscriptions;
    TestAdapter adapter = null;
    private List<SubscriptionMasters> subscriptionMasterData;
    SubscriptionMasters master;
    private String TAG="TestActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_subscription);
        ButterKnife.bind(this);

        loadSubscriptions();
    }

    private void loadSubscriptions() {
        subscriptionMasterData = new ArrayList<>();
        master = new SubscriptionMasters();
        master.setDocId("1223200000");
        subscriptionMasterData.add(master);

        master = new SubscriptionMasters();
        master.setDocId("5558705");
        subscriptionMasterData.add(master);

        adapter = new TestAdapter(TestActivity.this);

        adapter.addItems(subscriptionMasterData);
        lvSubscriptions.setAdapter(adapter);
    }


    public class TestAdapter extends AbstractBaseAdapter<SubscriptionMasters, SubscriptionHolder> {

        private int selectedPosition = -1;

        public TestAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.subscription_item;
        }

        @Override
        public SubscriptionHolder getViewHolder(View convertView) {
            return new SubscriptionHolder(convertView);
        }

        @Override
        public void bindView(final int position, final SubscriptionHolder holder, final SubscriptionMasters item) {
            holder.getSubscriptionName().setText(item.getSubscriptionName());
            holder.getSubscriptionFee().setText(item.getSubscriptionFee());
            holder.getTxtSubscriptionDuration().setText(item.getDuration());
            holder.getTxtSubscriptionType().setText(item.getSubscriptionType());
            holder.getTxtSubscriptionStartDate().setText(item.getStartDate());
            holder.getTxtSubscriptionEndDate().setText(item.getEndDate());


            holder.getRootView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    notifyDataSetChanged();
                }
            });

            Log.e(TAG,"selectedPosition::::"+selectedPosition+"position::::"+position);
            holder.getSubscriptionPay().setChecked(selectedPosition == position);

        }

    }

}
