package com.slam.gss.goldslam.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.fragments.SubscriptionFragment;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.holder.SubscriptionHolder;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;

/**
 * Created by Thriveni on 3/17/2017.
 */

public class SubscriptionAdapter extends AbstractBaseAdapter<SubscriptionMasters, SubscriptionHolder> {

    private SubscriptionMasters tempItem;
    private int selectedPosition = -1;
    private String TAG="SubscriptionAdapter";

    public SubscriptionAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.subscription_item;
    }

    @Override
    public SubscriptionHolder getViewHolder(View convertView) {
        return new SubscriptionHolder(convertView);
    }

    @Override
    public void bindView(final int position, final SubscriptionHolder holder, final SubscriptionMasters item) {
        holder.getSubscriptionName().setText(item.getSubscriptionName());
        holder.getSubscriptionFee().setText(item.getSubscriptionFee());
        holder.getTxtSubscriptionDuration().setText(item.getDuration());
        holder.getTxtSubscriptionType().setText(item.getSubscriptionType());
        holder.getTxtSubscriptionStartDate().setText(item.getStartDate());
        holder.getTxtSubscriptionEndDate().setText(item.getEndDate());


        holder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });

       // Log.e(TAG,"bindView():::selectedPosition>>>>"+selectedPosition+"position>>>"+position);
        holder.getSubscriptionPay().setChecked(selectedPosition == position);



        try{
               SubscriptionFragment.tempSelectedSubscription= SubscriptionFragment.subscriptionMasterData.get(selectedPosition);

          //  Log.e("selected sub amount",SubscriptionFragment.subscriptionMasterData.get(selectedPosition).getSubscriptionFee()+"");
        }catch (Exception e){

        }


    }

}
