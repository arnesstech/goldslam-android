package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.view.View;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.PricingHolder;
import com.slam.gss.goldslam.models.Pricing.PricingResponse;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;

/**
 * Created by Thriveni on 3/16/2017.
 */

public class PricingAdapter extends AbstractBaseAdapter<SubscriptionMasters,PricingHolder> {

    public PricingAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.pricing_item;
    }

    @Override
    public PricingHolder getViewHolder(View convertView) {
        return new PricingHolder(convertView);
    }

    @Override
    public void bindView(int position, PricingHolder holder, SubscriptionMasters item) {

        holder.getSubscriptionName().setText(item.getSubscriptionName());
        holder.getSubscriptionValidTime().setText(item.getDuration());
        holder.getSubscriptionPackName().setText(item.getSubscriptionFee());

    }
}
