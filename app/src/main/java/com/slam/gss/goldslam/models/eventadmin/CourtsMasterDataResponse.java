package com.slam.gss.goldslam.models.eventadmin;

/**
 * Created by Thriveni on 9/12/2016.
 */

    public class CourtsMasterDataResponse
    {
        private String status;

        private CourtsData data;

        public String getStatus ()
        {
            return status;
        }

        public void setStatus (String status)
        {
            this.status = status;
        }

        public CourtsData getData ()
        {
            return data;
        }

        public void setData (CourtsData data)
        {
            this.data = data;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [status = "+status+", data = "+data+"]";
        }
    }

