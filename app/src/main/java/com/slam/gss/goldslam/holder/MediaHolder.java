package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class MediaHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.im_pic)
    ImageView imMediaPic;

    @BindView(R.id.txt_media_info)
    TextView txtMediaInfo;

    public ImageView getImMediaPic() {
        return imMediaPic;
    }

    public TextView getTxtMediaInfo() {
        return txtMediaInfo;

    }


    public MediaHolder(View itemView) {
        super(itemView);
    }
}
