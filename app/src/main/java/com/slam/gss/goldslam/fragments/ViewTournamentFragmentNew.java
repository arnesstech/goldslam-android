package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.TournamentsListener;
import com.slam.gss.goldslam.adapters.TournamentListAdapter;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.Events.Events;
import com.slam.gss.goldslam.models.Events.EventsReponse;
import com.slam.gss.goldslam.models.Tournaments.Tournaments;
import com.slam.gss.goldslam.models.Tournaments.TournamentResponse;
import com.slam.gss.goldslam.models.UserSelectedAssociationsList;
import com.slam.gss.goldslam.network.RestApi;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 6/22/2017.
 */

public class ViewTournamentFragmentNew extends BaseFragment implements TournamentsListener {

    @BindView(R.id.im_filterr)
    ImageView imTournamentsFilter;

    @BindView(R.id.txt_no_tournaments)
    TextView txtNoTournaments;

    int totalHeadersCount = 0, count = 0;
    int ctrl = 0;
    private SharedPreferences sp;
    TournamentListAdapter listAdapter;

    @BindView(R.id.exlv_tournaments)
    ExpandableListView expListView;

    List<Tournaments> listDataHeader;
    HashMap<String, List<Events>> listDataChild;
    List<Tournaments> tournamentsList;
    private String app_verions;
    private TextView title, temp_single_menu;

    @BindView(R.id.ed_search_tournaments)
    EditText edSeachTournaments;

    private Context mContext;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        tournamentsList = new ArrayList<>();


    }

    private void searchTournaments(CharSequence s) {
        List<Tournaments> tempListDataHeader = new ArrayList<>(listDataHeader);
        HashMap<String, List<Events>> tempListDataChild = new HashMap(listDataChild);

        List<Tournaments> tempListDataHeader1 = new ArrayList<>();
        HashMap<String, List<Events>> tempListDataChild1 = new HashMap();

        if (!TextUtils.isEmpty(s)) {
            if (tempListDataHeader.size() > 0) {
                for (int i = 0; i < tempListDataHeader.size(); i++) {
                    if (tempListDataHeader.get(i).getTournamentName().toLowerCase().contains(s)) {
                        tempListDataHeader1.add(tempListDataHeader.get(i));
                        tempListDataChild1.put(tempListDataHeader.get(i).getTournamentName(), tempListDataChild.get(i));
                    }
                }
                listAdapter = new TournamentListAdapter(mContext, tempListDataHeader1, tempListDataChild1, tournamentsList, ViewTournamentFragmentNew.this, getActivity());
                expListView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();
            }
        } else {
            Log.e("empty string", "empty");
            listAdapter = new TournamentListAdapter(mContext, listDataHeader, listDataChild, tournamentsList, ViewTournamentFragmentNew.this, getActivity());
            expListView.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_tournament, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        title = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        title.setText("Tournaments");
        title.setGravity(Gravity.CENTER);
        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.INVISIBLE);
        }

        tournamentsList = new ArrayList<>();

        imTournamentsFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AssociationsFragment dialogFragment = AssociationsFragment.newInstance();
                dialogFragment.setListener(ViewTournamentFragmentNew.this);
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "Association Filter Fragment");

            }
        });

        edSeachTournaments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchTournaments(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        getAppVersion();

        getAppData();

    }

    private void getAppVersion() {
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
            Log.e("appversion", info.versionName + "");
            app_verions = info.versionName;
            // app_verions = "4.1";
        } catch (PackageManager.NameNotFoundException e) {
            //Log.e("nameNotFound", e.getMessage());
        }
    }

    private void getAppData() {
        //Every 24 hours , reprompt the user
        long cTime = sp.getLong("c_time", 0);
        long cDate = System.currentTimeMillis();
        long diff = cDate - cTime;
        long period = 86400000L;
        //
//        if (diff > period) {
        new GetAppDataAsynTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Connections.BASE_URL + "api/appdata/android");
//        } else {
        loadTournaments();
//        }
        //new GetAppDataAsynTask().execute("http://gss.goldslamsports.com/prosports/api/appdata/android");
    }

    class GetAppDataAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
//            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {

            Log.i("app data url", urls[0]);
            try {

                String url = urls[0];

                //Log.i("get profile url", url);
                HttpGet httpGet = new HttpGet(url);

                httpGet.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpGet.setHeader("Client-Type", "APP");
                httpGet.setHeader("App-Id", "PROSPORTS");


                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonResponse = new JSONObject(responseBody);
                // Log.i("getProfile Reponse", jsonResponse.toString());


                return responseBody.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.i("appData", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());

                    // Log.e("App Data ", obj.toString() + "appVaersion" + app_verions);

                    //app_verions = "3.4"; //3.8, 3.9
                    if (Double.parseDouble(app_verions) < Double.parseDouble(obj.getString("appVersion"))) {

                        Calendar calendar = Calendar.getInstance();

                        if (calendar.getTimeInMillis() < Long.parseLong(obj.getString("updateDeadLine"))) {
                            //Every 24 hours , reprompt the user
                            long cTime = sp.getLong("c_time", 0);
                            long cDate = System.currentTimeMillis();
                            long diff = cDate - cTime;
                            long period = 86400000L;

                            if (diff > period) {
                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setTitle("Update");
                                alertDialogBuilder.setMessage("Latest version available in the Playstore.Please Update");
                                alertDialogBuilder.setPositiveButton("Update",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                navigateToPlayStore();
                                            }
                                        });

                                alertDialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                        loadTournaments();
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.setCancelable(false);
                                alertDialog.setCanceledOnTouchOutside(false);
                                alertDialog.show();
                                sp.edit().putLong("c_time", cDate).apply();
                            } else {
//                                loadTournaments();
                            }

                        } else {
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setTitle("Update");
                            alertDialogBuilder.setMessage("Latest version available in the Playstore.Please Update");
                            alertDialogBuilder.setPositiveButton("Update",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            navigateToPlayStore();
                                        }
                                    });

                            alertDialogBuilder.setNegativeButton("", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    loadTournaments();
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.setCancelable(false);
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.show();
                            sp.edit().putLong("c_time", 0).apply();
                        }

                    } else {
                        sp.edit().putLong("c_time", System.currentTimeMillis()).apply();
//                        loadTournaments();
                    }


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    //Helper.showToast(, obj.getString("message"));
//                    loadTournaments();

                }

            } catch (JSONException e) {
                e.printStackTrace();
                //Helper.showToast(AppDashBoard.this, e.getMessage());
//                loadTournaments();
            }


        }
    }

    private void navigateToPlayStore() {
        final String appPackageName = getActivity().getPackageName();
        try {
            Intent viewIntent =
                    new Intent("android.intent.action.VIEW",
                            Uri.parse("market://details?id=" + appPackageName));
            startActivity(viewIntent);
        } catch (Exception e) {
            Helper.showToast(getActivity(), "Unable to Connect Try Again...");
            e.printStackTrace();
        }

    }


    private void loadTournaments() {
        Call<TournamentResponse> getTournaments = RestApi.get().getRestService().getTournaments();

        getTournaments.enqueue(new Callback<TournamentResponse>() {
            @Override
            public void onResponse(Call<TournamentResponse> call, Response<TournamentResponse> response) {
                TournamentResponse tournamentObj = response.body();
                if (response != null && response.body() != null) {
                    Log.e("Tournaments response", tournamentObj.toString());

                    tournamentsList = (List<Tournaments>) response.body().getData().getTournament();

                    prepareEventsList(tournamentsList);
                }
            }

            @Override
            public void onFailure(Call<TournamentResponse> call, Throwable t) {
                Log.e("Tournments", "onFailure:" + t.getMessage());
            }
        });
    }

    //


    private void prepareEventsList(List<Tournaments> tournamentsList) {
        this.tournamentsList = tournamentsList;
        ctrl = tournamentsList.size();
        count = 0;
        Tournaments tournament;
        listDataHeader.clear();
        listDataChild.clear();
        totalHeadersCount = 0;

        if (tournamentsList.isEmpty()) {
            listAdapter = new TournamentListAdapter(getActivity(), listDataHeader, listDataChild, tournamentsList, ViewTournamentFragmentNew.this, getActivity());
            expListView.setAdapter(listAdapter);
            expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    if (listDataChild.get(listDataHeader.get(groupPosition).getTournamentName()).size() == 0) {
                        Helper.showToast(getActivity(), "No events");
                    }
                    return false;
                }
            });
            return;
        }

        showProgress("Tournaments Data Retrieving.");
        for (int i = 0; i < tournamentsList.size(); i++) {
            totalHeadersCount++;
            tournament = tournamentsList.get(i);//mJsonArray.getJSONObject(i);
            if (tournament != null) {
                UserSelectedAssociationsList list = new Gson().fromJson(sp.getString("AssociationFilter", ""), UserSelectedAssociationsList.class);
                boolean isAtleastSelected = false;
                boolean isTournaments = false;
                if (list != null && !list.getUserSelectedAssociations().isEmpty()) {

                    for (int ctr = 0; ctr < list.getUserSelectedAssociations().size(); ctr++) {

                        String associationName = tournament.getAssociationName();
                        boolean selected = list.getUserSelectedAssociations().get(ctr).isAssociationSelectionFlag();
                        if (selected) {
                            isAtleastSelected = true;
                        }


                        if (list.getUserSelectedAssociations().get(ctr).getUserSelectedAssociationName().equalsIgnoreCase(associationName)
                                && (list.getUserSelectedAssociations().get(ctr).isAssociationSelectionFlag())) {
                            Log.v("flag val", list.getUserSelectedAssociations().get(ctr).getUserSelectedAssociationName() + "....." + list.getUserSelectedAssociations().get(ctr).isAssociationSelectionFlag());
                            count++;
                            // Adding child data
                            listDataHeader.add(tournament);
                            EventsAsyncTask mEventsAsyncTask = new EventsAsyncTask(getActivity(), tournament.getTournamentName().toString(), count);
                            mEventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, tournament.getDocId());
                        }
                    }
                }


                if (!isAtleastSelected) {
                    count++;
                    // Adding child data
                    listDataHeader.add(tournament);
                    EventsAsyncTask mEventsAsyncTask = new EventsAsyncTask(getActivity(), tournament.getTournamentName(), count);
                    mEventsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, tournament.getDocId());

                }


            }

        }
    }

    class EventsAsyncTask extends AsyncTask<String, Object, EventsReponse> {
        List<Events> eventsList;
        ProgressDialog dialog;
        int value;

        String title;

        public EventsAsyncTask(Context context, String title, int accValue) {
            mContext = context;
            value = accValue;
            eventsList = new ArrayList<>();
            this.title = title;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Tournaments Data Retrieving.");
//            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected EventsReponse doInBackground(String... urls) {
            try {
                Response<EventsReponse> eventResponse = RestApi.get().getRestService().getEvents(urls[0]).execute();
                return eventResponse.body();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        protected void onPostExecute(@Nullable EventsReponse result) {
            dialog.cancel();
            dismissDialog();
            if (eventsList != null) {
                eventsList.clear();
            }

            eventsList.addAll(result.getData().getEvents());
            try {
                listDataChild.put(listDataHeader.get(value - 1).getTournamentName(), eventsList);
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (totalHeadersCount == ctrl) {
                listAdapter = new TournamentListAdapter(mContext, listDataHeader, listDataChild, tournamentsList, ViewTournamentFragmentNew.this, getActivity());
                expListView.setAdapter(listAdapter);
                expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        if (listDataChild.get(listDataHeader.get(groupPosition).getTournamentName()).size() == 0) {
                            Helper.showToast(getActivity(), "No events");
                        }
                        return false;
                    }
                });
            }


        }
    }

    @Override
    public void notifyTournament(String str) {
        if ("s".equalsIgnoreCase(str)) {
            loadTournaments();
        } else {

        }
    }


}