package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.Teams;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ramesh on 13/9/16.
 */
public class AddScoreFragment extends DialogFragment {

    String matchRemarks;
    int winnerPosition;
    JSONArray team1Array = new JSONArray();
    JSONArray team2Array = new JSONArray();

    @BindView(R.id.txt_player1)
    TextView player1;
    @BindView(R.id.txt_player2)
    TextView player2;
    @BindViews({
            R.id.txt_pscore1, R.id.txt_pscore2, R.id.txt_pscore3,
            R.id.txt_pscore4, R.id.txt_pscore5,
    })
    List<TextView> scoreView1;
    @BindViews({
            R.id.txt_p2score1, R.id.txt_p2score2, R.id.txt_p2score3,
            R.id.txt_p2score4, R.id.txt_p2score5,
    })
    List<TextView> scoreView2;

    @BindView(R.id.etRemarks)
    EditText etRemarks;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.chMatchOver)
    CheckBox chMatchOver;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    Matches mMatches;

    public static AddScoreFragment newInstance(Matches matches) {
        AddScoreFragment fragment = new AddScoreFragment();
        fragment.setMatches(matches);
        return fragment;
    }

    private void setMatches(Matches matches) {
        this.mMatches = matches;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return LayoutInflater.from(getActivity()).inflate(R.layout.frag_add_score, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Teams> teams = mMatches.getTeams();
        //Set player names
        player1.setText(teams.size() > 0 ? teams.get(0).getName() : "");
        player2.setText(teams.size() > 1 ? teams.get(1).getName() : "");

        //First player scores
        setPlayerScores(teams.size() > 0 ? teams.get(0).getScores() : new ArrayList<String>(),
                scoreView1);
        //Second player scores
        setPlayerScores(teams.size() > 1 ? teams.get(1).getScores() : new ArrayList<String>(),
                scoreView2);


        if (mMatches.getMatchremarks().equalsIgnoreCase("")) {

        } else {
            etRemarks.setText(mMatches.getMatchremarks());
        }
        //
        spinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, teams));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                winnerPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (teams.get(0).getStatus() != null && teams.get(1).getStatus() != null) {

            if (teams.get(0).getStatus().equalsIgnoreCase("WINNER")) {
                Log.e("player1 winner", teams.get(0).getStatus());
                spinner.setSelection(0);
                chMatchOver.setChecked(true);
                spinner.setVisibility(View.VISIBLE);
            } else if (teams.get(1).getStatus().equalsIgnoreCase("WINNER")) {
                Log.e("player2 winner", teams.get(1).getStatus());
                spinner.setSelection(1);
                chMatchOver.setChecked(true);
                spinner.setVisibility(View.VISIBLE);
            }
            /*else {
                chMatchOver.setChecked(false);
            }*/

           /* else {
                chMatchOver.setChecked(false);
            }*/
        }


        chMatchOver.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    spinner.setVisibility(View.VISIBLE);
                } else {
                    spinner.setVisibility(View.INVISIBLE);
                }
            }
        });


    }


    private void setPlayerScores(List<String> scores2, List<TextView> viewList) {
        int index = 0;
        for (TextView textView : viewList) {
            if (index < scores2.size()) {
                textView.setText(scores2.get(index));
            } else {
                textView.setText("");
            }
            index++;
        }

    }

    @OnClick(R.id.btnSubmit)
    public void onClick() {
        //TODO: Click submit logic and dismiss dialog.
        try {
            matchResult();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void matchResult() throws JSONException {


        for (TextView textView : scoreView1) {
            String textViewData = textView.getText().toString();

            if (!(textViewData.equalsIgnoreCase(""))) {

                team1Array.put(textViewData);
            }

        }

        for (TextView textView : scoreView2) {
            String textViewData = textView.getText().toString();

            if (!(textViewData.equalsIgnoreCase(""))) {
                team2Array.put(textViewData);
            }
        }

        Log.i("1 player score", team1Array.toString());
        Log.i("2 player score", team2Array.toString());
        JSONObject jsonObject = new JSONObject();
        matchRemarks = etRemarks.getText().toString();
//

        //new MatchResultAsynTask().execute("http://gss.goldslamsports.com/prosports/api/match/score");

        String url = Connections.BASE_URL + "api/match/score";
        new MatchResultAsynTask().execute(url);

    }

    class MatchResultAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                // HttpPost httpPost = new HttpPost(urls[0]);

                Log.i("MatRes in addScoreFrag", matchRemarks);

                JSONObject matchResult = new JSONObject();
                matchResult.put("matchDocId", mMatches.getDocId());
                matchResult.put("team1Scores", team1Array);
                matchResult.put("team2Scores", team2Array);
                matchResult.put("matchremarks", matchRemarks);
                HttpClient httpClient = new DefaultHttpClient();
                String url = urls[0];
                HttpPost httpPost = new HttpPost(url);
                String apiToken = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE).getString("apiToken", "");

                Log.e("apiToken", apiToken);

                httpPost.setHeader("Authorization", "Basic " + apiToken);
                httpPost.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpPost.setEntity(new StringEntity(matchResult.toString()));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            Log.i("Add Score", result);
            JSONObject obj = null, jsonResponse = null;
            String status = null;
            try {
                jsonResponse = new JSONObject(result);

                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    obj = new JSONObject(jsonResponse.get("data").toString());


                    if (chMatchOver.isChecked()) {
                        // new MatchCompletedAnsynTask().execute("http://gss.goldslamsports.com/prosports/api/match/completed");
                        String url = Connections.BASE_URL + "api/match/completed";
                        new MatchCompletedAnsynTask().execute(url);

                    } else {
                        Helper.showToast(getActivity(), obj.get("message").toString());
                        onCloseDialog();

                    }

                } else {
                    obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                    Log.e("Add Score", obj.get("messade").toString());
                }


            } catch (JSONException e) {
                e.printStackTrace();
                // Helper.showToast(getActivity(), e.getMessage());
            }
        }
    }

    class MatchCompletedAnsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
           // dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                // HttpPost httpPost = new HttpPost(urls[0]);

                JSONObject matchCompleted = new JSONObject();
                matchCompleted.put("docId", mMatches.getDocId());
                matchCompleted.put("winnerPosition", mMatches.getTeams().get(winnerPosition).getPosition());

                HttpClient httpClient = new DefaultHttpClient();
                String url = urls[0];
                HttpPost httpPost = new HttpPost(url);
                String apiToken = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE).getString("apiToken", "");

                Log.e("API Token in Add Score ", apiToken);

                httpPost.setHeader("Authorization", "Basic " + apiToken);
                httpPost.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpPost.setEntity(new StringEntity(matchCompleted.toString()));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            Log.i("Add Score", result);
            JSONObject obj = null, jsonResponse = null;
            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                obj = new JSONObject(jsonResponse.get("data").toString());
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    Helper.showToast(getActivity(), obj.get("message").toString());
                    onCloseDialog();


                } else {
                    obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), e.getMessage());
            }
        }
    }


    @OnClick(R.id.close_dialog)
    public void onCloseDialog() {
        ((PrepareScheduleActivity) getActivity()).OnDBUpdated();
        dismiss();
    }


}
