package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;

/**
 * Created by Thriveni on 1/17/2017.
 */
public class UserAssociationsHolder extends AbstractViewHolder {

    TextView txtUserAssocName,txtUserAssocRegNo;

    public TextView getUserAssoc() {
        return txtUserAssocName;
    }

    public TextView getUserRegiNo() {
        return txtUserAssocRegNo;
    }

    public UserAssociationsHolder(View view) {
        super(view);
        txtUserAssocName = (TextView) view.findViewById(R.id.txt_user_assoc_name);
        txtUserAssocRegNo = (TextView) view.findViewById(R.id.txt_register_no);
    }
}

