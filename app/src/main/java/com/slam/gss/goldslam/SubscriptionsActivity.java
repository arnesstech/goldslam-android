package com.slam.gss.goldslam;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;

import com.slam.gss.goldslam.adapters.SubscriptionAdapter;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.Subscriptions;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 3/17/2017.
 */

public class SubscriptionsActivity extends Activity {
    @BindView(R.id.lv_subscriptions)
    ListView lvSubscriptions;

    private List<Subscriptions> subscriptionsList;
    private List<SubscriptionMasters> subscriptionMasterData;
    private SubscriptionAdapter adapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscription_activity);
        ButterKnife.bind(this);

        loadSubscriptions();

    }

    private void loadSubscriptions() {
        progressDialog = ProgressDialog.show(SubscriptionsActivity.this, "", "Loading...", true);

        Call<Subscriptions> getSubscriptions = RestApi.get().getRestService().getSubscriptions();
        getSubscriptions.enqueue(new Callback<Subscriptions>() {
            @Override
            public void onResponse(Call<Subscriptions> call, Response<Subscriptions> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null && response.body().getData().getSubscriptionMasters() != null) {

                        if (response.body().getData().getSubscriptionMasters().length > 0) {
                            adapter = new SubscriptionAdapter(SubscriptionsActivity.this);
                            subscriptionMasterData = new ArrayList<SubscriptionMasters>(Arrays.asList(response.body().getData().getSubscriptionMasters()));
                            adapter.addItems(subscriptionMasterData);
                            lvSubscriptions.setAdapter(adapter);
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                        }
                    } else {
                        progressDialog.dismiss();

                    }


                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Subscriptions> call, Throwable t) {

                Helper.showToast(SubscriptionsActivity.this, "NetWork Problem");
                progressDialog.dismiss();

            }
        });
    }
}
