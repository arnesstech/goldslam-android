package com.slam.gss.goldslam.models.Subscriptions.UserSubscriptionCreate;

/**
 * Created by Thriveni on 3/20/2017.
 */

public class Data {
    private String message;

    private String expiryDate;

    private String subscriptionType;

    private String expiryDays;

    private String subscriptionTypeId;

    private String subscriptionId;

    private String subscriptionName;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getExpiryDate ()
    {
        return expiryDate;
    }

    public void setExpiryDate (String expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public String getSubscriptionType ()
    {
        return subscriptionType;
    }

    public void setSubscriptionType (String subscriptionType)
    {
        this.subscriptionType = subscriptionType;
    }

    public String getExpiryDays ()
    {
        return expiryDays;
    }

    public void setExpiryDays (String expiryDays)
    {
        this.expiryDays = expiryDays;
    }

    public String getSubscriptionTypeId ()
    {
        return subscriptionTypeId;
    }

    public void setSubscriptionTypeId (String subscriptionTypeId)
    {
        this.subscriptionTypeId = subscriptionTypeId;
    }

    public String getSubscriptionId ()
    {
        return subscriptionId;
    }

    public void setSubscriptionId (String subscriptionId)
    {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionName ()
    {
        return subscriptionName;
    }

    public void setSubscriptionName (String subscriptionName)
    {
        this.subscriptionName = subscriptionName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", expiryDate = "+expiryDate+", subscriptionType = "+subscriptionType+", expiryDays = "+expiryDays+", subscriptionTypeId = "+subscriptionTypeId+", subscriptionId = "+subscriptionId+", subscriptionName = "+subscriptionName+"]";
    }
}
