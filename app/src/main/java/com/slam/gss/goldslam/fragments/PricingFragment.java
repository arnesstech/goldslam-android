package com.slam.gss.goldslam.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.ViewTournamentFragment;
import com.slam.gss.goldslam.adapters.PriceAdapter;
import com.slam.gss.goldslam.adapters.PricingAdapter;
import com.slam.gss.goldslam.adapters.UserSubscriptionCreateAdapter;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.interfaces.PricingListener;
import com.slam.gss.goldslam.models.MySubscriptions.MySubscriptionsResponse;
import com.slam.gss.goldslam.models.MySubscriptions.UserSubscriptions;
import com.slam.gss.goldslam.models.Pricing.PricingResponse;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.SubscriptionMasters;
import com.slam.gss.goldslam.models.Subscriptions.SubscriptionMaster.Subscriptions;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 3/16/2017.
 */

public class PricingFragment extends Fragment implements PricingListener {

    private TextView temp_page_title_menu, temp_single_menu;
    private ListView llPricing;
    private ImageView imFilter;
    private ArrayList<PricingResponse> pricingList;
    private PricingResponse pricing;
    private PricingAdapter adapter;

    private Button btnSubscribe;
    public static Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pricing, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        temp_page_title_menu = (TextView) getActivity().findViewById(R.id.temp_page_title_menu);
        temp_page_title_menu.setText("Pricing");
        temp_page_title_menu.setGravity(Gravity.CENTER);
        temp_page_title_menu.setPadding(0, 0, 20, 0);

        temp_single_menu = (TextView) getActivity().findViewById(R.id.temp_single_menu);
        if (temp_single_menu != null) {
            temp_single_menu.setVisibility(View.INVISIBLE);
        }

        imFilter = (ImageView) getActivity().findViewById(R.id.im_filter);
        if (imFilter != null) {
            imFilter.setVisibility(View.GONE);
        }


        llPricing = (ListView) view.findViewById(R.id.ll_pricing_list);

        //loadPricing();

        loadSubscriptions();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSubscribe = (Button) view.findViewById(R.id.btn_subscribe);
        btnSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubscriptionFragment dialogFragment = SubscriptionFragment.newInstance(getActivity(), "pricing");
                dialogFragment.setListener2(PricingFragment.this);
                FragmentActivity activity = (FragmentActivity) (getActivity());
                FragmentManager fm = activity.getSupportFragmentManager();
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                dialogFragment.show(fm, "SubscriptionFragment");
            }
        });
    }

    private void loadSubscriptions() {

        Call<Subscriptions> getSubscriptions = RestApi.get().getRestService().getSubscriptions();
        getSubscriptions.enqueue(new Callback<Subscriptions>() {
            @Override
            public void onResponse(Call<Subscriptions> call, Response<Subscriptions> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null && response.body().getData().getSubscriptionMasters() != null) {

                        if (response.body().getData().getSubscriptionMasters().length > 0) {
                            prepareData(response.body().getData().getSubscriptionMasters());

                        } else {

                        }
                    } else {

                    }


                } else {

                }
            }

            @Override
            public void onFailure(Call<Subscriptions> call, Throwable t) {
                Helper.showToast(getActivity(), "NetWork Problem");

            }
        });
    }

    private void prepareData(SubscriptionMasters[] userSubscriptions) {

        List<SubscriptionMasters> userSubscriptionss = new ArrayList<SubscriptionMasters>(Arrays.asList(userSubscriptions));
        adapter = new PricingAdapter(getActivity());
        Log.e("userSubscriptions", userSubscriptions.length + "");
        adapter.addItems(userSubscriptionss);
        llPricing.setAdapter(adapter);

    }

    private void loadPricing() {

        pricingList = new ArrayList<>();

        pricing = new PricingResponse();
        pricing.setSubscriptionName("GOLD");
        pricing.setSubscriptionTime("Rs 500 /Year");
        pricing.setSubscriptionPackName("Annual");
        pricingList.add(pricing);

        pricing = new PricingResponse();
        pricing.setSubscriptionName("BRONZE");
        pricing.setSubscriptionTime("Rs 400 /Half-yearly");
        pricing.setSubscriptionPackName("Semi Annual");
        pricingList.add(pricing);

        pricing = new PricingResponse();
        pricing.setSubscriptionName("SILVER");
        pricing.setSubscriptionTime("Rs 300 /Quarterly");
        pricing.setSubscriptionPackName("Quarterly");
        pricingList.add(pricing);

        /*PriceAdapter adapter = new PriceAdapter(getActivity(),pricingList);
       // adapter.addItems(pricingList);
        llPricing.setAdapter(adapter);
*/
    }

    @Override
    public void notifyPricing(String str) {
        if ("s".equalsIgnoreCase(str)) {
            loadSubscriptions();
        }
    }
}
