package com.slam.gss.goldslam.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.TournamentsListener;
import com.slam.gss.goldslam.adapters.TournamentListAdapter;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.Events.Events;
import com.slam.gss.goldslam.models.Login.LoginResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 6/21/2017.
 */

public class RegisterEventFragment extends DialogFragment implements View.OnClickListener {

    public static Events mEvents = null;

    @BindView(R.id.close_dialog)
    TextView txtCloseDialog;

    @BindView(R.id.et_rank)
    EditText etRank;

    @BindView(R.id.et_points)
    EditText etPoints;

    @BindView(R.id.et_registration_no)
    EditText etRegistrationNo;

    @BindView(R.id.et_patner_points)
    EditText etPatnerPoints;

    @BindView(R.id.et_patner_rank)
    EditText etpatnerRank;

    @BindView(R.id.et_patner_name)
    AutoCompleteTextView actPatnerName;

    @BindView(R.id.et_patner_reg_no)
    EditText etPatnerRegNo;

    @BindView(R.id.button_submit)
    Button btnSubmit;

    @BindView(R.id.patner_layout)
    LinearLayout patnerLayout;


    List<String> docIds = new ArrayList();
    List<String> docIds1 = new ArrayList();
    List<String> users = new ArrayList();
    List<String> userAistaNo = new ArrayList();
    List<String> userAitaNo = new ArrayList();
    Map<String, List<String>> cacheMapUsers = new HashMap<>();
    Map<String, List<String>> cacheMapdocIds = new HashMap<>();
    ArrayAdapter<String> adapter;
    TournamentsListener mListener = null;

    String rankString = "", pointsString = "", registratinNoString = "", patnerDocid = "", patnerRank = "", patnerPoints = "", patnerRegId = "";
    SharedPreferences sp;

    public static RegisterEventFragment newInstance(Events events) {
        mEvents = events;
        RegisterEventFragment fragment = new RegisterEventFragment();
        return fragment;
    }

    public void setListener(TournamentsListener listener) {
        mListener = listener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.dialog_event_registration, container, false);
        ButterKnife.bind(this, view);
        txtCloseDialog.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = getActivity().getSharedPreferences("GSS",Context.MODE_PRIVATE);

        SharedPreferences sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);
        LoginResponse loginResponse = new Gson().fromJson(sp.getString("loginResponse", ""), LoginResponse.class);

        if (loginResponse.getData() != null && loginResponse.getData().getUserAssociations().length > 0) {
            for (int i = 0; i < loginResponse.getData().getUserAssociations().length; i++) {
                Log.i("User Association Data", loginResponse.getData().getUserAssociations()[i].toString());
                Log.i("AsociationId", loginResponse.getData().getUserAssociations()[i].getAssociationid());
                //Log.i("RegistrationId", loginResponse.getData().getUserAssociations()[i].getRegistrationId());
                Log.i("Event AsociationId", mEvents.getAssociationDocId());

                if (loginResponse.getData().getUserAssociations()[i].getAssociationid().equalsIgnoreCase(mEvents.getAssociationDocId())) {
                    //etRegistrationNo.setText(loginResponse.getData().getUserAssociations()[i].getRegistrationId());
                    etRegistrationNo.setText(loginResponse.getData().getUserAssociations()[i].getRegistrationId() == "null" ? " " : loginResponse.getData().getUserAssociations()[i].getRegistrationId());

                } else {
                }
            }

        }


        actPatnerName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("user selected pos id is", position + "" + "id:::::::" + id);
                try {
                    // patnerDocid = docIds.get(position);
                    patnerDocid = docIds.get(position);
                    Log.e("selected patner id is", patnerDocid + "----->" + docIds1.get(position));

                } catch (Exception e) {
                    patnerDocid = "";
                    Log.e("exception", e.getMessage());
                }

            }
        });


        if (Integer.parseInt(mEvents.getTeamSize()) == 2) {
            patnerLayout.setVisibility(View.VISIBLE);

            actPatnerName.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable editable) {
                    // TODO Auto-generated method stub
                    String key = editable.toString().trim();

                    List<String> list = cacheMapUsers.get(key);
                    if (list != null) {
                        if (adapter != null) {
                            users = new ArrayList<>(list);
                            docIds = new ArrayList<>(cacheMapdocIds.get(key));
                            adapter.clear();
                            adapter.addAll(list);
                            adapter.notifyDataSetChanged();
                        }

                    } else if (!(TextUtils.isEmpty(key))) {
                        String k = key;
                        String temp = k.replaceAll(" ", "%20");
                        new GetUserssyncTask(key).execute(Connections.BASE_URL + "api/user/" + temp);
                    } else {
                        actPatnerName.setError("Please select partner");
                    }


                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // TODO Auto-generated method stub

                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String newText = s.toString();

                }

            });


            //autoCompleteTextView.setThreshold(1);
            adapter = new ArrayAdapter<String>(getActivity(), R.layout.drop_down_partner, users);
//            adapter = new ArrayAdapter<String>(_context, R.layout.list_item, users);
            actPatnerName.setAdapter(adapter);

        } else {
            Log.i("", "invisible");
            patnerLayout.setVisibility(View.GONE);
            patnerLayout.removeAllViewsInLayout();

        }

    }


    class GetUserssyncTask extends AsyncTask<String, Void, String> {

        String key;

        public GetUserssyncTask(String key) {
            this.key = key;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urls) {
            Log.i("getusers url", urls[0]);
            try {
                String url = urls[0];
                HttpGet httpGet = new HttpGet(url);
                String apiToken = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE).getString("apiToken", "");

                String base64EncodedapiToken = Base64.encodeToString(apiToken.getBytes(), Base64.NO_WRAP);
                httpGet.addHeader("Authorization", "Basic " + base64EncodedapiToken);

                Log.i("header", "Basic " + base64EncodedapiToken);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                Log.i("usersResponse", responseBody);
                return responseBody;

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            //dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    // Helper.showToast(_context, obj.getString("message"));
                    Log.i("httpResponse", obj.toString());

                    JSONArray usersArray = obj.getJSONArray("users");

                    users.clear();
                    docIds.clear();
                    docIds1.clear();

                    users = new ArrayList();
                    docIds = new ArrayList<>();
                    docIds1 = new ArrayList<>();

                    for (int i = 0; i < usersArray.length(); i++) {
                        JSONObject userObj = usersArray.getJSONObject(i);
                        String name = userObj.getString("name");
                        String docId = userObj.getString("docId");

                        if (name != null) {
                            docIds.add(docId);
                            docIds1.add(docId);
                            users.add(name.toLowerCase());
                        }

                    }


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    // Helper.showToast(_context, obj.getString("message"));

                }
                if (adapter != null) {
                    //
                    cacheMapUsers.put(key, users);
                    cacheMapdocIds.put(key, docIds);
                    adapter.clear();
                    adapter.addAll(users);
                    adapter.notifyDataSetChanged();
                    //  autoCompleteTextView.showDropDown();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                // Helper.showToast(_context, e.getMessage());
            }


        }
    }

    class EventRegistrationAsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;
        //TournamentEventsModel mTournamentEventsModel;
        Events mTournamentEventsModel;


        public EventRegistrationAsyncTask(Events mTournamentEventsModel) {
            this.mTournamentEventsModel = mTournamentEventsModel;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Registering , please wait");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {

            try {
                SharedPreferences sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);

                Log.i("eventTeamSize", urls[1]);
                JSONArray jsonArray = new JSONArray();
                JSONObject obj = new JSONObject();
                obj.put("docId", sp.getString("registrationDocId", ""));
                obj.put("rank", rankString);
                obj.put("points", pointsString);
                obj.put("regNo", registratinNoString);
                jsonArray.put(obj);

                if (Integer.parseInt(urls[1]) == 2) {
                    Log.i("If", "if");

                    Log.i("patner Id isss", patnerDocid);
                    JSONObject obj1 = new JSONObject();
                    obj1.put("docId", patnerDocid);
                    obj1.put("rank", patnerRank);
                    obj1.put("points", patnerPoints);
                    obj1.put("regNo", patnerRegId);
                    jsonArray.put(obj1);
                }

                Log.i("playersArray", jsonArray.toString());
                JSONObject entryObject = new JSONObject();
                entryObject.put("eventDocId", mTournamentEventsModel.getDocId());
                entryObject.put("players", jsonArray);
                entryObject.put("registerType", "SELF");

                Log.e("entryObj", entryObject.toString());

                HttpClient httpClient = new DefaultHttpClient();
                StringEntity stringEntity = new StringEntity(entryObject.toString());
                String url = urls[0];
                Log.i("url", url);
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpPost.setHeader("Authorization", "Basic " + sp.getString("apiToken", ""));
                httpPost.setEntity(new StringEntity(entryObject.toString()));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                Log.i("httpResponse", responseBody);

                return responseBody;


            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.i("Event entry response", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                //Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    //Toast.makeText(LoginActivity.this,"111111",Toast.LENGTH_SHORT).show();
                    mTournamentEventsModel.setAction("WITHDRAW");
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));

                    if (mListener != null) {
                        mListener.notifyTournament("s");
                    } else {
                        mListener.notifyTournament("c");
                    }
                    dismiss();
                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                    if (mListener != null) {
                        mListener.notifyTournament("c");
                    }
                    dismiss();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Unable to fetch data from server");
                if (mListener != null) {
                    mListener.notifyTournament("c");
                }
                dismiss();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_submit:
                registerEvent();
                break;
            case R.id.close_dialog:
                break;
            default:
                break;
        }

    }

    private void registerEvent() {
        clickOnEntryRegirstrationSubmitButton(mEvents);
    }


    private void clickOnEntryRegirstrationSubmitButton(final Events events) {
        rankString = etRank.getText().toString();
        pointsString = etPoints.getText().toString();
        registratinNoString = etRegistrationNo.getText().toString();

        if (Integer.parseInt(events.getTeamSize()) == 2) {

            if (actPatnerName.getText().toString().isEmpty()) {
                actPatnerName.setError("please select partner");
            } else {
                patnerRank = etpatnerRank.getText().toString();
                patnerPoints = etPatnerPoints.getText().toString();
                patnerRegId = etPatnerRegNo.getText().toString();

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etPatnerRegNo.getWindowToken(), 0);

                SharedPreferences sp = getActivity().getSharedPreferences("GSS", Context.MODE_PRIVATE);

                if (sp.getString("registrationDocId", "").equalsIgnoreCase(patnerDocid)) {
                    actPatnerName.setError("Please select other player");
                } else {
                    showConfirmationDialog(events, true);
                    Log.i("patnerDocid", patnerDocid);
                }
            }
        } else {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etPatnerRegNo.getWindowToken(), 0);
            showConfirmationDialog(events, false);
        }


    }

    private void showConfirmationDialog(final Events events, final boolean isDoubles) {
        String associationName = events.getAssociationName();
        String tournamentName = events.getTournamentName();
        String eventName = events.getEventName();

        StringBuilder builder = new StringBuilder();
        //Association Name
        builder.append("Association Name: <b>");
        builder.append(associationName);
        builder.append("</b><br>");
        //Tournament Name
        builder.append("Tournament Name: <b>");
        builder.append(tournamentName);
        builder.append("</b><br>");
        //Event Name
        builder.append("Event Name: <b>");
        builder.append(eventName);
        builder.append("</b><br>");
        //Player name
        builder.append("Player Name: <b>");
        builder.append("" + sp.getString("name", ""));
        builder.append("</b><br>");
        //Player registerno
        builder.append("Registration No: <b>");
        builder.append(registratinNoString);
        builder.append("</b><br>");
        //Player rank
        builder.append("Rank : <b>");
        builder.append(rankString);
        builder.append("</b><br>");
        //Player Points
        builder.append("Points : <b>");
        builder.append(pointsString);
        builder.append("</b><br>");

        if (isDoubles) {
            //Patner Name
            builder.append("Partner Name: <b>");
            builder.append("" + actPatnerName.getText().toString());
            builder.append("</b><br>");
            //Patner Register No
            builder.append("Partner Registration No: <b>");
            builder.append(patnerRegId);
            builder.append("</b><br>");
            //Patner Rank
            builder.append("Partner Rank : <b>");
            builder.append(patnerRank);
            builder.append("</b><br>");
            //Patner Points
            builder.append("Patner Points : <b>");
            builder.append(patnerPoints);
            builder.append("</b><br>");

        }

        builder.append("<br>");
        builder.append("<br>");
        //Patner Points
        builder.append("Your name will be displayed in the acceptance list once tournament admin approves your entry <b>");
        builder.append("</b><br>");

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(Html.fromHtml(builder.toString()));
        builder1.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isDoubles) {
                    new EventRegistrationAsyncTask(events).execute(Connections.BASE_URL + "api/entry", String.valueOf(events.getTeamSize()));
                } else {
                    new EventRegistrationAsyncTask(events).execute(Connections.BASE_URL + "api/entry", String.valueOf(events.getTeamSize()));
                }

            }
        });
        builder1.setNegativeButton("Cancel", null);
        builder1.setCancelable(false);
        AlertDialog dialog = builder1.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
