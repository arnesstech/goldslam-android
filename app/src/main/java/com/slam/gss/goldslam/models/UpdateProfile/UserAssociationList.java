package com.slam.gss.goldslam.models.UpdateProfile;

import java.util.ArrayList;

/**
 * Created by Thriveni on 1/12/2017.
 */
public class UserAssociationList {

    public ArrayList<UserAssociations> getUserAssociations() {
        return userAssociations;
    }

    public void setUserAssociations(ArrayList<UserAssociations> userAssociations) {
        this.userAssociations = userAssociations;
    }

    private ArrayList<UserAssociations> userAssociations;
}
