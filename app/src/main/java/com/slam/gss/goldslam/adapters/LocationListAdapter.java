package com.slam.gss.goldslam.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import java.util.List;

/**
 * Created by mahes on 6/1/2017.
 */

public class LocationListAdapter extends ArrayAdapter<LocationList> {
    Context context;

    public LocationListAdapter(Context context, int resourceId,
                                 List<LocationList> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtLocation;
      //  RatingBar txtRatting;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LocationList listShow = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_players_coachings, null);

            holder = new ViewHolder();
            holder.txtLocation = (TextView) convertView.findViewById(R.id.txt_location);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.list_image);
           // holder.txtRatting = (RatingBar) convertView.findViewById(R.id.MyRating);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txtLocation.setText(listShow.getlocation());
        holder.txtTitle.setText(listShow.gettitle());
       // holder.txtRatting.setRating(4.0f);
        holder.imageView.setImageResource(listShow.getImageId());
        return convertView;
    }
}
