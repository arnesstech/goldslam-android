package com.slam.gss.goldslam.models.ForgotUserName;

import com.slam.gss.goldslam.models.UpdateUserName.Errors;

/**
 * Created by Ramesh on 16/01/17.
 */

public class ExtraError {
    private String message;

    private Errors[] errors;

    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Errors[] getErrors() {
        return errors;
    }

    public void setErrors(Errors[] errors) {
        this.errors = errors;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", errors = " + errors + ", code = " + code + "]";
    }
}
