package com.slam.gss.goldslam.models.feedBack;

/**
 * Created by Thriveni on 6/30/2017.
 */

public class FeedBackRequest {
    private String message;

    private String phone;

    private String email;

    private String subject;

    private String name;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", phone = " + phone + ", email = " + email + ", subject = " + subject + ", name = " + name + "]";
    }
}
