package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.CountryHolder;
import com.slam.gss.goldslam.models.MasterData.Options;

/**
 * Created by Thriveni on 1/6/2017.
 */
public class GenderSpinner extends AbstractBaseAdapter<Options, CountryHolder> {


    public GenderSpinner(Context context) {
        super(context);
    }

    @Override
    public int getLayoutId() {
        return R.layout.spinner_text;
    }

    @Override
    public CountryHolder getViewHolder(View convertView) {
        return new CountryHolder(convertView);
    }

    @Override
    public void bindView(final int position, CountryHolder holder, Options item) {
        holder.getCountryName().setText(item.getValue());
    }
}