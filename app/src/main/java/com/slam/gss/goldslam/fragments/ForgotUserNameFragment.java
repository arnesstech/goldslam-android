package com.slam.gss.goldslam.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.slam.gss.goldslam.LoginActivity;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.ForgotUserName.ForgotUserNameRequest;
import com.slam.gss.goldslam.models.ForgotUserName.ForgotUserNameResponse;
import com.slam.gss.goldslam.network.HttpHelper;
import com.slam.gss.goldslam.network.RestApi;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 1/7/2017.
 */
public class ForgotUserNameFragment extends DialogFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    protected Context context;
    protected Fragment fragment;
    protected RadioButton rbGssId, rbMobileEmail;
    protected String rbValue = "GSSID";
    protected EditText edUserMobileNo;
    protected Button btnSubmit, btnCancel;
    protected ForgotUserNameRequest forgotPasswordReq;
    private JSONObject updateUserNameJsonObj;
    private SharedPreferences sp;


    public static ForgotUserNameFragment newInstance() {
        ForgotUserNameFragment fragment = new ForgotUserNameFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return LayoutInflater.from(getActivity()).inflate(R.layout.forgot_username, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        updateUserNameJsonObj = new JSONObject();
        sp = getActivity().getSharedPreferences("SheCabs", Context.MODE_APPEND);

        rbGssId = (RadioButton) view.findViewById(R.id.rb_gssid);
        rbMobileEmail = (RadioButton) view.findViewById(R.id.rb_mobile_email);

        edUserMobileNo = (EditText) view.findViewById(R.id.ed_user_mobile_no);
        edUserMobileNo.setImeOptions(EditorInfo.IME_ACTION_DONE);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnSubmit = (Button) view.findViewById(R.id.btn_submit);

        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        rbGssId.setOnCheckedChangeListener(this);
        rbMobileEmail.setOnCheckedChangeListener(this);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {

            case R.id.rb_gssid:
                if (isChecked) {
                    rbValue = "GSSID";
                    changeHintValue("GSSID");
                }
                break;

            case R.id.rb_mobile_email:
                if (isChecked) {
                    rbValue = "MOBILE/EMAIL";
                    changeHintValue("MOBILE/EMAIL");
                }
                break;

            default:
                break;
        }
    }

    private void changeHintValue(String hintValue) {
        edUserMobileNo.setHint(hintValue);
        edUserMobileNo.setText("");
        edUserMobileNo.setError(null);
    }

    public void onCloseDialog() {
        dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                try {
                    forgotUserName();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.btn_cancel:
                onCloseDialog();
                break;

            default:
                break;
        }
    }

    private void forgotUserName() throws JSONException {
        forgotPasswordReq = new ForgotUserNameRequest();

        if (edUserMobileNo.getText().toString().equalsIgnoreCase("")) {
            if (rbValue.equalsIgnoreCase("GSSID")) {
                edUserMobileNo.setError(getString(R.string.err_forgotUserName_GSSID));
                edUserMobileNo.setFocusable(true);
            } else if (rbValue.equalsIgnoreCase("MOBILE/EMAIL")) {
                edUserMobileNo.setError(getString(R.string.err_forgotUserName_Email_Or_Mobile));
                edUserMobileNo.requestFocus();
            }
        } else {
            if (rbValue.equalsIgnoreCase("GSSID")) {
                forgotPasswordReq.setGssid(edUserMobileNo.getText().toString());
                forgotPasswordReq.setMobileOrEmail("");

                String text = edUserMobileNo.getText().toString();
                updateUserNameJsonObj = new JSONObject();
                updateUserNameJsonObj.put("gssid", text);
                updateUserNameJsonObj.put("mobileOrEmail", "");
                //sendUserName();
            } else {
                String text = edUserMobileNo.getText().toString();
                forgotPasswordReq.setGssid("");
                forgotPasswordReq.setMobileOrEmail(text);

                updateUserNameJsonObj = new JSONObject();
                updateUserNameJsonObj.put("gssid", "");
                updateUserNameJsonObj.put("mobileOrEmail", text);

                //sendUserName();
            }
            /*else {
                String text = edUserMobileNo.getText().toString();
                if (text.matches("[0-9]+")) {
                    if (text.length() > 10 || text.length() < 10) {
                        edUserMobileNo.setError("Invalid Input");
                        edUserMobileNo.requestFocus();
                    } else {
                        forgotPasswordReq.setGssid("");
                        forgotPasswordReq.setMobileOrEmail(text);
                        sendUserName();
                    }
                } else {
                    Log.e("mobile/mail","mobile/mail");
                    Pattern pattern;
                    Matcher matcher;
                    final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                    pattern = Pattern.compile(EMAIL_PATTERN);
                    matcher = pattern.matcher(text);
                    if (!matcher.matches()) {
                        edUserMobileNo.setError("Invalid Input");
                        edUserMobileNo.requestFocus();
                    } else {
                        sendUserName();
                    }
                }
            }//if*/

            new ForGotUserNameSyncTask().execute(Connections.FORGOT_USERNAME);
        }
    }

    class ForGotUserNameSyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                Log.e("profile url is:", urls[0]);
                System.out.println("updateUserNameObject->" + updateUserNameJsonObj.toString());

                String apiToken = sp.getString("apiToken", "");
                String base64EncodedApiToken = Base64.encodeToString(apiToken.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
                HttpClient httpClient = new DefaultHttpClient();
                StringEntity stringEntity = new StringEntity(updateUserNameJsonObj.toString());
                String url = urls[0] ;
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Authorization", "Basic" + apiToken);
                httpPost.setHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json");
                httpPost.setHeader("Authorization", "Basic " + base64EncodedApiToken);
                httpPost.setEntity(new StringEntity(updateUserNameJsonObj.toString()));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());

                return responseBody;


            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            JSONObject jsonResponse = null;

            String status = null;

            Log.i("Registration Result", result);
            try {
                jsonResponse = new JSONObject(result);
                Log.i("registrationResponse", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                    onCloseDialog();

                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getActivity(), obj.getString("message"));
                    onCloseDialog();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getActivity(), "Network error");
                onCloseDialog();
            }

        }
    }

    private void sendUserName() {
        Log.e("mobile/email", edUserMobileNo.getText().toString());
        Log.e("id", rbValue);

        Call<ForgotUserNameResponse> forgotUserName = RestApi.get().getRestService().forgotUserName(forgotPasswordReq);

        forgotUserName.enqueue(new Callback<ForgotUserNameResponse>() {
            @Override
            public void onResponse(Call<ForgotUserNameResponse> call, Response<ForgotUserNameResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null && response.body().getStatus().equalsIgnoreCase("SUCCESS")) {
                        String message = response.body().getData().getMessage();
                        Helper.showToast(getActivity(), message);
                        onCloseDialog();
                    } else {
                        try {
                            String str = response.body().getError().getMessage();
                            Helper.showToast(getActivity(), str);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {

                    Helper.showToast(getActivity(), "Invalid Input!");
                }
            }

            @Override
            public void onFailure(Call<ForgotUserNameResponse> call, Throwable t) {
                Helper.showToast(getActivity(), "Network problem");
            }
        });
    }
}
