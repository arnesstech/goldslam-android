package com.slam.gss.goldslam.models.ForgotUserName;

/**
 * Created by Thriveni on 1/7/2017.
 */
public class ForgotUserNameRequest {
    private String gssid;

    private String mobileOrEmail;

    public String getGssid ()
    {
        return gssid;
    }

    public void setGssid (String gssid)
    {
        this.gssid = gssid;
    }

    public String getMobileOrEmail ()
    {
        return mobileOrEmail;
    }

    public void setMobileOrEmail (String mobileOrEmail)
    {
        this.mobileOrEmail = mobileOrEmail;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [gssid = "+gssid+", mobileOrEmail = "+mobileOrEmail+"]";
    }
}
