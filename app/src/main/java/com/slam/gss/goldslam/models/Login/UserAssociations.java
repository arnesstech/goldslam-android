package com.slam.gss.goldslam.models.Login;

/**
 * Created by Thriveni on 1/11/2017.
 */
public class UserAssociations {
    private String associationName;

    private String registrationId;

    private String associationid;

    public String getAssociationName() {
        return associationName;
    }

    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getAssociationid() {
        return associationid;
    }

    public void setAssociationid(String associationid) {
        this.associationid = associationid;
    }

    @Override
    public String toString() {
        return "ClassPojo [associationName = " + associationName + ", registrationId = " + registrationId + ", associationid = " + associationid + "]";
    }
}
