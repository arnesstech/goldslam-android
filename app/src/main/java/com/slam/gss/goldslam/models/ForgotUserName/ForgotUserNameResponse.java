package com.slam.gss.goldslam.models.ForgotUserName;

import com.slam.gss.goldslam.models.UpdateUserName.Error;

/**
 * Created by Thriveni on 1/7/2017.
 */
public class ForgotUserNameResponse {
    private String status;

    private Data data;

    private Error error;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }

    public Error getError() {
        return error;
    }
}
