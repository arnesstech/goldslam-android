package com.slam.gss.goldslam.models.drawresults;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class Data {
    private String message;

    private String teams;

    private String results;

    private String[] playerinfo;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private List<Object> createList(JSONArray array) {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                Object object = null;
                try {
                    object = array.get(i);
                } catch (Exception e) {
//                    e.printStackTrace();
                    list.add("");
                }
                if (object instanceof JSONArray) {
                    list.add(createList(array.getJSONArray(i)));
                } else if (object instanceof String) {
                    list.add(array.getString(i));
                } else if (object instanceof Integer) {
                    list.add(String.valueOf(array.getInt(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public List<Object> getResults1() {
        try {
            JSONArray a1 = new JSONArray(results);
            return createList(a1);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Object> getTeams() {
        try {
            JSONArray a1 = new JSONArray(teams);
            return createList(a1);
        } catch (JSONException e) {
            e.printStackTrace();
        }


//        Gson gson = new Gson();
//        List<List<List<String>>> lists = gson.fromJson(teams, new TypeToken<List<List<List<String>>>>() {
//        }.getType());
//        return lists;
        return null;
    }

    public void setTeams(String teams) {
        this.teams = teams;
    }

    public List<List<String>> getResults() {
        Gson gson = new Gson();
        List<List<String>> lists = gson.fromJson(results, new TypeToken<List<List<String>>>() {
        }.getType());
        return lists;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String[] getPlayerinfo() {
        return playerinfo;
    }

    public void setPlayerinfo(String[] playerinfo) {
        this.playerinfo = playerinfo;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", teams = " + teams + ", results = " + results + ", playerinfo = " + playerinfo + "]";
    }
}