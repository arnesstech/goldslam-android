package com.slam.gss.goldslam.models.Associations;

/**
 * Created by Thriveni on 1/10/2017.
 */
public class Associations {
    private String associationName;

    private String associationId;

    private String docId;

    public String getAssociationName() {
        return associationName;
    }

    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    public String getAssociationId() {
        return associationId;
    }

    public void setAssociationId(String associationId) {
        this.associationId = associationId;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    @Override
    public String toString() {
        return getAssociationName() == null ? "" : getAssociationName();
    }
}
