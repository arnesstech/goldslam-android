package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.OurClientsHolder;
import com.slam.gss.goldslam.holder.TeamHolder;
import com.slam.gss.goldslam.models.Team.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahes on 6/20/2017.
 */

public class TeamAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Team> mTeamsList = new ArrayList<>();
    private Context mContext;

    public TeamAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void addItem(List<Team> teamsList) {
        mTeamsList = new ArrayList<>(teamsList);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vEmp = inflater.inflate(R.layout.item_team, parent, false);
        viewHolder = new TeamHolder(vEmp);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TeamHolder mTeamHolderHolder = (TeamHolder) holder;
        configureViewHolderFilter(mTeamHolderHolder, position);
    }

    private void configureViewHolderFilter(TeamHolder holder, int position) {
        holder.getTxtTeamName().setText(mTeamsList.get(position).getTeamName());
    }

    @Override
    public int getItemCount() {
        return mTeamsList.size();
    }
}
