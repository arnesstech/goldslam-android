package com.slam.gss.goldslam;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.slam.gss.goldslam.fragments.AboutUsFragment;
import com.slam.gss.goldslam.fragments.ContactUsFragment;
import com.slam.gss.goldslam.fragments.CreateTournamentFragment;
import com.slam.gss.goldslam.fragments.MySubscriptionsFragment;
import com.slam.gss.goldslam.fragments.PlayerDashBoardFragment;
import com.slam.gss.goldslam.fragments.PlayersCoachingFragment;
import com.slam.gss.goldslam.fragments.PricingFragment;
import com.slam.gss.goldslam.fragments.UpdatePasswordFragment;
import com.slam.gss.goldslam.fragments.ViewTournamentFragmentNew;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.GetProfile.GetProfileResponse;
import com.slam.gss.goldslam.models.GetProfile.Profile;
import com.slam.gss.goldslam.models.UpdateProfile.UpdateProfileResponse;
import com.slam.gss.goldslam.models.UpdateProfile.UserAssociations;
import com.slam.gss.goldslam.models.UserSelectedAssociationsList;
import com.slam.gss.goldslam.models.uploadFile.UploadImageResponse;
import com.slam.gss.goldslam.network.RestApi;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppDashBoard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    public Intent intent;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private View headView;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private static final int CAMERA_REQUEST = 1888;
    private Toolbar toolbar;
    private String upLoadServerUri = null;
    private String imagepath = null;
    private LinearLayout navLayout;
    private Uri selectedImageUri;
    private TextView txtEventAdmin, txtHome, txtProfile, txtViewTournament, txtLogout, txtTermsCondition, txtPricing, txtAboutUs;
    private TextView txtMySubscriptions, txtDashBoard, txtPlayersCoaching;
    private Profile userProfile;
    private ProgressDialog progressDialog = null;
    //Permision code that will be checked in the method onRequestPermissionsResult
    private int STORAGE_PERMISSION_CODE = 23;

   /* @BindView(R.id.txt_feed_back)
    TextView txtFeedBack;

    @BindView(R.id.txt_contact_us)
    TextView txtContactUs;

    @BindView(R.id.txt_create_tournament)
    TextView txtCreateTournament;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_dash_board);

        ButterKnife.bind(this);

        mTitle = mDrawerTitle = getTitle();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        sp = getSharedPreferences("GSS", Context.MODE_PRIVATE);
        editor = sp.edit();

        userProfile = new Profile();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

               /* updateprofileData();*/

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                try {
                    getProfile();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        headView = navigationView.getHeaderView(0);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();


        try {
            getProfile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
//BUG FIX: Always user need to be shown with view tournments
//        if (sp.getString("role", "").equalsIgnoreCase("EVENT_ADMIN")) {
//            onNavigationItemSelected(navigationView.getMenu().getItem(1));
//        } else {
        // onNavigationItemSelected(navigationView.getMenu().getItem(3));
//

        onNavigationItemSelected(navigationView.getMenu().getItem(0));
        //onNavigationItemSelected(navigationView.getMenu().getItem(5));

       /* navLayout = (LinearLayout) findViewById(R.id.layout_drawer);
        txtHome = (TextView) findViewById(R.id.txt_home);
        txtEventAdmin = (TextView) findViewById(R.id.txt_event_admin);
        txtProfile = (TextView) findViewById(R.id.txt_profile);
        txtViewTournament = (TextView) findViewById(R.id.txt_view_tournament);
        txtLogout = (TextView) findViewById(R.id.txt_logout);
        txtDashBoard = (TextView) findViewById(R.id.txt_dashBoard);
        txtPlayersCoaching = (TextView) findViewById(R.id.txt_player_coaching);
        txtTermsCondition = (TextView) findViewById(R.id.txt_terms_conditionss);
        txtPricing = (TextView) findViewById(R.id.txt_pricing);
        txtAboutUs = (TextView) findViewById(R.id.txt_about_us);
        txtMySubscriptions = (TextView) findViewById(R.id.txt_my_subscriptions);

        txtHome.setOnClickListener(this);
        txtEventAdmin.setOnClickListener(this);
        txtProfile.setOnClickListener(this);
        txtViewTournament.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
        txtDashBoard.setOnClickListener(this);
        txtPlayersCoaching.setOnClickListener(this);
        txtTermsCondition.setOnClickListener(this);
        txtPricing.setOnClickListener(this);
        txtAboutUs.setOnClickListener(this);
        txtMySubscriptions.setOnClickListener(this);
        txtFeedBack.setOnClickListener(this);
        txtContactUs.setOnClickListener(this);*/

        if (sp.getString("role", "").equalsIgnoreCase("EVENT_ADMIN")) {
        } else {
            // txtEventAdmin.setVisibility(View.GONE);
            navigationView.getMenu().getItem(3).setVisible(false);
        }
        ((TextView) headView.findViewById(R.id.name)).setText(sp.getString("userName", ""));


        //selectItem(3);
        // selectItem(7);

        ((ImageView) headView.findViewById(R.id.edit_profilePic)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isReadStorageAllowed()) {
                    //If permission is already having then showing the toast
                   /* Intent intent = new Intent();
                    intent.setType("image*//**//*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(intent,1);*/

                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                    //startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);
                } else {
                    requestStoragePermission();
                }


            }
        });

        ((ImageView) headView.findViewById(R.id.profilePic)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isReadStorageAllowed()) {
                    //If permission is already having then showing the toast
                   /* Intent intent = new Intent();
                    intent.setType("image*//**//*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(intent,1);*/

                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                    //startActivityForResult(Intent.createChooser(intent, "Complete action using"), 1);
                } else {
                    requestStoragePermission();
                }


            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.e("AppDashBoard", "onResume()");
/*
        try {
            getProfile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //We are calling this method to check the permission status
    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            progressDialog = ProgressDialog.show(AppDashBoard.this, "", "Uploading...", true);

            Uri selectedImageUri = data.getData();
            Log.e("selectedImageUri", selectedImageUri.toString());
            imagepath = getPath(selectedImageUri);
            Log.e("image path is", imagepath);

            File file = new File(imagepath);// initialize file here

            long length = file.length();
            length = length / 1024;
            System.out.println("File Path : " + file.getPath() + ", File size : " + length + " KB");
            System.out.println("File Path : " + file.getPath() + ", File size : " + length / 1024 + " MB");

            if (length / 1024 < 1) {
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

                Call<UploadImageResponse> call = RestApi.get().getRestService().uploadAttachment(filePart);

                call.enqueue(new Callback<UploadImageResponse>() {
                    @Override
                    public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                // Log.e("response", response.body().getData().getMessage());

                                if (response.body().getData().getFileId() != null) {
                                    userProfile.setProfileImage(response.body().getData().getFileId());

                                    // Log.e("userUpdate Pro Object", userProfile.toString());

                                    updateProfile(userProfile);


                                }

                            }
                        } else {
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                    }
                });
            } else {
                Toast.makeText(AppDashBoard.this, "profile pic with size less than 1MB", Toast.LENGTH_SHORT).show();
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        }
    }

    private void updateProfile(Profile userProfile) {
        String docId = sp.getString("registrationDocId", "");
        Call<UpdateProfileResponse> updateProfileObj = RestApi.get().getRestService().updateProfile(docId, userProfile);
        updateProfileObj.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null) {
                        sp.edit().putString("profilePicId", response.body().getData().getProfile().getProfileImage()).commit();
                        getProfileImage(response.body().getData().getProfile().getProfileImage());
                    } else {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });


    }

    /*private void updateProfile(Profile userProfile){
        new UpdateProfileSync().execute();
    }*/

    /*class UpdateProfileSync extends AsyncTask<Void,Void,String>{

        @Override
        protected String doInBackground(Profile... params) {
            return null;
        }
    }*/

    public String getPath(Uri contentURI) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;


        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(AppDashBoard.this, contentURI)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(contentURI)) {
                final String docId = DocumentsContract.getDocumentId(contentURI);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(contentURI)) {

                final String id = DocumentsContract.getDocumentId(contentURI);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(AppDashBoard.this, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(contentURI)) {
                final String docId = DocumentsContract.getDocumentId(contentURI);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(AppDashBoard.this, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(contentURI.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(contentURI))
                return contentURI.getLastPathSegment();

            return getDataColumn(AppDashBoard.this, contentURI, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(contentURI.getScheme())) {
            return contentURI.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    private void updateprofileData() {
        //Get Profile Data from SP
        GetProfileResponse profileRes = new Gson().fromJson(sp.getString("userProfileObj", ""), GetProfileResponse.class);

        //userProfile = profileRes.getData().getProfile();

        if (profileRes != null && profileRes.getData() != null && profileRes.getData().getProfile() != null) {

            userProfile = profileRes.getData().getProfile();

            // Log.e("profileRe", profileRes.getData().getProfile().toString());

            if (profileRes.getData().getProfile().getUserAssociations() != null) {

                // Log.e("ass length:::", profileRes.getData().getProfile().getUserAssociations().toString() + "");

                if (profileRes.getData().getProfile().getUserAssociations() != null) {
                    if (profileRes.getData().getProfile().getUserAssociations().length > 0) {
                        for (int i = 0; i < profileRes.getData().getProfile().getUserAssociations().length; i++) {
                            //Log.e("user ass obj",profileRes.getData().getProfile().getUserAssociations()[i].getAssociationid());
                        }
                    }
                }

            }

            if (profileRes.getData().getProfile().getName() != null) {
                String str = profileRes.getData().getProfile().getName();
                sp.edit().putString("userName", str).commit();
                if (profileRes.getData().getProfile().getName().equalsIgnoreCase(sp.getString("userName", ""))) {
                    ((TextView) headView.findViewById(R.id.name)).setText(profileRes.getData().getProfile().getName());

                } else {
                    ((TextView) headView.findViewById(R.id.name)).setText(sp.getString("userName", ""));
                }
                ((TextView) headView.findViewById(R.id.name)).setText(str);
            }

            if (profileRes.getData().getProfile().getUserName() != null) {
                //Log.e("profile Username ", profileRes.getData().getProfile().getUserName());
            }
            if (profileRes.getData().getProfile().getName() != null) {
                //Log.e("profile name ", profileRes.getData().getProfile().getName());
            }

            ((TextView) headView.findViewById(R.id.city)).setText("Goldslam Code: " + sp.getString("gssid", ""));


            if (profileRes.getData().getProfile().getProfileImage() != null) {
                if (!TextUtils.isEmpty(profileRes.getData().getProfile().getProfileImage())) {
                    // Log.e("profileImageId", profileRes.getData().getProfile().getProfileImage());
                    // getProfileImage(profileRes.getData().getProfile().getProfileImage());

                } else {
                    if (profileRes.getData().getProfile().getGender() != null) {

                        if (profileRes.getData().getProfile().getGender().equalsIgnoreCase("female")) {
                            ((ImageView) headView.findViewById(R.id.profilePic)).setImageResource(R.mipmap.ic_female_profile);
                        } else {
                            ((ImageView) headView.findViewById(R.id.profilePic)).setImageResource(R.drawable.profile);
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_home:
                System.out.println("Click on home");
                selectItem(0);
                break;

            case R.id.txt_event_admin:
                selectItem(1);
                break;

            case R.id.txt_profile:
                selectItem(2);
                break;

            case R.id.txt_view_tournament:
                selectItem(3);
                break;

            case R.id.txt_pricing:
                selectItem(4);
                break;

            case R.id.txt_terms_conditionss:
                navigateTermsConditions();
                break;

            case R.id.txt_about_us:
                selectItem(5);
                break;

            case R.id.txt_my_subscriptions:
                selectItem(6);
                break;

            case R.id.txt_logout:
                showDialog();
                break;

            case R.id.txt_dashBoard:
                selectItem(7);
                break;

            case R.id.txt_player_coaching:
                selectItem(8);
                break;

            case R.id.txt_feed_back:
                selectItem(9);
                break;

            case R.id.txt_contact_us:
                selectItem(10);
                break;

            default:
                break;

        }

    }

    private void showDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Logout");
        alertDialogBuilder.setMessage("Would you like to logout?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Helper.showToast(AppDashBoard.this, "Logged out sucessfully");
                        UserSelectedAssociationsList list = new UserSelectedAssociationsList();
                        String listt = new Gson().toJson(list);
                        sp.edit().putString("userProfileObj", "").commit();
                        editor.putBoolean("auth", false);
                        sp.edit().putString("userCredentials", "").commit();

                        editor.putString("AssociationFilter", listt);
                        editor.commit();
                        Intent intent = new Intent(AppDashBoard.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void navigateTermsConditions() {
        //String url = "http://gss.goldslamsports.com/legel/#terms";//dev
        String url = "http://www.goldslamsports.com/legel/#terms";//prod
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new UpdatePasswordFragment();
                toolbar.setTitle("Update Password");
                break;
            case 1:
                fragment = new EventAdminFragment();
                toolbar.setTitle("Event Admin");
                break;
            case 2:
                fragment = new ProfileFragment();
                toolbar.setTitle("profile");
                break;
            case 3:
                //fragment = new ViewTournamentFragmentNew();
                fragment = new ViewTournamentFragmentNew();
                toolbar.setTitle("view tournament");
                break;

            case 4:
                fragment = new PricingFragment();
                toolbar.setTitle("Pricing");
                break;
            case 5:
                fragment = new AboutUsFragment();
                break;

            case 6:
                fragment = new MySubscriptionsFragment();
                break;

            case 7:
                fragment = new PlayerDashBoardFragment();
                break;

            case 8:
                fragment = new PlayersCoachingFragment();
                break;

            case 9:
                Intent intent = new Intent(AppDashBoard.this, FeedBackActivity.class);
                startActivity(intent);
                break;

            case 10:
                fragment = new ContactUsFragment();
                break;

            case 11:
                fragment = new CreateTournamentFragment();
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_content, fragment).commit();

        } else {
            // Log.e("App Dash Board", "Error in creating fragment");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    private void getProfile() throws IOException, JSONException {
        new GetProfilesyncTask().execute(Connections.GET_PROFILE);
    }

    class GetProfilesyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AppDashBoard.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            //dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... urls) {
            Log.i("getprofile url", urls[0]);
            try {

                String docId = sp.getString("registrationDocId", "");//"57ab68e50fff53405e09f1f1";
                String apiToken = sp.getString("apiToken", "");
                Log.i("ap values--->", docId + apiToken);
                String url = urls[0] + "/" + docId;

                //Log.i("get profile url", url);
                HttpGet httpGet = new HttpGet(url);

                // Log.e("credentials", apiToken);
                String base64EncodedapiToken = Base64.encodeToString(apiToken.getBytes(), Base64.NO_WRAP);
                httpGet.addHeader("Authorization", "Basic " + base64EncodedapiToken);

                //Log.i("header", "Basic " + base64EncodedapiToken);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(httpGet);
                String responseBody = EntityUtils.toString(httpResponse.getEntity());
                JSONObject jsonResponse = new JSONObject(responseBody);
                // Log.i("getProfile Reponse", jsonResponse.toString());


                return responseBody.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }

        }

        protected void onPostExecute(String result) {
            //dialog.cancel();
            JSONObject jsonResponse = null;

            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                Log.i("getProfle", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                //  Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    //Toast.makeText(LoginActivity.this,"111111",Toast.LENGTH_SHORT).show();
                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    //Helper.showToast(AppDashBoard.this, obj.getString("message"));

                    //Append profile data to corresponding fields
                    JSONObject profileObj = new JSONObject(obj.getString("profile"));
                    ((TextView) headView.findViewById(R.id.name)).setText(profileObj.getString("name").toString());
                    ((TextView) headView.findViewById(R.id.city)).setText("Goldslam Code: " + sp.getString("gssid", ""));

                    if (profileObj.has("profileImage")) {
                        if (!TextUtils.isEmpty(profileObj.getString("profileImage"))) {
                            // Log.e("profileImageId", profileObj.getString("profileImage"));
                            getProfileImage(profileObj.getString("profileImage"));

                        } else {
                            if (profileObj.getString("gender").equalsIgnoreCase("female")) {
                                ((ImageView) headView.findViewById(R.id.profilePic)).setImageResource(R.mipmap.ic_female_profile);
                            } else {
                                ((ImageView) headView.findViewById(R.id.profilePic)).setImageResource(R.drawable.profile);
                            }
                        }
                    }


                    userProfile.setDocId(profileObj.getString("docId"));
                    userProfile.setName(profileObj.getString("name"));
                    userProfile.setFirstName(profileObj.getString("firstName"));
                    //  Log.e("profile firstName", profileObj.getString("firstName"));
                    userProfile.setLastName(profileObj.getString("lastName"));
                    userProfile.setUserName(profileObj.getString("userName"));
                    userProfile.setEmail(profileObj.getString("email"));
                    userProfile.setMobile(profileObj.getString("mobile"));
                    userProfile.setStatus(profileObj.getString("status"));
                    userProfile.setRegisterType(profileObj.getString("registerType"));
                    userProfile.setDob(profileObj.getString("dob"));
                    userProfile.setGender(profileObj.getString("gender"));
                    userProfile.setTeamname(profileObj.getString("teamname"));
                    userProfile.setFatherName(profileObj.getString("fatherName"));
                    userProfile.setMotherName(profileObj.getString("motherName"));
                    userProfile.setOccupation(profileObj.getString("occupation"));
                    userProfile.setAddress(profileObj.getString("address"));
                    userProfile.setQualification(profileObj.getString("qualification"));

                    JSONArray jsonArray = new JSONArray(profileObj.getString("prefferedSports"));
                    String[] sports;
                    if (jsonArray.length() > 0) {
                        sports = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            sports[i] = jsonArray.get(i).toString();

                        }
                    } else {
                        sports = new String[]{};
                    }
                    userProfile.setPrefferedSports(sports);
                    userProfile.setProfileImage(profileObj.getString("profileImage"));

                    if (profileObj.has("profileImage")) {
                        if (!TextUtils.isEmpty(profileObj.getString("profileImage"))) {
                            sp.edit().putString("profilePicId", profileObj.getString("profileImage")).commit();
                        } else {

                        }
                    }
                    userProfile.setSchoolName(profileObj.getString("schoolName"));
                    userProfile.setClassLevel(profileObj.getString("classLevel"));
                    userProfile.setStateCode(profileObj.getString("stateCode"));

                    JSONArray jsonArray1 = new JSONArray(profileObj.getString("userAssociations"));
                    UserAssociations[] userAssociations;

                    ArrayList<UserAssociations> userAss;
                    if (jsonArray1.length() > 0) {
                        userAssociations = new UserAssociations[jsonArray1.length()];
                        userAss = new ArrayList<>();
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            UserAssociations associations = new UserAssociations();
                            JSONObject objj = new JSONObject(jsonArray1.get(i).toString());
                            associations.setAssociationid(objj.getString("associationid"));
                            associations.setAssociationName(objj.getString("associationName"));
                            associations.setRegistrationId(objj.getString("registrationId"));
                            userAss.add(associations);
                            // Log.e("userAssociations", jsonArray1.get(i).toString());

                            userAssociations[i] = associations;
                        }

                    } else {
                        userAssociations = new UserAssociations[]{};
                    }
                    userProfile.setUserAssociations(userAssociations);
                    userProfile.setGssid(profileObj.getString("gssid"));
                    userProfile.setCountry(profileObj.getString("country"));
                    userProfile.setPincode(profileObj.getString("pincode"));


                    updateprofileData();


                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(AppDashBoard.this, obj.getString("message"));

                }

            } catch (JSONException e) {
                e.printStackTrace();
                //Helper.showToast(AppDashBoard.this, e.getMessage());
            }


        }
    }

    private void getProfileImage(String profileImage) {
        String baseUrl = Connections.BASE_URL;
        // Log.e("base url", baseUrl);
        String getProfileImage = baseUrl + "fileManager/getImageFileById/" + profileImage + ".jpg";
        new GetProfilesImageyncTask().execute(getProfileImage);


    }

    class GetProfilesImageyncTask extends AsyncTask<String, Void, Bitmap> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AppDashBoard.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            //dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            Log.i("getproImage url", urls[0]);
            try {

                String docId = sp.getString("registrationDocId", "");//"57ab68e50fff53405e09f1f1";
                String apiToken = sp.getString("apiToken", "");
                // Log.i("ap values--->", docId + apiToken);
                String url = urls[0];


                URL urll = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urll.openConnection();

                InputStream is = connection.getInputStream();
                Bitmap img = BitmapFactory.decodeStream(is);
                return img;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        protected void onPostExecute(Bitmap img) {

            try {
                if (img != null) {

                    ((ImageView) headView.findViewById(R.id.profilePic)).setImageBitmap(img);
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                } else {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                //Helper.showToast(AppDashBoard.this, e.getMessage());

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }


        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
//            intent = new Intent(this, LoginActivity.class);
//            startActivity(intent);
            finish();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.app_dash_board, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.txt_home:
                selectItem(0);
                break;

            case R.id.txt_event_admin:
                selectItem(1);
                break;

            case R.id.txt_profile:
                selectItem(2);
                break;

            case R.id.txt_view_tournament:
             //   selectItem(3);
                break;

            case R.id.txt_pricing:
                selectItem(4);
                break;

            case R.id.txt_terms_conditionss:
                navigateTermsConditions();
                break;

            case R.id.txt_about_us:
                selectItem(5);
                break;

            case R.id.txt_my_subscriptions:
                selectItem(6);
                break;

            case R.id.txt_logout:
                showDialog();
                break;

            case R.id.txt_dashBoard:
                selectItem(7);
                break;

            case R.id.txt_player_coaching:
                selectItem(8);
                break;

            case R.id.txt_feed_back:
                selectItem(9);
                break;

            case R.id.txt_contact_us:
                selectItem(10);
                break;

            case R.id.txt_create_tournament:
                selectItem(11);
                break;

            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
