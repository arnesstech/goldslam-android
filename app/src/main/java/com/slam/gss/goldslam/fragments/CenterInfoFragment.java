package com.slam.gss.goldslam.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.slam.gss.goldslam.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.slam.gss.goldslam.fragments.AddCoachingCenterActivity.viewPager;


/**
 * Created by mahes on 6/19/2017.
 */

public class CenterInfoFragment extends Fragment {

    @BindView(R.id.btn_center_info_next)
    Button btnCenterInfoNext;

    public CenterInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.item_center_info, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnCenterInfoNext.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
            }
        });
    }
}
