package com.slam.gss.goldslam.models.scheduleplayers;

import java.util.List;

public class Data {
    private String message;

    private List<Matches> matches;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Matches> getMatches() {
        return matches;
    }

    public void setMatches(List<Matches> matches) {
        this.matches = matches;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", matches = " + matches + "]";
    }
}