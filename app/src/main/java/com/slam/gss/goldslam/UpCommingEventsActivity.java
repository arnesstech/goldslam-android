package com.slam.gss.goldslam;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.slam.gss.goldslam.adapters.UpcommingEventsAdapter;
import com.slam.gss.goldslam.models.upCommingEvents.Tournaments;
import com.slam.gss.goldslam.models.upCommingEvents.UpCommingEventsResponse;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Thriveni on 7/4/2017.
 */

public class UpCommingEventsActivity extends AppCompatActivity {

    @BindView(R.id.rv_upcomming_events)
    RecyclerView rvUpCommingEvents;

    private ProgressDialog progressDialog;
    ArrayList<Tournaments> mUpCommingEventsList;
    RecyclerView.LayoutManager mLayoutManager;
    UpcommingEventsAdapter mUpCommingEventsAdapter;

    @BindView(R.id.txt_exploreMore)
    TextView txtExlporeMore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_upcomming_events);

        mLayoutManager = new LinearLayoutManager(this);
        rvUpCommingEvents.setLayoutManager(mLayoutManager);
        rvUpCommingEvents.setItemAnimator(new DefaultItemAnimator());
        rvUpCommingEvents.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mUpCommingEventsList = new ArrayList<>();
        mUpCommingEventsAdapter = new UpcommingEventsAdapter(this);
        txtExlporeMore.setVisibility(View.GONE);
        loadUpCommingEvents();
    }

    private void loadUpCommingEvents() {
        Call<UpCommingEventsResponse> upcommingEvents = RestApi.get().getRestService().getUpCommingEvents();

        upcommingEvents.enqueue(new Callback<UpCommingEventsResponse>() {
            @Override
            public void onResponse(Call<UpCommingEventsResponse> call, Response<UpCommingEventsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        List<Tournaments> UpCommingTournamentsObj = response.body().getData().getTournaments();
                        mUpCommingEventsAdapter.add(UpCommingTournamentsObj);
                        rvUpCommingEvents.setAdapter(mUpCommingEventsAdapter);
                        mUpCommingEventsAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpCommingEventsResponse> call, Throwable t) {

            }
        });
    }
}
