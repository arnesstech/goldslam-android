package com.slam.gss.goldslam.models.News.GetNews;

/**
 * Created by Thriveni on 5/9/2017.
 */

public class News {
    private String updatedby;

    private String status;

    private String createdby;

    private String createddate;

    private String updateddate;

    private String newscontent;

    private String newsid;

    public String getUpdatedby ()
    {
        return updatedby;
    }

    public void setUpdatedby (String updatedby)
    {
        this.updatedby = updatedby;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getCreatedby ()
    {
        return createdby;
    }

    public void setCreatedby (String createdby)
    {
        this.createdby = createdby;
    }

    public String getCreateddate ()
    {
        return createddate;
    }

    public void setCreateddate (String createddate)
    {
        this.createddate = createddate;
    }

    public String getUpdateddate ()
    {
        return updateddate;
    }

    public void setUpdateddate (String updateddate)
    {
        this.updateddate = updateddate;
    }

    public String getNewscontent ()
    {
        return newscontent;
    }

    public void setNewscontent (String newscontent)
    {
        this.newscontent = newscontent;
    }

    public String getNewsid ()
    {
        return newsid;
    }

    public void setNewsid (String newsid)
    {
        this.newsid = newsid;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [updatedby = "+updatedby+", status = "+status+", createdby = "+createdby+", createddate = "+createddate+", updateddate = "+updateddate+", newscontent = "+newscontent+", newsid = "+newsid+"]";
    }
}
