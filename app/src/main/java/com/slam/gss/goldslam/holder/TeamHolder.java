package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slam.gss.goldslam.R;

import butterknife.BindView;

/**
 * Created by mahes on 6/20/2017.
 */

public class TeamHolder extends AbstrctRecyclerViewholder {

    @BindView(R.id.im_team_logo)
    ImageView imTeamLogo;

    @BindView(R.id.txt_team_name)
    TextView txtTeamName;

    public TextView getTxtTeamName() {
        return txtTeamName;
    }


    public ImageView getImTeamLogo() {
        return imTeamLogo;
    }

    public TeamHolder(View itemView) {
        super(itemView);
    }
}
