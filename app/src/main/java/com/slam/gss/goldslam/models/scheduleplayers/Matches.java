package com.slam.gss.goldslam.models.scheduleplayers;

import com.slam.gss.goldslam.models.eventadmin.Courts;

import java.util.Collections;
import java.util.List;

public class Matches {
    private String matchremarks;

    private String condition;

    private String status;

    private String location;

    private String roundName;

    private Court court;

    private String tournamentDocId;

    private String remarks;

    private String endTime;

    private String matchNo;

    private String eventDocId;

    private String rules;

    private String startTime;

    private List<Teams> teams;

    private String associationName;

    private String roundNo;

    private String tournamentName;

    private String eventName;

    private String docId;

    private String associationDocId;
    //
    private boolean header;
    private String courtId;
    private String venueDocId;
    private String time;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCourtId() {
        return courtId;
    }

    public void setCourtId(String courtId) {
        this.courtId = courtId;
    }

    public String getVenueDocId() {
        return venueDocId;
    }

    public void setVenueDocId(String venueDocId) {
        this.venueDocId = venueDocId;
    }

    public String getMatchremarks() {
        return matchremarks;
    }

    public void setMatchremarks(String matchremarks) {
        this.matchremarks = matchremarks;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRoundName() {
        return roundName;
    }

    public void setRoundName(String roundName) {
        this.roundName = roundName;
    }

    public Court getCourt() {
        return court;
    }

    public void setCourt(Court court) {
        this.court = court;
    }

    public String getTournamentDocId() {
        return tournamentDocId;
    }

    public void setTournamentDocId(String tournamentDocId) {
        this.tournamentDocId = tournamentDocId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMatchNo() {
        return matchNo;
    }

    public void setMatchNo(String matchNo) {
        this.matchNo = matchNo;
    }

    public String getEventDocId() {
        return eventDocId;
    }

    public void setEventDocId(String eventDocId) {
        this.eventDocId = eventDocId;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public List<Teams> getTeams() {
        if (teams == null) {
            return Collections.emptyList();
        }
        return teams;
    }

    public void setTeams(List<Teams> teams) {
        this.teams = teams;
    }

    public String getAssociationName() {
        return associationName;
    }

    public void setAssociationName(String associationName) {
        this.associationName = associationName;
    }

    public String getRoundNo() {
        return roundNo;
    }

    public void setRoundNo(String roundNo) {
        this.roundNo = roundNo;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getAssociationDocId() {
        return associationDocId;
    }

    public void setAssociationDocId(String associationDocId) {
        this.associationDocId = associationDocId;
    }

    @Override
    public String toString() {
        return "ClassPojo [ matchremarks = " + matchremarks + ", venueId="+ venueDocId+ ",time="+time+",date="+date+", condition = " + condition + ", status = " + status + ", location = " + location + ", roundName = " + roundName + ", court = " + court + ", tournamentDocId = " + tournamentDocId + ", remarks = " + remarks + ", endTime = " + endTime + ", matchNo = " + matchNo + ", eventDocId = " + eventDocId + ", rules = " + rules + ", startTime = " + startTime + ", teams = " + teams + ", associationName = " + associationName + ", roundNo = " + roundNo + ", tournamentName = " + tournamentName + ", eventName = " + eventName + ", docId = " + docId + ", associationDocId = " + associationDocId + "]";
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }


}