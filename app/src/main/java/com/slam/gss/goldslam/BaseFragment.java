package com.slam.gss.goldslam;

import android.support.v4.app.Fragment;

/**
 * Created by Ramesh on 20/8/16.
 */
public class BaseFragment extends Fragment {

    protected void showProgressDialog() {

    }

    protected void dismissProgressDialog() {

    }
}
