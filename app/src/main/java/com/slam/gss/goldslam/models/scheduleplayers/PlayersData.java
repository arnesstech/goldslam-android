package com.slam.gss.goldslam.models.scheduleplayers;

import java.util.List;

/**
 * Created by Ramesh on 21/8/16.
 */
public class PlayersData {
    private String message;

    private List<Entries> entries;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Entries> getEntries() {
        return entries;
    }

    public void setEntries(List<Entries> entries) {
        this.entries = entries;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", entries = " + entries + "]";
    }
}
