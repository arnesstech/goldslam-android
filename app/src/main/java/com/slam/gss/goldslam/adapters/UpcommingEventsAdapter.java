package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.UpCommingEventsHolder;
import com.slam.gss.goldslam.models.upCommingEvents.Tournaments;
import com.slam.gss.goldslam.models.upCommingEvents.UpCommingEvents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thriveni on 6/7/2017.
 */

public class UpcommingEventsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Tournaments> mUpCommingEvents = new ArrayList<>();
    private Context mContext;

    public UpcommingEventsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void add(List<Tournaments> upcommingEvents) {
        mUpCommingEvents = upcommingEvents;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vEmp = inflater.inflate(R.layout.item_upcomming_events, parent, false);
        viewHolder = new UpCommingEventsHolder(vEmp);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UpCommingEventsHolder upCommingEventsHolder = (UpCommingEventsHolder) holder;
        configureViewHolderFilter(upCommingEventsHolder, position);
    }

    private void configureViewHolderFilter(UpCommingEventsHolder holder, int position) {

        holder.getTxtEventDescription().setText(mUpCommingEvents.get(position).getTournamentName());
        holder.getTxtEventDate().setText(mUpCommingEvents.get(position).getEntryStartDate());
        holder.getTxtEventLocation().setText(mUpCommingEvents.get(position).getLocation());

        //Picasso.with(mContext).load(R.mipmap.ic_launcher);//Add Image from network response based on URL

        holder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mUpCommingEvents.size();
    }
}
