package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.models.eventadmin.Courts;

/**
 * Created by Thriveni on 9/16/2016.
 */
public class MatchVenueAdapter extends BaseAdapter implements View.OnClickListener {

    private Context _context;
    Courts[] _courts;
    private LayoutInflater inflater = null;

    public MatchVenueAdapter(Context context, Courts[] courts) {
        System.out.print("Addapter Constructor");
        this._context = context;
        this._courts = courts;


        /***********  Layout inflator to call external xml layout () ***********/
        inflater = (LayoutInflater) _context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        if (_courts.length <= 0)
            return 0;
        return _courts.length;
    }

    @Override
    public Object getItem(int position) {
        return _courts[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(View v) {

    }

    public static class VenueViewHolder {
        public TextView textView;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        VenueViewHolder holder;
        TextView venueName = null;
        if (convertView == null) {
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.match_schedule_spinner_text, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new VenueViewHolder();
            holder.textView = (TextView) view.findViewById(R.id.txtSpinner);

            /************  Set holder with LayoutInflater ************/
            view.setTag(holder);
        } else {
            holder = (VenueViewHolder) view.getTag();
        }

        if (_courts.length <= 0) {

        } else {
            /************  Set Model values in Holder elements ***********/
            String _venueText = _courts[position].getVenueName() + "--" + _courts[position].getCourtName();
           // holder.textView.setText(_courts[position].getVenueName());
            holder.textView.setText(_venueText);
        }
        return view;
    }

}

