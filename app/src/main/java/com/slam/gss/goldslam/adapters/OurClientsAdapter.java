package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.holder.OurClientsHolder;
import com.slam.gss.goldslam.holder.PlayingClubLocatorHolder;
import com.slam.gss.goldslam.models.OurClients.Clients;
import com.slam.gss.goldslam.models.media.MediaResponse;
import com.slam.gss.goldslam.models.playingClubLocators.PlayingClubLocators;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class OurClientsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Clients> mClientsList = new ArrayList<>();
    private Context mContext;

    public OurClientsAdapter(Context mContext) {
        this.mContext = mContext;
    }
    public void addItem(List<Clients> clientsList) {
        mClientsList = new ArrayList<>(clientsList);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vEmp = inflater.inflate(R.layout.item_our_clients, parent, false);
        viewHolder = new OurClientsHolder(vEmp);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        OurClientsHolder mOurClientsHolder = (OurClientsHolder) holder;
        configureViewHolderFilter(mOurClientsHolder, position);
    }

    private void configureViewHolderFilter(OurClientsHolder holder, int position) {



        //Picasso.with(mContext).load(R.mipmap.ic_launcher);//Add Image from network response based on URL

        holder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return mClientsList.size();
    }
}
