package com.slam.gss.goldslam.models.media;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class MediaResponse {
    String mediaText;
    String id;

    public String getMediaText() {
        return mediaText;
    }

    public void setMediaText(String mediaText) {
        this.mediaText = mediaText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
