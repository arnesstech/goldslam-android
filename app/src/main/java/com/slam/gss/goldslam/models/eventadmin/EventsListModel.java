package com.slam.gss.goldslam.models.eventadmin;

/**
 * Created by Thriveni on 9/8/2016.
 */
public class EventsListModel
{
    private String signinStartDateTime;

    private String series;

    private String tournamentDocId;

    private String remarks;

    private String eventEndDate;

    private String type;

    private String teamSize;

    private String action;

    private String tournamentName;

    private String associationDocId;

    private String ageGroup;

    private String prizemoney;

    private String publishEntries;

    private String adminName;

    private String dailyAllowances;

    private String status;

    private String eventStartDate;

    private String entryFee;

    private String rules;

    private String adminDocId;

    private String associationName;

    private String category;

    private String signinEndDateTime;

    private String eventName;

    private String docId;

    public String getSigninStartDateTime ()
    {
        return signinStartDateTime;
    }

    public void setSigninStartDateTime (String signinStartDateTime)
    {
        this.signinStartDateTime = signinStartDateTime;
    }

    public String getSeries ()
    {
        return series;
    }

    public void setSeries (String series)
    {
        this.series = series;
    }

    public String getTournamentDocId ()
    {
        return tournamentDocId;
    }

    public void setTournamentDocId (String tournamentDocId)
    {
        this.tournamentDocId = tournamentDocId;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    public String getEventEndDate ()
    {
        return eventEndDate;
    }

    public void setEventEndDate (String eventEndDate)
    {
        this.eventEndDate = eventEndDate;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getTeamSize ()
    {
        return teamSize;
    }

    public void setTeamSize (String teamSize)
    {
        this.teamSize = teamSize;
    }

    public String getAction ()
    {
        return action;
    }

    public void setAction (String action)
    {
        this.action = action;
    }

    public String getTournamentName ()
    {
        return tournamentName;
    }

    public void setTournamentName (String tournamentName)
    {
        this.tournamentName = tournamentName;
    }

    public String getAssociationDocId ()
    {
        return associationDocId;
    }

    public void setAssociationDocId (String associationDocId)
    {
        this.associationDocId = associationDocId;
    }

    public String getAgeGroup ()
    {
        return ageGroup;
    }

    public void setAgeGroup (String ageGroup)
    {
        this.ageGroup = ageGroup;
    }

    public String getPrizemoney ()
    {
        return prizemoney;
    }

    public void setPrizemoney (String prizemoney)
    {
        this.prizemoney = prizemoney;
    }

    public String getPublishEntries ()
    {
        return publishEntries;
    }

    public void setPublishEntries (String publishEntries)
    {
        this.publishEntries = publishEntries;
    }

    public String getAdminName ()
    {
        return adminName;
    }

    public void setAdminName (String adminName)
    {
        this.adminName = adminName;
    }

    public String getDailyAllowances ()
    {
        return dailyAllowances;
    }

    public void setDailyAllowances (String dailyAllowances)
    {
        this.dailyAllowances = dailyAllowances;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getEventStartDate ()
    {
        return eventStartDate;
    }

    public void setEventStartDate (String eventStartDate)
    {
        this.eventStartDate = eventStartDate;
    }

    public String getEntryFee ()
    {
        return entryFee;
    }

    public void setEntryFee (String entryFee)
    {
        this.entryFee = entryFee;
    }

    public String getRules ()
    {
        return rules;
    }

    public void setRules (String rules)
    {
        this.rules = rules;
    }

    public String getAdminDocId ()
    {
        return adminDocId;
    }

    public void setAdminDocId (String adminDocId)
    {
        this.adminDocId = adminDocId;
    }

    public String getAssociationName ()
    {
        return associationName;
    }

    public void setAssociationName (String associationName)
    {
        this.associationName = associationName;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getSigninEndDateTime ()
    {
        return signinEndDateTime;
    }

    public void setSigninEndDateTime (String signinEndDateTime)
    {
        this.signinEndDateTime = signinEndDateTime;
    }

    public String getEventName ()
    {
        return eventName;
    }

    public void setEventName (String eventName)
    {
        this.eventName = eventName;
    }

    public String getDocId ()
    {
        return docId;
    }


    public void setDocId (String docId)
    {
        this.docId = docId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [signinStartDateTime = "+signinStartDateTime+", series = "+series+", tournamentDocId = "+tournamentDocId+", remarks = "+remarks+", eventEndDate = "+eventEndDate+", type = "+type+", teamSize = "+teamSize+", action = "+action+", tournamentName = "+tournamentName+", associationDocId = "+associationDocId+", ageGroup = "+ageGroup+", prizemoney = "+prizemoney+", publishEntries = "+publishEntries+", adminName = "+adminName+", dailyAllowances = "+dailyAllowances+", status = "+status+", eventStartDate = "+eventStartDate+", entryFee = "+entryFee+", rules = "+rules+", adminDocId = "+adminDocId+", associationName = "+associationName+", category = "+category+", signinEndDateTime = "+signinEndDateTime+", eventName = "+eventName+", docId = "+docId+"]";
    }
}
