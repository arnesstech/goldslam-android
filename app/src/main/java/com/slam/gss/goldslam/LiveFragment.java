package com.slam.gss.goldslam;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.SchedulePlayerResponse;
import com.slam.gss.goldslam.models.scheduleplayers.Teams;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ramesh on 20/8/16.
 */
public class LiveFragment extends BaseFragment {
    String docId;
    Call<SchedulePlayerResponse> call;

    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.empty)
    TextView empty;

    private boolean isRefreshing;
    private LiveAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        docId = getActivity().getIntent().getStringExtra("docId");
        adapter = new LiveAdapter(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_live_player_schedule, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        listView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadPlayer();
    }

    public void loadPlayer() {
        if (isRefreshing) {
            return;
        }
        showProgressDialog();
        isRefreshing = true;
        Call<SchedulePlayerResponse> call = RestApi.get()
                .getRestService()
                .getScheduleMatches(docId, "ALL", "CURRENT");
        //
        call.enqueue(new Callback<SchedulePlayerResponse>() {
            @Override
            public void onResponse(Call<SchedulePlayerResponse> call, Response<SchedulePlayerResponse> response) {
                isRefreshing = false;
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null) {
                        filterWithDates(response.body().getData().getMatches());
                    }
                } else {
                    //Handle errors
                }
                dismissProgressDialog();
                showEmptyView();
            }

            @Override
            public void onFailure(Call<SchedulePlayerResponse> call, Throwable t) {
                isRefreshing = false;
                if (call.isCanceled()) {
                    //Do nothing
                } else {
                    //Handle errors
                }
                showEmptyView();
                dismissProgressDialog();
            }
        });
    }

    public void filterWithDates(List<Matches> matches) {
        Collections.sort(matches, new Comparator<Matches>() {
            @Override
            public int compare(Matches lhs, Matches rhs) {
                /*return (int) (Utils.convertLiveDateToMillis(rhs.getStartTime()) -
                        Utils.convertLiveDateToMillis(lhs.getStartTime()));*/

                return (Integer.parseInt(rhs.getRoundNo())-Integer.parseInt(lhs.getRoundNo()));
            }
        });
        List<Matches> live = new ArrayList<>();
        List<Matches> completed = new ArrayList<>();
        for (Matches matches1 : matches) {
            if ("live".equalsIgnoreCase(matches1.getStatus())) {
                live.add(matches1);
            } else if ("completed".equalsIgnoreCase(matches1.getStatus())) {
                completed.add(matches1);
            }
        }
        //
        Map<String, List<Matches>> map = new LinkedHashMap<>();
        filterAsHeaders(map, live);
        filterAsHeaders(map, completed);
        //
        List<Matches> finalList = new ArrayList<>();
        for (Map.Entry<String, List<Matches>> entry : map.entrySet()) {
            finalList.addAll(entry.getValue());
        }
        if (adapter != null) {
            adapter.addItems(finalList);
        }
        showEmptyView();

    }

    private void showEmptyView() {
        if (!isAdded()) {
            return;
        }
        if (adapter.getCount() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
        }
    }

    private void filterAsHeaders(Map<String, List<Matches>> map, List<Matches> list) {
        for (Matches m : list) {
            String roundName = m.getRoundName();
            Log.e("round number is",m.getRoundNo());
            if (map.get(roundName) != null) {
                map.get(roundName).add(m);
            } else {
                List<Matches> temp = new ArrayList<>();
                m.setHeader(true);
                temp.add(m);
                map.put(roundName, temp);
            }
        }
    }

    @OnClick(R.id.refresh_layout)
    public void onClick() {
        loadPlayer();
    }

    class LiveViewHolder extends AbstractViewHolder {
        @BindView(R.id.txt_date)
        TextView date;

        public TextView getPlayer1Position() {
            return player1Position;
        }

        public TextView getPlayer2Position() {
            return player2Position;
        }

        @BindView(R.id.txt_player1_position)
        TextView player1Position;
        @BindView(R.id.txt_player2_position)
        TextView player2Position;

        @BindView(R.id.txt_header)
        TextView header;

        @BindView(R.id.txt_time)
        TextView time;
        @BindView(R.id.txt_venue)
        TextView venue;
        @BindView(R.id.txt_player1)
        TextView player1;
        @BindView(R.id.txt_player2)
        TextView player2;
        @BindViews({
                R.id.txt_pscore1, R.id.txt_pscore2, R.id.txt_pscore3,
                R.id.txt_pscore4, R.id.txt_pscore5,
        })
        List<TextView> scoreView1;
        @BindViews({
                R.id.txt_p2score1, R.id.txt_p2score2, R.id.txt_p2score3,
                R.id.txt_p2score4, R.id.txt_p2score5,
        })
        List<TextView> scoreView2;

        public LiveViewHolder(View view) {
            super(view);
        }

        public TextView getPlayer2() {
            return player2;
        }

        public TextView getPlayer1() {
            return player1;
        }

        public List<TextView> getScoreView1() {
            return scoreView1;
        }

        public TextView getHeader() {
            return header;
        }

        public List<TextView> getScoreView2() {
            return scoreView2;
        }

        public TextView getDate() {
            return date;
        }

        public TextView getTime() {
            return time;
        }

        public TextView getVenue() {
            return venue;
        }


    }

    private class LiveAdapter extends AbstractBaseAdapter<Matches, LiveViewHolder> {

        public LiveAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.frag_players_live_row;
        }

        @Override
        public LiveViewHolder getViewHolder(View convertView) {
            return new LiveViewHolder(convertView);
        }

        @Override
        public void bindView(int position, LiveViewHolder holder, Matches item) {
            try {
                if (item.isHeader()) {
                    holder.getHeader().setVisibility(View.VISIBLE);
                } else {
                    holder.getHeader().setVisibility(View.GONE);
                }
                holder.getHeader().setText(item.getRoundName()+" Scores");
                List<Teams> teams = item.getTeams();
                holder.getPlayer1Position().setText(teams.get(0).getPosition());
                holder.getPlayer2Position().setText(teams.get(1).getPosition());

                holder.getVenue().setText(item.getCourt().getCourtName());
                final String[] split = item.getStartTime().split(" ");
                String sDate = split[0];
                String sTime = split[1];
                holder.getDate().setText(sDate);
                holder.getTime().setText(sTime);
                //
                holder.getPlayer1().setTextColor(getResources().getColor(R.color.text_view_grey_color));
                holder.getPlayer2().setTextColor(getResources().getColor(R.color.text_view_grey_color));

                if ("winner".equalsIgnoreCase(teams.get(0).getStatus())) {
                   // holder.getPlayer1().setTextColor(Color.GREEN);
                    holder.getPlayer1().setTextColor(Color.parseColor("#f59121"));
                } else if ("winner".equalsIgnoreCase(teams.get(1).getStatus())) {
                   // holder.getPlayer2().setTextColor(Color.GREEN);
                    holder.getPlayer2().setTextColor(Color.parseColor("#f59121"));
                }

                holder.getPlayer1().setText(teams.get(0).getPosition()+" "+teams.get(0).getName());
                Log.i("player 1 position",teams.get(0).getPosition());
                holder.getPlayer2().setText(teams.get(1).getPosition()+" "+teams.get(1).getName());
                //First player scores
                setPlayerScores(teams.get(0).getScores(), holder.getScoreView1());
                //Second player scores
                setPlayerScores(teams.get(1).getScores(), holder.getScoreView2());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setPlayerScores(List<String> scores2, List<TextView> viewList) {
        int index = 0;
        for (TextView textView : viewList) {
            if (index < scores2.size()) {
                textView.setText(scores2.get(index));
            } else {
                textView.setText("-");
            }
            index++;
        }

    }

    @Override
    protected void showProgressDialog() {
        super.showProgressDialog();
        if (progress != null) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void dismissProgressDialog() {
        super.dismissProgressDialog();
        if (progress != null) {
            progress.setVisibility(View.GONE);
        }
    }
}
