package com.slam.gss.goldslam.holder;

import android.view.View;
import android.widget.TextView;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thriveni on 5/19/2017.
 */

public class UserSubscriptionCreateHolder extends AbstractViewHolder {

    @BindView(R.id.txt_user_subscription_type)
    TextView txtSubscriptionType;

    @BindView(R.id.txt_user_subscription_name)
    TextView txtSubscriptionName;

    @BindView(R.id.txt_user_subscription_fee)
    TextView txtSubscriptionFee;

    @BindView(R.id.txt_user_subscription_start_date)
    TextView txtSubscriptionStartDate;

    @BindView(R.id.txt_user_subscription_end_date)
    TextView txtSubscriptionEndDate;

    public TextView getTxtSubscriptionType() {
        return txtSubscriptionType;
    }

    public TextView getTxtSubscriptionName() {
        return txtSubscriptionName;
    }

    public TextView getTxtSubscriptionFee() {
        return txtSubscriptionFee;
    }

    public TextView getTxtSubscriptionStartDate() {
        return txtSubscriptionStartDate;
    }

    public TextView getTxtSubscriptionEndDate() {
        return txtSubscriptionEndDate;
    }

    public UserSubscriptionCreateHolder(View view) {
        super(view);
        ButterKnife.bind(view);
    }
}
