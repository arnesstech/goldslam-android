package com.slam.gss.goldslam.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.slam.gss.goldslam.EventAdminFragment;
import com.slam.gss.goldslam.PrepareScheduleActivity;
import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.eventadmin.EventAdminTournaments.Events;
import com.slam.gss.goldslam.models.eventadmin.EventsListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thriveni on 9/8/2016.
 */
public class EventsListAdapter extends BaseAdapter implements View.OnClickListener {

    private Context _context;
    private SharedPreferences sp;
    private SharedPreferences.Editor edit;
    ArrayList<EventsListModel> _events;
    EventsListModel tempValues=null;
    ArrayAdapter<String> adapter;
    private static LayoutInflater inflater = null;

    public EventsListAdapter(Context context, ArrayList<EventsListModel> events) {
        this._context = context;
        this._events = events;
        sp = _context.getSharedPreferences("GSS", Context.MODE_PRIVATE);
        edit = sp.edit();

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = (LayoutInflater) _context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        if (_events.size() <= 0)
            return 1;
        return _events.size();
    }

    @Override
    public Object getItem(int position) {
        return _events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(View v) {

    }

    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public static class EventViewHolder {

        public TextView tournamentName;
        public TextView associationName;
        public TextView eventName;
        public TextView eventStartDate;
        public TextView eventEndDate;

        public ImageView info_image,arrorw_image;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        EventViewHolder holder;
        if(convertView == null){
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.tournament_group, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new EventViewHolder();
            holder.tournamentName = (TextView) view.findViewById(R.id.collegeName);
            holder.associationName=(TextView)view.findViewById(R.id.address);
            holder.eventStartDate=(TextView) view.findViewById(R.id.fromDate);
            holder.eventEndDate=(TextView) view.findViewById(R.id.toDate);
            holder.arrorw_image = (ImageView) view.findViewById(R.id.arrorw_image);
            holder.info_image=(ImageView) view.findViewById(R.id.info_image);

            holder.arrorw_image.setVisibility(View.INVISIBLE);
            holder.info_image.setVisibility(View.INVISIBLE);
            /************  Set holder with LayoutInflater ************/
                view.setTag( holder );
            }else{
                holder=(EventViewHolder) view.getTag();
            }

        if(_events.size()<=0)
        {
            holder.tournamentName.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
           // _events=null;
            tempValues = ( EventsListModel ) _events.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.tournamentName.setText( tempValues.getTournamentName() );
            holder.associationName.setText( tempValues.getAssociationName()+" "+tempValues.getEventName() );
            holder.eventStartDate.setText( tempValues.getEventStartDate() );
            holder.eventEndDate.setText( "-" +tempValues.getEventEndDate() );

            /******** Set Item Click Listner for LayoutInflater for each row *******/

            view.setOnClickListener(new OnItemClickListener( position,tempValues ));
        }
        return view;
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener{
        private int mPosition;
        private EventsListModel mTempValues;

        OnItemClickListener(int position, EventsListModel tempValues){
            mPosition = position;
            mTempValues = tempValues;
        }

        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent((Activity)_context, PrepareScheduleActivity.class);

            Log.e("selected event docId",mTempValues.getDocId());
            Log.e("selected event Name",mTempValues.getEventName());
            Log.e("selected Tourn docId",mTempValues.getTournamentDocId());
            intent.putExtra("eventposition",mPosition);
            intent.putExtra("eventName",mTempValues.getEventName());
            intent.putExtra("docId",mTempValues.getDocId());
            intent.putExtra("tournamentId",mTempValues.getTournamentDocId());
            _context.startActivity(intent);

        }
    }

}



