package com.slam.gss.goldslam;

/**
 * Created by Thriveni on 26-07-2016.
 */

import android.annotation.TargetApi;
import android.app.Activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.slam.gss.goldslam.adapters.GenderSpinner;
import com.slam.gss.goldslam.adapters.SpinnerAdapter;
import com.slam.gss.goldslam.helpers.Connections;
import com.slam.gss.goldslam.helpers.Helper;
import com.slam.gss.goldslam.models.MasterData.Country;
import com.slam.gss.goldslam.models.eventadmin.MatchConditionsResponse;
import com.slam.gss.goldslam.network.HttpHelper;
import com.slam.gss.goldslam.network.RestApi;

import org.apache.http.client.HttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by myHome on 7/24/16.
 */
public class RegisterActivity extends Activity implements View.OnClickListener {

    private Intent intent;
    private Button btnLogin, btnRegister;
    private EditText edFirstName, edLastName, edEmail, edMobile, edDob, edPassword, edConfirmPassword, edUserName;
    private String firstName = "", lastName = "", email = "", mobile = "", dob = "", gender = "", password = "", confirmpassword = "";
    private JSONObject registrationObj = null;
    public HttpHelper httpHelper = null;
    private String registrationResponse = "", verificationCode = "";
    private Spinner spGender, spCountry;
    private Dialog otpDialog;
    Calendar myCalendar;
    private ArrayList<String> countryList;
    private String country, username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_demo_layout);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        initializeViews();

        getCountries();
    }

    private void getCountries() {
        Call<Country> getCountries = RestApi.get()
                .getRestService()
                .getCountries();

        getCountries.enqueue(new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null && response.body().getData() != null) {
                        //Log.e("get the data", response.body().getData().getMasterData().getOptions().toString());

                        //Log.e("country details", response.body().getData().getMasterData().getOptions().length + "");
                        int defaultIndia = -1;
                        countryList = new ArrayList<String>();
                        countryList.add("Select country");
                        if (response.body().getData().getMasterData().getOptions().length > 0) {
                            for (int i = 0; i < response.body().getData().getMasterData().getOptions().length; i++) {
                                String value = response.body().getData().getMasterData().getOptions()[i].getValue();
                                countryList.add(value);
                                if ("india".equalsIgnoreCase(value)) {
                                    defaultIndia = countryList.size() - 1;
                                }
                            }
                        } else {
                            countryList = new ArrayList<String>();
                        }
                        SpinnerAdapter adapter = new SpinnerAdapter(RegisterActivity.this, android.R.layout.simple_spinner_dropdown_item, countryList);
                        spCountry.setAdapter(adapter);
                        if (defaultIndia >= 0) {
                            spCountry.setSelection(defaultIndia);
                        }
                    }

                } else {
                    Log.e("getCountries", "not successfull");
                }
            }

            @Override
            public void onFailure(Call<Country> call, Throwable t) {

            }
        });
    }


    public class MyAdapter extends ArrayAdapter<String> {

        ArrayList<String> genderList;

        public MyAdapter(Context ctx, int txtViewResourceId, ArrayList<String> objects) {
            super(ctx, txtViewResourceId, objects);
            genderList = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.spinner_text, parent, false);

            TextView txtGender = (TextView) mySpinner.findViewById(R.id.txtGender);
            txtGender.setText(genderList.get(position));
            return mySpinner;
        }
    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            view.setMaxDate(System.currentTimeMillis());
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void updateLabel() {

        String myFormat = "dd-MMM-yyyy";//"MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edDob.setText(sdf.format(myCalendar.getTime()));
    }


    public void initializeViews() {
        myCalendar = Calendar.getInstance();

        btnRegister = (Button) findViewById(R.id.register);
        edFirstName = (EditText) findViewById(R.id.firstname);
        edLastName = (EditText) findViewById(R.id.lastname);
        edEmail = (EditText) findViewById(R.id.email);
        edMobile = (EditText) findViewById(R.id.mobile);
        edDob = (EditText) findViewById(R.id.dob);
        edUserName = (EditText) findViewById(R.id.username);
        edPassword = (EditText) findViewById(R.id.pwd);
        edConfirmPassword = (EditText) findViewById(R.id.confirm_pwd);

        spCountry = (Spinner) findViewById(R.id.country);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                country = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spGender = (Spinner) findViewById(R.id.gender);

        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                gender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayList<String> genderList = new ArrayList<>();
        genderList.add("Gender");
        genderList.add("Male");
        genderList.add("Female");

        ImageView iv = (ImageView) findViewById(R.id.back);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        edDob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RegisterActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        spGender.setAdapter(new SpinnerAdapter(this, R.layout.spinner_text, genderList));
      //  spGender.setPopupBackgroundResource(R.drawable.spinner_items_background);

        btnRegister.setOnClickListener(this);
        edFirstName.addTextChangedListener(new MyTextWatcher(edFirstName));
        edLastName.addTextChangedListener(new MyTextWatcher(edLastName));
        edEmail.addTextChangedListener(new MyTextWatcher(edEmail));
        edMobile.addTextChangedListener(new MyTextWatcher(edMobile));
        edDob.addTextChangedListener(new MyTextWatcher(edDob));
        edUserName.addTextChangedListener(new MyTextWatcher(edUserName));
        edPassword.addTextChangedListener(new MyTextWatcher(edPassword));
        edConfirmPassword.addTextChangedListener(new MyTextWatcher(edConfirmPassword));

    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.username:
                    validateUserName();
                    break;

                case R.id.pwd:
                    validatePassword();
                    break;

                case R.id.confirm_password:
                    validateConfirmPassword();
                    break;

                case R.id.firstname:
                    validateFirstName();
                    break;

                case R.id.lastname:
                    validateLastName();
                    break;

                case R.id.email:
                    validateEmail();
                    break;

                case R.id.mobile:
                    validateMobile();
                    break;


                default:
                    break;
            }
        }
    }


    private boolean validateUserName() {
        if (edUserName.getText().toString().trim().isEmpty()) {
            edUserName.setError(getString(R.string.err_reg_userName));
            edUserName.requestFocus();
            return false;
        } else if ((edUserName.getText().toString().length() < 6) || (edUserName.getText().toString().contains(" "))) {
            edUserName.setError("User Name*\n" +
                    "Minimum of 6 characters length. Allowed characters are alphabets, numbers, dot (.), underscore (_), @. Spaces not allowed.");
            edUserName.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private boolean validatePassword() {
        if (edPassword.getText().toString().trim().equalsIgnoreCase("")) {
            edPassword.setError(getString(R.string.err_reg_password));
            edPassword.requestFocus();
            return false;
        } else if ((edPassword.getText().toString().length() < 6) || (!(edPassword.getText().toString().matches(".*\\d+.*")))) {
            edPassword.setError(getString(R.string.err_reg_password_Length));
            return false;
        } else {
            return true;
        }

    }

    private boolean validateConfirmPassword() {
        if (edConfirmPassword.getText().toString().trim().isEmpty()) {
            edConfirmPassword.setError(getString(R.string.err_reg_confirm_password));
            edConfirmPassword.requestFocus();
            return false;
        } else {

            if (!(edConfirmPassword.getText().toString().equalsIgnoreCase(edPassword.getText().toString()))) {
                edConfirmPassword.setError(getString(R.string.err_reg_confirm1_password));
                edConfirmPassword.requestFocus();
                return false;
            } else {
                return true;
            }
        }

    }

    public boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private boolean validateMobile() {
        if (edMobile.getText().toString().trim().isEmpty()) {
            edMobile.setError(getString(R.string.err_reg_mobile));
            edMobile.requestFocus();
            return false;
        } else {
            //edMobile.setErrorEnabled(false);
            if (edMobile.getText().toString().length() < 10) {
                edMobile.setError(getString(R.string.err_reg_mobileNo_length));
                edMobile.setFocusable(true);
                return false;
            } else {
                return true;
            }
        }


    }



    private boolean validateEmail() {
        if (edEmail.getText().toString().trim().isEmpty()) {
            return true;
        } else {
            Pattern pattern;
            Matcher matcher;
            final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            pattern = Pattern.compile(EMAIL_PATTERN);
            matcher = pattern.matcher(edEmail.getText().toString());
            if (matcher.matches()) {
                Log.i("matcher value", String.valueOf(matcher.matches()));
                return true;

            } else {
                Log.i("matcher value", String.valueOf(matcher.matches()));
                edEmail.setError(getString(R.string.err_reg_validate_email));
                edEmail.requestFocus();
                return false;
            }


        }

        // return true;
    }

    private boolean validateFirstName() {
        if (edFirstName.getText().toString().trim().isEmpty()) {
            edFirstName.setError(getString(R.string.err_reg_firstName));
            edFirstName.requestFocus();
            return false;
        } else {
            return true;
        }
    }


    private boolean validateLastName() {
        if (edLastName.getText().toString().trim().isEmpty()) {
            edLastName.setError(getString(R.string.err_reg_lastName));
            edLastName.requestFocus();
            return false;
        } else {
            //inputLayoutLastName.setErrorEnabled(false);
            return true;
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();

                break;
            case R.id.register:
                register();
                break;

            default:
                break;
        }

    }


    public void register() {

        if (!validateUserName()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }
        if (!validateConfirmPassword()) {
            return;
        }
        if (!validateFirstName()) {
            return;
        }
       /* if (!validateLastName()) {
            return;
        }*/
        if (!validateEmail()) {
            return;
        }
        if (!validateMobile()) {
            return;
        } else if (country.equalsIgnoreCase("select country")) {
            Toast.makeText(RegisterActivity.this, "Please select country", Toast.LENGTH_SHORT).show();
        }
        /*else if (TextUtils.isEmpty(edDob.getText().toString())) {
            edDob.setError("Please enter valid dob");
            edDob.requestFocus();

        } */
        /*else if (gender.equalsIgnoreCase("Gender")) {
            Toast.makeText(RegisterActivity.this, "Please select gender", Toast.LENGTH_SHORT).show();
        } */

        else {

            firstName = edFirstName.getText().toString().trim();
            lastName = edLastName.getText().toString().trim();
            mobile = edMobile.getText().toString().trim();
            email = edEmail.getText().toString().trim();
            password = edPassword.getText().toString().trim();
            dob = edDob.getText().toString().trim();
            username = edUserName.getText().toString();

            new RegistrationSyncTask().execute(Connections.REGISTRATION_URL);
        }
    }

    class RegistrationSyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(RegisterActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                URL url = new URL(urls[0]);
                httpHelper = new HttpHelper();

                JSONObject registrationObj = new JSONObject();
                registrationObj.put("firstName", firstName);
                registrationObj.put("lastName", lastName);
                registrationObj.put("mobile", mobile);
                registrationObj.put("email", email);
                registrationObj.put("gender", gender);
                registrationObj.put("registerType", "self");
                registrationObj.put("userType", "user");
                registrationObj.put("dob", dob);
                registrationObj.put("userName", username);
                registrationObj.put("password", password);
                registrationObj.put("country", country);


                registrationResponse = httpHelper.userRegistration(urls[0], registrationObj, RegisterActivity.this);
                return registrationResponse;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            JSONObject jsonResponse = null;

            String status = null;

            Log.i("Registration Result", result);
            try {
                jsonResponse = new JSONObject(result);
                Log.i("registrationResponse", jsonResponse.toString());
                status = jsonResponse.get("status").toString();
                Log.i("status", status);
                if (status.equalsIgnoreCase("SUCCESS")) {

                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    //Helper.showToast(getApplicationContext(), obj.getString("message"));


                    showDialog(obj);
                    /*intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();*/

                    //showOtpVerification();
                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getApplicationContext(), obj.getString("message"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getApplicationContext(), "Network error");
            }

        }
    }

    private void showDialog(JSONObject obj) {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
            String alertMsg = "";
            if (obj.has("alertmessage")) {
                alertMsg=obj.getString("alertmessage");
            } else {
                alertMsg="";
            }
            alert.setMessage(alertMsg); //Message here

            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                } // End of onClick(DialogInterface dialog, int whichButton)
            }); //End of alert.setPositiveButton

            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showOtpVerification() {
        // Create custom dialog object
        otpDialog = new Dialog(RegisterActivity.this);
        // Include dialog.xml file
        otpDialog.setContentView(R.layout.otp_verification);

        WindowManager.LayoutParams lp = otpDialog.getWindow().getAttributes();
        lp.dimAmount = 0.7f;
        otpDialog.getWindow().setAttributes(lp);
        otpDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);


        otpDialog.show();

        final EditText edVerificationCode = (EditText) otpDialog.findViewById(R.id.verificationCode);

        TextView submit = (TextView) otpDialog.findViewById(R.id.confirmOtp);
        // if decline button is clicked, close the custom dialog
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verificationCode = edVerificationCode.getText().toString();
                if (TextUtils.isEmpty(edVerificationCode.getText().toString())) {
                    edVerificationCode.setError("Please Enter OTP");
                } else {
                    new OtpVerificationAsynTask().execute(Connections.OTP_URL);
                    // Close dialog
                    otpDialog.dismiss();
                }

            }
        });

    }

    private void updatePassword() {
        // Create custom dialog object
        otpDialog = new Dialog(RegisterActivity.this);
        // Include dialog.xml file
        otpDialog.setContentView(R.layout.password_updation);

        WindowManager.LayoutParams lp = otpDialog.getWindow().getAttributes();
        lp.dimAmount = 0.7f;
        otpDialog.getWindow().setAttributes(lp);
        otpDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);


        otpDialog.show();

        final EditText password = (EditText) otpDialog.findViewById(R.id.password);
        final EditText confirmPassword = (EditText) otpDialog.findViewById(R.id.conform_password);

        TextView submit = (TextView) otpDialog.findViewById(R.id.submit_password);
        // if decline button is clicked, close the custom dialog
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(password.getText().toString())) {
                    password.setError("Please Enter Password");

                } else if (TextUtils.isEmpty(confirmPassword.getText().toString())) {
                    confirmPassword.setError("Please Enter Password");

                } else if (!password.getText().toString().equalsIgnoreCase(confirmPassword.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Both passwords are not same", Toast.LENGTH_SHORT).show();
                } else {

                    new UpdatePasswordAsynTask(password.getText().toString()).execute(Connections.UPDATE_PASSWORD_URL);
                    // Close dialog
                    otpDialog.dismiss();
                }

            }
        });

    }

    class OtpVerificationAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(RegisterActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                URL url = new URL(urls[0]);
                httpHelper = new HttpHelper();

                JSONObject otpverificationObj = new JSONObject();
                otpverificationObj.put("mobileOrEmail", mobile);
                otpverificationObj.put("verificationCode", verificationCode);


                Log.e("params", otpverificationObj.toString() + urls[0]);

                String response = httpHelper.otpVerification(urls[0], otpverificationObj);
                return response;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            JSONObject jsonResponse = null;
            String status = null;

            try {
                jsonResponse = new JSONObject(result);
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {

                    JSONObject obj = new JSONObject(jsonResponse.get("data").toString());
                    Log.i("OtpVerificationData", obj.toString());
                    Helper.registrationDocId = obj.getString("registrationDocId");
                    Helper.apiToken = obj.get("apiToken").toString();

                    Helper.showToast(getApplicationContext(), obj.getString("message"));

//                    intent = new Intent(RegisterActivity.this,PasswordActivity.class);
//                    startActivity(intent);
//                    finish();
                    updatePassword();
                } else {
                    JSONObject obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getApplicationContext(), obj.getString("message"));

                    Log.i("error otp", obj.getString("message").toString());
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getApplicationContext(), e.getMessage());
            }

        }

    }


    class UpdatePasswordAsynTask extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;
        String newPassword;

        public UpdatePasswordAsynTask(String password) {
            this.newPassword = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.print("onPreExecute");
            dialog = new ProgressDialog(RegisterActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected String doInBackground(String... urls) {

            try {

                URL url = new URL(urls[0]);
                httpHelper = new HttpHelper();

                JSONObject updatePasswordObj = new JSONObject();
                //updatePasswordObj.put("newPassword", password);
                updatePasswordObj.put("registrationDocId", Helper.registrationDocId);
                updatePasswordObj.put("userId", Helper.registrationDocId);
                updatePasswordObj.put("newPassword", newPassword);


                Log.e("params", updatePasswordObj.toString() + urls[0]);

                String result = httpHelper.updatePassword(urls[0], updatePasswordObj);
                return result;

            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            Log.i("Update Password Result", result);
            JSONObject obj = null, jsonResponse = null;
            String status = null;
            try {
                jsonResponse = new JSONObject(result);
                obj = new JSONObject(jsonResponse.get("data").toString());
                status = jsonResponse.get("status").toString();
                if (status.equalsIgnoreCase("SUCCESS")) {
                    Helper.showToast(getApplicationContext(), obj.get("message").toString());
                    intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    obj = new JSONObject(jsonResponse.get("error").toString());
                    Helper.showToast(getApplicationContext(), obj.getString("message"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Helper.showToast(getApplicationContext(), e.getMessage());
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}

