package com.slam.gss.goldslam.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slam.gss.goldslam.R;
import com.slam.gss.goldslam.fragments.PlayersCoachingFragment;
import com.slam.gss.goldslam.fragments.PlayingClubLocatorInfoActivity;
import com.slam.gss.goldslam.holder.PlayingClubLocatorHolder;
import com.slam.gss.goldslam.holder.    UpCommingEventsHolder;
import com.slam.gss.goldslam.models.playingClubLocators.PlayingClubLocators;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thriveni on 6/19/2017.
 */

public class PlayingClubLocatorsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<PlayingClubLocators> mPlayingClubLocators = new ArrayList<>();
    private Context mContext;

    public PlayingClubLocatorsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void addItem(List<PlayingClubLocators> playingClubLocators) {
        mPlayingClubLocators = new ArrayList<>(playingClubLocators);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View vEmp = inflater.inflate(R.layout.item_players_club_locators, parent, false);
        viewHolder = new PlayingClubLocatorHolder
                (vEmp);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PlayingClubLocatorHolder playingClubLocatorHolder = (PlayingClubLocatorHolder) holder;
        configureViewHolderFilter(playingClubLocatorHolder, position);
    }

    private void configureViewHolderFilter(PlayingClubLocatorHolder holder, int position) {

        holder.getTxtPlayingClubName().setText(mPlayingClubLocators.get(position).getName());
        holder.getTxtLocation().setText(mPlayingClubLocators.get(position).getLocation());
        holder.getTxtRating().setText(mPlayingClubLocators.get(position).getRating());
        holder.getRbRating().setRating(Float.parseFloat(mPlayingClubLocators.get(position).getRating()));

        //Picasso.with(mContext).load(R.mipmap.ic_launcher);//Add Image from network response based on URL

        holder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PlayingClubLocatorInfoActivity.class);
                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mPlayingClubLocators.size();
    }
}
