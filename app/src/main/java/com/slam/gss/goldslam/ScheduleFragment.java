package com.slam.gss.goldslam;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.slam.gss.goldslam.adapters.AbstractBaseAdapter;
import com.slam.gss.goldslam.adapters.AbstractViewHolder;
import com.slam.gss.goldslam.helpers.Utils;
import com.slam.gss.goldslam.models.scheduleplayers.Matches;
import com.slam.gss.goldslam.models.scheduleplayers.SchedulePlayerResponse;
import com.slam.gss.goldslam.models.scheduleplayers.Teams;
import com.slam.gss.goldslam.network.RestApi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ramesh on 21/8/16.
 */
public class ScheduleFragment extends BaseFragment {
    private String docId;
    private Call<SchedulePlayerResponse> call;

    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.empty)
    TextView empty;

    private boolean isRefreshing;
    private ScheduleAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        docId = getActivity().getIntent().getStringExtra("docId");
        adapter = new ScheduleAdapter(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_live_player_schedule, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        listView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadPlayer();
    }

    public void loadPlayer() {
        if (isRefreshing) {
            return;
        }
        showProgressDialog();
        isRefreshing = true;
        Call<SchedulePlayerResponse> call = RestApi.get()
                .getRestService()
                .getScheduleMatches(docId, "ALL", "CURRENT");
        //
        call.enqueue(new Callback<SchedulePlayerResponse>() {
            @Override
            public void onResponse(Call<SchedulePlayerResponse> call, Response<SchedulePlayerResponse> response) {
                isRefreshing = false;

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null) {
                        filterWithDates(response.body().getData().getMatches());
                    }
                } else {
                    //Handle errors
                }
                dismissProgressDialog();
                showEmptyView();
            }

            @Override
            public void onFailure(Call<SchedulePlayerResponse> call, Throwable t) {
                isRefreshing = false;
                if (call.isCanceled()) {
                    //Do nothing
                } else {
                    //Handle errors
                }
                showEmptyView();
                dismissProgressDialog();
            }
        });
    }

    public void filterWithDates(List<Matches> matches) {
//        Collections.sort(matches, new Comparator<Matches>() {
//            @Override
//            public int compare(Matches lhs, Matches rhs) {
//                return (int) (Utils.convertLiveDateToMillis(rhs.getStartTime()) -
//                        Utils.convertLiveDateToMillis(lhs.getStartTime()));
//            }
//        });
        Collections.sort(matches, new Comparator<Matches>() {
            @Override
            public int compare(Matches lhs, Matches rhs) {
                try {
                    // Log.i("date",Integer.parseInt(lhs.getDate()) - Integer.parseInt(rhs.getDate())+"");
                    return (Integer.parseInt(lhs.getMatchNo()) - Integer.parseInt(rhs.getMatchNo()));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });


        //
        List<Matches> live = new ArrayList<>();
        List<Matches> completed = new ArrayList<>();
        List<Matches> scheduled = new ArrayList<>();
        for (Matches matches1 : matches) {
            //Debug purpose use this code.
//            if ("live".equalsIgnoreCase(matches1.getStatus())) {
//                live.add(matches1);
//            } else if ("completed".equalsIgnoreCase(matches1.getStatus())) {
//                completed.add(matches1);
//            } else
            Log.i("match status", matches1.getStatus());
            if ("scheduled".equalsIgnoreCase(matches1.getStatus())) {
                scheduled.add(matches1);
            }
        }
        //
        Map<String, List<Matches>> map = new LinkedHashMap<>();
        filterAsHeaders(map, live);
        filterAsHeaders(map, completed);
        filterAsHeaders(map, scheduled);
        //
        List<Matches> finalList = new ArrayList<>();
        for (Map.Entry<String, List<Matches>> entry : map.entrySet()) {
            finalList.addAll(entry.getValue());
        }
        if (adapter != null) {
            adapter.addItems(finalList);
        }
        showEmptyView();

    }

    private void showEmptyView() {
        if (!isAdded()) {
            return;
        }
        if (adapter.getCount() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
        }
    }

    private void filterAsHeaders(Map<String, List<Matches>> map, List<Matches> list) {
        for (Matches m : list) {
            String roundName = m.getRoundName();
            if (map.get(roundName) != null) {
                map.get(roundName).add(m);
            } else {
                List<Matches> temp = new ArrayList<>();
                m.setHeader(true);
                temp.add(m);
                map.put(roundName, temp);
            }
        }
    }

    @OnClick(R.id.refresh_layout)
    public void onClick() {
        loadPlayer();
    }

    class ScheduleViewHolder extends AbstractViewHolder {
        @BindView(R.id.txt_header)
        TextView header;
        @BindView(R.id.txt_time)
        TextView time;
        @BindView(R.id.txt_match_condition)
        TextView txtMatchCondition;
        @BindView(R.id.txt_venue)
        TextView venue;
        @BindView(R.id.txt_player1)
        TextView player1;
        @BindView(R.id.txt_player2)
        TextView player2;

        @BindView(R.id.txt_v)
        TextView txtTournamentVenue;

        @BindView(R.id.txt_vs)
        TextView txtVs;

        @BindView(R.id.txt_date)
        TextView txtDate;

        public TextView getTxtTournamentVenue() {
            return txtTournamentVenue;
        }

        public TextView getTxtVs() {
            return txtVs;
        }

        public TextView getTxtDate() {
            return txtDate;
        }

        public ScheduleViewHolder(View view) {
            super(view);
        }

        public TextView getPlayer2() {
            return player2;
        }

        public TextView getPlayer1() {
            return player1;
        }


        public TextView getHeader() {
            return header;
        }

        public TextView getTime() {
            return time;
        }

        public TextView getVenue() {
            return venue;
        }

        public TextView getTxtMatchCondition() {
            return txtMatchCondition;
        }


    }

    private class ScheduleAdapter extends AbstractBaseAdapter<Matches, ScheduleViewHolder> {

        public ScheduleAdapter(Context context) {
            super(context);
        }

        @Override
        public int getLayoutId() {
            return R.layout.frag_schedule_row;
        }

        @Override
        public ScheduleViewHolder getViewHolder(View convertView) {
            return new ScheduleViewHolder(convertView);
        }

        @Override
        public void bindView(int position, ScheduleViewHolder holder, Matches item) {
            try {
                if (item.isHeader()) {
                    holder.getHeader().setVisibility(View.VISIBLE);
                } else {
                    holder.getHeader().setVisibility(View.GONE);
                }

                holder.getHeader().setText(item.getRoundName() + " Schedule");
                List<Teams> teams = item.getTeams();
                holder.getTxtTournamentVenue().setText("Venue:");
                holder.getTxtVs().setText("VS");
                holder.getTxtDate().setText("Date/Time");
                holder.getVenue().setText(item.getCourt().getCourtName());
                final String[] split = item.getStartTime().split(" ");
                String sDate = split[0];
                String sTime = split[1];
                Log.i("match condition", item.getCondition());
                String matchCondition = item.getCondition();
                if (matchCondition.equalsIgnoreCase("NONE")) {
                    matchCondition = "";
                }

                holder.getTxtMatchCondition().setText(matchCondition);
                 holder.getTime().setText(sDate+" "+ sTime);
               // holder.getTime().setText(String.format("%s\n%s\n" + matchCondition + "\n%s", sDate, sTime, ""));
                holder.getPlayer1().setText(teams.get(0).getPosition() + " " + teams.get(0).getName());
                holder.getPlayer2().setText(teams.get(1).getPosition() + " " + teams.get(1).getName());
                //
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void showProgressDialog() {
        super.showProgressDialog();
        if (progress != null) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void dismissProgressDialog() {
        super.dismissProgressDialog();
        if (progress != null) {
            progress.setVisibility(View.GONE);
        }
    }
}
